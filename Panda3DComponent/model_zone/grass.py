import direct.directbase.DirectStart
from panda3d.core import *
        
grass = loader.loadModel("resource/grass4")
grass.reparentTo(render)
grass.setTwoSided(True)
grass.setDepthWrite(False)
grass.setTransparency( TransparencyAttrib.MAlpha )
grass.setBillboardAxis()

smile = loader.loadModel("smiley")
smile.reparentTo(render)
smile.setPos(0, 0, -5)
run()