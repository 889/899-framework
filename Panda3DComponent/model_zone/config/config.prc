###########################################################
###                                                     ###
### Panda3D Configuration File -  User-Editable Portion ###
###                                                     ###
###########################################################

# Uncomment one of the following lines to choose whether you should
# run using OpenGL, DirectX or TinyPanda (software) rendering.
# There can only be one load-display line, but you can use
# multiple aux-display lines to specify fallback modules.
# When the module indicated by load-display fails, it will fall
# back to the next display module indicated by aux-display,
# when that fails, the next aux-display line, and so on.

load-display pandagl
#load-display pandadx9
#load-display pandadx8
#load-display pandagles
#load-display p3tinydisplay

# Uncomment this line if you want to run Panda fullscreen instead of
# in a window.

fullscreen #f

# The framebuffer-hardware flag forces it to use an accelerated driver.
# The framebuffer-software flag forces it to use a software renderer.
# If you don't set either, it will use whatever's available.


# These set the minimum requirements for the framebuffer.
# A value of 1 means: get as many bits as possible,
# consistent with the other framebuffer requirements.

depth-bits 1
color-bits 1
alpha-bits 0
stencil-bits 0

# These control the amount of output Panda gives for some various
# categories.  The severity levels, in order, are "spam", "debug",
# "info", "warning", and "error"; the default is "info".  Uncomment
# one (or define a new one for the particular category you wish to
# change) to control this output.

notify-level warning
default-directnotify-level warning

# These specify where model files may be loaded from.  You probably
# want to set this to a sensible path for yourself.  $THIS_PRC_DIR is
# a special variable that indicates the same directory as this
# particular Config.prc file.

model-path    $MAIN_DIR
model-path    /usr/share/panda3d
model-path    /usr/share/panda3d/models

# This enable the automatic creation of a TK window when running
# Direct.

want-directtools  #f
want-tk           #f

# Enable/disable performance profiling tool and frame-rate meter

want-pstats            #f

# Enable audio using the OpenAL audio library by default:

audio-library-name p3openal_audio

# Enable the use of the new movietexture class.

use-movietexture #t

# The new version of panda supports hardware vertex animation, but it's not quite ready

hardware-animated-vertices #f

# Enable the model-cache, but only for models, not textures.

model-cache-dir $HOME/.panda3d/cache
model-cache-textures #f

# This option specifies the default profiles for Cg shaders.
# Setting it to #t makes them arbvp1 and arbfp1, since these
# seem to be most reliable. Setting it to #f makes Panda use
# the latest profile available.

basic-shaders-only #f


#http://www.panda3d.org/manual/index.php/Configuring_Panda3D
show-frame-rate-meter #t
show-occlusion 0
show-tex-mem 0

#constrained to a harmonic of 60fps (that is, 60, 30, 20, 15, 12, and so on)
sync-video 0

# field of view of screen Y in degrees (panda default is 30)
# A larger fov brings more local detail in the picture and makes the limit of
# the terrain a bit less noticeable.
default-fov 60

# Camera clipping distance
default-far 10000
default-near 0.01

# Portal support so we can clip large chunks of geometry...
allow-portal-cull 1

# Automatically determine that the GPU supports textures-power-2...
textures-power-2 up
textures-auto-power-2 #t

# Enable multisampling...
framebuffer-multisample #t
multisamples 1

#http://www.panda3d.org/manual/index.php/Multithreaded_Render_Pipeline
threading-model Cull/Draw
