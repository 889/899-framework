import direct.directbase.DirectStart
from direct.directutil import Mopath
from direct.interval.MopathInterval import *

butterfly = base.loader.loadModel('resource/Butterfly.egg')
butterfly.setPos(0, 5, 0)
butterfly.setScale(0.1, 0.1, 0.1)
butterfly.setTwoSided(True)
butterfly.reparentTo(base.render)


butterflyMopath = Mopath.Mopath()
butterflyMopath.loadFile("resource/Butterfly_motion_path.egg")
butterfly_fly = MopathInterval(butterflyMopath, butterfly)
butterfly_fly.loop() 
       
run()

# command line for motion path:
# ex : maya2egg -o curve.egg curve.mb