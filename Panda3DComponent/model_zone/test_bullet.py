import direct.directbase.DirectStart
from panda3d.core import Vec3, Point3, NodePath
from panda3d.bullet import BulletWorld
from panda3d.bullet import BulletPlaneShape
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletBoxShape
from panda3d.bullet import BulletDebugNode
from panda3d.bullet import BulletConvexHullShape
from panda3d.bullet import BulletTriangleMesh
from panda3d.bullet import BulletTriangleMeshShape

base.cam.setPos(0, -10, 0)
base.cam.lookAt(0, 0, 0)
 
# World
world = BulletWorld()
world.setGravity(Vec3(0, 0, -9.81))
 
 # Debug node
debugNode = BulletDebugNode('Debug')
debugNode.showWireframe(True)
debugNode.showConstraints(True)
debugNode.showBoundingBoxes(False)
debugNode.showNormals(False)
debugNP = render.attachNewNode(debugNode)
debugNP.hide()
world.setDebugNode(debugNP.node())

# Plane
shape = BulletPlaneShape(Vec3(0, 0, 1), 1)
node = BulletRigidBodyNode('Ground')
node.addShape(shape)
np = NodePath(node)
np.setPos(0, 0, -2)
world.attachRigidBody(node)
np.reparentTo(render)
 
geomNodes = loader.loadModel('models/box.egg').findAllMatches('**/+GeomNode')
print geomNodes
geomNode = geomNodes.getPath(0).node()
geom = geomNode.getGeom(0)
mesh = BulletTriangleMesh()
mesh.addGeom(geom)
shape = BulletTriangleMeshShape(mesh, dynamic=True)

node = BulletRigidBodyNode('Box1')
node.setMass(3)
node.addShape(shape)
np = render.attachNewNode(node)
np.setPos(0, 0, 10)

world.attachRigidBody(node)

model = loader.loadModel('models/box.egg')
#model.setPos(-0.5, -0.5, -0.5)
#model.flattenLight()
model.reparentTo(np)


def toggleDebug():
  if debugNP.isHidden():
    debugNP.show()
  else:
    debugNP.hide()

# Update
def update(task):
  dt = globalClock.getDt()
  world.doPhysics(dt)
  return task.cont
 
base.accept('f1', toggleDebug)
taskMgr.add(update, 'update')
run()