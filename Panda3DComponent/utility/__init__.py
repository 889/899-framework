import mouse
from win import *
import sys
import os

def getModule(filepath):
        scriptFile = filepath
        PROJECT_FOLDER = os.getcwd()
        scriptFilePath = os.path.join(PROJECT_FOLDER, scriptFile)
        name = os.path.basename(scriptFilePath)
        name, ext = os.path.splitext(name)
        SCRIPT_FOLDER = os.path.dirname(scriptFilePath)
        if sys.path.count(SCRIPT_FOLDER) == 0:
            sys.path.append(SCRIPT_FOLDER)
        scriptFile = [name]
        print "script file", scriptFile
        modules = map(__import__, scriptFile)
        return modules