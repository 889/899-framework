from panda3d.core import WindowProperties

class PandaWindow(object):
    def __init__(self, window):
        self.win = window
        self.win.addPythonEventHandler(self.onEvent, 'editor event handler')
        self.properties = WindowProperties()     
        self.properties.setForeground(True)
        self.win.requestProperties(self.properties)
        self.isMinimized = False
        self.isFocus = False 
        self.isClose = False
        
    def setLocation(self, x, y):
        self.properties.setOrigin(x, y)
        self.win.requestProperties(self.properties)
        
    def setSize(self, x, y):
        self.properties.setSize(x, y)
        self.win.requestProperties(self.properties)
        
    def setTitleBar(self, hidden):
        self.properties.setUndecorated(hidden)
        self.win.requestProperties(self.properties)
    
    def setFocus(self, focus):    
        self.isFocus = focus
        self.properties.setForeground(focus)
        print "focus:", self.isFocus
        self.win.requestProperties(self.properties)
    
    def close(self):   
        self.setFocus(True)
        self.properties.setOpen(False)
        self.win.requestProperties(self.properties)                 
     
    def onEvent(self, event):
        # onMoved
        if not self.properties.hasOrigin():
            return
        x, y = self.properties.getXOrigin(), self.properties.getYOrigin()
        curProperties = self.win.getProperties()
        xCur, yCur = curProperties.getXOrigin(), curProperties.getYOrigin()
        if x != xCur or y != yCur:
            self.properties.setOrigin(xCur, yCur)
            self.onMoved([xCur, yCur])
        # onResized
        if not self.properties.hasSize():
            return
        xSize, ySize = self.properties.getXSize(), self.properties.getYSize()
        xSizeCur, ySizeCur = curProperties.getXSize(), curProperties.getYSize()
        if xSize != xSizeCur or ySize != ySizeCur:
            self.properties.setSize(xSizeCur, ySizeCur)
            self.onResized([xSizeCur, ySizeCur])
        # onClosed
        if self.win.isClosed():
            self.onClosed()
        # onMinimized
        if curProperties.getMinimized():
            self.onMinimizing()
            if not self.isMinimized:
                self.onMinimized()
                self.isMinimized = True
        else:
            self.isMinimized = False
        # onFocus   
        if curProperties.getForeground():
            if not self.isFocus:            
                self.onFocus()
                self.isFocus = True
        else:
            self.isFocus = False     
            
    def onFocus(self):
        pass
            
    def onMoved(self, pos):
        pass
    
    def onResized(self, size):
        pass
    
    def onMinimizing(self):
        pass
    
    def onMinimized(self): 
        pass
    
    def onClosed(self):
        pass
    
    def isSupportVideoCard(self):
        return self.win.getGsg().getSupportsBasicShaders() != 0