import wx
from Panda3DKit.wxPython_Panda3D_Kit_Design import PandaKitFrame


def abcEvent(event):
    print "ABC Event Clicked"
    print event

app = wx.PySimpleApp(0)
wx.InitAllImageHandlers()
editorFrame = PandaKitFrame(None, -1, "")

editorFrame.closeChoose = abcEvent
app.SetTopWindow(editorFrame)
editorFrame.Show()
app.MainLoop()
