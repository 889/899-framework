from game.map import Map

class Command():
    def __init__(self, client, gameMap, oldObject, newObject, parentTreeId, itemTreeId):
        assert isinstance(gameMap, Map)
        self.map = gameMap
        self.oldObject = oldObject
        self.newObject = newObject
        self.tree = client.tree
        self.parentId = parentTreeId
        self.itemId = itemTreeId
            
    def undo(self):        
        if self.oldObject == None and self.newObject != None:
            # Add case
            self.map.remove(self.newObject)
            self.map.removeObject = self.newObject
            self.map.readyObject = None
            self.tree.Delete(self.itemId)
        elif self.newObject == None and self.oldObject != None:
            # Delete case
            self.map.add(self.oldObject)
            self.map.readyObject = self.oldObject   
            self.map.removeObject = None
            self.itemId = self.tree.AppendItem(self.parentId, self.oldObject.nodeName)         
        elif self.newObject != None and self.oldObject != None:
            # Edit case
            self.map.replace(self.newObject, self.oldObject)
            self.map.readyObject = self.oldObject
            self.map.removeObject = self.newObject
            self.itemId = self.tree.AppendItem(self.parentId, self.oldObject.nodeName) 
            self.tree.Delete(self.itemId)  
    
    def redo(self):
        if self.oldObject == None and self.newObject != None:
            # Add case
            self.map.add(self.newObject)
            self.map.readyObject = self.newObject
            self.map.removeObject = None
            self.itemId = self.tree.AppendItem(self.parentId, self.newObject.nodeName)
        elif self.newObject == None and self.oldObject != None:
            # Delete case
            self.map.remove(self.oldObject)
            self.map.removeObject = self.oldObject
            self.map.readyObject = None
            self.tree.Delete(self.itemId)  
        elif self.newObject != None and self.oldObject != None:
            # Edit case
            self.map.replace(self.oldObject, self.newObject)
            self.map.readyObject = self.newObject            
            self.map.removeObject = self.oldObject
            self.tree.Delete(self.itemId)  
            self.itemId = self.tree.AppendItem(self.parentId, self.newObject.nodeName)