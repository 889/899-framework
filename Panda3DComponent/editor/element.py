import utility.mouse as mousefunc

from panda3d.core import Mat4, Point3, WindowProperties, loadPrcFileData
from panda3d.core import CollisionRay, CollisionNode, CollisionHandlerQueue, CollisionTraverser, CollisionTube
from panda3d.core import Geom, GeomVertexFormat, GeomVertexWriter, GeomLines, GeomVertexData, GeomNode 
  
# Grid
class Grid():
    def __init__(self, size=10, blockSize=1):
        self.size = size
        self.blockSize = blockSize
        
    def generate(self):
        vFormat = GeomVertexFormat.getV3c4()
        vdata = GeomVertexData('name', vFormat, Geom.UHStatic)
        vertex = GeomVertexWriter(vdata, 'vertex')
        color = GeomVertexWriter(vdata, 'color')
        # Create columne
        for i in range(-self.size, self.size + 1):
            vertex.addData3f(self.size, i, 0)
            color.addData4f(0.8, 0.8, 0.8, 0.8)
            vertex.addData3f(-self.size, i, 0)
            color.addData4f(0.8, 0.8, 0.8, 0.8)
            vertex.addData3f(i, self.size, 0)
            color.addData4f(0.8, 0.8, 0.8, 0.8)
            vertex.addData3f(i, -self.size, 0)
            color.addData4f(0.8, 0.8, 0.8, 0.8)
            
        
        prim = GeomLines(Geom.UHStatic)        
        for i in range(self.size * 2 + 1):
            offset = i * 4
            prim.addVertices(offset, offset + 1)
            prim.addVertices(offset + 2, offset + 3)        
        
        geom = Geom(vdata)
        geom.addPrimitive(prim)
         
        node = GeomNode('gnode')
        node.addGeom(geom)
        
        return node

class ObjectsManager():      
    def __init__(self):
        self.objects = {}
    
    def addNewType(self, typeName):
        if not self.objects.has_key(typeName):
            self.object[typeName] = {}
            
    def addObject(self, objId, obj, typeName):
        if not self.objects.has_key(typeName):
            self.addNewType(typeName)
        if self.objects[typeName].has_key(objId):
            self.objects[typeName][objId] = obj
            
    def getObject(self, objId, typeName):
        if self.objects.has_key(typeName):
            return self.objects[typeName][objId]
        
    
class Pointer():       
    
    def __init__(self, rootNode):
        self.rootNode = rootNode
        self.action = {'rotate': False, 'zoom': False, 'move': False}
        self.selectedNode = None
        self.readyObject = None
        self.prevPos = mousefunc.getMousePos()
        self.rotSpeed = 8000
        self.zoomSpeed = 10
        self.moveSpeed = 10
        self.p = 0
        self.h = 0
        self.wheel = 0
        base.accept("mouse1", self.actionChange, ['rotate', True])        
        base.accept("mouse1-up", self.actionChange, ['rotate', False])
        base.accept("mouse3", self.actionChange, ['zoom', True])
        base.accept("mouse3-up", self.actionChange, ['zoom', False])
        base.accept("mouse2", self.actionChange, ['move', True])
        base.accept("mouse2-up", self.actionChange, ['move', False])
        base.accept("wheel_up", self.wheelChange, [1])
        base.accept("wheel_down", self.wheelChange, [-1])
        
        taskMgr.add(self.update, "pointer update")
        # Collision
        self.cQueue = CollisionHandlerQueue()
        
        self.cTrav = CollisionTraverser() ###
        pickerNode = CollisionNode('mouseRay') ###
        pickerNP = base.camera.attachNewNode(pickerNode) ###
        pickerNode.setFromCollideMask(GeomNode.getDefaultCollideMask())###
        self.pickerRay = CollisionRay()###
        pickerNode.addSolid(self.pickerRay)###
        self.cTrav.addCollider(pickerNP, self.cQueue)###
        
    def addReadyObject(self, readyObject):
        self.readyObject = readyObject
        
    def select(self):        
        if base.mouseWatcherNode.hasMouse(): ###
            # This gives up the screen coordinates of the mouse.
            mpos = base.mouseWatcherNode.getMouse()    ###        
            # This makes the ray's origin the camera and makes the ray point 
            # to the screen coordinates of the mouse.
            self.pickerRay.setFromLens(base.camNode, mpos.getX(), mpos.getY())###
            self.cTrav.traverse(base.render) ###
            # Assume for simplicity's sake that myHandler is a CollisionHandlerQueue.            
            if self.cQueue.getNumEntries() > 0:  ###
                self.clearSelected()              
                self.selectedNode = self.cQueue.getEntry(0).getIntoNodePath()###                
                self.selectedNode.setBin('background', 0) 
                self.selectedNode.setDepthWrite(0)                
            else:
                self.clearSelected()
                                 
    def clearSelected(self):
        if self.selectedNode != None:
            self.selectedNode.setDepthWrite(1)
            self.selectedNode = None     
    
    def wheelChange(self, value):        
        camVecForward = base.render.getRelativeVector(base.camera,(0,1,0))
        base.camera.setPos(base.camera.getPos() + camVecForward * value * self.zoomSpeed * 0.5)
        
    def onAdded(self):
        pass
    
    def actionChange(self, name, isActive):     
        if self.readyObject != None:
            if hasattr(self.readyObject.nodePath, 'reparentTo'):
                self.readyObject.nodePath.reparentTo(self.rootNode)
            self.readyObject = None
            self.onAdded()
            return   
        self.action[name] = isActive
        if name == "rotate" and isActive:
            self.select()
    
    def update(self, task):
        dt = globalClock.getDt()
        mousePos = mousefunc.getMousePos()
        if mousePos != None and self.prevPos != None:  
            dX = mousePos[0] - self.prevPos[0]
            dY = mousePos[1] - self.prevPos[1]   
            if self.action['rotate'] == True:
                # Rotate action
                camVecRight = Point3(1,0,0)
                camVecRight = base.render.getRelativeVector(base.camera,(1,0,0))                             
                self.p += dY * dt * self.rotSpeed  
                pos = self.rootNode.getPos()    
                mat = self.rootNode.getMat()
                rot1 = Mat4.rotateMat(-self.p, camVecRight)
                self.rootNode.setMat(rot1)
                rootVecHead = base.render.getRelativeVector(self.rootNode,(0,0,1))                
                self.h += dX * dt * self.rotSpeed
                mat = self.rootNode.getMat()
                rot2 = Mat4.rotateMat(self.h, rootVecHead)
                self.rootNode.setMat(mat * rot2) 
                self.rootNode.setPos(pos)
            if self.action['zoom'] == True:
                # Zoom action
                camVecForward = base.render.getRelativeVector(base.camera,(0,1,0))
                base.camera.setPos(base.camera.getPos() + camVecForward * dY * self.zoomSpeed)
            if self.action['move'] == True:
                camVecRight = base.render.getRelativeVector(base.camera,(1,0,0))
                camVecUp = base.render.getRelativeVector(base.camera,(0,0,1))
                self.rootNode.setPos(self.rootNode.getPos() + camVecRight * dX * self.moveSpeed)                
                self.rootNode.setPos(self.rootNode.getPos() + camVecUp * dY * self.moveSpeed)    
                           
                
        self.prevPos = mousefunc.getMousePos()
            
        return task.cont 
    
    def replaceObject(self, oldObject, newObject):        
        if hasattr(oldObject, 'nodePath'):
            oldNode = self.rootNode.find(oldObject.nodeName)            
            parentNode = oldNode.getParent()
            if hasattr(newObject, 'nodePath'):
                newObject.nodePath.reparentTo(parentNode)
            oldNode.removeNode()

    
    def addObject(self, newObject):
        newObject.reparentTo(self.rootNode)

    
    def unredo(self, readyObject, removeObject):
        if readyObject != None:
            if hasattr(readyObject, 'nodePath'):                
                readyObject.nodePath.reparentTo(self.rootNode)
        if removeObject != None:
            if hasattr(removeObject, 'nodePath'):
                removeObject.nodePath.detachNode()

    
    
    
    
    
    
    
        
    
    
