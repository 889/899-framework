from designer import *
from element import *
import direct.directbase.DirectStart

        
class PandaView():
    def __init__(self, client):
        self.client = client
        # Grid
        grid = Grid()                
        self.rootNode = base.render.attachNewNode("Root")
        self.rootNode.attachNewNode(grid.generate())
        
        # Pointer
        self.control = Pointer(self.rootNode)
        self.control.onAdded = self.onAdded
        # Init camera pos
        base.disableMouse()
        base.camera.setPos(0, -25, 10)        
        base.camera.setHpr(0, -20, 0)
                
        taskMgr.add(self.myLoop, "my loop")
        
    def update(self, gameMap, event):
        if event == "ready":
            self.control.addReadyObject(gameMap.readyObject)
        elif event =='edit':
            self.control.replaceObject(gameMap.oldObject, gameMap.newObject)
        elif event =='load':
            self.control.addObject(gameMap.readyObject)
        elif event =='undo' or event =='redo':
            self.control.unredo(gameMap.readyObject, gameMap.removeObject)
        
    def onAdded(self):
        self.client.update("added")
    
    def clearSceneGraph(self):
        for childNode in self.rootNode.getChildren():
            childNode.removeNode()
        # Grid
        grid = Grid()        
        self.rootNode.attachNewNode(grid.generate())
    
    def myLoop(self, task):
        return task.cont

    

    
#if __name__ == "__main__":
#    app = wx.PySimpleApp(0)
#    wx.InitAllImageHandlers()
#    frame_1 = MainFrame(None, -1, "")
#    panda = PandaViewport(parent=frame_1.pandaPanel)  
#    # Attach object selected event
#    
#    # Test ObjectTree api
#    treeApi = ObjectTreeAPI(frame_1.objectTree)
#    treeApi.defaultTreeMap()
#    # Initialize object manager
#    # Add object to object tree
#    # Attach tree select changed:  def treeSelChanged(self, event):
#    
#    # Test PropertyCtrl api
#    propApi = PropertyCtrlAPI(frame_1.properties)
#    propApi.defaultProperty()   
#    propApi.addLine("hehe", '12') 
#    
#      
#    app.SetTopWindow(frame_1)
#    frame_1.Show()
#    app.MainLoop()
    
    