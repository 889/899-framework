import wx
import wx.grid, wx.lib.scrolledpanel
import os
class MainFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: MainFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        
        # Menu Bar
        self.frame_1_menubar = wx.MenuBar()
        wxglade_tmp_menu = wx.Menu()
        wxglade_tmp_menu.Append(0, "Open", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(1, "Save", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(6, "Save As", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(2, "Exit", "", wx.ITEM_NORMAL)
        self.frame_1_menubar.Append(wxglade_tmp_menu, "File")
        wxglade_tmp_menu = wx.Menu()
        wxglade_tmp_menu.Append(3, "Duplicate", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(4, "Undo", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(5, "Redo", "", wx.ITEM_NORMAL)
        self.frame_1_menubar.Append(wxglade_tmp_menu, "Edit")
        self.SetMenuBar(self.frame_1_menubar)
        # Menu Bar end
        self.toolPanel = wx.Panel(self, -1)
        self.notebook_1 = wx.Notebook(self.toolPanel, -1, style=0)
        self.modelTab = wx.Panel(self.notebook_1, -1)
        self.label_1 = wx.StaticText(self.modelTab, -1, "Model Type:")
        self.modelType = wx.ComboBox(self.modelTab, -1, choices=[], style=wx.CB_DROPDOWN | wx.CB_DROPDOWN | wx.CB_READONLY)
        self.lightTab = wx.Panel(self.notebook_1, -1)
        self.label_2 = wx.StaticText(self.lightTab, -1, "Light Type:")
        self.lightType = wx.ComboBox(self.lightTab, -1, choices=[], style=wx.CB_DROPDOWN | wx.CB_DROPDOWN | wx.CB_READONLY)
        self.cameraTab = wx.Panel(self.notebook_1, -1)
        self.label_3 = wx.StaticText(self.cameraTab, -1, "Camera Type:")
        self.cameraType = wx.ComboBox(self.cameraTab, -1, choices=[], style=wx.CB_DROPDOWN | wx.CB_DROPDOWN | wx.CB_READONLY)
        self.skyTap = wx.Panel(self.notebook_1, -1)
        self.label_5 = wx.StaticText(self.skyTap, -1, "Sky Type:")
        self.skyType = wx.ComboBox(self.skyTap, -1, choices=[], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.terrainTab = wx.Panel(self.notebook_1, -1)
        self.label_6 = wx.StaticText(self.terrainTab, -1, "Terrain Type:")
        self.terrainType = wx.ComboBox(self.terrainTab, -1, choices=[], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.fogTab = wx.Panel(self.notebook_1, -1)
        self.label_4 = wx.StaticText(self.fogTab, -1, "Fog Type:")
        self.fogType = wx.ComboBox(self.fogTab, -1, choices=[], style=wx.CB_DROPDOWN | wx.CB_DROPDOWN | wx.CB_READONLY)
        self.notebook_2 = wx.Notebook(self, -1, style=0)
        self.notebook_2_pane_1 = wx.Panel(self.notebook_2, -1)
        self.objectTree = wx.TreeCtrl(self.notebook_2_pane_1, -1, style=wx.TR_HAS_BUTTONS | wx.TR_NO_LINES | wx.TR_LINES_AT_ROOT | wx.TR_DEFAULT_STYLE | wx.SUNKEN_BORDER)
        self.notebook_2_pane_2 = wx.ScrolledWindow(self.notebook_2, -1, style=wx.RAISED_BORDER | wx.TAB_TRAVERSAL)
        self.panel_6 = wx.Panel(self, -1)

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_MENU, self.onLoadMap, id=0)
        self.Bind(wx.EVT_MENU, self.onSaveMap, id=1)
        self.Bind(wx.EVT_MENU, self.onSaveAs, id=6)
        self.Bind(wx.EVT_MENU, self.exitFrame, id=2)
        self.Bind(wx.EVT_MENU, self.onDuplicate, id=3)
        self.Bind(wx.EVT_MENU, self.onUndo, id=4)
        self.Bind(wx.EVT_MENU, self.onRedo, id=5)
        self.Bind(wx.EVT_COMBOBOX, self.comboboxChanged, self.modelType)
        self.Bind(wx.EVT_COMBOBOX, self.comboboxChanged, self.lightType)
        self.Bind(wx.EVT_COMBOBOX, self.comboboxChanged, self.cameraType)
        self.Bind(wx.EVT_COMBOBOX, self.comboboxChanged, self.skyType)
        self.Bind(wx.EVT_COMBOBOX, self.comboboxChanged, self.terrainType)
        self.Bind(wx.EVT_COMBOBOX, self.comboboxChanged, self.fogType)
        self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.typeTabChanged, self.notebook_1)
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.treeSelChanged, self.objectTree)
        self.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.treeItemActivated, self.objectTree)
        # end wxGlade
        self.mapFilePath = None
        self.onTabChanged = EventHook()
        self.onComboboxChanged = EventHook()
        self.onTreeSelChanged = EventHook()
        self.onTreeItemActive = EventHook()
        self.onApplyClicked = EventHook()
        self.onOpenMapAction = EventHook()
        self.onSaveMapAction = EventHook()
        self.onSaveAsMapAction = EventHook()
        self.onUndoAction = EventHook()
        self.onRedoAction = EventHook()
        self.onExit = EventHook()
        self.onDuplicateAction = EventHook()

    def __set_properties(self):
        # begin wxGlade: MainFrame.__set_properties
        self.SetTitle("Map Editor")
        self.SetSize((350, 600))
        self.notebook_1.SetMinSize((790, 92))
        self.toolPanel.SetMinSize((0, 120))
        self.notebook_2_pane_2.SetScrollRate(10, 10)
        self.panel_6.SetMinSize((0,0))
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: MainFrame.__do_layout
        grid_sizer_2 = wx.FlexGridSizer(3, 1, 0, 0)
        grid_sizer_3 = wx.FlexGridSizer(1, 1, 0, 0)
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        sizer_1 = wx.FlexGridSizer(1, 1, 0, 0)
        grid_sizer_6 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_8 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_7 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_5 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_4 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_1 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_1.Add(self.label_1, 0, wx.LEFT | wx.TOP, 10)
        grid_sizer_1.Add(self.modelType, 0, wx.LEFT | wx.TOP, 10)
        self.modelTab.SetSizer(grid_sizer_1)
        grid_sizer_1.AddGrowableCol(0)
        grid_sizer_4.Add(self.label_2, 0, wx.LEFT | wx.TOP, 10)
        grid_sizer_4.Add(self.lightType, 0, wx.LEFT | wx.TOP, 10)
        self.lightTab.SetSizer(grid_sizer_4)
        grid_sizer_4.AddGrowableCol(0)
        grid_sizer_5.Add(self.label_3, 0, wx.LEFT | wx.TOP, 10)
        grid_sizer_5.Add(self.cameraType, 0, wx.LEFT | wx.TOP, 10)
        self.cameraTab.SetSizer(grid_sizer_5)
        grid_sizer_5.AddGrowableCol(0)
        grid_sizer_7.Add(self.label_5, 0, wx.LEFT | wx.TOP, 10)
        grid_sizer_7.Add(self.skyType, 0, wx.LEFT | wx.TOP, 10)
        self.skyTap.SetSizer(grid_sizer_7)
        grid_sizer_7.AddGrowableCol(0)
        grid_sizer_8.Add(self.label_6, 0, wx.LEFT | wx.TOP, 10)
        grid_sizer_8.Add(self.terrainType, 0, wx.LEFT | wx.TOP, 10)
        self.terrainTab.SetSizer(grid_sizer_8)
        grid_sizer_8.AddGrowableCol(0)
        grid_sizer_6.Add(self.label_4, 0, wx.LEFT | wx.TOP, 10)
        grid_sizer_6.Add(self.fogType, 0, wx.LEFT | wx.TOP, 10)
        self.fogTab.SetSizer(grid_sizer_6)
        grid_sizer_6.AddGrowableCol(0)
        self.notebook_1.AddPage(self.modelTab, "Model")
        self.notebook_1.AddPage(self.lightTab, "Light")
        self.notebook_1.AddPage(self.cameraTab, "Camera")
        self.notebook_1.AddPage(self.skyTap, "Sky")
        self.notebook_1.AddPage(self.terrainTab, "Terrain")
        self.notebook_1.AddPage(self.fogTab, "Fog")
        sizer_1.Add(self.notebook_1, 1, wx.EXPAND, 0)
        self.toolPanel.SetSizer(sizer_1)
        sizer_1.AddGrowableRow(0)
        sizer_1.AddGrowableCol(0)
        grid_sizer_2.Add(self.toolPanel, 1, wx.EXPAND, 0)
        sizer_2.Add(self.objectTree, 1, wx.EXPAND, 0)
        self.notebook_2_pane_1.SetSizer(sizer_2)
        self.notebook_2.AddPage(self.notebook_2_pane_1, "objectList")
        self.notebook_2.AddPage(self.notebook_2_pane_2, "properties")
        grid_sizer_3.Add(self.notebook_2, 1, wx.EXPAND, 0)
        grid_sizer_3.AddGrowableRow(0)
        grid_sizer_3.AddGrowableCol(0)
        grid_sizer_2.Add(grid_sizer_3, 1, wx.EXPAND, 0)
        grid_sizer_2.Add(self.panel_6, 1, wx.EXPAND, 0)
        self.SetSizer(grid_sizer_2)
        grid_sizer_2.AddGrowableRow(1)
        grid_sizer_2.AddGrowableCol(0)
        self.Layout()
        # end wxGlade
        
    def exitFrame(self, event):  # wxGlade: MainFrame.<event_handler>
        self.onExit.fire(event)

    def treeSelChanged(self, event):  # wxGlade: MainFrame.<event_handler>
        self.onTreeSelChanged.fire(event)

    def typeTabChanged(self, event):  # wxGlade: MainFrame.<event_handler>        
        self.onTabChanged.fire(event)

    def comboboxChanged(self, event):  # wxGlade: MainFrame.<event_handler>
        self.onComboboxChanged.fire(event)

    def treeItemActivated(self, event):  # wxGlade: MainFrame.<event_handler>
        self.onTreeItemActive.fire(event)

    def onLoadMap(self, event):  # wxGlade: MainFrame.<event_handler>
        dlg = wx.FileDialog(self, message="Open Map", 
                            defaultDir=os.getcwd(),
                            defaultFile="", style=wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            self.mapFilePath = filename            
        dlg.Destroy()
        self.onOpenMapAction.fire(self.mapFilePath)

    def onSaveMap(self, event):  # wxGlade: MainFrame.<event_handler>
        if self.mapFilePath == None:
            dlg = wx.FileDialog(self, message="Save As Map", 
                            defaultDir=os.getcwd(),
                            defaultFile="", style=wx.SAVE)
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                self.mapFilePath = filename
            dlg.Destroy()
        self.onSaveMapAction.fire(self.mapFilePath)

    def onUndo(self, event):  # wxGlade: MainFrame.<event_handler>
        self.onUndoAction.fire(event)

    def onRedo(self, event):  # wxGlade: MainFrame.<event_handler>
        self.onRedoAction.fire(event)

    def onSaveAs(self, event):  # wxGlade: MainFrame.<event_handler>
        dlg = wx.FileDialog(self, message="Save As Map", 
                            defaultDir=os.getcwd(),
                            defaultFile="", style=wx.SAVE)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            self.mapFilePath = filename
        dlg.Destroy()
        self.onSaveAsMapAction.fire(self.mapFilePath)

    def onDuplicate(self, event):  # wxGlade: MainFrame.<event_handler>
        self.onDuplicateAction.fire(event)

# end of class MainFrame

    def getPropertiesDict(self):
        return self.__getPropertyDict(self.notebook_2_pane_2)
                
    def __getPropertyDict(self, parent):    
        ret = {}
        childs = parent.GetChildren() 
        length = len(childs)       
        i = 0        
        for child in childs:
            if i < length - 1 and isinstance(child, wx.StaticText) and isinstance(childs[i+1], wx.TextCtrl):
                name = child.GetLabelText()
                value = childs[i+1].GetValue()
                ret[name] = value    
            elif isinstance(child, wx.Panel) and len(child.GetChildren()) > 0:
                name = child.GetChildren()[len(child.GetChildren())-1].GetLabelText()                
                subDict = self.__getPropertyDict(child)
                ret[name] = subDict
            i+=1
        return ret
        
        
        
    def applyClicked(self, event):
        self.onApplyClicked.fire(event)
        
    def clearAllProperties(self):
        self.notebook_2_pane_2.DestroyChildren()       
    
    def addProperties(self, properties):
        numProp = len(properties)
        # Init dynamic flex grid sizer
        self.prop_sizer = wx.FlexGridSizer(numProp + 1, 1, 0, 0)        
        self.custom_prop_size = wx.FlexGridSizer(numProp + 1, 3, 0, 0)
        # Init property
        for key, val in properties.items():
            if not isinstance(val, dict):
                self.__addProperty(self.notebook_2_pane_2, self.custom_prop_size, key, val, "blank")
            else:
                self.__addGroupProperty(self.notebook_2_pane_2, self.prop_sizer, key, val)
            
        button_apply = wx.Button(self.notebook_2_pane_2, -1, "Apply")       
        self.Bind(wx.EVT_BUTTON, self.applyClicked, button_apply)
        self.prop_sizer.Add(self.custom_prop_size, 0, wx.LEFT | wx.TOP, 0)  
        self.prop_sizer.Add(button_apply, 0, wx.LEFT | wx.TOP, 5)    
        self.notebook_2_pane_2.SetSizer(self.prop_sizer)
        
        # Resize frame (trick to layout automatically)
        size = self.GetSize()
        size.IncBy(1, 1)
        self.SetSize(size)
    
    def __addGroupProperty(self, parent, sizer, name, val):
        groupPanel = wx.Panel(parent, -1)        
        alignSizer = wx.FlexGridSizer(2, 1, 0, 0)
        containSizer = wx.FlexGridSizer(len(val), 1, 0, 0)
        customSizer = wx.FlexGridSizer(len(val), 3, 0, 0)
        
        for key, value in val.items():
            if not isinstance(value, dict):    
                self.__addProperty(groupPanel, customSizer, key, value, "blank")
            else:
                self.__addGroupProperty(groupPanel, containSizer, key, value)    
                    
        groupLabel = wx.StaticText(groupPanel, -1, name)
        containSizer.Add(customSizer, 0, wx.LEFT | wx.TOP, 0)  
        alignSizer.Add(groupLabel, 0, wx.LEFT | wx.TOP, 0)
        alignSizer.Add(containSizer, 0, wx.LEFT, 10)    
        groupPanel.SetSizer(alignSizer)
        sizer.Add(groupPanel, 0, wx.LEFT | wx.TOP, 5)
              
    def __addProperty(self, parent, sizer, name, value, button=""):     
        propLabel = wx.StaticText(parent, -1, name)
        sizer.Add(propLabel, 0, wx.LEFT | wx.TOP, 5)        
        propTextCtrl = wx.TextCtrl(parent, -1, value)
        
        if button == "blank":            
            propBank = wx.Panel(parent, -1)
            sizer.Add(propTextCtrl, 0, wx.LEFT | wx.TOP, 5)
            sizer.Add(propBank, 0, wx.EXPAND, 0)
        else:
            propBank = wx.Button(parent, -1, "...")
            propBank.SetMinSize((20, 20))
            sizer.Add(propTextCtrl, 0, wx.LEFT | wx.TOP, 5)
            sizer.Add(propBank, 0, wx.LEFT | wx.TOP, 5)
        if name == "HaveId":
            propLabel.Hide()
            propTextCtrl.Hide()
            propBank.Hide()
      
#source: http://stackoverflow.com/questions/1092531/event-system-in-python  
class EventHook(object):

    def __init__(self):
        self.__handlers = []

    def __iadd__(self, handler):
        self.__handlers.append(handler)
        return self

    def __isub__(self, handler):
        self.__handlers.remove(handler)
        return self

    def fire(self, *args, **keywargs):
        for handler in self.__handlers:
            handler(*args, **keywargs)

    def clearObjectHandlers(self, inObject):
        for theHandler in self.__handlers:
            if theHandler.im_self == inObject:
                self -= theHandler        
