from pandaview import PandaView
from designer import wx, MainFrame
from game.factory import *
from game.builder import *
from game.map import Map
from command import Command
import copy

class Editor():
    def __init__(self):        
        print "TRACE: Editor --> __init__()"
        self.gameTypes = self.createObjectTypes()       
        self.map = Map()
        self.commands = []
        self.idx = -1
        # init wxpython
        self.wxApp = wx.PySimpleApp(0)
        wx.InitAllImageHandlers()
        self.frame_1 = MainFrame(None, -1, "")
        self.wxApp.SetTopWindow(self.frame_1)
        self.frame_1.Show()        
        self.selectedType = self.gameTypes['GameModels']
        self.selectedTypeName = "model"
        self.frame_1.onTabChanged += self.tabChanged 
        self.frame_1.onComboboxChanged += self.comboboxChanged
        self.frame_1.onTreeSelChanged += self.treeSelChanged
        self.frame_1.onTreeItemActive += self.onTreeItemActive
        self.frame_1.onApplyClicked += self.applyClicked
        self.frame_1.onOpenMapAction += self.openMap
        self.frame_1.onSaveAsMapAction += self.saveMap
        self.frame_1.onSaveMapAction += self.saveMap
        self.frame_1.onUndoAction += self.undo
        self.frame_1.onRedoAction += self.redo
        self.frame_1.onExit += self.exit
        self.frame_1.onDuplicateAction += self.duplicate
        self.tree = self.frame_1.objectTree
        self.defaultTreeMap()
        # init panda view
        self.view = PandaView(self) 
        
        # fill object types to wxpython
        self.fillObjectTypeToGUI()
        
        taskMgr.add(self.handleWxEvents, 'handleWxEvents')  
        run()
        
    def openMap(self, filePath):
        self.view.clearSceneGraph()
        self.clearTreeMapItems()
        self.map.clear()
        mapObjects = MapFactory.product(filename = filePath) 
        self.addGroupItemToTree(mapObjects,'GameModels' , self.model)
        self.addGroupItemToTree(mapObjects,'Lights' , self.light)
        self.addGroupItemToTree(mapObjects,'Fogs' , self.fog)
        self.addGroupItemToTree(mapObjects,'Cameras' , self.camera)
        self.addGroupItemToTree(mapObjects,'Skys' , self.sky)
        self.addGroupItemToTree(mapObjects,'Terrains' , self.terrain)
        self.map.readyObject = nodeRoot
        self.notifyChange('load')
    
    def addGroupItemToTree(self, mapObjects, groupName, parent):
        if mapObjects.has_key(groupName):
            group = mapObjects[groupName]
            for objectName, gameObject in group.items():   
                print "name name name", objectName, gameObject         
                self.tree.AppendItem(parent, objectName)
                self.map.add(gameObject)            
      
    def saveMap(self, filePath):
        mapDict = self.generateMapDict()
        xmlSaver = XmlIO(filePath)
        xmlSaver.writeFile(mapDict)
    
    def defaultTreeMap(self):
        self.tree.DeleteAllItems()
        self.root = self.tree.AddRoot("Game")
        self.model = self.tree.AppendItem(self.root, "GameModel")
        self.light = self.tree.AppendItem(self.root, "Light")
        self.fog = self.tree.AppendItem(self.root, "Fog")
        self.camera = self.tree.AppendItem(self.root, "Camera")
        self.sky = self.tree.AppendItem(self.root, "Sky")
        self.terrain = self.tree.AppendItem(self.root, "Terrain")        
    
    def clearTreeMapItems(self):   
        self.tree.DeleteChildren(self.model)
        self.tree.DeleteChildren(self.light)
        self.tree.DeleteChildren(self.fog)
        self.tree.DeleteChildren(self.camera)
        self.tree.DeleteChildren(self.sky)
        self.tree.DeleteChildren(self.terrain)    
       
    def update(self, event):
        if event == "added":        
            if self.map.add(self.map.readyObject):                
                subTree= getattr(self, self.selectedTypeName)            
                newTreeItemId = self.tree.AppendItem(subTree, self.map.readyObject.nodeName)
                self.lastTreeGroup = subTree
                self.tree.ExpandAll()
                self.updateProperties(self.map.readyObject.describeDict)
                command = Command(self, self.map, None, self.map.readyObject, subTree, newTreeItemId)
                self.commands.append(command)
                self.idx += 1
                self.map.readyObject = None
                self.selectedObjectId = newTreeItemId
                self.frame_1.notebook_2.SetSelection(1)
                
        print self.map.gameObjects
                
    def updateProperties(self, describeDict):
        self.frame_1.clearAllProperties()
        self.frame_1.addProperties(describeDict)        
                
    def generateName(self):
        subTree= getattr(self, self.selectedTypeName)
        return self.selectedTypeName + str(self.tree.GetChildrenCount(subTree))
    
    def generateMapDict(self):
        ret = {}      
        gameDict = {}  
        rootName = self.tree.GetItemText(self.root)
        modelName = self.tree.GetItemText(self.model) + "s"
        lightName = self.tree.GetItemText(self.light) + "s"
        fogName = self.tree.GetItemText(self.fog) + "s"
        skyName = self.tree.GetItemText(self.sky) + "s"
        cameraName = self.tree.GetItemText(self.camera) + "s"
        terrainName = self.tree.GetItemText(self.terrain) + "s"
        self.__generateMapPartDict(gameDict, self.model, modelName)
        self.__generateMapPartDict(gameDict, self.light, lightName)
        self.__generateMapPartDict(gameDict, self.fog, fogName)
        self.__generateMapPartDict(gameDict, self.sky, skyName)
        self.__generateMapPartDict(gameDict, self.camera, cameraName)
        self.__generateMapPartDict(gameDict, self.terrain, terrainName)
        ret[rootName] = gameDict
        return ret
        
    def __iterTree(self, treectrl, node):
        cid, citem = treectrl.GetFirstChild(node)
        while cid.IsOk(): 
            yield cid
            cid, citem = treectrl.GetNextChild(node, citem)
        
    def __generateMapPartDict(self, parentDict, parent, nodeName):
        ret = {}
        for child in self.__iterTree(self.tree, parent):
            childName = self.tree.GetItemText(child)
            ret[childName] = self.map.getObject(childName).describeDict
        if len(ret) > 0:
            parentDict[nodeName] = ret
        
    def applyClicked(self, event):        
        # Get parent item name in tree (prefix name for builder)
        selectedId = self.selectedObjectId      
        parentId = self.tree.GetItemParent(selectedId)        
        parentName = self.tree.GetItemText(parentId)        
        describeDict = self.frame_1.getPropertiesDict()
        oldObjectName = self.tree.GetItemText(selectedId)
        newObjectName = describeDict['Name']
        # Create new object
        builderType = parentName + "Builder"
        print builderType
        builder = getattr(sys.modules[__name__], builderType)()
        newObject = builder.buildGameObject(describeDict, self.map.gameObjects)   
        # Change selected item in tree to new name
        self.tree.SetItemText(selectedId, newObjectName)
        # Update to pandaview
        self.map.newObject = newObject
        self.map.oldObject = self.map.getObject(oldObjectName)
        self.notifyChange('edit')
        # Delete old object
        self.map.remove(oldObjectName)
        # Add new object to map
        self.map.add(newObject)
        command = Command(self, self.map, self.map.oldObject, self.map.newObject, parentId, selectedId)
        self.commands.append(command)
        self.idx += 1
        self.lastTreeGroup = parentId
        
    def onTreeItemActive(self, event):
        self.frame_1.notebook_2.SetSelection(1)        
     
    def treeSelChanged(self, event):
        selectedId = event.GetItem()
        parentId = self.tree.GetItemParent(selectedId)        
        if parentId.IsOk() and parentId != self.root:      
            self.selectedObjectId = selectedId      
            selectedObjectName = self.tree.GetItemText(selectedId)
            gameObject = self.map.getObject(selectedObjectName)
            if gameObject:
                self.updateProperties(gameObject.describeDict)
            
    
    def comboboxChanged(self, event):
        if self.selectedType != None:
            name = event.GetString()
            originObject = self.selectedType[name]
            self.map.readyObject = copy.copy(originObject)            
            name = self.generateName()
            
            self.map.readyObject.changeName(name) 
            self.notifyChange('ready')        
        
    def tabChanged(self, event):        
        idx = event.GetSelection()
        matchType = ['GameModels', 'Lights', 'Cameras', 'Skys', 'Terrains', 'Fogs']
        matchCombobox = ['model', 'light', 'camera', 'sky', 'terrain', 'fog']
        objectTypeName = matchType[idx]
        self.selectedType = self.gameTypes[objectTypeName]
        cbbName = matchCombobox[idx]
        self.selectedTypeName = cbbName 
        cbb = getattr(self.frame_1, cbbName+"Type")     
        cbb.SetSelection(-1)        
        
    def fillObjectTypeToGUI(self):
        models = self.gameTypes['GameModels']
        lights = self.gameTypes['Lights']
        cameras = self.gameTypes['Cameras']
        fogs = self.gameTypes['Fogs']
        terrains = self.gameTypes['Terrains']
        skys = self.gameTypes['Skys']
        self.frame_1.cameraType.Clear()
        self.frame_1.modelType.Clear()
        self.frame_1.fogType.Clear()
        self.frame_1.lightType.Clear()
        self.frame_1.terrainType.Clear()
        self.frame_1.skyType.Clear()
        for name in skys.keys():
            self.frame_1.skyType.Append(name)
        for name in terrains.keys():
            self.frame_1.terrainType.Append(name)
        for name in models.keys():
            self.frame_1.modelType.Append(name)
        for name in lights.keys():
            self.frame_1.lightType.Append(name)
        for name in cameras.keys():
            self.frame_1.cameraType.Append(name)
        for name in fogs.keys():
            self.frame_1.fogType.Append(name)            
        
    
    def createObjectTypes(self):
        print "TRACE: Editor --> createObjectTypeList()"
        return TypeFactory.product(filename = 'model_types_1.xml')        
        
    def duplicate(self, event):
        print "duplicate"
        
    def undo(self, event):        
        if self.idx > -1:            
            self.frame_1.clearAllProperties()            
            self.commands[self.idx].undo()
            self.notifyChange('undo')
            self.idx -= 1
            print "undo", len(self.map.gameObjects)
    
    def redo(self, event):        
        if self.idx < len(self.commands) - 1:
            self.frame_1.clearAllProperties()            
            self.commands[self.idx+1].redo()
            self.notifyChange('redo')
            self.idx += 1
            print "redo", len(self.map.gameObjects)
    
    def notifyChange(self, event):        
        self.view.update(self.map, event)
    
    def exit(self, event):
        sys.exit(0)
        
    def handleWxEvents(self, task):
        if "linux" in sys.platform.lower():
            evtloop = wx.EventLoop() 
            wx.EventLoop.SetActive(evtloop)    
            while evtloop.Pending(): 
                evtloop.Dispatch()
        else:
            while self.wxApp.Pending():
                self.wxApp.Dispatch() 
        return task.cont  

if __name__ == "__main__":
    editor = Editor()
