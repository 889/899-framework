#Only for test command pattern, DO NOT use
class Client():
    def __init__(self):
        self.commands = []
        self.doc = Doc()
        self.idx = -1
        
    def add(self, text):
        self.doc.add(text)
        command = Command(self.doc, text)
        self.commands.append(command)
        self.idx += 1
        
    def undo(self):
        if self.idx > -1:
            self.commands[self.idx].undo()
            self.idx -= 1
        
    def redo(self):
        if self.idx < len(self.commands) - 1:
            self.commands[self.idx+1].redo()
            self.idx += 1        
            
    def show(self):
        print self.doc.text
        
    
class Doc():
    def __init__(self):
        self.text = ""        
        
    def add(self, text):        
        self.text += text
        
    def remove(self, text):        
        self.text = self.text[0: -len(text)]
        
        
class Command():
    def __init__(self, doc, text):
        self.doc = doc
        self.text = text
        
    def undo(self):           
        self.doc.remove(self.text)
        
    def redo(self):
        self.doc.add(self.text)
        
if __name__ == '__main__':
    client = Client()
    client.add("abc")
    client.add("def")
    client.add("gjk")
    client.show()
    client.undo()
    client.show()
    client.undo()
    client.show()
    client.undo()
    client.show()
    client.redo()
    client.show()
    client.redo()
    client.show()
    client.redo()
    client.show()
    client.redo()
    client.show()
    client.redo()
    client.show()
    client.undo()
    client.show()
    client.redo()
    client.show()
        
        