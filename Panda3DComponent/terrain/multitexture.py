"""
    To much texture to panda3d generate shader file :O
"""

from panda3d.core import loadPrcFileData
#loadPrcFileData('', 'load-display pandadx9')
loadPrcFileData('', 'threading-model Cull/Draw')
loadPrcFileData('', 'sync-video 0')

import direct.directbase.DirectStart
from panda3d.core import *
base.setFrameRateMeter(True)

terrain = GeoMipTerrain("ben_map_limestone")
terrain.setHeightfield('benmap.png')

terrain.setBlockSize(32)
terrain.setNear(1000)
terrain.setFar(2000)
terrain.setFocalPoint(base.camera)

terrain.generate()
terrainNP = terrain.getRoot()
terrainNP.reparentTo(render)
terrainNP.setSz(65)
terrainNP.setTexture(loader.loadTexture('terrain_texture.png'))

#ts = TextureStage("stage-1")
#ts.setSort(0)
#ts.setMode(TextureStage.MReplace)
#ts.setSavedResult(True)
#terrainNP.setTexture(ts, loader.loadTexture('longGrass.png'))
#terrainNP.setTexScale(ts, 32, 32)
#
#ts = TextureStage("stage-2")
#ts.setSort(1)
#ts.setMode(TextureStage.MReplace)
#terrainNP.setTexture(ts, loader.loadTexture('cliff.png'))
#terrainNP.setTexScale(ts, 16, 16)
#
#ts = TextureStage("stage-3")
#ts.setSort(2)
#ts.setCombineRgb(TextureStage.CMInterpolate,
#                 TextureStage.CSLastSavedResult, TextureStage.COSrcColor,                  
#                 TextureStage.CSPrevious, TextureStage.COSrcColor,                 
#                 TextureStage.CSTexture, TextureStage.COSrcAlpha)
#terrainNP.setTexture(ts, loader.loadTexture('default_d_red.png'))
#ts.setSavedResult(True)
#
#ts = TextureStage("stage-4")
#ts.setSort(3)
#ts.setMode(TextureStage.MReplace)
#terrainNP.setTexture(ts, loader.loadTexture('greenrough.png'))
#terrainNP.setTexScale(ts, 8, 8)
#
#ts = TextureStage("stage-5")
#ts.setSort(4)
#ts.setCombineRgb(TextureStage.CMInterpolate,
#                 TextureStage.CSLastSavedResult, TextureStage.COSrcColor,                  
#                 TextureStage.CSPrevious, TextureStage.COSrcColor,                 
#                 TextureStage.CSTexture, TextureStage.COSrcAlpha)
#terrainNP.setTexture(ts, loader.loadTexture('default_d_green.png'))
#ts.setSavedResult(True)
#
#ts = TextureStage("stage-6")
#ts.setSort(5)
#ts.setMode(TextureStage.MReplace)
#terrainNP.setTexture(ts, loader.loadTexture('bigRockFace.png'))
#terrainNP.setTexScale(ts, 8, 8)
#
#ts = TextureStage("stage-7")
#ts.setSort(6)
#ts.setCombineRgb(TextureStage.CMInterpolate,
#                 TextureStage.CSLastSavedResult, TextureStage.COSrcColor,                  
#                 TextureStage.CSPrevious, TextureStage.COSrcColor,                 
#                 TextureStage.CSTexture, TextureStage.COSrcAlpha)
#terrainNP.setTexture(ts, loader.loadTexture('default_d_blue.png'))

# Stage 4: reapply vertex colour
#ts = TextureStage("stage-vertexcolour")
#ts.setSort(7)
#ts.setCombineRgb(TextureStage.CMModulate, 
#                 TextureStage.CSPrevious, TextureStage.COSrcColor, 
#                 TextureStage.CSPrimaryColor, TextureStage.COSrcColor)
#terrainNP.setTexture(ts, "final")

#lighting
alight = AmbientLight('alight')
alight.setColor(VBase4(0.2, 0.2, 0.2, 1))
alnp = render.attachNewNode(alight)
render.setLight(alnp)

dlight = DirectionalLight('dlight')
dlight.setColor(VBase4(0.8, 0.8, 0.8, 1))
dlight.setShadowCaster(True, 1024, 1024)

dlnp = render.attachNewNode(dlight)
dlnp.setHpr(0, -30, 0)
dlnp.setPos(128, 0, 100)
dlnp.node().getLens().setNearFar(50, 300)
dlnp.node().getLens().setFilmSize(128, 128)
dlnp.node().showFrustum()
render.setLight(dlnp)

#base.wireframeOn()
#render.setShaderAuto()

run()