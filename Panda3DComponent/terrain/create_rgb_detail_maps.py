from panda3d.core import *
import os

class RGBType():
    Red = 'red'
    Green = 'green'
    Blue = 'blue'
    
def createRGBMaps(filepath, image_type=RGBType.Red):
    image = PNMImage(filepath)
    x_size, y_size = image.getXSize(), image.getYSize()
    result = PNMImage(x_size, y_size, 4)    
    

    for x in xrange(0, x_size):
        for y in xrange(0, y_size):
            blue = image.getBlue(x, y)
            green = image.getGreen(x, y)
            red = image.getRed(x, y)
            alpha = image.getAlpha(x, y)
            if alpha < 1:
                if image_type == RGBType.Red and red > 0:
                    result.setXelA(x, y, VBase4D(red, red, red, alpha))
                elif image_type == RGBType.Green and green > 0:
                    result.setXelA(x, y, VBase4D(green, green, green, alpha))
                elif image_type == RGBType.Blue and blue > 0:
                    result.setXelA(x, y, VBase4D(blue, blue, blue, alpha))
                else:
                    result.setXelA(x, y, VBase4D(0, 0, 0, 1))
            else:
                result.setXelA(x, y, VBase4D(0, 0, 0, 1))
            
            
                    
    dirname, filename = os.path.split(os.path.abspath(filepath))
    filename, ext = os.path.splitext(filename)
    filename += "_" + image_type
    outpath = filename + ext
    result.write(outpath)
    
    
            
    
    
createRGBMaps('default_d.png', RGBType.Red)
createRGBMaps('default_d.png', RGBType.Green)
createRGBMaps('default_d.png', RGBType.Blue)