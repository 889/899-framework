'''
Created on Mar 18, 2013

@author: anhsaodem
'''
from panda3d.core import loadPrcFileData
from panda3d.core import ConfigVariableString, Camera, DirectionalLight
from pandac.PandaModules import *
from config import *

import direct.directbase.DirectStart
from game.manager import TerrainManager
base.setFrameRateMeter(True)

from game.factory import TotalMapManager, CacheMapManager
from utility import instruction
from direct.task import Task

def nextMap(mm):
    mm.nextMap()
    
def initInstruction():
        inst1 = instruction.addText(0.95, "Press C to change Map")
        inst2 = instruction.addText(0.90, "HCMUS - 0912452 - 0912447")
        inst2 = instruction.addText(0.85, "Panda3D Game Engine - Terrain Demo")
        
if __name__ == "__main__":
#    base.disableMouse()
#    cam = Camera(base.cam.node())
#    cam.setScene(render)
#    
#    camNP = render.attachNewNode(cam)
#    camNP.setPos(500, 500, 30)
#    camNP.node().setActive(True)
#    base.cam = camNP
    
    mapMgr.loadMapListFromFile('map/mapList.xml', '1')
#    for light in Map.current.gameObjects['Lights'].values():
#        if isinstance(light.node(), DirectionalLight):
#            shadow = PSSMShadow(light)
#            shadow.applyShadowToMap(Map.current, True)
    initInstruction()
#     base.accept('c', nextMap, [mm])
#     base.accept("v", base.bufferViewer.toggleEnable)
#     base.accept("V", base.bufferViewer.toggleEnable)
#     PStatClient.connect()
    
  
    run() 
