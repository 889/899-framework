from game.camera import CameraBase
from game.map import Map
from direct.task import Task
from direct.task.TaskManagerGlobal import taskMgr
class Scripting():    
    def __init__(self, model):    
#        self.world = Map.current.world
        self.gameModel = model
        self.gameObjectList = None
        taskMgr.add(self.preProcess, 'MyTaskName', sort=1)
        
    
    def preProcess(self, task):
        self.currentMap = Map.current
        self.gameObjectList = Map.current.gameObjects
        return task.done
    
    
    def run(self, task):
#        print "COLISION TEST", Map.collisionPair(self.gameModel, self.gameModel)
#        print "CURRENT OBJECT LIST", self.gameObjectList['GameModels']
        for key, value in Map.current.gameObjects['GameModels'].items():
            if Map.collisionPair(self.gameModel, value) and value != self.gameModel:
                print "COLISON!!!", key

        return task.cont
    
    
    def cleanUp(self, task):
        # Code here
        return task.done
