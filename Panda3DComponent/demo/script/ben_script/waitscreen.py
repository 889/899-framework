from game.map import Map
from panda3d.core import TransparencyAttrib

def waitrun(task):
    gui = Map.current.gui
    waitNP = gui.getGui('wait')
    waitNP.setTransparency(TransparencyAttrib.MAlpha)
    dt = globalClock.getDt()    
    roll = waitNP.getR()
    roll += 0.1
    waitNP.setR(roll)
    return task.cont