from game.map import Map
from game.message import GameMessage

def buttonClick(buttonName, postBackToTagList):
    print "Button Click Detected"
    postBackToTagTuple = tuple(postBackToTagList)
    print "postBackToTupe", postBackToTagTuple
    message = GameMessage((buttonName,), postBackToTagTuple, {'event': "click"})
    Map.current.dispatch(message)
    print "Message Click Done"