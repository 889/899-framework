from game.camera import CameraBase, FollowCamera
from game.map import Map
from direct.showbase.InputStateGlobal import inputState
from panda3d.core import Vec3, Point3, BitMask32, TransformState
from panda3d.bullet import BulletBodyNode, BulletSphereShape
from direct.fsm.FSM import FSM
from game.message import GameQuestV2, QuestManager, QuestDispatcher
from game.message import GameMessage
from utility import instruction
from game.factory import GuiFactory
from game.guiTemplate import DialogBox
import copy
import datetime
from game.sun import Sun

class Scripting(FSM):
    bullets =[]
    def __init__(self, model):
        self.model = model
        self.v = 0
        self.model.appendFuncs(self.init)
        self.shootBullet()
        self.vecForward.y *= -1
        self.startTime = datetime.datetime.now()
        self.isCollision = False
        self.model.setHpr(0,90,0)
        self.sun = Sun()
        
       
    def init(self, task):
        return task.done
    
    
    def run(self, task):        
        # Catch event
        self.model.physicNode.node().setLinearVelocity(self.v)
        self.model.physicNode.setPos(self.model.getPos())
        current = datetime.datetime.now()
        timeDelta = current - self.startTime
        if timeDelta.seconds > 1:
            Map.current.removeObject(self.model)
        
        self.sun.setPos(self.model.getPos())
        
#         penetration = 0.0
#  
#         tsFrom = TransformState.makePos(self.model.getPos())
#         tsTo = TransformState.makePos(self.model.getPos() + self.vecForward * 100)
#          
#         shape = BulletSphereShape(0.5)
#         penetration = 0.0
#         
        for key, value in Map.current.gameObjects['GameModels'].items():
            if Map.collisionPair(self.model, value) and value != self.model:
                print "COLISON!!! WITH ", key
                self.isCollision = True
                messageToTarget = GameMessage(self.model.getName(), (key,), ("Bullet Collision",))
                Map.dispatch(messageToTarget)
#                 Map.current.removeObject(self.model)


        return task.cont
    
    def cleanUp(self, task):
        print "I'm Dying :(("
        self.sun.destroy()
        return task.done
    
    
    def shootBullet(self):
        pMouse = base.mouseWatcherNode.getMouse()
        pFrom = Point3()
        pTo = Point3()
        base.camLens.extrude(pMouse, pFrom, pTo)
        
        pFrom = render.getRelativePoint(base.cam, pFrom)
        pTo = render.getRelativePoint(base.cam, pTo)
        
        v = pTo - pFrom
        v.normalize()
        v *= 10
        self.v = v
        
        print "Velocity : ", v
        if store.active == "car":
            player =  Map.current.gameObjects['GameModels']['BatMobil_1']
            self.forward = (0, 1, 0)
            self.up = (0,0,2)
        else:
            player =  Map.current.gameObjects['GameModels']['Player']
            self.forward = (0, -1, 0)
            self.up = (0,0,0)

        vecForward = base.render.getRelativeVector(player.physicNode,self.forward)
        
        self.model.physicNode.setPos(player.getPos() + vecForward * 3)
        self.model.setPos(player.getPos() + vecForward * 10 + self.up)
        print "TRUE POS", self.model.getPos()
        self.vecForward = vecForward
        
        self.model.physicNode.node().setLinearVelocity(v)
        self.model.physicNode.setCollideMask(BitMask32.allOn())
        
#         
        self.model.physicNode.node().setCcdMotionThreshold(1e-7)
        self.model.physicNode.node().setCcdSweptSphereRadius(0.50)
        
        
        
    
