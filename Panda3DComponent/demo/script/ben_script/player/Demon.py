from game.camera import CameraBase, FollowCamera
from game.map import Map
from direct.showbase.InputStateGlobal import inputState
from panda3d.core import Vec3
from direct.fsm.FSM import FSM
from game.message import GameQuestV2, QuestManager, QuestDispatcher
from game.message import GameMessage
from utility import instruction
from game.factory import GuiFactory
from game.guiTemplate import DialogBox
import copy
from game.Vfx import vfx

class Scripting(FSM):
    def __init__(self, model):
        FSM.__init__(self, 'DeathstrokeFSM')
        self.model = model
        self.model.realNP.setPos(0, 0, -1.8)
        self.model.appendFuncs(self.init)
        self.destroyAfter = -1
        self.destroy = False
        
       
    def init(self, task):
        self.request('Stand')
        self.directions = []
        return task.done
    
    def run(self, task):        
        # Catch event
        if not self.destroy:
            for event in self.model.eventQueue:            
                
                #Check event
                print "@@ Event.Info from ZOMBIE", event.info
                if "Bullet Collision" in event.info or "BatMobil Collision" in event.info:
                    print "I'm die"
                    #Send event to Player
                    vfx(self.model, texture='vfx/blast2.png',scale=3.0).start()
                    self.destroyAfter = 3
                    self.destroy = True
                    messageToTarget = GameMessage('zombie', ("Player",), ("Zombie Kill",))                    
                    Map.dispatch(messageToTarget)
                    
                if len(self.directions) == 0:
                    self.request('Stand')
                else:
                    self.request('Walk', *self.directions)
                    
                self.model.eventQueue.remove(event) 
            # Do stuff
            self.model.move()
        else:
            if self.destroyAfter < 0:
                Map.current.removeObject(self.model)
            else:
                self.destroyAfter -= globalClock.getDt()
        
        
        return task.cont
    
    def cleanUp(self, task):
        return task.done
    
    def removeItemList(self, lst, item):
        while item in lst:
            lst.remove(item)        
        print "after remove", lst
    
    def doJump(self):
        #Character control bullet node
        self.model.physicNode.node().setMaxJumpHeight(5.0)
        self.model.physicNode.node().setJumpSpeed(8.0)
        self.model.physicNode.node().doJump()
        
    def enterStand(self):                
        self.model.realNP.loop('stand')
        
    def exitStand(self):
        print "exit stand"
        
    def enterWalk(self, *direction):
        walk = self.model.realNP.getAnimControl('walk', 'body')
        print "enter walk", self.model.realNP, walk
        if 'forward' in direction:          
            self.model.direction = Vec3(0, -7, 0)
            if not walk.isPlaying():
                self.model.realNP.loop('walk')
            self.model.realNP.setPlayRate(10.0, 'walk')
        if 'back' in direction:     
            self.model.direction = Vec3(0, 5, 0)
            if not walk.isPlaying():
                self.model.realNP.loop('walk')
            self.model.realNP.setPlayRate(10.0, 'walk')
        if 'right' in direction:     
            self.model.omega = -90
        if 'left' in direction:     
            self.model.omega = 90
               
    def exitWalk(self):    
        print "exit walk"    
        self.model.direction = Vec3(0, 0, 0)   
        self.model.omega = 0
        self.model.setPos(self.model.getPos())
        self.model.nodePath.node().setFallSpeed(50)
    
