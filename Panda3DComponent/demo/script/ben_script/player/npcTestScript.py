from game.camera import CameraBase, FollowCamera
from game.map import Map
from direct.showbase.InputStateGlobal import inputState
from panda3d.core import Vec3
from direct.fsm.FSM import FSM
from game.message import GameQuestV2, QuestManager, QuestDispatcher
from game.message import GameMessage
from utility import instruction
from game.factory import GuiFactory
from game.guiTemplate import DialogBox
import copy

class Scripting(FSM):
    def __init__(self, model):
        FSM.__init__(self, 'DeathstrokeFSM')
        self.model = model
        self.model.realNP.setPos(0, 0, -1.8)
        self.model.appendFuncs(self.init)
        
        # Quest Test
        self.statisticText = []
        messQuest = GameMessage("System", ("a",), ("a",))
        messQuest1 = GameMessage("System", ("d",), ("d",))
        messQuest3 = GameMessage("System", ("j",), ("j",))
        self.testQuest = GameQuestV2("Quest A D", "Testing Quest - Click A,D to test", {messQuest: [0, 1, "You click A @#current_number#@ / @#require_number#@ times"], messQuest1: [0, 1, "You click D @#current_number#@ / @#require_number#@ times"]})
        self.testQuest1 = GameQuestV2("Quest J (depend Quest A D)", "Testing Quest - Click J to test", {messQuest3: [0, 5, "You click J @#current_number#@ / @#require_number#@ times"]})
        
        self.questDispatcher = QuestDispatcher()
        self.pushQuestToQuestDispatcher()
        self.questInShow = None #The quest current show in panel
        self.overThisStep = False
        self.sendPacket = False         # Flag to mark the packet send before
        
    def init(self, task):
        self.request('Stand')
        self.directions = []
        return task.done
    
    def run(self, task):        
        # Catch event
        player =  Map.current.gameObjects['GameModels']['Player']
        vecForward = base.render.getRelativeVector(player.physicNode,(0,-1,0))
        
        self.model.setPos(self.model.getPos() + vecForward)
        
        return task.cont
    
    def cleanUp(self, task):
        return task.done
    
    def removeItemList(self, lst, item):
        while item in lst:
            lst.remove(item)        
        print "after remove", lst
    
    def doJump(self):
        #Character control bullet node
        self.model.physicNode.node().setMaxJumpHeight(5.0)
        self.model.physicNode.node().setJumpSpeed(8.0)
        self.model.physicNode.node().doJump()
        
    def enterStand(self):                
        self.model.realNP.loop('stand')
        
    def exitStand(self):
        print "exit stand"
        
    def enterWalk(self, *direction):
        walk = self.model.realNP.getAnimControl('walk', 'body')
        print "enter walk", self.model.realNP, walk
        if 'forward' in direction:          
            self.model.direction = Vec3(0, -7, 0)
            if not walk.isPlaying():
                self.model.realNP.loop('walk')
            self.model.realNP.setPlayRate(10.0, 'walk')
        if 'back' in direction:     
            self.model.direction = Vec3(0, 5, 0)
            if not walk.isPlaying():
                self.model.realNP.loop('walk')
            self.model.realNP.setPlayRate(10.0, 'walk')
        if 'right' in direction:     
            self.model.omega = -90
        if 'left' in direction:     
            self.model.omega = 90
               
    def exitWalk(self):    
        print "exit walk"    
        self.model.direction = Vec3(0, 0, 0)   
        self.model.omega = 0
        self.model.setPos(self.model.getPos())
        self.model.nodePath.node().setFallSpeed(50)
    
    def pushQuestToQuestDispatcher(self):
        messQuest = GameMessage("System", ("a",), ("a",))
        messQuest1 = GameMessage("System", ("d",), ("d",))
        messQuest3 = GameMessage("System", ("j",), ("j",))
        messQuestMeetMaster = GameMessage("NPC", ("Player", "NPC"), ("ClickNPC",))
        testQuest = GameQuestV2("Quest A D", "Testing Quest - Click A,D to test", {messQuest: [0, 1, "You click A @#current_number#@ / @#require_number#@ times"], messQuest1: [0, 1, "You click D @#current_number#@ / @#require_number#@ times"]})
        testQuest1 = GameQuestV2("Quest J (depend Quest A D)", "Testing Quest - Click J to test", {messQuest3: [0, 5, "You click J @#current_number#@ / @#require_number#@ times"]})
        returnMaster = GameQuestV2("return your result to Master", "Find Master to receive new quest", {messQuestMeetMaster: [0, 1, "Report your result to master"]})
 
         
        BigQuest = GameQuestV2("Keyboard Quest", "Testing keyboard event", {})
        BigQuest.addChildQuest(testQuest, [])
        BigQuest.addChildQuest(testQuest1,['Quest A D'])
        BigQuest.addChildQuest(returnMaster,['Quest J (depend Quest A D)'])
        
        self.questDispatcher.addQuest({}, BigQuest)
