from game.camera import CameraBase, FollowCamera
from game.map import Map
from direct.showbase.InputStateGlobal import inputState
from panda3d.core import Vec3
from direct.fsm.FSM import FSM
from game.message import GameQuestV2, QuestManager
from game.message import GameMessage
from utility import instruction
from game.manager import Prototype
import datetime

class Scripting(FSM):
    def __init__(self, model):
        FSM.__init__(self, 'DeathstrokeFSM')
        self.model = model
        self.model.realNP.setPos(0, 0, -1.8)
        self.model.appendFuncs(self.init)
        self.model.statusFlag = {"Quest1":"undone"}
        
        # Quest Test
        self.statisticText = []
#         messQuest = GameMessage("NPC", ("Player", "NPC"), ("ClickNPC",))
#         self.testQuest = GameQuestV2("Find NPC", "Find NPC to receive quest", {messQuest: [0, 1, "Find NPC in location X:Y = -2:-2"]})
        self.model.questManager = QuestManager({})
#         self.model.questManager.editQuest(self.testQuest)
        self.model.statusFlag['InQuest'] = []
#         self.posPointer = instruction.addText(0.3, str(self.model.getPos()))
        store.active = "Deathstroke"
#
        
    def init(self, task):
        self.activeCamera()
        self.request('Stand')
        self.directions = []
        return task.done
    
    def run(self, task):        
        # Catch event
        if (store.active == "Deathstroke"):
            for event in self.model.eventQueue:            
                #check quest
                self.model.questManager.updateQuest(event, self.model)
                #Show Position
#                 self.posPointer.destroy()
#                 self.posPointer = instruction.addText(0.3, str(self.model.getPos()))
                #Check event
                if "w" in event.toTagList:
                    self.directions.append('forward')       
                if "d" in event.toTagList:
                    self.directions.append('right')    
                if "s" in event.toTagList:
                    self.directions.append('back')
                if "a" in event.toTagList:
                    self.directions.append('left')    
                    
                if "w-up" in event.toTagList:                
                    self.removeItemList(self.directions, 'forward')
                if "d-up" in event.toTagList:
                    self.removeItemList(self.directions, 'right')                
                if "s-up" in event.toTagList:
                    self.removeItemList(self.directions, 'back')                
                if "a-up" in event.toTagList:
                    self.removeItemList(self.directions, 'left')        
                    
                if "space" in event.toTagList:
                    self.doJump()           
                    
                if "Right Click" in event.toTagList:                
                    entity = entityMgr.build(Prototype.GameModel, 'bullet', 'bullet_' + str(datetime.datetime.now()), Scripts = './script/ben_script/player/bullet.py', Tags ='Left Click')
#                     entity.setPos(self.model.getPos() + (5,5,5))
                    entity.start()      
                    entity.startScripts()
                    
                if "j" in event.toTagList: 
                    store.active = "car"
                    BatMobil =  Map.current.gameObjects['GameModels']['BatMobil_1']
                    messageToPlayer = GameMessage(self.model.getName(), (BatMobil.getName(),), ("SwitchToCar",))
                    print "Message Sending ", messageToPlayer
                    print "Buoc vao xe o day", self.model.getPos()
                    Map.dispatch(messageToPlayer)
                    self.model.setPos(1000,1000,1000)
                    
                if "SwitchToPlayer" in event.info:
                    print "Switch 2 Player"
                    BatMobil =  Map.current.gameObjects['GameModels']['BatMobil_1']
                    self.model.setPos(BatMobil.getPos() + (5,5,5))
                    self.activeCamera()         
                    print "Dang o day ", self.model.getPos()
                
                              
                if len(self.directions) == 0:
                    self.request('Stand')
                else:
                    self.request('Walk', *self.directions)
                
                self.model.eventQueue.remove(event)
                            
            # Do stuff
            self.model.move()
            
        
        return task.cont
    
    def cleanUp(self, task):
        return task.done
    
    def removeItemList(self, lst, item):
        while item in lst:
            lst.remove(item)        
        print "after remove", lst
    
    def doJump(self):
        #Character control bullet node        
        self.model.physicNode.node().setMaxJumpHeight(5.0)
        self.model.physicNode.node().setJumpSpeed(8.0)
        self.model.physicNode.node().doJump()
        
    def enterStand(self):                
        self.model.realNP.loop('stand')
        
    def exitStand(self):
        print "exit stand"
        
    def enterWalk(self, *direction):
        walk = self.model.realNP.getAnimControl('walk', 'body')
        print "enter walk", self.model.realNP, walk
        if 'forward' in direction:          
            self.model.direction = Vec3(0, -7, 0)
            if not walk.isPlaying():
                self.model.realNP.loop('walk')
            self.model.realNP.setPlayRate(10.0, 'walk')
        if 'back' in direction:     
            self.model.direction = Vec3(0, 5, 0)
            if not walk.isPlaying():
                self.model.realNP.loop('walk')
            self.model.realNP.setPlayRate(10.0, 'walk')
        if 'right' in direction:     
            self.model.omega = -90
        if 'left' in direction:     
            self.model.omega = 90
               
    def exitWalk(self):    
        print "exit walk"    
        self.model.direction = Vec3(0, 0, 0)   
        self.model.omega = 0
        self.model.setPos(self.model.getPos())
        self.model.nodePath.node().setFallSpeed(50)
    
    
    def activeCamera(self):
        camera = CameraBase.current
        camera.setPos(self.model.getPos())
        camera.target = self.model
        camera.height = 2.2
        camera.back = 8       
        camera.lookHeight = 2
