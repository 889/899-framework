from game.camera import CameraBase, FollowCamera
from game.map import Map
from direct.showbase.InputStateGlobal import inputState
from panda3d.core import Vec3
from direct.fsm.FSM import FSM
from game.message import GameQuestV2, QuestManager, QuestDispatcher
from game.message import GameMessage
from utility import instruction
from game.factory import GuiFactory
from game.guiTemplate import DialogBox
import copy

class Scripting(FSM):
    def __init__(self, model):
        FSM.__init__(self, 'DeathstrokeFSM')
        self.model = model
        self.model.realNP.setPos(0, 0, -1.8)
        self.model.appendFuncs(self.init)
        
        # Quest Test
        self.statisticText = []
        messQuest = GameMessage("System", ("a",), ("a",))
        messQuest1 = GameMessage("System", ("d",), ("d",))
        messQuest3 = GameMessage("System", ("j",), ("j",))
        self.testQuest = GameQuestV2("Quest A D", "Testing Quest - Click A,D to test", {messQuest: [0, 1, "You click A @#current_number#@ / @#require_number#@ times"], messQuest1: [0, 1, "You click D @#current_number#@ / @#require_number#@ times"]})
        self.testQuest1 = GameQuestV2("Quest J (depend Quest A D)", "Testing Quest - Click J to test", {messQuest3: [0, 5, "You click J @#current_number#@ / @#require_number#@ times"]})
        
        self.questDispatcher = QuestDispatcher()
        self.pushQuestToQuestDispatcher()
        self.questInShow = None #The quest current show in panel
        self.overThisStep = False
        self.sendPacket = False         # Flag to mark the packet send before
        
    def init(self, task):
        self.request('Stand')
        self.directions = []
        return task.done
    
    def run(self, task):        
        # Catch event
        for event in self.model.eventQueue:            
            
            #Check event
            print "@@ Event.Info", event.info
            if "Left Click" in event.toTagList:
                print "Model so khop ", self.model.getName(),' = ', self.model.physicNode.node(), " == ? ", event.info[2]
                if self.model.physicNode.node() == event.info[2].getNode():
                    #NPC is clicked
                    #Send it 2 times 
                    """
                    Send this Packet 2 time: 
                    Reason:
                    If not --> player won't receive any packet and click to NPC will return wrong result.
                    If send 1 time --> not really exactly in this case:
                        NPC        ---         Player
                                            Player update
                    NPC update        
                    ---------------------------------------------
                    NPC update
                                            Player update
                    
                    In this case. Player receive the packet but it have no time to process it.
                    So in this case we must send 2 type of packet and try to delay it 2 time for sync
                    """
                        
                    player =  Map.current.gameObjects['GameModels']['Player']
                    messageToPlayer = GameMessage(self.model.getName(), ("Player", self.model.getName()), ("ClickNPC",))
                    print "Message Sending ", messageToPlayer
                    Map.dispatch(messageToPlayer)
            
            if "ClickNPC" in event.info:    
                messageToPlayer = GameMessage(self.model.getName(), (self.model.getName(),), ("QuestProcess",))
                Map.dispatch(messageToPlayer)
                
                
            if "QuestProcess" in event.info:                    
                player =  Map.current.gameObjects['GameModels']['Player']
                #Check Player Status
                self.overThisStep = False
                # Return Quest Done.
                for currentQuest in self.questDispatcher.currentQuests:
                    if len(player.questManager.findQuest(currentQuest)) == 0:                        
                        if player.statusFlag.has_key('Quest2') and player.statusFlag['Quest2'] == "Done":
                            #Delete All instruction
                            player.questManager.clearInstruction()
                            mapMgr.goToWaitMap('WaitScreen', 'Downtown', delay=5)
                            return task.done
                            
                        self.dialogBox = DialogBox('Quest Complete', 'Quest Complete', "The Quest " + currentQuest + " is done", [self.model.nodeName])
                        self.dialogBox.openDialog()
                        self.questDispatcher.currentQuests.remove(currentQuest)
                        self.overThisStep = True
                        
                
                if self.overThisStep == False:
                    # Check new Quest Available
                    quests = self.questDispatcher.getQuestValidWithStatus(player.statusFlag)
                    
                    for quest in quests:
                        print "Available quest : " + 'Quest Panel' + quest.name + ' \n \n ' + quest.info
                        self.dialogBox = DialogBox('Quest', 'Quest Panel', quest.name + ' \n \n ' + quest.info, [self.model.nodeName])
                        self.dialogBox.openDialog()
                        self.questInShow = copy.deepcopy(quest)# quest
                        break
                    
                        
                        
            if type(event.info) is dict and event.fromTag[0] == "close_button" and event.info.has_key('event'): # close button
                self.dialogBox.closeDialog()
            
            if type(event.info) is dict and event.fromTag[0] == "OK_button" and event.info.has_key('event'): # OK button
                self.dialogBox.closeDialog()
                player =  Map.current.gameObjects['GameModels']['Player']
                
                if self.questInShow != None:
                    player.questManager.editQuest(self.questInShow)
                    self.questDispatcher.currentQuests.append(self.questInShow.name)
                    self.questInShow = None
            
                
            
            if len(self.directions) == 0:
                self.request('Stand')
            else:
                self.request('Walk', *self.directions)
                
            self.model.eventQueue.remove(event) 
        # Do stuff
        self.model.move()
        
        
        return task.cont
    
    def cleanUp(self, task):
        return task.done
    
    def removeItemList(self, lst, item):
        while item in lst:
            lst.remove(item)        
        print "after remove", lst
    
    def doJump(self):
        #Character control bullet node
        self.model.physicNode.node().setMaxJumpHeight(5.0)
        self.model.physicNode.node().setJumpSpeed(8.0)
        self.model.physicNode.node().doJump()
        
    def enterStand(self):                
        self.model.realNP.loop('stand')
        
    def exitStand(self):
        print "exit stand"
        
    def enterWalk(self, *direction):
        walk = self.model.realNP.getAnimControl('walk', 'body')
        print "enter walk", self.model.realNP, walk
        if 'forward' in direction:          
            self.model.direction = Vec3(0, -7, 0)
            if not walk.isPlaying():
                self.model.realNP.loop('walk')
            self.model.realNP.setPlayRate(10.0, 'walk')
        if 'back' in direction:     
            self.model.direction = Vec3(0, 5, 0)
            if not walk.isPlaying():
                self.model.realNP.loop('walk')
            self.model.realNP.setPlayRate(10.0, 'walk')
        if 'right' in direction:     
            self.model.omega = -90
        if 'left' in direction:     
            self.model.omega = 90
               
    def exitWalk(self):    
        print "exit walk"    
        self.model.direction = Vec3(0, 0, 0)   
        self.model.omega = 0
        self.model.setPos(self.model.getPos())
        self.model.nodePath.node().setFallSpeed(50)
    
    def pushQuestToQuestDispatcher(self):
        messQuest = GameMessage("System", ["a"], ["a"])
        messQuest1 = GameMessage("System", ["d"], ["d"])
        messQuest3 = GameMessage("System", ["j"], ["j"])
        messQuestMeetMaster = GameMessage("NPC", ("Player", "NPC"), ("ClickNPC",))
        testQuest = GameQuestV2("Quest A D", "Testing Quest - Click A,D to test", {messQuest: [0, 1, "You click A @#current_number#@ / @#require_number#@ times"], messQuest1: [0, 1, "You click D @#current_number#@ / @#require_number#@ times"]})
        testQuest1 = GameQuestV2("Quest J (depend Quest A D)", "Testing Quest - Click J to test", {messQuest3: [0, 5, "You click J @#current_number#@ / @#require_number#@ times"]})
        returnMaster = GameQuestV2("return your result to Master", "Find Master to receive new quest", {messQuestMeetMaster: [0, 1, "Report your result to master"]})
 
         
        BigQuest = GameQuestV2("Keyboard Quest", "Testing keyboard event", {}, {"Quest1":"Done"})
        BigQuest.addChildQuest(testQuest, [])
        BigQuest.addChildQuest(testQuest1,['Quest A D'])
        BigQuest.addChildQuest(returnMaster,['Quest J (depend Quest A D)'])
        
        zombieKill = GameMessage('zombie', ("Player",), ("Zombie Kill",))
        zombieQuest = GameQuestV2("Kill zombie", "Kill All zombie", {zombieKill: [0, 2, "You kill @#current_number#@ / @#require_number#@ zombie"]}, {"Quest2":"Done"})
        
        self.questDispatcher.addQuest({"Quest1":"undone"}, BigQuest)
        self.questDispatcher.addQuest({"Quest1":"Done"}, zombieQuest)
