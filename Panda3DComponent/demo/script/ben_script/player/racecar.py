from panda3d.core import Vec3
from game.manager import Prototype
from direct.fsm.FSM import FSM
from direct.interval.IntervalGlobal import Sequence, Wait, Func
from game.camera import CameraBase, FollowCamera
from game.map import Map
from game.message import GameMessage
import datetime

class Scripting(FSM):
    def __init__(self, model):
        FSM.__init__(self, "RacerCar")
        self.model = model
        self.model.appendInitFunc(self.init)
        store.active = "Deathstroke"
        self.force = 5000
        self.brake = 200
        
    def init(self, task):       
        store.car = self.model     
        
        self.model.realNP.setShaderAuto()
        tuning = self.model.vehicle.getTuning()
        tuning.setMaxSuspensionTravelCm (40)
        tuning.setSuspensionCompression (4.4)
        tuning.setSuspensionDamping (2.3)
        tuning.setSuspensionStiffness (5)
        self.request('Ready')
        
        Wait(3)
        s = Sequence ( Wait(3),
                   Func(self.request, 'Start'))
        s.start()
        return task.done
    

        
    def checkInput(self):
        pass        
        
        
    def run(self, task):
        if store.active == "car":
            for key, value in Map.current.gameObjects['GameModels'].items():
                if Map.collisionPair(self.model, value) and value != self.model:
                    print "COLISON!!! WITH ", key
                    self.isCollision = True
                    messageToTarget = GameMessage(self.model.getName(), (key,), ("BatMobil Collision",))
                    Map.dispatch(messageToTarget)
                
            self.model.updateEvent()
            self.model.updateVehicle()
            
            for event in self.model.eventQueue:  
                if "SwitchToCar" in event.info:
                    print "Switch 2 Car"
                    self.ActiveCam()
                
                
                if "o" in event.toTagList: 
                    store.active = "Deathstroke"
                    player =  Map.current.gameObjects['GameModels']['Player']
                    messageToPlayer = GameMessage(self.model.getName(), (player.getName(),), ("SwitchToPlayer",))
                    print "Message Sending ", messageToPlayer
                    Map.dispatch(messageToPlayer)
                    
                if "Right Click" in event.toTagList:                
                    entity = entityMgr.build(Prototype.GameModel, 'bullet', 'bullet_' + str(datetime.datetime.now()), Scripts = './script/ben_script/player/bullet.py')
                    entity.start()      
                    entity.startScripts()
                    
                self.model.eventQueue.remove(event)
                
                    
                   
        return task.cont
    def cleanUp(self, task):
        return task.done
    
    def roga(self):        
        print "roga"
        
    def moveForward(self):
        self.model.go(force = self.force, brake = self.brake)
        
    def moveBack(self):
        self.model.back(force = self.force, brake = self.brake)
        
    def turnLeft(self):
        self.model.turnLeft()
        
    def turnRight(self):
        self.model.turnRight()
    
    def enterReady(self):
        self.model.registryEvent( "w", self.roga, None, "w")
        
    def notMove(self):
        self.model.slide()
        
    def notTurn(self):
        self.model.notTurn()
        
    
    def exitReady(self):
        pass
    
    def enterStart(self):
        self.model.registryEvent( "w", self.moveForward, None, "w")
        self.model.registryEvent( "s", self.moveBack, None, "s")
        self.model.registryEvent( "a", self.turnLeft, None, "a")
        self.model.registryEvent( "d", self.turnRight, None, "d")
        
        self.model.registryEvent( "w-up", self.notMove, None, "w-up")
        self.model.registryEvent( "s-up", self.notMove, None, "s-up")
        self.model.registryEvent( "a-up", self.notTurn, None, "a-up")
        self.model.registryEvent( "d-up", self.notTurn, None, "d-up")
    
    def exitStart(self):
        pass
    
    def enterFinish(self):
        pass
    
    def exitFinish(self):
        pass
    
    def ActiveCam(self):        
        camera = CameraBase.current
        camera.setPos(self.model.getPos())
        camera.target = self.model
        camera.height = 5
        camera.back = -20       
        camera.distance = 35
        camera.lookHeight = 3