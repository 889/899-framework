import sys
from game.factory import GuiFactory, MapManager
from game.map import Map
from direct.interval.IntervalGlobal import LerpScaleInterval, Sequence, Func
from panda3d.core import TransparencyAttrib
    
frame = {'options': False}


def refresh():
    gui = Map.current.gui
    if gui.existGui('options_frame'):
        frame['options'] = True
        remove_options_frame()
    

def newgame_clicked():
    print "newgame click"
    refresh()
    mapMgr = MapManager.current
#    mapMgr.goToMap('LimestoneMtn')
    mapMgr.goToWaitMap('WaitScreen', 'LimestoneMtn', delay=1)
    

def remove_options_frame():  
    print "remove options"   
    gui = Map.current.gui
    options_frame = gui.getGui('options_frame')
    options_frame.cleanup()
    gui.removeGui('options_frame')    
    frame['options'] = False

def options_clicked():
    print "options click"
    gui = Map.current.gui
    if frame['options'] == False:
        optionsGui = GuiFactory.product(filename="map/ben_game/mainscreen_options.json")
        optionsGui.startScripts()
        frame['options'] = True
        gui.updateGui(optionsGui)
        options_frame = gui.getGui('options_frame')
        options_frame.setTransparency(TransparencyAttrib.MAlpha)
        options_frame.show()
        current_scale = options_frame.getScale()
        # Animation
        bigger = LerpScaleInterval(options_frame, 0.05, current_scale, (0.1, 0.1, 0.1))
        bigger.start()
    else:
        options_frame = gui.getGui('options_frame')
        current_scale = options_frame.getScale()
        smaller = LerpScaleInterval(options_frame, 0.05, (0.1, 0.1, 0.1), current_scale)
        fade = Sequence(smaller,
                        Func(remove_options_frame)
                        )
        fade.start()


def exit_clicked():
    print "Exit click"
    sys.exit()
    

# Options dialog event
def options_close_clicked():
    frame['options'] = True
    options_clicked()