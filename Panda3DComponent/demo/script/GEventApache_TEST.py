class Scripting():    
    def __init__(self, model):    
        self.model = model
        self.speed = 0.3
        self.rotate = 5
        
        base.accept('time-z-repeat', self.RotationX)
        base.accept('time-x-repeat', self.RotationY)
        print "GEVENT +TEST"
    
    def run(self, task):
        #Code here
        return task.cont
    
    def RotationX(self):
        self.model.setPos(self.model.getPos() + (self.speed, 0, 0))
    
    def RotationY(self):
        self.model.setHpr(self.model.getHpr() + (-self.rotate, 0, 0))
    
    def cleanUp(self, task):
        #Code here
        return task.done
