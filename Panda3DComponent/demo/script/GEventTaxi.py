from game.camera import CameraBase
from game.builder import GameModelBuilder
from game.map import Map
from game.manager import GameEntityManager
from game.message import MessageDispatcher
from panda3d.core import NodePath, Vec3

import sys
import direct.directbase.DirectStart

from direct.showbase.DirectObject import DirectObject
from direct.showbase.InputStateGlobal import inputState

from panda3d.core import AmbientLight
from panda3d.core import DirectionalLight
from panda3d.core import Vec3
from panda3d.core import Vec4
from panda3d.core import Point3
from panda3d.core import TransformState
from panda3d.core import BitMask32

from panda3d.bullet import BulletWorld
from panda3d.bullet import BulletPlaneShape
from panda3d.bullet import BulletBoxShape
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletDebugNode
from panda3d.bullet import BulletVehicle
from panda3d.bullet import ZUp
import math
from panda3d.core import *

# giai quyet lai bang cach tao moi 1 new object
# Add thong so can thiet
# Xoa object nay di
# Vay cho le.....

class Scripting():    
    def __init__(self, model):    
        self.model = model
#        self.model.setH(180)
        self.direction  = {'forward': False, 'backward': False, 'right': False, 'left': False}
        taskMgr.add(self.setup, 'MyTaskName', sort=1)
        
    
    def run(self, task):
        if CameraBase.current:
            CameraBase.current.target = self.model
            CameraBase.current.height = 5
            CameraBase.current.back = -8
            CameraBase.current.lookHeight = 8
#            CameraBase.current.
        
#        print self.model.nodePath.getPos(), self.model.realNP.getPos(), "\t Speed :", self.vehicle.getCurrentSpeedKmHour()
#        self.model.nodePath.setPos()
        while(len(self.model.eventQueue) > 0):
            message = self.model.eventQueue[0]     
            self.setDirection(message, "w", 'forward', True)
            self.setDirection(message, "w-up", 'forward', False)
            self.setDirection(message, "s", 'backward', True)
            self.setDirection(message, "s-up", 'backward', False)
            self.setDirection(message, "a", 'left', True)
            self.setDirection(message, "a-up", 'left', False)
            self.setDirection(message, "d", 'right', True)
            self.setDirection(message, "d-up", 'right', False)    
            
            self.model.eventQueue.remove(message)
            
        
        self.doMoving()
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        return task.done
    
    def doMoving(self):
        dt = globalClock.getDt()
        engineForce = 0.0
        brakeForce = 0.0
        
        if self.steering > 1:
            self.steering -= dt * self.steeringIncrement/3
        elif self.steering < 1:
            self.steering += dt * self.steeringIncrement/3
        
        if self.direction['forward'] == True:
            engineForce = -3000.0
            brakeForce = 0.0
            
        if self.direction['backward'] == True:    
            engineForce = 3000.0
            brakeForce = 0.0
            
        if self.direction['left'] == True:    
            self.steering += dt * self.steeringIncrement
            self.steering = min(self.steering, self.steeringClamp)
            
            
        if self.direction['right'] == True:
            self.steering -= dt * self.steeringIncrement
            self.steering = max(self.steering, -self.steeringClamp)
            
        
        # Apply steering to front wheels
        self.vehicle.setSteeringValue(self.steering, 0);
        self.vehicle.setSteeringValue(self.steering, 1);
        
        # Apply engine and brake to rear wheels
        self.vehicle.applyEngineForce(engineForce, 2);
        self.vehicle.applyEngineForce(engineForce, 3);
        self.vehicle.setBrake(brakeForce, 2);
        self.vehicle.setBrake(brakeForce, 3);        
#        self.model.nodePath.setPos(self.model.realNP.getPos())
        
    def setDirection(self, message, key, direction, active):        
        if message.fromTag == "System" and key in message.info :            
                self.direction[direction] = active

    def setup(self, task):
        shape = BulletBoxShape(Vec3(0.6, 1.4, 0.5))
        ts = TransformState.makePos(Point3(0, 0, 0.5))
        
        #world is bulletWorld
        #worldNP is RootNode
#        self.model.realNP = NodePath("NewNodePath")
#        for child in self.model.realNP.getChild():
#            child.removeNode()
        
#        Map.current.world.remove(self.model.physicNode.node())
        
        print "LS REAL NODE", self.model.realNP.ls()
        print "LS nodePath NODE", self.model.nodePath.ls()
        print "LS Physic NODE", self.model.physicNode.ls()
        for child in self.model.realNP.getChildren():
            child.removeNode()
            print "Real NodePath --- Remove 1 Node"
            
        for child in self.model.nodePath.getChildren():
            child.removeNode()
            print "nodepath NodePath --- Remove 1 Node"
            
        for child in self.model.physicNode.getChildren():
            child.removeNode()
            print "nodepath NodePath --- Remove 1 Node"
#        self.model.realNP.getChild(0).removeNode()
#        self.model.realNP.detachNode()
#        self.model.nodePath.getChild(0).removeNode()
        print "LS REAL NODE", self.model.realNP.ls()
        print "LS nodePath NODE", self.model.nodePath.ls()
        print "LS Physic NODE", self.model.physicNode.ls()
        self.model.realNP.reparentTo(self.model.nodePath)
           
        self.worldNP = Map.current.nodeRoot
        self.world = Map.current.world
        np = self.worldNP.attachNewNode(BulletRigidBodyNode('Vehicle'))
        np.node().addShape(shape, ts)
        np.setPos(self.model.nodePath.getPos())
        np.node().setMass(800.0)
        np.node().setDeactivationEnabled(False)
    
        self.world.attachRigidBody(np.node())
#        self.model.realNP.setPos(self.model.nodePath.getPos())
#        self.model.physicNode.setPos(self.model.nodePath.getPos())
        self.model.nodePath = np
        
        # Vehicle
        self.vehicle = BulletVehicle(self.world, np.node())
        self.vehicle.setCoordinateSystem(ZUp)
        self.world.attachVehicle(self.vehicle)
    
        self.yugoNP = loader.loadModel('Models/3DModels/taxi/chase.egg')
        self.yugoNP.reparentTo(np)
    
        # Right front wheel
        np = loader.loadModel('Models/3DModels/taxi/rear_right.egg')
        np.reparentTo(self.worldNP)
        self.addWheel(Point3( .90,  -1.45, 0.5), True, np)
    
        # Left front wheel
        np = loader.loadModel('Models/3DModels/taxi/rear_left.egg')
        np.reparentTo(self.worldNP)
        self.addWheel(Point3(-.8,  -1.45, 0.5), True, np)
    
        # Right rear wheel
        np = loader.loadModel('Models/3DModels/taxi/rear_right.egg')
        np.reparentTo(self.worldNP)
        self.addWheel(Point3(0.9, 1.65, 0.5), False, np)
    
        # Left rear wheel
        np = loader.loadModel('Models/3DModels/taxi/rear_left.egg')
        np.reparentTo(self.worldNP)
        self.addWheel(Point3(-.80, 1.65, 0.5), False, np)
    
        # Steering info
        self.steering = 0.0            # degree
        self.steeringClamp = 35.0      # degree
        self.steeringIncrement = 60.0 # degree per second
        
        
#        self.model.nodePath.setShaderOff()
#        rig = NodePath('rig')
#        buffer = base.win.makeCubeMap("cube map", 256, rig, BitMask32(5))
##        tex = buffer.getTexture()
#        rig.reparentTo(self.model.nodePath)
#        lens = rig.find('**/+Camera').node().getLens()
#        lens.setNearFar(3, 1000)
##        self.model.setTexGen(TextureStage.getDefault(), TexGenAttrib.MWorldCubeMap)
#        self.model.setTexture(buffer.getTexture())
#        ts = TextureStage('ts')
#        ts.setMode(TextureStage.MModulate)
#        self.model.setTexture(ts, buffer.getTexture())
#        self.model.nodePath.hide(BitMask32(5))
        
        myMaterial = Material()
        myMaterial.setAmbient(Vec4(0,0,0,0))
        myMaterial.setDiffuse(Vec4(0.5,0.5,0.5,1))
        myMaterial.setEmission(Vec4(0.5,0.5,0.5,1))
        myMaterial.setShininess(5.0)
        myMaterial.setSpecular(Vec4(0.5,0.5,0.5,1))
        self.model.nodePath.setMaterial(myMaterial)
        return task.done
    
    
    def addWheel(self, pos, front, np):
        wheel = self.vehicle.createWheel()
    
        wheel.setNode(np.node())
        wheel.setChassisConnectionPointCs(pos)
        wheel.setFrontWheel(front)
    
        wheel.setWheelDirectionCs(Vec3(0, 0, -1))
        wheel.setWheelAxleCs(Vec3(1, 0, 0))
        wheel.setWheelRadius(0.25)
        wheel.setMaxSuspensionTravelCm(40.0)
    
        wheel.setSuspensionStiffness(40.0) # He thong nhung'/ Treo thuy luc
        wheel.setWheelsDampingRelaxation(0.0023) # Giam xoc
        wheel.setWheelsDampingCompression(4.4)
        wheel.setFrictionSlip(100.0);
        wheel.setRollInfluence(0.1)
