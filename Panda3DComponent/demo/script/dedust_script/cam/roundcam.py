from utility.mouse import getMousePos, setMousePos, getCenter, getRatio
from panda3d.core import Vec3, NodePath, TransformState, Point3
from panda3d.bullet import BulletTriangleMeshShape, BulletRigidBodyNode
from utility.math import lerp

class Scripting():    
    def __init__(self, cam):    
        self.cam = cam   
        self.cam.target = None     
        self.camNP = self.cam.nodePath        
        self.camParent = self.camNP.getParent()
        self.centerNP = None
        self.cam.appendInitFunc(self.init)
        
    def init(self, task):
        if not self.cam.target:            
            return task.cont        
        self.centerNP = NodePath('center cam')   
        self.centerNP.setPos(self.cam.target.getPos())
        self.centerNP.setHpr(self.cam.target.getHpr())     
        self.camNP.reparentTo(self.centerNP)
        self.range = 3
        self.minRange = 3
        self.maxRange = 8
        self.rotHpr = Vec3(0, 0, 0)
        self.curHpr = Vec3(0, 0, 0)
        self.factor = 50
        self.zoomSpeed = 1
        
        self.ratio = getCenter()[0] / getCenter()[1]        
        self.mouseState = {'left': False, 'right': False}
        self.mouseValue = {'left' : [0, 0], 'right': [0, 0]}
        
        self.camNP.setPos( 0, -self.range * self.zoomSpeed, 0)
        self.camNP.lookAt(self.centerNP)     
        self.prevPos = self.cam.getPos(render)
        
        # Registry event
        self.cam.registryEvent('left down', self.setMouseState, ['left', True], 'left mouse down', 'System')
        self.cam.registryEvent('left up', self.setMouseState,   ['left', False],'left mouse up',   'System')
        self.cam.registryEvent('wheel down', self.setZoom,      [-1],           'wheel down',      'System')
        self.cam.registryEvent('wheel up', self.setZoom,        [1],            'wheel up',        'System')
        print "events -.-", self.cam.eventConditions   
        self.initFinish = True     
        self.hit = False
        return task.done
    
    def setMouseState(self, stateName, stateValue):
        self.mouseState[stateName] = stateValue
        self.mouseValue = getMousePos()

    def setZoom(self, value):
        self.range += value
        self.range = max(self.range, self.minRange)
        self.range = min(self.range, self.maxRange)

    def processRotation(self):
        dt = globalClock.getDt()
        pos = getMousePos()
        if pos:
            if self.mouseState['left']:                
                dx , dy = pos[0] - self.mouseValue[0], pos[1] - self.mouseValue[1]
                h = - dx *  self.factor * self.ratio
                p = + dy *  self.factor 
                self.rotHpr = self.curHpr + Vec3(h, p, 0)
                self.centerNP.setHpr(self.rotHpr)
            else:
                self.curHpr = self.centerNP.getHpr()

        self.centerNP.setPos(self.cam.target.getPos() + Vec3(0, 0, 2))
        pos = lerp(self.camNP.getY(), -self.range * self.zoomSpeed, dt)
        self.camNP.setPos(0, pos, 0) 

        if self.hit == True and self.range == self.minRange:
            newrange = -lerp(self.range, self.range+1, dt)
            self.setZoom(newrange)
        
    def processPhysic(self):
        dt = globalClock.getDt()
        world = screen.current.world
        pn = self.cam.physicNode
        shape = pn.node().getShape(0)
        toPos = self.camNP.getPos(render)
        
        result = world.sweepTestClosest(shape, TransformState.makePos(self.prevPos),
                                        TransformState.makePos(toPos), 1)
        if result.hasHit():
            node = result.getNode()
            if isinstance(node, BulletRigidBodyNode):
                shape = node.getShape(0)
                if isinstance(shape, BulletTriangleMeshShape):
                    distance = self.camNP.getY()                    
                    hitpos = result.getHitPos()                    
                    normal = result.getHitNormal()
                    #hitInterval = self.camNP.posInterval(0.1, Point3(0, -self.minRange,0))
                    #hitInterval.start()                    
                    newrange = -lerp(self.range, self.range-0.003, dt)
                    self.setZoom(newrange)
                    self.hit = True
                    return
        self.hit = False


            

    def run(self, task):        
        if hasattr(self, 'initFinish') and self.initFinish == True:
            self.cam.updateEvent()
            self.processRotation()
            self.processPhysic()
            self.prevPos = self.camNP.getPos(render)
        return task.cont
    
    
    def cleanUp(self, task):
        if self.cam.target:
            self.camNP.reparentTo(self.camParent)
            self.initFinish = False
            if self.centerNP:
                self.centerNP.removeNode()
            self.cam.target = None
            self.cam.clearEvent()
        return task.done