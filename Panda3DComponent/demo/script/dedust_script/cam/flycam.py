from utility.mouse import getMousePos, setMousePos, getCenter, getRatio
from panda3d.core import Vec3
from utility.math import lerp

class Scripting():    
    def __init__(self, cam):    
        self.cam = cam        
        self.cam.appendInitFunc(self.init)
        self.camNP = cam.nodePath
        
    def init(self, task):
        # Set basic attribute for fly camera
        setMousePos(0, 0)        
        self.camNP.setHpr(0, 0, 0)
        self.factor = 50
        self.ratio = getRatio() # screen resolution ratio (width / height)
        self.speed = 5 # move speed
        self.accelerator = 1 # move accelerator
        self.direction = "" # move direction
        self.value = 0 # direction value
        # Registry event from framework
        self.cam.registryEvent('w down', self.setDirection, ['forward', 1],'w',    'System')
        self.cam.registryEvent('w up', self.setDirection,   ['forward', 0],'w-up', 'System')
        self.cam.registryEvent('s down', self.setDirection, ['back', 1],   's',    'System')
        self.cam.registryEvent('s up', self.setDirection,   ['back', 0],   's-up', 'System')
        self.cam.registryEvent('a down', self.setDirection, ['left', 1],   'a',    'System')
        self.cam.registryEvent('a up', self.setDirection,   ['left', 0],   'a-up', 'System')
        self.cam.registryEvent('d down', self.setDirection, ['right', 1],  'd',    'System')
        self.cam.registryEvent('d up', self.setDirection,   ['right', 0],  'd-up', 'System')
        return task.done
    
    def setDirection(self, direction, value):
        self.direction = direction
        self.value = value       
        print "set direction", self.direction, self.value 
        
    def processDirection(self):
        # Accelerator value if press enough long
        if self.value == 0:
            self.accelerator = 0
        self.accelerator += 0.1   
        # Moving vector
        forward = render.getRelativeVector(self.cam.nodePath, Vec3(0, 1, 0))
        right = render.getRelativeVector(self.cam.nodePath, Vec3(1, 0, 0))
        # Positioning
        dt = globalClock.getDt()
        cam = self.cam
        pos = cam.getPos()
        if self.direction == 'forward':            
            pos = lerp(pos, 
                       pos + forward * self.accelerator * self.speed * self.value, 
                       dt)            
        elif self.direction == 'back':
            pos = lerp(pos, 
                       pos - forward * self.accelerator * self.speed * self.value, 
                       dt)               
        elif self.direction == 'left':
            pos = lerp(pos, 
                       pos - right * self.accelerator * self.speed * self.value, 
                       dt)           
        elif self.direction == 'right':
            pos = lerp(pos, 
                       pos + right * self.accelerator * self.speed * self.value, 
                       dt)       
                 
        self.camNP.setPos(pos)
        
        
            
    def processRotation(self):
        dt = globalClock.getDt()
        pos = getMousePos()
        if pos:
            cam = self.cam
            dx = pos[0]
            dy = pos[1]
            h = - dx * self.speed * self.factor * self.ratio * 0.16
            p = + dy * self.speed * self.factor * 0.16
            self.camNP.setHpr(cam.nodePath, h, p, 0)
            setMousePos(*getCenter())
        
    
    def run(self, task):        
        # Process event
        self.cam.updateEvent()
        # Process moving direction
        self.processDirection()
        # Process rotation when move mouse
        self.processRotation()
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        self.cam.clearEvent()
        return task.done