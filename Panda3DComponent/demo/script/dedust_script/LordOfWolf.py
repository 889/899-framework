"""Move point appear when left mouse click into game object"""
from game.message import GameMessage
import random
from panda3d.core import RigidBodyCombiner, NodePath
from game.manager import Prototype
from direct.interval.IntervalGlobal import Wait, Sequence, Func

class Scripting():    
    def __init__(self, model):  
        self.model = model
        self.group = []
        self.model.appendInitFunc(self.init)
        rbc = RigidBodyCombiner("rbc")
        self.combiner = NodePath(rbc)        
        #self.model.setScale(10)
        
    def init(self, task):
        self.combiner.reparentTo(screen.current.nodeRoot)
        self.size = self.model.attributes.size
        self.generate()
        # Registry event
            
        return task.done  
    
    def checkAlive(self, idx):
        monster = self.group[idx]
        attributes = monster.attributes
        if attributes.hp < 0:
            return False
        return True
    
    def receiveDamage(self, info):
        if info[0] == "die" and info[1] in self.group:      
            monster = info[1]
            idx = self.group.index(monster)
            # Monster has been killed
            self.model.removeEvent('beAttacked' + str(idx))
            screen.current.removeObject(monster)
            self.group.remove(monster)
            seq = Sequence(Wait(1), 
                            Func(self.generatePart,idx))
            seq.start()                
                
    
    def generate(self):
        print "generate monster"        
        for idx in xrange(0, self.size):            
            self.generatePart(idx)
            #print "part %d"%idx, self.group[idx].getPos()
            
    
    def generatePart(self, idx):
        print "generate part", self.model.realNP.getName(), self.model.getName()
        prototypeId = self.model.attributes.prototype
        name = self.model.realNP.getName() + str(idx)
        model_i = entityMgr.build(Prototype.GameModel, 
                                             prototypeId, 
                                             name,
                                  Scripts="script/dedust_script/namelabel3d.py, script/dedust_script/wolf.py",
                                  Tags="monster")
        
        self.model.registryEvent('beAttacked' + str(idx), self.receiveDamage, 
                                 ["require info"], 
                                 self.model.getName(), name)
        model_i.lord = self.model
        model_i.startScripts()
        model_i.setPos(self.randomPos())        
        self.group.insert(idx, model_i)
        self.randomHpr(model_i)
        
        
    def randomHpr(self, monster): 
        h = random.randint(0, 360)        
        monster.setHpr(h, 0, 0)        
        
    def randomPos(self):
        pos = self.model.getPos()
        padx = random.randint(3, 30)
        pady = random.randint(3, 30)
        pos.setX(pos.getX() + padx); pos.setY(pos.getY() + pady)
        return pos
    
    def run(self, task):
        #update event
        self.model.updateEvent()
        return task.cont
    
    def cleanUp(self, task):
        self.combiner.detachNode()
        # Code here
        for idx, part in enumerate(self.group):
            self.model.removeEvent('beAttacked' + str(idx))
            screen.current.removeObject(part)
        self.group = []
        return task.done