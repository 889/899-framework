from direct.fsm.FSM import FSM
from utility.mouse import getMousePos
from direct.interval.IntervalGlobal import LerpPosInterval, Sequence, Func, Wait
from panda3d.core import Vec3, NodePath
from math import pi
from game.manager import Prototype
from panda3d.bullet import BulletCharacterControllerNode, BulletHeightfieldShape, BulletRigidBodyNode, BulletTriangleMeshShape
from game.message import GameMessage, QuestReceiverReturn
from panda3d.ai import *
from game.Vfx import vfx, P2Pvfx, MovingVfx
from game.map import Map
from utility.rand import *

class Scripting(FSM):    
    def __init__(self, model):  
        FSM.__init__(self, model.getName())  
        self.model = model
        self.model.appendInitFunc(self.init)
        self.upPos = 0
        self.downPos = 0
        self.model.speed = 8
        self.target = None
        # Init some prototype for use in game
        mapMgr.initPrototype(["arrow down"])
        self.arrow = None
        # Quest 
        self.questMgr = QuestReceiverReturn()
        #Create AI World
        store.Aiworld = AIWorld(render)
        self.model.appendInitFunc(self.AIUpdate)
        self.currentSkill = None
        self.skills = {}
        # Attack
        self.attackSeq = Sequence()
        self.inattack = False
    
    def init(self, task):
        store.player = self.model
        cam = screen.current.getObject('round cam')
        cam.target = self.model.nodePath     
        self.actor = self.model.realNP
        self.request('Stand')
        # Registry Event
        self.model.registryEvent('left down', self.leftClick, ['down'], 'left mouse down', 'System')
        self.model.registryEvent('left up', self.leftClick, ['up'], 'left mouse up', 'System')        
        self.model.registryEvent('right down', self.rightClick, ['down'], 'right mouse down', 'System')
        self.model.registryEvent('right up', self.rightClick, ['up'], 'right mouse up', 'System')        
        self.model.registryEvent('stop move', self.stopMove, None, 'player', 'move arrow')
        self.model.registryEvent('npc', self.npc, ["require info" ], 'player', 'npc')        
        self.model.registryFlush('flush talk', 'player', None, 'talk')
        self.model.registryEvent('skill cast',self.castSkill, ["require info" ], 'player', 'skill')
        self.model.registryEvent('respone from victim', self.victimResponse, ["require info"], 'player', 'victim')        
        
        return task.done
    
    def victimResponse(self, info):
        if info[0] == 'killed':
            victim = info[1]
            self.target = None
            self.request('Stand')
        # victim = screen.current.getObjectByType(Prototype.GameModel, info[0])
        # if self.currentSkill == "ElectrixSkill":
        #     P2Pvfx(self.model, victim.nodePath, 'vfx/lightning.png', scale=2.0).start()
            
        # elif self.currentSkill == "FireSkill":
        #     MovingVfx(self.model, victim.nodePath, 'vfx/fire_ball.png',  scale=2.0).start()


    def sendSkillInfo(self):
        if self.target and self.target.nodePath:
            skillmess = GameMessage('player', [self.target.getName()], ('attack', 'magic', 110, self.model))
            screen.dispatch(skillmess)

    def getTargetPos(self):
        if self.target and self.target.nodePath:
            return self.target.getPos()

        
    def castSkill(self, info):
        print "Skill is activating : ", info
        print "Position : ", self.model.getPos()
        name = info[0]
        skill_type = info[1]
        if skill_type == 'attack':
            if self.target and 'monster' in self.target.tags:                
                if name == "FireSkill":
                    vfx(self.model, texture='vfx/vfx3.png',scale=2.0).start()                     
                    skill = entityMgr.build(Prototype.Particle, "point1", randomString(9))                    
                    duration = (self.model.getPos() - self.target.getPos()).length() / 18
                    castIval = LerpPosInterval(skill.nodePath, duration=duration, startPos=self.model.getPos(), pos=self.getTargetPos)                                        
                    seq = Sequence(castIval, Func(screen.current.removeObject, skill), Func(self.sendSkillInfo))
                    seq.start()                 
                    self.lookAt(self.target.nodePath, True)    
                    self.actor.setPlayRate(2, 'cast')
                    self.actor.play('cast')                    
        elif skill_type == 'buff':
            if name == "ElectrixSkill":            
                vfx(self.model, texture='vfx/vfx3.png',scale=2.0).start() 
                self.model.attributes.hp += 50
        
    
    def run(self, task):                

        # Code here             
        # Update event      
        self.model.updateEvent()
        
        # Check for quest
        for event in self.model.eventQueue:
            if self.questMgr.update(event):
                self.model.eventQueue.remove(event)
        self.model.eventQueue = []        
        self.model.move()
        
        # Flush event
        self.model.flushEvent()
        for event in self.model.eventQueue:
            print "character event:", event.fromTag, event.toTagList, event.info
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        return task.done
    
    def npc(self, info):
        if info[0] == "send quest":
            quest = info[1]
            print "receiver quest", quest.name
            if not self.questMgr.isDoing(quest.name):
                print "add quest"
                self.questMgr.add(quest)
        elif info[0] == "reward":
            print "reward from npc:", info
    
    def stopMove(self):     
        if self.state == 'RunThenTalk':            
            self.request('Stand')
            self.talkTo()                      
        elif self.state == 'Run':              
            self.request('Stand')            
        if self.arrow:
            screen.current.removeObject(self.arrow)  
            self.arrow = None           
            
            
    def talkTo(self):
        mess = GameMessage('player', 
                           [self.target.realNP.getName()], 
                           ("clicked", self.questMgr, self.model.attributes))
        screen.dispatch(mess)        

    def attackTo(self, target):
        if target and target.nodePath:
            mess = GameMessage('player', 
                               [target.realNP.getName()], 
                               ("attack", 'physic', self.model.attributes.attack, self.model))
            screen.dispatch(mess)                

    def rightClick(self, state):
        pos = getMousePos()
        if state == 'down':
            closest = screen.current.pointer.getClosest()
            if closest:
                self.checkRightClickNode(closest)
                
    def checkRightClickNode(self, result):
        node = result.getNode()
        self.target = screen.current.getObjectFromPhysicNode(node)

        if "npc" in self.target.tags:               
            talkrange = self.model.attributes.talkrange
            direction = self.target.getPos() - self.model.getPos()        
            if direction.length() <= talkrange:        
                self.talkTo(self.target)
            else:                
                direction.normalize()
                destination = self.target.getPos() - direction * talkrange
                self.request('RunThenTalk', destination)
        elif "monster" in self.target.tags:
            attackrange = self.model.attributes.attackrange        
            self.request('ChaseAttack', self.target, attackrange)           
                

    
    def leftClick(self, state):
        if state == 'down':
            self.downPos = getMousePos()
            # Send quest state to all npc
            mess = GameMessage('player', ['npc'], ("player quest", self.questMgr, self.model.attributes))
            screen.dispatch(mess)
        else:
            self.upPos = getMousePos()
            if self.upPos == self.downPos:
                closest = screen.current.pointer.getClosest()
                if closest:                    
                    self.checkClickNode(closest)
                    
                    
    def checkClickNode(self, result):
        node = result.getNode()
        self.target = screen.current.getObjectFromPhysicNode(node)
        
        # if "npc" in self.target.tags:
        #     # Click to NPC or other player
        #     mess = GameMessage('player', 
        #                        [self.target.realNP.getName()], 
        #                        ("clicked", self.questMgr, self.model.attributes))
        #     screen.dispatch(mess)
        #     print "Send Message to ", self.target.realNP.getName(), "mess", mess.info            
        # elif "monster" in self.target.tags:
        #     # Click to monster
        #     mess = GameMessage('player', 
        #                            [self.target.realNP.getName()], 
        #                            ("attack", "physic", self.model.attributes.attack))
        #     screen.dispatch(mess)
        if self.target and "land" in self.target.tags:
            # Click to new pos
            self.request('Run', result.getHitPos())            
            self.target = None     

    def inAttack(self, value):
        self.inattack = value    
            
    def chaseToAttack(self, target, distance, task):    
        if not (target and target.nodePath):    
            self.request('Stand')
            return task.done
        direction = target.getPos() - self.model.getPos()
        if direction.length() <= distance:               
            self.model.direction = Vec3(0, 0, 0)
            self.lookAt(target.nodePath, True) 
            if len(self.attackSeq) < 2:                           
                idx = random.randint(0, 1)
                anims = ['punch', 'kick']            
                attackanim = self.actor.actorInterval(anims[idx], loop=0, duration=0.5, startFrame=1, endFrame=64, playRate=6)
                inattack = self.actor.actorInterval('inattack', loop=1, duration=1.5)
                self.attackSeq.append(Func(self.inAttack, True))
                self.attackSeq.append(attackanim)                
                self.attackSeq.append(Func(self.attackTo, target))     
                self.attackSeq.append(Func(self.inAttack, False))
                self.attackSeq.append(inattack)                                  
                self.attackSeq.loop()
            print "attack"
        elif self.inattack == False:                        
            anim = self.actor.getAnimControl('run', 'body')
            if not anim.isPlaying():
                self.actor.loop('run')
                self.actor.setPlayRate(3, 'run')
            self.lookAt(target.nodePath)    
            self.forceForward()
        
        return task.cont

    def forceForward(self):
        direction = screen.current.nodeRoot.getRelativeVector(self.actor, Vec3(0, 1, 0))
        direction.normalize()        
        self.actor.setH(self.actor.getH() + 180)
        self.model.direction = direction   

    def lookAt(self, target, invert=False):
        self.actor.lookAt(target)
        self.actor.setP(0)
        if invert:
            self.actor.setH(self.actor.getH() + 180)
                        
    def goTo(self, pos): 
        # Create arrow pointer at destination position     
        if self.arrow:
            screen.current.removeObject(self.arrow)  
            self.arrow = None        
        
        self.arrow = entityMgr.build(Prototype.GameModel, "arrow down", "move arrow",
                                     Scripts="script/dedust_script/movepoint.py",
                                     Pos=[pos.getX(), pos.getY(), pos.getZ()])
        self.arrow.owner = self.model
        self.arrow.startScripts()
        np = self.arrow.realNP
        np.setColor(1, 1, 0, 1)
        np.setScale(0.5, 0.5, 0.5)
        np.loop('rotate')        
        self.lookAt(self.arrow.nodePath)        
        self.forceForward()        
            
        
    #Update AI World
    def AIUpdate(self, task):
        store.Aiworld.update()
        return task.cont
    
    # State
    def enterChaseAttack(self, target, distance):
        self.chaseTask = taskMgr.add(self.chaseToAttack, 'chase state', extraArgs=[target, distance], appendTask=True)

    def exitChaseAttack(self):   
        if self.chaseTask:     
            taskMgr.remove(self.chaseTask)
            
    def enterStand(self):
        self.attackSeq.pause()
        del self.attackSeq
        self.attackSeq = Sequence()
        self.actor.loop('stand')
        self.model.direction = Vec3(0, 0, 0)
        
    def exitStand(self):
        pass

    def enterAttack(self, target):
        self.attackTo(target)

    def exitAttack(self):
        pass
        
    def enterRun(self, pos):
        self.goTo(pos)
        self.actor.setPlayRate(3, 'run')
        self.actor.loop('run')
                
    def exitRun(self):
        self.model.direction = Vec3(0, 0, 0)

    def enterRunThenTalk(self, pos):
        self.goTo(pos)
        self.actor.setPlayRate(3, 'run')
        self.actor.loop('run')

    def exitRunThenTalk(self):
        self.model.direction = Vec3(0, 0, 0)

