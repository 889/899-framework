"""Move point appear when left mouse click into game object"""
from game.message import GameMessage

class Scripting():    
    def __init__(self, model):  
        self.model = model
            
    def run(self, task):
        # Code here             
        result = screen.current.collisionPair(self.model, self.model.owner)
        if result:
            mess = GameMessage(self.model.getName(), ['player'], 'touched')
            print "message: ", self.model.getName()
            screen.dispatch(mess)       
            return task.done
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        self.model.clearEvent()
        return task.done