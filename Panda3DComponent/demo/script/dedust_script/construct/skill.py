from game.message import GameMessage

class Skill():
	def __init__(self, owner, curlv=1, maxlv=20, target=None):
		self.owner = owner
		self.target = target
		self.minlv = 1
		self.maxlv = maxlv
		self.curlv = curlv

	def increaseLv(self):
		self.curlv += 1
		if self.curlv <= self.maxlv:
			self.upgrade()
		else:
			self.curlv = self.maxlv

	def upgrade(self):
		pass

	def start(self):
		pass

class ElectrixBuff(Skill):

	def __init__(self, *args, **kwargs):
		Skill.__init__(*args, **kwargs)
		self.hpamount = 10

	def upgrade(self):
		self.hpamount += 10 * self.curlv / 2

	def start(self):
		attributes = self.owner.attributes
		vfx(self.model, texture='vfx/vfx3.png',scale=2.0).start() 
		attributes.hp += self.hpamount


class FireBall(Skill):
	def __init__(self, *args, **kwargs):
		Skill.__init__(*args, **kwargs)
		self.dmamount = 50

	def upgrade(self):
		self.dmamount += 3 * self.curlv

	def start(self):
		vfx(self.model, texture='vfx/vfx3.png',scale=2.0).start() 
		skillmess = GameMessage('player', [self.target.getName()], 'attack', 'magic', self.dmamount)
		screen.dispatch(skillmess)
