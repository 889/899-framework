from panda3d.core import NodePath, Vec3, TextureStage
import os

class Maze():
    Cross1 = 1
    Cross2 = 2
    Cross3 = 3
    Cross4 = 4
    Cross5 = 5

    RIGHT = 3
    TOP = 2
    LEFT = 1
    BOTTOM = 0

    def __init__(self, name = 'maze_root'):
        self.root = NodePath(name)
        self.tiles = {}

    def load(self, data, path=''):
        self.path = path
        self.data = data        
        self.refreshSize()

    def refreshSize(self):
        self.width = len(self.data)
        self.height = len(self.data[0])

    def getNeightbor(self, ri, ci):
        if ci == 0 or ci == self.width - 1 or ri == 0 or ri == self.height - 1:
            return 0
        return self.data[ri][ci]

    def getCellType(self, ri, ci):
        top = self.getNeightbor(ri-1, ci)
        bottom = self.getNeightbor(ri+1, ci)
        left = self.getNeightbor(ri, ci-1)
        right = self.getNeightbor(ri, ci+1)
        cell_type = top + bottom + left + right
        nears = [bottom, left, top, right]
        if cell_type == self.Cross2:
                if top != bottom or left != right:
                    cell_type = self.Cross5
        return nears, cell_type

    def getRotate(self, nears, cell_type):            
        rotate = 0        
        if cell_type == self.Cross1:
            for idx, near in enumerate(nears):    
                if near == 1:
                    rotate = -90 * idx
        elif cell_type == self.Cross2:
            for idx, near in enumerate(nears):    
                if near == 1:
                    rotate = -90 * idx - 90
        elif cell_type == self.Cross3:
            for idx, near in enumerate(nears):    
                if near == 0:
                    rotate = -90 * idx - 180
        elif cell_type == self.Cross4:            
            rotate = 0
        elif cell_type == self.Cross5:
            if nears == [1, 1, 0, 0]:
                rotate = -90
            elif nears == [1, 0, 0, 1]:
                rotate = 0
            elif nears == [0, 1, 1, 0]:
                rotate = -180
            elif nears == [0, 0, 1, 1]:
                rotate = 90
                
        return rotate


    def getCellCoord(self, ri, ci, nearIdx):
        if nearIdx == self.RIGHT:
            ci += 1
        elif nearIdx == self.LEFT:
            ci -= 1
        elif nearIdx == self.BOTTOM:
            ri += 1
        else:
            ri -= 1
        return ri, ci


    def addTile(self, ri, ci, position):        
        nears, cell_type = self.getCellType(ri, ci)
        rotate = self.getRotate(nears, cell_type)        
        tile = loader.loadModel(os.path.join(self.path, 'cross_' + str(cell_type)))        
        tile.setPos(position)
        tile.setH(rotate)
        nm = loader.loadTexture('Texture/tiles_ns.png')
        ts = TextureStage('ts')
        ts.setMode(TextureStage.MNormal)
        tile.setTexture(ts, nm)
        self.tiles[ri, ci] = tile
        tile.reparentTo(self.root)
        connects = tile.findAllMatches("*/connect*")
        for connect in connects:
            name = connect.getName()
            pos = connect.getPos(render)            
            connect.removeNode()
            connects.removePath(connect)
            nearIdx = abs( (int(name[-1]) - (rotate / 90))%4 )
            if nears[nearIdx] == 1:            
                n_ri, n_ci = self.getCellCoord(ri, ci, nearIdx)
                if not (n_ri, n_ci) in self.tiles.keys():    
                    self.addTile(n_ri, n_ci, pos)
        

    def trasever(self, ri, ci):
        self.addTile(ri, ci, Vec3(0, 0, 0))


    def generate(self):
        '''
        Find first start point.
        '''
        # ri : row index
        # ci : colume index
        for ri, row in enumerate(self.data):
            for ci, cell in enumerate(row):
                if cell == 1:
                    self.trasever(ri, ci)
                    return ri, ci

    def getRoot(self):
        return self.root