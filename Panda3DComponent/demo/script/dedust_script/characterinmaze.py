from game.manager import Prototype
class Scripting():    
    def __init__(self, model):  
        self.model = model
        self.model.setLightOff(1)
        self.model.appendInitFunc(self.init)
        
    def init(self, task):
        self.light = entityMgr.build(Prototype.Light, "CaveFire", "my fire")
        self.light.nodePath.reparentTo(self.model.nodePath)
        self.light.setPos(0, -2, self.model.attributes.height)
        print self.light.getPos()
        return task.done
    
    def run(self, task):
        # Code here            
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        self.model.clearEvent()
        return task.done