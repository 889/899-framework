"""Move point appear when left mouse click into game object"""
from game.message import GameMessage
from direct.fsm.FSM import FSM
import random
from game.manager import Prototype
from direct.interval.IntervalGlobal import Wait, Sequence, Func
from game.map import Map
from panda3d.ai import *
import time
from game.Vfx import vfx

class Scripting(FSM):    
    def __init__(self, model):  
        FSM.__init__(self, model.getName()) 
        self.model = model
        self.model.appendInitFunc(self.init)        
        self.target = None

        
    def init(self, task):        
        # Set AI
        self.AIchar = AICharacter("WolfWander_" + self.model.getName(),self.model.nodePath ,100, 0.5, 5)
        store.Aiworld.addAiChar(self.AIchar)
        self.AIbehaviors = self.AIchar.getAiBehaviors()                
        self.model.registryEvent('be attacked', self.receiveDamage, 
                                     ["require info"], 
                                     self.model.getName(), 'player')
        self.request('Wander')
        #self.player = Map.current.gameObjects['GameModels']['Lora']
        return task.done
        
    def receiveDamage(self, eventInfo):
        if eventInfo[0] == 'attack':
            self.target = eventInfo[3]
            beattackedVfx = vfx(self.model, texture='vfx/blast2.png',scale=1.0)  
            beattackedVfx.start()
            self.model.attributes.hp -= self.calculateDamage(eventInfo, self.model.attributes)        
            if self.model.attributes.hp < 0:                      
                mess = GameMessage('victim', 
                                   ['player'], 
                                   ('killed', self.model))
                screen.dispatch(mess)
                mess = GameMessage('wolf', 
                                   ['player'], 
                                   'killed')
                screen.dispatch(mess)
                self.request('Die')    
            if self.state != 'Attack':                
                self.request('Attack', eventInfo[3])  
        


    def calculateDamage(self, info, attributes):
        amount = info[2]
        attackType = info[1]
        true_damage = 0
        if attackType == 'physic':
            true_damage += amount - attributes.armor * 0.01 
        elif attackType == 'magic':
            true_damage += amount - attributes.resist * 0.01
        return true_damage
        
    def enterElectrix(self):        
        vfx(self.model, texture='vfx/plasm2.png',scale=2.0).start() 
    
    def exitElectrix(self):
        pass

    def endAnim(self, name, func, args, task):
        anim = self.model.realNP.getAnimControl(name)
        if not anim.isPlaying():
            func(*args)
            return task.done
        return task.cont

    def endDie(self):        
        
        taskMgr.add('death', self.endAnim, (mess))
    
    def enterDie(self):   
        self.AIchar.setMaxForce(0) 
        dievfx = vfx(self.model, texture='vfx/blast2.png',scale=3.0)        
        death = self.model.realNP.actorInterval("death",
                                                loop=0,                        
                                                duration=1.5
                                                )
        mess = GameMessage(self.model.getName(), 
                           [self.model.lord.getName()], 
                           ("die", self.model))
        seq = Sequence(death,
                        Func(screen.dispatch, mess),
                        Func(dievfx.start))
        seq.start()
        
        
    def exitDie(self):
        pass        
    
    def enterWander(self):
        self.AIbehaviors.wander(2)
        self.model.realNP.loop("walk")
    
    def exitWander(self):
        pass
    
    def enterAttack(self, target):
        self.AIbehaviors.pursue(target.nodePath)        
        print "WWW I will bite you"   
        self.AIchar.setMaxForce(15)
             
           
        
    def exitAttack(self):
        self.AIchar.setMaxForce(5) 
    
    def cleanUp(self, task):
        # Code here
        return task.done
    
    def run(self, task):
        self.model.nodePath.setP(0)
        #distance = (self.player.getPos() - self.model.getPos()).length()
        
        #update event
        self.model.updateEvent()
        
        if self.state == "Attack":
            if self.target and (self.target.getPos() - self.model.getPos()).length() < self.model.attributes.attackrange: 
                self.AIbehaviors.pauseAi('pursue')
                self.AIbehaviors.pauseAi('wander')
                attack = self.model.realNP.getAnimControl('attack', 'body')
                if not attack.isPlaying():
                    self.model.realNP.loop("attack")
                pos = self.model.getPos()
                self.model.realNP.lookAt(self.target.nodePath)
                self.model.realNP.setH(self.model.realNP.getH() + 180)
                self.model.realNP.setP(0)                                
                self.model.realNP.setR(0)   
            else:
                self.model.realNP.setH(0)
                pursue = self.AIbehaviors.behaviorStatus('pursue')                
                if pursue == 'paused':
                    self.AIbehaviors.resumeAi('pursue')
                run = self.model.realNP.getAnimControl('run', 'body')
                if not run.isPlaying():
                    self.model.realNP.loop("run")
        #PandaAI
#         if distance < 3:
#             if self.behavior != "attack":
# #                print "Require ATTACK !!!!!"
#                 self.request("Attack")
#         elif distance < 50:
#             if self.behavior != "pursue":
#                 self.request("Pursue")
#         elif self.behavior != "wander":
#             self.request("Wander")
            
            
        return task.cont
    