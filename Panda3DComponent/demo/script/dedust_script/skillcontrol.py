from demo.script.dedust_script.gui.name import createName
from utility.position import compute2dPosition
from panda3d.core import Point3
from utility.pritority import Priority
from demo.script.dedust_script.gui.skill import createSkillBar, setSkillShortcut,\
    createSkill
from game.message import GameMessage


class Scripting():    
    def __init__(self, model):    
        self.model = model
        self.model.appendInitFunc(self.init)        
        skillbar = createSkillBar()
        skill1 = createSkill(skillbar, "ElectrixSkill", "image/skills/skill01", ["skill01"], self.cast, ['buff'], 1)
        skill2 = createSkill(skillbar, "FireSkill", "image/skills/skill02", ["skill02"], self.cast, ['attack'], 2)
        skill3 = createSkill(skillbar, "skill 3", "image/skills/skill03", ["skill03"], self.cast, ['attack'], 3)
        skill4 = createSkill(skillbar, "skill 4", "image/skills/skill04", ["skill04"], self.cast, ['attack'], 4)
        setSkillShortcut(skill1, "1")
        setSkillShortcut(skill2, "2")
        setSkillShortcut(skill3, "3")
        setSkillShortcut(skill4, "4")
                
    def init(self, task):       
        # Name label
        return task.done
    
    def run(self, task):  
        return task.cont    
    
    def cleanUp(self, task):
        return task.done
            
    def cast(self, name, skill_type):
        mess = GameMessage('skill', ['player'], (name, skill_type))
        screen.dispatch(mess)
                            
    
