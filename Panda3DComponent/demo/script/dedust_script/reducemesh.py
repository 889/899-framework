from game.message import GameMessage

class Scripting():    
    def __init__(self, model):  
        self.model = model
        self.model.appendInitFunc(self.init)

    def init(self, task):
        self.model.realNP.flattenStrong()
        print "reduce mesh"
        return task.done
            
    def run(self, task):
        # Code here           
        return task.done
    
    def cleanUp(self, task):
        # Code here
        return task.done