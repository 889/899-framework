from game.message import GameQuestV2, GameMessage, QuestDeliver,\
    QuestDeliverReturn, GameQuestReturn
from demo.script.dedust_script.gui.messagedlg import createDialog,\
    createMissionButton
from demo.script.dedust_script.gui.missiondlg import createMissionDialog
from direct.fsm.FSM import FSM
from game.manager import Prototype

class Scripting(FSM):    
    def __init__(self, model):
        FSM.__init__(self, model.getName())    
        self.model = model
        self.questDeliver = QuestDeliverReturn()
        self.initQuest()
        self.model.appendInitFunc(self.init)
        self.missiondlg = None
        self.missionlst = None
        
    def init(self, task):        
        self.model.realNP.loop('stand')
        self.model.registryEvent('npcEvent on npc', self.npcEvent, ["require info" ], self.model.realNP.getName(), 'player')
        self.model.registryEvent('npc event', self.npcEvent, ["require info" ], 'npc', 'player')
        return task.done
    
    def showQuestDetail(self, quest_name):
        quest = self.questDeliver.get(quest_name)
        if quest:
            if self.missiondlg:
                self.missiondlg.removeNode()
              
            rewards = []
            missions = []
            for reward in quest.finishMessages:
                rewards.append( str(reward.info[2]) + " " + reward.info[1] )
            for mission in quest.messages.values():
                info = mission[2]
                info += " (%d/%d)" % (mission[0], mission[1])
                missions.append(info)
            self.missiondlg = createMissionDialog(quest_name, quest_name, quest.info, missions, rewards, self.sendQuestToPlayer,
                                                  [quest_name])
            self.missiondlg.setPos(0.5, 0, 0)
            
    def sendQuestToPlayer(self, quest_name):
        print "send quest", quest_name, self.questDeliver.get(quest_name).name
        mess = GameMessage("npc", ['player'], 
                           ("send quest", self.questDeliver.get(quest_name)) )
        screen.dispatch(mess)
        self.missiondlg.removeNode()
       
        
    def npcEvent(self, info):
        print "weapon npc has been npcEvent"
        if info[0] == 'clicked':
            # Create avaiable quests dialog
            returnQuest = self.questDeliver.getReturnQuest(info[1])
            if returnQuest:
                self.playerReturnQuest(returnQuest)
            else:
                self.playerAskQuest(info)
        elif info[0] == "player quest":
            print "player quest"
            returnQuest = self.questDeliver.getReturnQuest(info[1])
            if returnQuest:
                print "finish quest icon"
                self.request("FinishQuest")
            else:               
                print "check has quest icon"
                quests = self.questDeliver.getAvaiQuests(info[1], info[2])
                if len(quests) > 0:
                    print "has quest"
                    self.request("HasQuest")
                else:
                    self.request("NoQuest")
    def enterHasQuest(self):
        self.missionbutton = entityMgr.build(Prototype.GameModel, "has mission", "weaponc has mission")
        pos = self.model.getPos()
        pos.setZ(pos.getZ() + 2.5)
        self.missionbutton.setPos(pos)
        self.missionbutton.nodePath.loop('updown')
        self.missionbutton.setLightOff(1)
        
        
    def exitHasQuest(self):
        screen.current.removeObject(self.missionbutton)
        
    def enterFinishQuest(self):
        self.finishbutton = entityMgr.build(Prototype.GameModel, "finish mission", "weaponc finish mission")
        pos = self.model.getPos()
        pos.setZ(pos.getZ() + 2.5)
        self.finishbutton.setPos(pos)
        self.finishbutton.nodePath.loop('updown')
        self.finishbutton.setLightOff(1)
    
    def exitFinishQuest(self):
        screen.current.removeObject(self.finishbutton)
    
    def enterNoQuest(self):
        pass
    
    def exitNoQuest(self):
        pass              
                    
    def playerAskQuest(self, info):
        # Send talk message
        talk = GameMessage('pit', ['player'], "talk")
        screen.dispatch(talk)
        quests = self.questDeliver.getAvaiQuests(info[1], info[2])
        if len(quests) > 0:
            content = """
Hello. This village is peaceful and beauty. I love to live here!      
Would you like to do something for me?               
"""
        else:
            content = """
This is lovely village. Nice weather!     
"""
        if self.missionlst:
            self.missionlst.removeNode()
        self.missionlst= createDialog('avaiquestdlg', "Quest list", content)
        self.missionlst.setPos(-0.5, 0, 0)
        
        for idx, quest in enumerate(quests):
            createMissionButton(self.missionlst, "bt%d"%idx, quest.name, self.showQuestDetail, idx)
            
    def playerReturnQuest(self, quest):  
        # Send talk message
        talk = GameMessage('pit', ['player'], "talk")
        screen.dispatch(talk) 
        content = """
Oh oh oh, you just "%s"!
Here your reward.          
"""%quest.name
        if self.missionlst:
            self.missionlst.removeNode()
        self.missionlst= createDialog('avaiquestdlg', "Quest list", content)
        self.missionlst.setPos(-0.5, 0, 0)        

    def run(self, task):  
        self.model.updateEvent()      
        return task.cont    
    
    def cleanUp(self, task):
        return task.done
    
    def initQuest(self):
        # Conversation quest 1
        # Mission 1   
        kill_wolf = GameMessage('wolf', ['player'], "killed")
        talk_to_me =  GameMessage('pit', ['player'], "talk")
        protect_quest = GameQuestReturn("Protect village", "Our village is surround by wolfs. Kill them to protect our village.",
                            { 
                             kill_wolf: [0, 3, 'Kill 3 black wolfs at the entertrance to village.']
                            }, talk_to_me)
        # Reward
        reward1 = GameMessage('npc', ['player'], ("reward", "gold", 500))
        reward2 = GameMessage('npc', ['player'], ("reward", "exp", 800))
        protect_quest.addFinishMessage(reward1)
        protect_quest.addFinishMessage(reward2)
        # Mission 2
        go_to_portal = GameMessage('mazeportal', ['player'], "touched")
        go_to_finish_portal = GameMessage('finishportal', ['player'], "touched")
        goto_maze = GameQuestReturn("Go to maze", "Goto portal at the campain tent, you can see in grass field. Then you will be in maze.",
                            { 
                             go_to_portal: [0, 1, 'Go to maze portal.']
                            }, go_to_finish_portal)
        goto_maze.addChildQuest(protect_quest)
        self.questDeliver.addDependent('Talk to pit')
        self.questDeliver.add(goto_maze)
#        
#        # Conversation quest 2
#        # Mission    
#        talk_to_busman = GameMessage('busman', ['player'], "talk")
#        talk_to_me =  GameMessage('sam', ['player'], "talk")
#        conversation_quest2 = GameQuestReturn("Talk to business man", "Go to the port of village, then talk to a man there. Finally, talk to Sam to finish your quest",
#                            { 
#                             talk_to_busman: [0, 1, 'Ask business man about info of the way out of village.']
#                            }, talk_to_me)
#        # Reward
#        reward = GameMessage('sam', ['player'], ("reward", "exp", 500))
#        conversation_quest2.addFinishMessage(reward)
#        self.questDeliver.add(conversation_quest2)
                            
    
