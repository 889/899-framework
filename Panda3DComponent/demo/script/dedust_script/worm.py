"""Move point appear when left mouse click into game object"""
from game.message import GameMessage
from direct.fsm.FSM import FSM
import random
from game.manager import Prototype
from direct.interval.IntervalGlobal import Wait, Sequence, Func
from game.map import Map
from panda3d.ai import *
import time
from game.Vfx import vfx

class Scripting(FSM):    
    def __init__(self, model):  
        FSM.__init__(self, model.getName()) 
        self.model = model
        self.model.appendInitFunc(self.init)
        self.behavior = "None"
        self.HP = 1000
        self.damePerHit = 200
        self.model.setScale(0.1)        
        self.player = Map.current.gameObjects['GameModels']['Lora']

        
    def init(self, task):        
        # Set AI
        self.AIchar = AICharacter("WolfWander_" + self.model.getName(),self.model.nodePath ,100, 0.5, 0.5)
        store.Aiworld.addAiChar(self.AIchar)
        self.AIbehaviors = self.AIchar.getAiBehaviors()
#        self.AIbehaviors.wander(5,0,10,1.0)
        self.behavior = "wander"
        self.model.realNP.setHpr(90, 0, 0)
        self.model.realNP.loop("walk")
        self.model.registryEvent('be attacked', self.receiveDamage, 
                                     ["require info"], 
                                     self.model.getName(), 'player')
        
        return task.done
        
    def receiveDamage(self, eventInfo):
        self.model.attributes.hp = -1
        mess = GameMessage(self.model.getName(), 
                           ['Wolf01'], 
                           ("attack"))
        screen.dispatch(mess)
        mess = GameMessage('victim', 
                           ['player'], 
                           (self.model.getName()))
        screen.dispatch(mess)
        store.wormCount -= 1
        seq = Sequence()
#        seq.append(Func(self.request, "Electrix"))
        seq.append(Wait(2))
        seq.append(Func(self.request, "Boom"))
        seq.append(Wait(1.5))
        seq.append(Func(self.request, "Die"))
        seq.append(Wait(2.5))
        seq.append(Func(Map.current.removeObject, self.model))
        seq.start()
#        Map.current.removeObject(self.model)
        
    def enterElectrix(self):        
        vfx(self.model, texture='vfx/plasm2.png',scale=2.0).start() 
    
    def exitElectrix(self):
        pass
    
    def enterBoom(self):        
        vfx(self.model, texture='vfx/blast2.png',scale=3.0).start()
        
    def exitBoom(self):
        pass
    
    def enterDie(self):
        print "I'm Die" , self.model.getName()        
        self.model.realNP.setHpr(0, 0, 90)
        
    def exitDie(self):
        pass
    
    def enterPursue(self):
#        self.AIbehaviors.pursue(self.player.nodePath)
        self.behavior = "pursue"
#        self.AIchar.setMaxForce(15)
#        self.model.realNP.setPlayRate(3, "run")
        self.model.realNP.loop("stand")
        
    def exitPursue(self):
        self.AIchar.setMaxForce(5) 
    
    def enterWander(self):
#        self.AIbehaviors.wander(5,0,10,1.0)
        self.behavior = "wander"
#        self.model.realNP.loop("walk")
    
    def exitWander(self):
        pass
    
    def enterAttack(self):
        print "WWW I will bite you"
        self.behavior = "attack"
        self.model.realNP.loop("attack")      
           
        
    def exitAttack(self):
        pass
    
    def cleanUp(self, task):
        # Code here
        return task.done
    
    def run(self, task):
        distance = (self.player.getPos() - self.model.getPos()).length()
        
        
        #update event
        self.model.updateEvent()
        self.model.lookAt(self.player.realNP)
        
        #PandaAI
        if distance < 53:
            if self.behavior != "attack":
#                print "Require ATTACK !!!!!"
                self.request("Attack")
        elif distance < 150:
            if self.behavior != "pursue":
                self.request("Pursue")
        elif self.behavior != "wander":
            self.request("Wander")
            
            
        return task.cont
    