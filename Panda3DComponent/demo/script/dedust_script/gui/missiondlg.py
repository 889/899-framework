from game.factory import GuiFactory
from panda3d.core import TextNode

text_wrap = 20
dialog_name = "message_dialog"
title = "Message"
content_pos = [-0.85, 0.63]
reward_pos = [-0.85, -0.3]
def getDialogData(dialog_name, title="Message"):
    dialog_data = {
    "Direct" : {
        dialog_name: {        
            "type": "OnscreenImage",
            "image": "image/dedust_image/dialog/mdialog_bg.png",    
            "scale" : [0.6, 1, 0.5],
            "trans" : "True",
            "childs" : {    
                "%s_title"%dialog_name: {        
                    "type" : "OnscreenText",
                    "scale" : [0.13, 0.13],
                    "align" : "TextNode.ALeft",
                    "font" : "font/palabi.ttf",
                    "text" : title,
                    "fg" : [1, 1, 1, 1],
                    "pos" : [-0.9, 0.83]
                },
                "%s_accept"%dialog_name: {
                    "type" : "DirectButton",            
                    "relief" : None,
                    "geom" : {
                        "MapPath": "image/dedust_image/dialog/button",
                        "ImageNames": ["button", "button_hot", "button_hot", "button"]
                    }, 
                    "scale": [0.8, 1, 0.8],     
                    "geom_scale" : [0.35, 1, 0.2],
                    "text" : "accept",
                    "text_font" : "font/comic.ttf",
                    "text_fg": [1, 1, 1, 1],
                    "text_scale" : [0.07, 0.07],
                    "text_pos": [0, -0.03],
                    "pos" : [0.5, 0, -0.8]              
                }#,
#                "%s_return"%dialog_name: {
#                    "type" : "DirectButton",            
#                    "relief" : None,
#                    "geom" : {
#                        "MapPath": "image/dedust_image/dialog/button",
#                        "ImageNames": ["button", "button_hot", "button_hot", "button"]
#                    },            
#                    "scale": [0.8, 1, 0.8],
#                    "geom_scale" : [0.35, 1, 0.2],
#                    "text" : "return",
#                    "text_font" : "font/comic.ttf",
#                    "text_fg": [1, 1, 1, 1],
#                    "text_scale" : [0.075, 0.075],
#                    "text_pos": [0, -0.03],
#                    "pos" : [0.5, 0, -0.8]              
#                },
#                "%s_reject"%dialog_name: {
#                    "type" : "DirectButton",            
#                    "relief" : None,
#                    "geom" : {
#                        "MapPath": "image/dedust_image/dialog/button",
#                        "ImageNames": ["button", "button_hot", "button_hot", "button"]
#                    },            
#                    "scale": [0.8, 1, 0.8],
#                    "geom_scale" : [0.35, 1, 0.2],
#                    "text" : "reject",
#                    "text_font" : "font/comic.ttf",
#                    "text_fg": [1, 1, 1, 1],
#                    "text_scale" : [0.075, 0.075],
#                    "text_pos": [0, -0.03],
#                    "pos" : [-0.5, 0, -0.8]              
#                }
                ,
                "%s_close"%dialog_name: {
                    "type" : "DirectButton",            
                    "relief" : None,
                    "geom" : {
                        "MapPath": "image/dedust_image/dialog/button",
                        "ImageNames": ["button", "button_hot", "button_hot", "button"]
                    },            
                    "scale": [0.8, 1, 0.8],
                    "geom_scale" : [0.35, 1, 0.2],
                    "text" : "close",
                    "text_font" : "font/comic.ttf",
                    "text_fg": [1, 1, 1, 1],
                    "text_scale" : [0.075, 0.075],
                    "text_pos": [0, -0.03],
                    "pos" : [-0.5, 0, -0.8]              
                }

                        
            }     
        }
    }
    }
    return dialog_data

def closeDialog(gui):
    gui.removeNode()

def createMissionDialog(name, title, content, missions, rewards, acceptEvent, args=[]):    
    dialog_data = getDialogData(name, title)
    guiMgr = GuiFactory.product(data=dialog_data)
    gui = guiMgr.getGui(name)
    # Content
        
   
    # Mission
    mission_text = "Mission:\n\t"
    mission_text += "\n\t".join(missions)
    
    # Reward
    reward_text = "Reward:\n\t"
    reward_text += "\n\t".join(rewards)
    
    text = TextNode(name+'contet')
    
    text.setText("\n\n".join([content, mission_text , reward_text]))
    font = loader.loadFont('font/comic.ttf')
    text.setFont(font)
    text.setWordwrap(text_wrap)  
    textNodePath = gui.attachNewNode(text)
    textNodePath.setScale(0.07)    
    textNodePath.setPos(content_pos[0], 0, content_pos[1])
    
    # Close dialog
    close = guiMgr.getGui("%s_close"%name)
    close['command'] = closeDialog
    close['extraArgs'] = [gui]
    
    # Accept mission
    accept = guiMgr.getGui("%s_accept"%name)
    accept['command'] = acceptEvent
    accept['extraArgs'] = args   
    return gui

    
    