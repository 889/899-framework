from game.factory import GuiFactory

skillbar_data = {
    "Direct" : {
        "skillbar" : {
            "type" : "OnscreenImage",
            "image" : "image/skills/skillbar.png",
            "trans" : "True",
            "scale" : [0.9, 1, 0.1],
            "pos" : [0, 0, -0.9]
        }
    }
}

skill_data= """{
    "Direct" : {
        "{name}_skill" : {
            "type" : "DirectButton",
            "relief": None,
            "geom" : {
                    "MapPath": "{mappath}",
                    "ImageNames": {listbutton}
            },
            "geom_scale": [0.1, 1, 1],
            "text_fg": [1, 1, 1, 1],
            "pos" : [0.47, 0, 0],
            "text_scale" : [0.04, 0.4],
            "text_pos" : [0, 0.6],
            "text_font" : "font/timesbd.ttf",      
            "text" : ""
        }
    }
}"""

slot_pos = {
1 : [-0.725, 0, 0],
2 : [-0.620, 0, 0],
3 : [-0.515, 0, 0],
4 : [-0.405, 0, 0],
5 : [-0.295, 0, 0],
6 : [-0.190, 0, 0],
7 : [-0.080, 0, 0],
8 : [0.03, 0, 0],
9 : [0.14, 0, 0],
10 : [0.25, 0, 0],
11 : [0.36, 0, 0],
11 : [0.47, 0, 0]
}

max_slot = 11


def createSkillBar():
    gui = GuiFactory.product(data=skillbar_data)
    return gui.getGui('skillbar')
    
def createSkill(skillbar, skillname, mappath, imagelist, clickEvent, args=[], slot=1):
    if slot > max_slot:
        raise Exception("Slot position is out of range")
    data = skill_data.replace("{name}", skillname)
    data = data.replace("{mappath}", mappath)
    data = data.replace("{listbutton}", str(imagelist))    
    print data
    dict_data = eval(data)
    guiMgr = GuiFactory.product(data=dict_data)
    gui = guiMgr.getGui(skillname+'_skill')
    gui.reparentTo(skillbar)
    gui.setPos(*slot_pos[slot])
    gui['command'] = clickEvent
    extraArgs = [skillname]
    extraArgs.extend(args)
    gui['extraArgs'] = extraArgs
    return gui

def setSkillShortcut(skill, shortcut):
    skill['text'] = shortcut
    base.accept(shortcut, skill.commandFunc, ["click"])
    
    