from game.factory import GuiFactory
from panda3d.core import TextNode
import direct.gui.DirectGuiGlobals as DGG

text_wrap = 17
dialog_name = "message_dialog"
title = "Message"
content_pos = [-0.37, 0.63]
quest_pos = [-0.37, 0, -0.3]

def getButtonData(button_name, content="OK"):
    button_data = {
    "Direct" : {
        button_name: {
            "type" : "DirectButton",
            "relief" : None,
            "text_font" : "font/pala.ttf",
            "text" : content,
            "text_scale" : [0.2, 0.2],
            "text_fg" : [0.8, 0.8, 0.8, 1],
            "text_align": "TextNode.ALeft"
        }
    }
    }
    return button_data

def getDialogData(dialog_name, title="Message"):
    dialog_data = {
    "Direct" : {
        dialog_name: {        
            "type": "OnscreenImage",
            "image": "image/dedust_image/dialog/mdialog_bg_verticle.png",    
            "scale" : [0.9, 1, 0.5],
            "trans" : "True",
            "childs" : {    
                "%s_title"%dialog_name: {        
                    "type" : "OnscreenText",
                    "scale" : [0.06, 0.08],
                    "align" : "TextNode.ALeft",
                    "font" : "font/palabi.ttf",
                    "text" : title,
                    "fg" : [1, 1, 1, 1],
                    "pos" : [-0.4, 0.83]
                },
                "%s_close"%dialog_name: {
                    "type" : "DirectButton",            
                    "relief" : None,
                    "geom" : {
                        "MapPath": "image/dedust_image/dialog/button",
                        "ImageNames": ["button", "button_hot", "button_hot", "button"]
                    },            
                    "geom_scale" : [0.15, 1, 0.12],
                    "text" : "Close",
                    "text_font" : "font/comic.ttf",
                    "text_fg": [1, 1, 1, 1],
                    "text_scale" : [0.03, 0.06],
                    "text_pos": [0, -0.01],
                    "pos" : [0, 0, -0.8]              
                }
            }     
        }
    }
    }
    return dialog_data

def closeDialog(gui):
    gui.removeNode()

def createDialog(name, title, content):    
    dialog_data = getDialogData(name, title)
    guiMgr = GuiFactory.product(data=dialog_data)
    gui = guiMgr.getGui(name)
    # Content
    text = TextNode(name+'contet')
    text.setText(content)
    font = loader.loadFont('font/comic.ttf')
    text.setFont(font)
    text.setWordwrap(text_wrap)
    textNodePath = gui.attachNewNode(text)
    textNodePath.setScale(0.04, 1, 0.06)    
    textNodePath.setPos(content_pos[0], 0, content_pos[1])
    gui.content = textNodePath
    # Close dialog
    close = guiMgr.getGui("%s_close"%name)
    close['command'] = closeDialog
    close['extraArgs'] = [gui]    
    return gui
    
def changeTextColor(gui, color, pos):
    gui['text_fg'] = color
    
def createMissionButton(parent, name, content, clickEvent, idx=0):
    global quest_pos
    button_data = getButtonData(name, content)
    guiMgr = GuiFactory.product(data=button_data)
    gui = guiMgr.getGui(name)

    pos = [item for item in quest_pos]
    gui.setScale(0.2, 1, 0.3)    
    pos[2] += idx * 0.1
    gui.setPos(*pos)
    gui.bind(DGG.ENTER, changeTextColor, [gui, [1, 1, 0, 1]])
    gui.bind(DGG.EXIT, changeTextColor, [gui, [0.8, 0.8, 0.8, 1]])
    gui.reparentTo(parent)
    gui['command'] = clickEvent
    gui['extraArgs'] = [content]
    return gui