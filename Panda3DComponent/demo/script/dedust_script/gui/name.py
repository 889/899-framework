import json
from game.factory import GuiFactory
from panda3d.core import TextNode

name_gui_str= """
{
    "Direct" : {
        "{name}_name_label": {
            "type" : "OnscreenText",
            "text" : "{name}",
            "scale" : [0.04, 0.04],
            "fg" : [1, 1, 1, 1]            
        }
    }
}
"""


def createName(name, always=True):
    text = TextNode(name+'_name_label')
    text.setText(name)
    text.setAlign(TextNode.ACenter)
    font = loader.loadFont('font/timesbd.ttf')
    text.setFont(font)
    if always==True:
        textNodePath = render2d.attachNewNode(text)
    else:
        textNodePath = render.attachNewNode(text)
        textNodePath.setBillboardPointEye()
    
    textNodePath.setScale(0.04)
    return textNodePath
    

    