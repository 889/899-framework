
from game.message import GameMessage
from demo.script.dedust_script.construct.maze import Maze
from game.builder import BaseBuilder
from game.manager import Prototype

maze_shape =[
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0],
[0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0],
[0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0],
[0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0],
[0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]

bullet_shape = {
    "PhysicBody":{
        "Name" : "maze physic body",
        "Type" : "BulletRigidBodyNode",
        "Shapes" : {
            "shape1" : {
                "Type" : "TriangleMesh",
                "Geom" : "Auto",
                "Dynamic" : ""
            } 
        }
    }
}

class Scripting():    
    def __init__(self, model):  
        self.model = model
        self.maze = Maze(name='maze root')
        self.model.appendInitFunc(self.init)
        
    def init(self, task):
        screen.current.nodeRoot.setShaderAuto()
        base.setBackgroundColor(0, 0, 0)
        self.maze.load(maze_shape, "Models/map/maze")        
        self.maze.generate()
        np = self.maze.getRoot()
        np.flattenStrong()
        np.reparentTo(self.model.realNP)
        
        # Create physic body
        mbuilder = BaseBuilder()
        mbuilder.processBulletData(bullet_shape, self.model, screen.current.nodeRoot, screen.current.world)
        screen.current.updateObject(Prototype.GameModel, self.model)
        
        return task.done
    
    def run(self, task):
        # Code here            
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        self.model.clearEvent()
        return task.done