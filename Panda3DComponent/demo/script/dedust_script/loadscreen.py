from direct.interval.IntervalGlobal import LerpColorInterval, Sequence, Wait, Func
from game.baseobject import FlagObject

def finish_loadscreen():
    print "finish loadscreen"
    mapMgr.goToMap('MainScreen')
    
flag_obj, fade = None, None

def loadscreen_init(task):
    print "init loadscreen"
    # Tag initliaze
    global flag_obj, fade
    flag_obj = FlagObject("LoadScreen", "Left Click", "Escape")
    screen.dispatcher.addObject(flag_obj)
    
    # Load screen interval
    gui = screen.current.gui
    loadImageNP = gui.getGui('loadImage')
    disappearI = LerpColorInterval(loadImageNP, 3, (0, 0, 0, 1), (1, 1, 1, 1))
    appearI = LerpColorInterval(loadImageNP, 3, (1, 1, 1, 1), (0, 0, 0, 1))
    fade = Sequence(appearI,
                    Wait(1.5),
                    disappearI,
                    Func(finish_loadscreen),
                    Wait(2),
                    name="fade")
    fade.start()    
    return task.done

def inTagList(tagList, *tags):
    for tag in tags:
        if tag in tagList:
            return True
    return False

def loadscreen_run(task):
    global flag_obj
    if flag_obj:
        for event in flag_obj.eventQueue:
            if inTagList(event.toTagList, "Left Click", "Escape"):
                # Finish fade interval                
                fade.finish()
                # Finish load screen
                finish_loadscreen()
            flag_obj.eventQueue.remove(event)
    return task.cont