from demo.script.dedust_script.gui.name import createName
from utility.position import compute2dPosition
from panda3d.core import Point3
from utility.pritority import Priority
class Scripting():    
    def __init__(self, model):    
        self.model = model
        self.model.appendInitFunc(self.init)
        self.model.appendFuncs(self.update, sort=Priority.Last) 
        self.nameLabel = createName(self.model.realNP.getName(), False)
        
    def init(self, task):       
        # Name label
        
        self.nameLabel.setScale(0.3)        
        return task.done
    
    def update(self, task):
        # Update label name
        attributes = self.model.attributes
        self.nameLabel.setPos(self.model.nodePath, 0, 0, attributes.height)        
        return task.cont

    def run(self, task):  
        return task.cont    
    
    def cleanUp(self, task):
        self.nameLabel.removeNode()
        return task.done
                            
    
