from demo.script.dedust_script.gui.name import createName
from utility.position import compute2dPosition
from panda3d.core import Point3
from utility.pritority import Priority
class Scripting():    
    def __init__(self, model):    
        self.model = model
        self.model.appendInitFunc(self.init)
        self.model.appendFuncs(self.update, sort=Priority.Last) 
        
    def init(self, task):       
        # Name label
        
        self.nameLabel = createName(self.model.realNP.getName())
        return task.done
    
    def update(self, task):
        # Update label name
        attributes = self.model.attributes
        pos = compute2dPosition(self.model.nodePath, Point3(0, 0, attributes.height))
        if pos:
            self.nameLabel.setPos(pos.getX(), 0, pos.getY())
        else:
            self.nameLabel.setPos(100, 100, 100)
        return task.cont

    def run(self, task):  
        return task.cont    
    
    def cleanUp(self, task):
        self.nameLabel.removeNode()
        return task.done
                            
    
