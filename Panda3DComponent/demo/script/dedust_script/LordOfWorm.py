"""Move point appear when left mouse click into game object"""
from game.message import GameMessage
import random
from game.manager import Prototype
from direct.interval.IntervalGlobal import Wait, Sequence, Func

class Scripting():    
    def __init__(self, model):  
        self.model = model
        self.group = []
        self.model.appendInitFunc(self.init)
        self.model.setScale(0.10)
        
    def init(self, task):
        self.size = random.randint(8, 10)
        store.wormCount = self.size
        self.currentIdx = self.size
        self.group.append(self.model)
        self.generate()
        # Registry event
        for idx, part in enumerate(self.group):
            print "EVENT REGISTER FROM TAG ", self.model.getName() + str(idx) 
            self.model.registryEvent('beAttacked', self.receiveDamage, 
                                     [idx, "require info"], 
                                     self.model.getName(), self.model.getName() + str(idx))
        
        return task.done
        
    def randomAnimate(self, idx):
        monster = self.group[idx]
        anims = monster.realNP.getAnimNames()
        seq = Sequence()
        for anim in anims:            
            #seq.append(Func(monster.realNP.loop, anim))
            seq.append( Wait(random.randint(10, 30)))
            seq.append(Func(self.randomHpr, monster))
        seq.loop()     
    
    def checkAlive(self, idx):
        monster = self.group[idx]
        attributes = monster.attributes
        if attributes.hp < 0:
            return False
        return True
    
    def receiveDamage(self, idx, info):
        monster = self.group[idx]
        if info[0] == "attack":         
            if not self.checkAlive(idx):
                screen.current.removeObject(monster)
                self.group.remove(monster)
                seq = Sequence(Wait(15), self.generatePart(idx))
                seq.start()                
                
    def attackTarget(self, monster, info):
        target = info[3]     
        monster.target = target
          
            
    def calculateDamage(self, info, attributes):
        amount = info[2]
        attackType = info[1]
        true_damage = 0
        if attackType == 'physic':
            true_damage += amount - attributes.armor * 0.01 
        elif attackType == 'magic':
            true_damage += amount - attributes.resist * 0.01
        return true_damage
        
    
    def generate(self):
        print "generate monster"
        for idx in xrange(0, self.size):
            print "part %d"%idx, self.group[idx].getPos()
            self.generatePart(idx)
            
    
    def generatePart(self, idx):
        prototypeId = self.model.describeDict['Prototype']
        name = self.model.realNP.getName() + str(idx)
        model_i = entityMgr.build(Prototype.GameModel, 
                                             prototypeId, 
                                             name,
                                  Scripts="script/dedust_script/namelabel3d.py, script/dedust_script/worm.py",
                                  Tags="monster")
        
        model_i.setPos(self.randomPos())
        model_i.startScripts()
        self.group.append(model_i)
#        self.randomAnimate(idx)
        
    def processForce(self, monster, anim_name):
        if anim_name == 'walk':
            self
        
    def randomHpr(self, monster):   
        pass
    
    def createNewChild(self):
        if (store.wormCount < self.size):
            self.currentIdx += 10
            self.generatePart(self.currentIdx)
            store.wormCount +=1
    
    def randomPos(self):
        pos = self.model.getPos()
        padx = random.randint(3, 30)
        pady = random.randint(3, 30)
        pos.setX(pos.getX() + padx); pos.setY(pos.getY() + pady)
        return pos
    
    def run(self, task):
        #update event
        self.createNewChild()
        self.model.updateEvent()
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        for part in self.group:
            screen.current.removeObject(part)
        self.group = []
        return task.done