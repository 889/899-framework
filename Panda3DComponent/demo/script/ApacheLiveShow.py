class Scripting():    
    def __init__(self, model):    
        self.model = model
        print "name ", self.model.getName()
    
    def run(self, task):
        # Code here      
        self.model.setPos(self.model.getPos() + (0,-10,0))
        if self.model.getPos().y < 0:
            reset = self.model.getPos()
            reset.y = 256
            self.model.setPos(reset)  
            self.cleanUp(task)      
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        return task.done
    
#def run(model, task):    
    #Only a test
#    model.setPos(model.getPos() + (0,-10,0))
#    if model.getPos().y < 0:
#        reset = model.getPos()
#        reset.y = 256
#        model.setPos(reset)
#    return task.cont
