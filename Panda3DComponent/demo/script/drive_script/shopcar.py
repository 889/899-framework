from panda3d.core import Vec3
from game.manager import Prototype

class Scripting():
    def __init__(self, model):
        self.model = model
        self.model.appendFuncs(self.init)
        self.checkCar()
        
    def init(self, task):            
        self.checkCar()
        self.changeCar()             
        return task.done    
    
    def checkCar(self):                
        if not hasattr(store, 'selectedCar'):
            store.selectedCar = 'YellowTaxi'            
        self.selectedCar = store.selectedCar 
    
    def changeCar(self):
        print "init shop car"
        entity = entityMgr.build(Prototype.GameModel, store.selectedCar, self.model.getName())
        screen.current.replaceObject(entity, self.model)
        self.model = entity     
        self.model.setShaderAuto()
        self.selectedCar = store.selectedCar        
        if self.model.physicNode:
            self.model.physicNode.node().setGravity(0)
        
    def run(self, task):
        if self.selectedCar != store.selectedCar:
            self.changeCar()
        dt = globalClock.getDt()
        hpr = self.model.getHpr()
        hpr += Vec3(30, 0, 0) * dt
        self.model.setHpr(hpr)
        return task.cont
    
    def cleanUp(self, task):
        return task.done