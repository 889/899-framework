from game.factory import GuiFactory
from game.manager import Prototype


blockIdx = 0
blockSize = 4

def back_clicked():
    global blockIdx    
    blockIdx = 0
    store.save()
    mapMgr.goToMap('MainScreen')
    
def clearCars():
    gui = screen.current.gui
    frame = gui.getGui('choosecar_frame')
    for child in frame.getChildren():
        gui.removeGui(child.getName())
    
def selectCar(selected):    
    store.selectedCar = selected
    print "selected", store.selectedCar
    
def addCars():
    global blockIdx, blockSize
    # Load cars data then add dynamic gui
    cars = {"Aventador" : ["image/drive_image/cars/aventador/aventador", "aventador"],
            "YellowTaxi" : ["image/drive_image/cars/yellow_taxi/avatar", "avatar"],
            "BatMobil" : ["image/drive_image/cars/BatMobil/batMobil", "42424294"]
            }
    
    gui = screen.current.gui
    loadSize = blockSize   
    if blockIdx * blockSize > len(cars):
        blockIdx -= 1 
    elif blockIdx < 0:
        blockIdx = 0
    if ((blockIdx * blockSize) + blockSize) > len(cars):
        loadSize = len(cars) - (blockIdx * blockSize)
    start_pos = blockIdx * blockSize
    end_pos = start_pos + loadSize
    idx = 0
    for name, images in cars.items()[start_pos:end_pos]:
        data = {"Direct": {
                   # Data here
                   name: {
                        "type" : "DirectButton",
                        "relief" : None,
                        "geom" : {
                            "MapPath": images[0],
                            "ImageNames": [images[1]]
                        },
                        "geom_scale" : [0.4, 1, 0.4],
                        "pos": [-0.75 + idx * 0.5, 0, -0.5],
                        "command": selectCar,
                        "extraArgs": [name],
                        "parent" : "choosecar_frame"
                    }
                }}
        idx += 1    
        GuiFactory.product(gui=gui, data=data)
    
def next_clicked():
    global blockIdx
    blockIdx +=1
    clearCars()
    addCars()

def previous_clicked():
    global blockIdx
    blockIdx -= 1
    clearCars()
    addCars()
    

def choosecar_frame_init(task):
    addCars()    
    return task.done