from direct.interval.LerpInterval import LerpPosInterval
import sys
from game.factory import GuiFactory
from panda3d.core import TransparencyAttrib

def init_buttons_frame(task):    
    # Load store variable   
    store.load()
    # Do animation
    gui = screen.current.gui
    buttons = gui.getGui('buttons_frame')
    dest_pos = buttons.getPos()
    start_pos = dest_pos + ( 1, 0 ,0)
    appear = LerpPosInterval(buttons, 0.4, dest_pos, start_pos)    
    appear.start()    
    return task.done

def newgame_clicked():
    mapMgr.goToMap('AdventureMode')

def quit_clicked():
    sys.exit(0)
    
def shop_clicked():
    mapMgr.goToMap('ShopScreen')
    
def options_clicked():
    gui = screen.current.gui
    GuiFactory.product(gui=gui, filename="map/drive_game/options_dialog.json")    
    options_dialog = gui.getGui('options_dialog')
    options_dialog.setTransparency(TransparencyAttrib.MAlpha)
    options_dialog.show()
    gui.startGuiScripts('options_dialog')
    