from panda3d.core import Vec3
from utility.shortcut import getForward

class Scripting():
    def __init__(self, model):
        self.camera = model
        self.direction = -1
        self.back = -5
        self.height = 2
        self.lookHeight = 2
        
    
    def run(self, task):   
        if hasattr(store, 'car'):    
#             self.target = store.car
#             forward = getForward(self.target.nodePath)
#             forward *= -15
#             targetPos = self.target.getPos() + forward + Vec3(0, 0, 2)            
#             self.camera.setPos(targetPos)
#             base.camera.lookAt(self.target.getPos()+ Vec3(0, 0, 2))
            
            dt = globalClock.getDt()            
            self.target = store.car          
            
            #forward vector not meaning forward. It's define where camera position vs self.target
            vecForward = base.render.getRelativeVector(self.target.physicNode, (0,self.direction,0))                        
            vecForward *= -self.back
            vecForward += Vec3(0, 0, self.height)
            prevPos = self.camera.getPos()
            
            pos = self.target.getPos() + vecForward     #current position
            disVector = self.target.getPos() - self.camera.getPos()
            distance = disVector.length()
            #better 12 < distance < 15
            if distance > 12:
                dt = 15.0/distance
                           
            p = prevPos * dt                 # newCamPos = prePos * (1-dt) + currentPos * (dt)
            p += pos * ( 1 - dt)                           #What the hell is it ?
                
            self.camera.setPos(p)
            
            lookPos = self.target.getPos()
            lookPos.setZ(lookPos.getZ() + self.lookHeight)
            self.camera.lookAt(lookPos)
        return task.cont
    
    def cleanUp(self, task):
        return task.done