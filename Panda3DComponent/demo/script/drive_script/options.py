def setCheckButtonValue(name, value):
    gui = screen.current.gui
    btn = gui.getGui(name)
    btn["indicatorValue"] = value
    btn.setIndicatorValue()
    
def setSliderValue(name, value):
    gui = screen.current.gui
    slider = gui.getGui(name)
    slider["value"] = value    
 
def checkMissingOption():
    if not hasattr(store, 'fullscreen'):
        store.fullscreen = True
    if not hasattr(store, 'sfxeffect'):
        store.sfxeffect = True
    if not hasattr(store, 'antialiasing'):
        store.antialiasing = True
    if not hasattr(store, 'sound_volume'):
        store.sound_volume = 75
    if not hasattr(store, 'music_volume'):
        store.music_volume = 75
    if not hasattr(store, 'graphic'):
        store.graphic = "medium"
        
def setRadioGroup(*radio_name):
    buttons = []
    gui = screen.current.gui
    for name in radio_name:
        buttons.append( gui.getGui(name) )
        
    for button in buttons:
        button.setOthers(buttons)
        if store.graphic == button['value']:
            button.check()
        else:
            button.uncheck()

def options_init(task):
    store.load()        
    checkMissingOption()        
    setCheckButtonValue('fullscreen', store.fullscreen)
    setCheckButtonValue('sfxeffect', store.sfxeffect)
    setCheckButtonValue('antialiasing', store.antialiasing)    
    setSliderValue('sound_volume', store.sound_volume)
    setSliderValue('music_volume', store.music_volume)
    
    setRadioGroup("graphic_high", "graphic_medium", "graphic_low")
    
    return task.done

def ok_clicked():
    gui = screen.current.gui
    store.fullscreen = gui.getGui('fullscreen')["indicatorValue"]
    store.sfxeffect = gui.getGui('sfxeffect')["indicatorValue"]
    store.antialiasing = gui.getGui('antialiasing')["indicatorValue"]
    
    global filepath
    store.save()
    gui = screen.current.gui
    dialog = gui.getGui('options_dialog')
    dialog.cleanup()
    gui.removeGui("options_dialog")
    
def sound_slide():
    gui = screen.current.gui
    sound_volume = gui.getGui('sound_volume')
    store.sound_volume = sound_volume['value']
    
def music_slide():
    gui = screen.current.gui
    music_volume = gui.getGui('music_volume')
    store.music_volume = music_volume['value']
    
def graphic_switch(value):
    store.graphic = value