from panda3d.core import Vec3
from game.manager import Prototype
from direct.fsm.FSM import FSM
from direct.interval.IntervalGlobal import Sequence, Wait, Func
from panda3d.core import FilterProperties

class Scripting(FSM):
    def __init__(self, model):
        FSM.__init__(self, "RacerCar")
        self.model = model
        self.model.appendInitFunc(self.init)
        self.checkCar()
        self.gears = [0, 100, 300, 600, 900, 1500, 2600, 100000000]
        self.timeUpdateSound = 0
        
    def init(self, task): 
        self.checkCar()  
        self.changeCar()             
        
        road = screen.current.getObject('Road')
        road.setShaderAuto()     
        start_pos = road.find("**/start_point").getPos() + Vec3(0, 0, 2.5)
        self.model.setPos(start_pos)
        store.car = self.model     
        
        self.soundFX = loader.loadSfx("SFX/CarEngine.wav")
        self.soundFX.setLoop(True)
        self.soundFX.play()
        
        
        self.soundFilter = FilterProperties()
        self.soundFilter.addPitchshift(0.5, 0, 0)
        
        
        # tuning = self.model.vehicle.getTuning()
        
        # tuning.setMaxSuspensionTravelCm (40)
        # tuning.setSuspensionCompression (4.4)
        # tuning.setSuspensionDamping (2.3)
        # tuning.setSuspensionStiffness (5)
        self.request('Ready')
        
        Wait(3)
        s = Sequence ( Wait(3),
                   Func(self.request, 'Start'))
        s.start()
        return task.done
    
    def checkCar(self):               
        if not hasattr(store, 'selectedCar'):
            store.selectedCar = 'Aventador'    
            print "I dont know"        
        self.selectedCar = store.selectedCar 
    
    def changeCar(self):
        print "init shop car"
        entity = entityMgr.build(Prototype.GameModel, store.selectedCar, self.model.getName())
        entity.tags = self.model.tags
        entity.setHpr(self.model.getHpr())
        screen.current.replaceObject(entity, self.model)           
        self.model = entity          
        self.model.setShaderAuto()
        self.selectedCar = store.selectedCar
        print self.model.tags
        
    def checkInput(self):
        pass        
        
        
    def run(self, task):
        if self.selectedCar != store.selectedCar:
            self.changeCar()
            
            
        self.model.updateEvent()
        self.model.updateVehicle()   
                         
        
        speed = self.model.vehicle.getCurrentSpeedKmHour()
        if speed > 0 and self.timeUpdateSound > 0.2:
            gear = self.findGear(speed)
            minVelocity = self.gears[gear - 1]
            maxVelocity = self.gears[gear]
            print "Min velocity", minVelocity
            print "Max velocity", maxVelocity
            pitch = (speed - minVelocity) / (maxVelocity + 1) + speed / 1000 + 0.05 # 0.2 is base sound
            pitch = min(pitch, 0.7)
            soundFilter = FilterProperties()
            soundFilter.addPitchshift(pitch, 0, 0)
            self.timeUpdateSound = 0            
            base.sfxManagerList[0].configureFilters(soundFilter)
#            self.soundFX.setVolume(pitch)
        
        self.timeUpdateSound += globalClock.getDt()
        return task.cont
    
    def findGear(self, speed):
        for i in range (0, 9):
            if speed < self.gears[i]:
                return i
    
    def cleanUp(self, task):
        return task.done
    
    def roga(self):        
        print "roga"
        
    def moveForward(self):
        self.model.go()
        
    def moveBack(self):
        self.model.back()
        
    def turnLeft(self):
        self.model.turnLeft()
        
    def turnRight(self):
        self.model.turnRight()
    
    def enterReady(self):
        self.model.registryEvent( "w", self.roga, None, "w")
        
    def notMove(self):
        self.model.slide()
        
    def notTurn(self):
        self.model.notTurn()
        
    
    def exitReady(self):
        pass
    
    def enterStart(self):
        self.model.registryEvent( "w", self.moveForward, None, "w")
        self.model.registryEvent( "s", self.moveBack, None, "s")
        self.model.registryEvent( "a", self.turnLeft, None, "a")
        self.model.registryEvent( "d", self.turnRight, None, "d")
        
        self.model.registryEvent( "w-up", self.notMove, None, "w-up")
        self.model.registryEvent( "s-up", self.notMove, None, "s-up")
        self.model.registryEvent( "a-up", self.notTurn, None, "a-up")
        self.model.registryEvent( "d-up", self.notTurn, None, "d-up")
    
    def exitStart(self):
        pass
    
    def enterFinish(self):
        pass
    
    def exitFinish(self):
        pass