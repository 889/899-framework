from game.camera import CameraBase
from game.builder import GameModelBuilder
from game.map import Map
from game.manager import GameEntityManager
from game import MessageDispatcher
import math
from panda3d.core import *


class Scripting():    
    def __init__(self, model):    
        self.model = model
        self.speed = 3
        self.rotate = 1
        self.direction  = {'forward': False, 'backward': False, 'right': False, 'left': False}
        self.child = 0
        
        self.force = 2200 # Power of engine
        self.mass = 100 # Mass
        self.vehicle =  0 
        self.accelerator = 0
        self.lastTime = globalClock.getDt()
        self.model.physicNode.setHpr(180,0,0)
        self.model.realNP.setHpr(180,0,0)
        if self.model.physicNode:
            node = self.model.physicNode.node()
            node.setFriction(5)
            node.setRestitution(1) 
        
        
    def setDirection(self, message, key, direction, active):        
        if message.fromTag == "System" and key in message.info :            
                self.direction[direction] = active
                
    def doMoving(self):
        vecForward = base.render.getRelativeVector(self.model.physicNode,(0,1,0))
        vecRight = base.render.getRelativeVector(self.model.physicNode,(1,0,0))
        vecUp = base.render.getRelativeVector(self.model.physicNode,(0,0,1))
        physicNP = self.model.physicNode
        force = Vec3(0, 0, 0)
        
            
        if self.direction['forward'] == True:            
#            self.model.setPos(self.model.getPos() + (vecForward * self.speed))
            self.accelerator =  self.force * 1.0/self.mass # Base on F = m * a  => a = F/m
            self.vehicle = self.vehicle + self.accelerator * globalClock.getDt()
            
            vecForward *= self.vehicle
            vecForward.setZ(0)
#            force = vecForward
            physicNP.node().setLinearVelocity(vecForward)
            physicNP.setPos(self.model.getPos())
            
        if self.direction['backward'] == True:                        
            self.accelerator =  self.force * 1.0/self.mass # Base on F = m * a  => a = F/m
            self.vehicle = self.vehicle - self.accelerator * globalClock.getDt()
            
            vecForward *= self.vehicle
            vecForward.setZ(0)
            physicNP.node().setLinearVelocity(vecForward)
            physicNP.setPos(self.model.getPos())
            
        if self.direction['right'] == True:
            self.model.setH(self.model.getH() - self.rotate)
            
        if self.direction['left'] == True:
            self.model.setH(self.model.getH() + self.rotate)
        
        if math.fabs(self.vehicle) > 0.001:
            self.vehicle -= globalClock.getDt() * self.vehicle
            
        
        physicNP.node().setLinearVelocity(vecUp * 10)
        physicNP.setPos(self.model.getPos())
        print "bring it up"
        
    def run(self, task):
        if CameraBase.current:
            CameraBase.current.target = self.model
            CameraBase.current.height = 0.4
            CameraBase.current.back = 25
        print self.model.getPos(), "Vehicle = ", self.vehicle, "Accelerator = ", self.accelerator
#        base.disableMouse()
            
            
        while(len(self.model.eventQueue) > 0):
#            print "EVENT QUEUE: ", self.model.eventQueue
            message = self.model.eventQueue[0]     
                       
            self.setDirection(message, "w", 'forward', True)
            self.setDirection(message, "w-up", 'forward', False)
            self.setDirection(message, "s", 'backward', True)
            self.setDirection(message, "s-up", 'backward', False)
            self.setDirection(message, "a", 'left', True)
            self.setDirection(message, "a-up", 'left', False)
            self.setDirection(message, "d", 'right', True)
            self.setDirection(message, "d-up", 'right', False)            
            
            print message.info
            if message.fromTag == "System" and 'Left Click' in message.info :   
                print "no click chuot ne` anh"
                goBuilder = GameModelBuilder()
                print "Game Entity Manager ", GameEntityManager.prototypeList
                HPR = str(self.model.getHpr().x) + ',' + str(self.model.getHpr().y) + ',' + str(self.model.getHpr().z)
                POS = str(self.model.getPos().x) + ',' + str(self.model.getPos().y) + ',' + str(self.model.getPos().z + 3)
                Data = {'Name': 'T21222' + str(self.child), 'objectID': 'Bullet_01', 'Tags': 'w, w-up, s, s-up, a, a-up, d, d-up, Left Click', 'Hpr': HPR, 'Pos': POS, 'Scripts': 'script/CloneGEventT90.py, script/SEventT90.py', 'Prototype': 'Bullet_01', 'HaveId': 'True'}
                myBullet = goBuilder.buildGameObject(Data, GameEntityManager.prototypeList, Map.current.nodeRoot, Map.current.world)
                        
                Map.current.gameObjects['GameModels'][Data['Name']] = myBullet
                Map.dispatcher.addObject(myBullet)
                myBullet.startScripts()
                
#                Map.current.rejectFromScene()
#                Map.current.applyToScene(render)
                print "MY TANK CLONE ", myBullet
                
                print "THIS HPR : ", self.model.getHpr(), "X ", self.model.getHpr().x, self.model.getHpr().y, self.model.getHpr().z
                
                self.child += 1
            
            self.model.eventQueue.remove(message)
        
        self.doMoving()
        
        
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        return task.done
