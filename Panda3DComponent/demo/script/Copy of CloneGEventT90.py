from game.camera import CameraBase
from game.builder import GameModelBuilder
from game.map import Map
from game.manager import GameEntityManager
from game import MessageDispatcher
class Scripting():    
    def __init__(self, model):    
        self.model = model
        self.speed = 10
        self.rotate = 1
        self.direction  = {'forward': False, 'backward': False, 'right': False, 'left': False}
        
    def setDirection(self, message, key, direction, active):        
        if message.fromTag == "System" and key in message.info :            
                self.direction[direction] = active
                
    def doMoving(self):
        vecForward = base.render.getRelativeVector(self.model.physicNode,(0,1,0))
        vecRight = base.render.getRelativeVector(self.model.physicNode,(1,0,0))
        node = self.model.physicNode.node()
        
        if self.direction['forward'] == True:            
            #self.model.setPos(self.model.getPos() + (vecForward * self.speed))
            vecForward *= self.speed
            vecForward.setZ(0)
            self.model.physicNode.node().setLinearVelocity(vecForward)
            #self.model.physicNode.node().setLinearDamping(0.2)
            self.model.physicNode.setPos(self.model.getPos())
            
        if self.direction['backward'] == True:                        
            vecForward *= -self.speed
            vecForward.setZ(0)
            self.model.physicNode.node().setLinearVelocity(vecForward)
            self.model.physicNode.setPos(self.model.getPos())
        
        if self.direction['right'] == True:
            self.model.setH(self.model.getH() - self.rotate)
            
        if self.direction['left'] == True:
            self.model.setH(self.model.getH() + self.rotate)
        
    def run(self, task):
#        base.disableMouse()
            
        while(len(self.model.eventQueue) > 0):
            print "EVENT QUEUE: ", self.model.eventQueue
            message = self.model.eventQueue[0]     
                       
            self.setDirection(message, "w", 'forward', True)
            self.setDirection(message, "w-up", 'forward', False)
            self.setDirection(message, "s", 'backward', True)
            self.setDirection(message, "s-up", 'backward', False)
            self.setDirection(message, "a", 'left', True)
            self.setDirection(message, "a-up", 'left', False)
            self.setDirection(message, "d", 'right', True)
            self.setDirection(message, "d-up", 'right', False)            
            
            self.model.eventQueue.remove(message)
        
        self.doMoving()
        
        
        return task.cont
    
    def cleanUp(self, task):
        # Code here
        return task.done
