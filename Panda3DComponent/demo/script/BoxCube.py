from game.camera import CameraBase
from game.builder import GameModelBuilder
from game.map import Map
from game.manager import GameEntityManager
from game.message import MessageDispatcher
from panda3d.core import NodePath, Vec3


import sys
import direct.directbase.DirectStart

from direct.showbase.DirectObject import DirectObject
from direct.showbase.InputStateGlobal import inputState

from panda3d.core import AmbientLight
from panda3d.core import DirectionalLight
from panda3d.core import Vec3
from panda3d.core import Vec4
from panda3d.core import Point3
from panda3d.core import TransformState
from panda3d.core import BitMask32

import math
from panda3d.core import *

class Scripting():    
    def __init__(self, model):    
        self.model = model
#        self.model.setCompass()
#        self.model.nodePath.setShaderOff()
#        rig = NodePath('rig')
#        buffer = base.win.makeCubeMap("env", 256, rig)
#        lens = rig.find('**/+Camera').node().getLens()
#        lens.setNearFar(1, 10000)
##        tex = buffer.getTexture()
#        rig.reparentTo(self.model.nodePath)
#        self.model.setTexGen(TextureStage.getDefault(), TexGenAttrib.MEyeSphereMap)
#        self.model.setTexture(buffer.getTexture())
        
        myMaterial = Material()
        myMaterial.setAmbient(Vec4(0,0,0,0))
        myMaterial.setDiffuse(Vec4(0.5,0.5,0.5,1))
        myMaterial.setEmission(Vec4(0.5,0.5,0.5,1))
        myMaterial.setShininess(5.0)
        myMaterial.setSpecular(Vec4(0.5,0.5,0.5,1))
        self.model.nodePath.setMaterial(myMaterial)
    
    def run(self, task):
        pass
    
    def cleanUp(self, task):
        # Code here
        return task.done
    
#def run(model, task):    
    #Only a test
#    model.setPos(model.getPos() + (0,-10,0))
#    if model.getPos().y < 0:
#        reset = model.getPos()
#        reset.y = 256
#        model.setPos(reset)
#    return task.cont
