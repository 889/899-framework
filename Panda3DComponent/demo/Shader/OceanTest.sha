//Cg
//Cg profile arbvp1 arbfp1

// shader loosely based on pro-rsoft's from the panda3d forums
// shader assumes 6 texture stages:
// 0-2 textures
// 3-5 alpha maps
// vertex shader accepts unit 0 and 3 texture coords as input 
// and outputs unit 0 scaled and unit 3 unchanged
// fragment shader uses unit 0 coordinates to access textures
// and unit 3 coordinates (un-scaled) to access alpha maps
// gsk, june-2008

void vshader( in float4 vtx_position : POSITION,
	      in float3 vtx_normal : NORMAL,
              in float2 vtx_texcoord0 : TEXCOORD0,
              in float2 vtx_texcoord3 : TEXCOORD3,
              in uniform float4x4 mat_modelproj,
	      in uniform float4x4 trans_model_to_world,
	      in uniform float4 k_lightvec,
	      in uniform float4 k_lightcolor,
	      in uniform float4 k_ambientlight,
	      in uniform float4 k_time,
	      in uniform float4 k_tscale,
	      in uniform float3 k_CamPos, 
	      in uniform float k_zscale,
	      out float4 l_brightness,
	      out float4 l_lightcolor,
	      out float4 l_mpos,
              out float2 l_texcoord0 : TEXCOORD0,
              out float2 l_texcoord3 : TEXCOORD3,
              out float4 l_position : POSITION)
{

  // worldspace position, for clipping in the fragment shader
  //l_mpos = mul(trans_model_to_world, vtx_position);
  float4 position = vtx_position;
  float A1 = 1;
  float A2 = 1;
  float A3 = 1;
  float F1 = 2;
  float F2 = 1;
  float F3 = 6;
  float3 Emiter1Pos = (300, 300, 0);
  float3 Emiter2Pos = (-300, 300, 0);
  float3 Emiter3Pos = (-100, 500, 0);
  Emiter1Pos += k_CamPos;
  Emiter2Pos += k_CamPos;
  Emiter3Pos += k_CamPos;
  float lamda1 = 30;
  float lamda2 = 40;
  float lamda3 = 20;
  
  float D1 = length(Emiter1Pos - position.xzy);
  float D2 = length(Emiter2Pos - position.xyz);
  float D3 = length(Emiter3Pos - position.xyz);
  
  position.z = A1 * sin(6.28*(F1*k_time + D1/lamda1));
  position.z += A2 * sin(6.28*(F2*k_time + D2/lamda2));
  position.z += A3 * sin(6.28*(F3*k_time + D3/lamda3));
  
  //position.z = 2 * sin(30 * k_time + 60 * position.x);
  
  l_position=mul(mat_modelproj,position);
  l_texcoord0=vtx_texcoord0*k_tscale;
  l_texcoord3=vtx_texcoord3;
  vtx_normal.z /= k_zscale;

  // lighting
  float3 N = normalize( vtx_normal );
  float3 L = normalize( k_lightvec.xyz );
  l_brightness = (max( dot( -N, L ), 0.0f )*k_lightcolor)+k_ambientlight;


}

void fshader( in float4 l_position : POSITION,
              in float2 l_texcoord0 : TEXCOORD0,
              in float2 l_texcoord3 : TEXCOORD3,
	      in float4  l_brightness,
	      in float4 l_lightcolor,
	      in float4 l_mpos,
	      in uniform float4 k_waterlevel,
              in uniform sampler2D tex_0 : TEXUNIT0,
              in uniform sampler2D tex_1 : TEXUNIT1,
              in uniform sampler2D tex_2 : TEXUNIT2,
              in uniform sampler2D tex_3 : TEXUNIT3,
              in uniform sampler2D tex_4 : TEXUNIT4,
              in uniform sampler2D tex_5 : TEXUNIT5,
              out float4 o_color : COLOR )
{
  

  // alpha splatting and lighting
  float4 tex1=tex2D(tex_0,l_texcoord0);
  float4 tex2=tex2D(tex_1,l_texcoord0);
  float4 tex3=tex2D(tex_2,l_texcoord0);
  float alpha1=tex2D(tex_3,l_texcoord3).z;
  float alpha2=tex2D(tex_4,l_texcoord3).z;
  float alpha3=tex2D(tex_5,l_texcoord3).z;
  o_color =tex1*alpha1;
  o_color+=tex2*alpha2;
  o_color+=tex3*alpha3;
  o_color=o_color*(l_brightness);
  o_color.a=1.0;
}
