//Cg
//Cg profile arbvp1 arbfp1

void vshader( in float4 vtx_position : POSITION,
	      in float3 vtx_normal : NORMAL,
              in float2 vtx_texcoord0 : TEXCOORD0,
              in float2 vtx_texcoord3 : TEXCOORD3,
              in uniform float4x4 mat_modelproj,
              in uniform float4x4 trans_model_to_view,
              in uniform float4x4 trans_model_to_world,
    	  	  in uniform float4x4 tpose_view_to_model,  
	      in uniform float4 k_tscale,	      
	      in uniform float4 k_scale,
	      out float4 l_mpos,          
              out float2 l_texcoord0 : TEXCOORD0,
              out float2 l_texcoord3 : TEXCOORD3,
              out float3 l_normal : TEXCOORD2,
              out float4 l_shadowcoord : TEXCOORD1,
              out float4 l_pos : TEXCOORD4,
              out float4 l_position : POSITION)
{
  // worldspace position, for clipping in the fragment shader
  l_mpos = mul(trans_model_to_world, vtx_position);

  float4 position = vtx_position * k_scale;  
  l_position=mul(mat_modelproj,vtx_position);
  l_texcoord0=vtx_texcoord0*k_tscale;
  l_texcoord3=vtx_texcoord3;
  vtx_normal.z /= k_scale.z;
   
  l_normal.xyz = mul((float3x3)tpose_view_to_model, vtx_normal.xyz);
  l_pos = mul(trans_model_to_view, vtx_position);
  
}

void fshader( in float4 l_position : POSITION,
              in float2 l_texcoord0 : TEXCOORD0,
              in float2 l_texcoord3 : TEXCOORD3,
              in float3 l_normal : TEXCOORD2,
              in float4 l_shadowcoord : TEXCOORD1,	      
	      in float4 l_mpos,
	      in uniform float4 mspos_eye,
	      in uniform float k_waterlevel,
          	  in uniform float4 alight_ambient0,	
          	  in uniform float4x4 dlight_direct0_rel_view,	      	  
	      	  in uniform float4x4 attr_material,
              in uniform sampler2D tex_0 : TEXUNIT0,
              in uniform sampler2D tex_1 : TEXUNIT1,
              in uniform sampler2D tex_2 : TEXUNIT2,
              in uniform sampler2D tex_3 : TEXUNIT3,
              in uniform sampler2D tex_4 : TEXUNIT4,
              in uniform sampler2D tex_5 : TEXUNIT5,
              in uniform sampler2D k_Ldepthmap : TEXUNIT6,           	  
              out float4 o_color : COLOR )
{
  // clipping
  if ( l_mpos.z < k_waterlevel) 
  	discard;

//------------------Calculate the surface lighting factor-------------------------------------------------------------------
  float4 lightColor = dlight_direct0_rel_view[0];
  float4 lightSpecularColor= dlight_direct0_rel_view[1];
  float3 lightDirection = dlight_direct0_rel_view[2];
  float4 lightHalf = dlight_direct0_rel_view[3];
  
  // Object material
  float4 m_ambient = attr_material[0];
  float4 m_diffuse = attr_material[1];
  float4 m_emission = attr_material[2];
  float4 m_specular = attr_material[3];
  float4 m_shininess = attr_material[3].w;

  // lighting ambient
  float4 l_ambient = alight_ambient0 * m_ambient;

  // lighting diffuse
  float3 N = normalize( l_normal );
  float3 L = normalize( lightDirection.xyz );
  float diffuseLight = max(dot(-N, L), 0);
  float4 l_diffuse = diffuseLight * lightColor * m_diffuse;  

  // lighting specular  
  float3 H = normalize(lightHalf);
  float specularLight = pow(max(dot(-N, H), 0), m_shininess);

  if (diffuseLight <= 0) 
	  specularLight = 0;
  float4 l_specular = specularLight * m_specular * lightSpecularColor;

  // alpha splatting and lighting
  float4 tex1=tex2D(tex_0,l_texcoord0);
  float4 tex2=tex2D(tex_1,l_texcoord0);
  float4 tex3=tex2D(tex_2,l_texcoord0);
  float alpha1=tex2D(tex_3,l_texcoord3).z;
  float alpha2=tex2D(tex_4,l_texcoord3).z;
  float alpha3=tex2D(tex_5,l_texcoord3).z;
  o_color =tex1*alpha1;
  o_color+=tex2*alpha2;
  o_color+=tex3*alpha3;
    
  o_color=o_color*(l_diffuse  + l_specular + l_ambient);
  o_color.a=1.0;   
}
