from panda3d.core import loadPrcFileData
from demo.script.dedust_script.gui.name import createName
from demo.script.dedust_script.gui.skill import createSkillBar, createSkill,\
    setSkillShortcut
from demo.script.dedust_script.gui.messagedlg import createDialog,\
    createMissionButton
from demo.script.dedust_script.gui.missiondlg import createMissionDialog

loadPrcFileData('', 'threading-model Cull/Draw')
loadPrcFileData('', 'sync-video 0')
loadPrcFileData('', 'win-size 1024 768')
#loadPrcFileData('', 'clock-mode limited')
#loadPrcFileData('', 'clock-frame-rate 60')

#loadPrcFileData('', 'support-threads #f')

import direct.directbase.DirectStart
from panda3d.core import PStatClient
base.setFrameRateMeter(True)

import game
        
if __name__ == "__main__":
    dlg = createMissionDialog("name", "title", "content", ['kill him', 'kill yourself'],
                              ['300 gold', '500 exp'])
    
    
    run() 

