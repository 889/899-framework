import add_path
from panda3d.core import loadPrcFileData

#loadPrcFileData('', 'threading-model Cull/Draw')
loadPrcFileData('', 'sync-video 0')
loadPrcFileData("", "audio-library-name p3fmod_audio")
#loadPrcFileData('', 'support-threads #f')

import direct.directbase.DirectStart
from panda3d.core import PStatClient
base.setFrameRateMeter(True)

import game

if __name__ == "__main__":	
	
	render.setShaderAuto()
	mapMgr.loadMapListFromFile('map/drive_game/map_list.xml', 'MainScreen')    
	PStatClient.connect()
	base.accept("v", base.bufferViewer.toggleEnable)
	base.accept("V", base.bufferViewer.toggleEnable)
	
	run()
