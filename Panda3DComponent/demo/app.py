from panda3d.core import loadPrcFileData
from panda3d.core import ConfigVariableString
from config import *
from game.factory import MapManager
from utility import instruction

# Panda3d Initialize
import direct.directbase.DirectStart

# Show fps
base.setFrameRateMeter(True)

# Game Initialize
class Game():
    def __init__(self):        
        self.mm = MapManager()
        self.mm.loadMapListFromFile('map/mapList.xml')        

# Game run

g = Game()
run()