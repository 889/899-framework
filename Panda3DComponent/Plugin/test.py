import imp, os, glob

def loadPlugin(name):   
    if name != "__init__":        
        f, filename, desc = imp.find_module(name, [os.getcwd()])
        return imp.load_module(name, f, filename, desc)         
    
def loadAllPlugin():
    os.chdir("./pluginfolder")
    for files in glob.glob("*.py"):      
        name, ext = os.path.splitext(files)  
        plugin = loadPlugin(name)
        if plugin != None:
            yield plugin

# Load all Plugin
lstPlugin = loadAllPlugin()
for plugin in lstPlugin:
    #Plugin do stuff
    print plugin.calculate(2, 3)
