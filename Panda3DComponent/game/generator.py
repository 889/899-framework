"""
Generator / Shader module

"""
template_areas = ["[vertex]", "[texturetransform]",  "[extcode]", "[texture]", 
                          "[texturemode]", "[light]", "[fog]", "[shadow]", "[color]"]
input_areas = [ "[vin]", "[vout]", "[fin]", "[fout]"]

shader_template = """//Cg
//Cg profile {profile}

void vshader( in float4 vtx_position : POSITION,
              in float3 vtx_normal : NORMAL,
              in uniform float4x4 mat_modelproj,
              [vin]
              [vout]
              out float4 l_position : POSITION)
{
[vertex]
l_position = mul(mat_modelproj, vtx_position);
[texturetransform]
[extcode]
}

void fshader( [fin]
              [fout]
              out float4 o_color : COLOR)
{
[texture]
[texturemode]
[light]
[fog]
[shadow]
[color]
}
"""
from panda3d.core import DirectionalLight, AmbientLight, Spotlight

class Generator(object):
    '''Base generator class'''
    def generate(self):
        '''Base generate method'''
        pass
    
class VertexProfile():
    '''
    Vertex profile is series of profiles for vertex in cg. 
    It support for type of vertex compiler cg will use.
    '''
    DirectX8 = "vs_1_1"    
    DirectX9 = "vs_2_0"
    DirectX9X = "vs_2_x"
    
    OpenGl2 = "vp20"
    OpenGl3 = "vp30"
    OpenGl4 = "vp40"
    
    ARB = "arbvp1"
    
class FragmentProfile():
    '''
    Fragment profile is series of profiles for fragment in cg. 
    It support for type of fragment compiler cg will use.
    '''
    DirectX8_1 = "ps_1_1"
    DirectX8_2 = "ps_1_2"
    DirectX8_3 = "ps_1_3"
    DirectX9 = "ps_2_0"
    DirectX9X = "ps_2_x"
    
    OpenGl_2 = "fp20"
    OpenGl_3 = "fp30"
    OpenGl_4 = "fp40"
    
    ARB = "arbfp1"
    
class VarType():
    '''Type of data using in cg'''
    float = "float"
    float2 = "float2"
    float3 = "float3"
    float4 = "float4"
    float3x3 = "float3x3"
    float4x4 = "float4x4"    
    sampler2D = "sampler2D"

class Semantic():
    '''Sematic of data using in cg'''
    position = "POSITION"
    color = "COLOR"
    texcoord = "TEXCOORD{0}"
    texunit = "TEXUNIT{0}"
    
    @staticmethod
    def getTexCoord(idx):
        return Semantic.texcoord.format(idx)
    
    @staticmethod
    def getTexUnit(idx):
        return Semantic.texunit.format(idx)
    
class Var():
    '''Variable in code in cg'''
    def __init__(self, var_type, var_name, var_user=False, var_semantic=None):
        self.type = var_type
        self.name = var_name
        self.user = var_user
        self.semantic = var_semantic
        
    
class ShaderProfile():
    '''Profile compiler type for cg using when compiling.
    '''
    def __init__(self, vshader, fshader):
        self.vshader = vshader
        self.fshader = fshader
        
    def getString(self):
        return self.vshader + " " + self.fshader
    
class ShaderGenerator(Generator):
    '''
    Generator for shader in cg. It is a part of GameObject. It can be loaded from file or string.
    It parse raw string data readed to list of specific data (fog, lights, ts, tex, ...)
    Then shader generator using these list for set shader each frame at first time and
    at the time something new inserted to these list.  
    '''
    def __init__(self, gameObject):
        '''
        Store gameObject to check new value of gameObject for update shade.
        '''
        self.gameObject = gameObject
        self.inputs = []
        self.lights = []
        self.fog = None
        self.ts = []
        self.texs = []
        self.profile = ShaderProfile(VertexProfile.ARB, FragmentProfile.ARB)        
        self.vIn = []
        self.fIn = []
        self.vOut = []        
        self.fOut = []
        self.vTof = []
        
            
    def setProfile(self, profile):
        '''
        Change new profile for cg shader.
        '''
        self.profile = profile
    
    def processTexture(self):
        idx = 0
        for tex in self.texs:     
            # Create parameters       
            # Texture coordinate
            vin_texcoord = Var(VarType.float2, "vtx_texcoord{0}".format(idx), 
                           False, Semantic.getTexCoord(idx))
            self.vIn.append(vin_texcoord)
            
            vout_texcoord = Var(VarType.float2, "l_texcoord{0}".format(idx), 
                           False, Semantic.getTexCoord(idx))
            self.vOut.append(vout_texcoord)
            self.vTof.append(vout_texcoord)
            # Texture sampler
            fin_sampler = Var(VarType.sampler2D, "tex_{0}".format(idx), 
                           False, Semantic.getTexUnit(idx))
            self.fIn.append(fin_sampler)            
            idx += 1
    
    def processTextureStage(self):
        pass        
    
    def processLight(self):
        for light in self.lights:
            # Create parameters
            if isinstance(light.node(), DirectionalLight):
                default_name = "dlight_{0}_rel_world"
                name = default_name.format(light.getName()) 
                var = Var(VarType.float4x4, name)
                self.fIn.append(var)
            elif isinstance(light.node(), AmbientLight):
                default_name = "alight_{0}"
                name = default_name.format(light.getName()) 
                var = Var(VarType.float4, name)
                self.vIn.append(var)
    
    def processFog(self):
        if self.fog:       
            # Create parameters     
            fog = Var(VarType.float4, "attr_fog")
            fog_color = Var(VarType.float4, "attr_fogcolor")
            self.fIn.append(fog)
            self.fIn.append(fog_color)
            
    def addCodeReplaceArea(self, area, code):
        shader_template.replace(area, code)
        
    def addCodeRemainArea(self, area, code):
        shader_template.replace(area, code + "\n" + area)
            
    
    def cleanTemplate(self):
        for area in input_areas:
            shader_template.replace(area, "")
        for area in template_areas:
            shader_template.replace(area, "")
            
    def generate(self):
        self.update()
        # Clone template
        template = shader_template
        # Process vshader
        for var in self.vIn:
            if var.user:
                code = "in uniform " + var.type + " k_" + var.name
            code = "in uniform " + var.type + " " + var.name
            if var.semantic:
                code += " :" + var.semantic
            code += ",\n"
            self.addCodeRemainAreaArea("vin", code)
        # Process fshader
        
        print template
    
    def setShadow(self, active=True):
        pass
    
    def update(self):
        self.updateFrom(self.gameObject)
    
    def updateFrom(self, game_obj):
        self.gameObject = game_obj
        # Update shader input
        self.inputs = game_obj.shaderInputs
        # Update lights
        lights = Map.current.getLights()
        for light in lights:
            if game_obj.hasLight(light.nodePath):
                self.lights.append(light)
        # Update fog
        fogs = Map.current.getFogs()
        for fog in fogs:
            if game_obj.hasFog(fog.nodePath):
                self.fog = fog
                break
        # Update texture
        self.texs = game_obj.texs
        # Update texture stage
        self.ts = game_obj.ts
        
    def __loadFromString(self, string):
        raw_shader = string
        
    def __loadFromFile(self, filePath):
        fp = open(data, 'r')
        raw_shader = fp.read()  
        
    def load(self, data, mode='file'):
        '''
        Load exit shader file, then parse string from file to value of shader generator.
        '''        
        if mode=='file':
            self.__loadFromFile(data)
        elif mode=='string':
            self.__loadFromString(data)
 
import copy

cg = """
//Cg
/* lesson12.sha */

/*
Cg does not offers lot of high level constructs like classes in C++. It is more
like C. Functions are therefore possible, and are used in this example.
*/

/*
Like in the previous shader we do all our work in the pixel shader, therefore we
do not have to add anything to our vertex shader.
*/
void vshader(
    uniform float4x4 mat_modelproj,
    in float4 vtx_position : POSITION,
    in float3 vtx_normal : NORMAL,
    in float4 vtx_color : COLOR,
    out float4 l_color : COLOR,
    out float3 l_myposition : TEXCOORD0,
    out float3 l_mynormal : TEXCOORD1,
    out float4 l_position : POSITION)
{
    l_position = mul(mat_modelproj, vtx_position);

    l_myposition = vtx_position.xyz;
    l_mynormal = normalize(vtx_normal);

    l_color = vtx_color;
}

float lit(float3 lightposition, float3 modelposition, float3 normal)
{
    float3 direction = lightposition - modelposition;
    float distance = length(direction);
    float diffuse = saturate(dot(normalize(normal), normalize(direction)));
    /*
    Normally you would define the following constants in Python source and pass
    them this shader. We are a bit lazy here and hard code them in this
    function.
    */
    float a = 0.0;
    float b = 0.0;
    float c = 1.0;
    /*
    DIRTY
    If you like to achieve the same results as the fixed function pipeline you
    should add a saturate here. But I think, that it looks quite nice, if you
    get extremely bright spots when a light source is near a face, even is the
    face is dark.
    */
    float attenuation = 1.0 / (a + b * distance + c * distance * distance);
    return attenuation * diffuse;
}

/*
Because we have extended the lighting equations in a separate function, the
fragment shader looks clean. It would be simple to add more lights. The only
limits here is that every GPU only supports a limited number of uniforms and
shader instructions. So you maybe cannot add hundreds of lights, besides that it
takes an endless amount of time to do per pixel lighting with hundred lights.
*/
void fshader(
    uniform float4 mspos_light, #loop Light
    in float3 l_myposition : TEXCOORD0, 
    in float3 l_mynormal : TEXCOORD1,
    in float4 l_color : COLOR,
    out float4 o_color : COLOR)
{
    float brightness = 0.0;
    [# Light]
    brightness += lit(mspos_light.xyz, l_myposition, l_mynormal);
    [#]
    o_color = l_color * brightness;
}
"""
class DynamicShader(object):

    def __parseParameter(self, string):
        string = string.strip()
        parts = string.split(':')
        # Parse semantic
        semantic = None
        if len(parts) > 1:
            semantic = parts[1].strip()
        # Parse parameter fields
        var_field = parts[0]
        fields = var_field.split(' ')        
        for idx, field in enumerate(fields):
            fields[idx] = field.strip()
        # Combine parameter fields and sematic
        fields.append(semantic)

        return fields

    def __getSub(self, string, sub, start=0):    
        start_index = string.find(sub, start)
        end_index = start_index + len(sub)
        return start_index, end_index

    def __getSubs(self, string, sub):
        subs = []
        # start searching index
        start_index = 0    
        while True:        
            start, end = self.__getSub(string, sub, start_index)        
            if not start > -1:        
                break        
            subs.append( [start, end] )
            start_index = end
        return subs

    def getSpecificVars(self, string, pattern):
        vars = {}
        vars_pos = self.__getSubs(string, pattern)
        for var_pos in vars_pos:            
            start, end = var_pos[0], var_pos[1]
            end_line = string.find('\n', end)
            # Variable type
            var_info = string[end:end_line].strip()            
            # Get param fields list
            param_end = string.rfind(',', 0, start)
            param_start = string.rfind('\n', 0, param_end) + 1
            param = string[param_start:param_end]
            param = self.__parseParameter(param)
            param_name = param[2]
            vars[param_name] = {'param':param, 
                                'info':var_info,
                                'range':[param_start, end_line]
                                }            
        return vars

    def getVars(self, string):        
        loops = self.getSpecificVars(string, "#loop")
        dynamics = self.getSpecificVars(string, "#dynamic")
        return loops, dynamics
        
    def getLoops(self, string):
        loops = {}
        start_loops = self.__getSubs(string, "[# ")
        for start_loop in start_loops:            
            start_name_index = start_loop[1]
            end_name_index, start_code_index = self.__getSub(string, ']', start_name_index)
            loop_name = string[start_name_index:end_name_index].strip()
            end_code, end_loop = self.__getSub(string, '[#]', start_code_index)
            end_code_index = end_code - 1
            loops[loop_name] = {'code':string[start_code_index:end_code_index],
                                'range': [start_loop[0], end_loop],
                                'vars': {}
                                }
        return loops        


    def match(self, loops, vars):
        for name, var in vars.items():
            loop_name = var['info']
            if loops.has_key(loop_name):
                loops[loop_name]['vars'][name] = var
        return loops

    def duplicate(self, loops, name, nTime):
        result = {}
        for idx in xrange(0, nTime):            
            loop_name = name + str(idx)
            result[loop_name] = copy.deepcopy(loops[name])
            vars = result[loop_name]['vars']
            for var_name, var in vars.items():
                # Replace var name
                vars_field = result[loop_name]['vars']
                vars_field.pop(var_name)
                var_name += str(idx)                                
                vars_field[var_name] = var
        return result

    def __renameCode(self, code, var_name, index=""):
        subs = self.__getSub(code, var_name)
        for sub in subs:
            start, end = sub[0], sub[1]
            code[start:end] = var_name + str(index)

    def __createParamString(self, var, var_name, index=""):
        new_var = copy.deepcopy(var)
        param = new_var['param']
        info = new_var['info']
        if len(info) > 0:
            info = " :" + info
        param[2] = var_name + str(index)
        return " ".join(param) + info + ",\n"


    def applyLoop(self, string, loops, name, nTime):
        loop = loops[name]
        for idx in xrange(0, nTime):                    
            code = loop['code']    
            vars = loop['vars']
            for var_name, var in vars.items():
                self.__renameCode(code, var_name, idx)
                string = self.__createParamString(var, var_name, idx)
        



if __name__ == "__main__":
    print "Start testing module..."
    shader = DynamicShader()

    vars = shader.getVars(cg)
    loops = shader.getLoops(cg)
    match_loops = shader.match(loops, vars[0])
    print "duplicate:"
    duplicate_loops = shader.duplicate(match_loops, 'Light', 5)
    print "loops:"
    for name, value in match_loops.items():
        print "..." + name + "..."
        print "code:", value['code']
        print "vars:", value['vars']     
    
    print "Finish testing module!"   