from panda3d.core import *
from direct.particles.Particles import Particles
from direct.particles.ParticleEffect import ParticleEffect
from direct.particles.ForceGroup import ForceGroup
from direct.gui.DirectGui import *
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.DirectObject import DirectObject
from panda3d.bullet import *
from direct.actor.Actor import Actor
#from game.factory import nodeRoot
from game.sky import *
from game.terrain import *
from game.water import *
from game.grass import *
from game.camera import *
from game.light import *
from game.fog import *
from game.model import *
from game.guihelper import getGeom
import sys, os
import copy
from game.shadow import SingleShadow, ShadowFactory, ShadowBase
from game.input import Panda3DInput
from game.particle import ParticleBase
import json
from game.gui import GuiManager

########################################################
class BaseBuilder(object):
    def __init__(self, *args, **kwargs):
        self.data = kwargs
        
    def buildGameObject(self, world=None):
        pass
    
    def buildType(self):
        pass
    
    def asynchronize(self, *nodePaths):
        pass
    
    def processAttributes(self, data, gameObject):
        if data.has_key("Attribute"):
            attributes_data = data['Attribute']
            gameObject.appendAttribute(attributes_data)
                
    
    def resetTransform(self, nodePath):
        nodePath.setPos(0, 0, 0)
        nodePath.setHpr(0, 0, 0)
        nodePath.setScale(1)
        
    def createWheel(self, data, obj, nodeRoot):
        vehicle = obj.vehicle        
        i = 0
        if data.has_key('Wheels'):
            for wheel_data in data['Wheels'].values():                
                wheel = vehicle.createWheel()
                wheelNP = loader.loadModel(wheel_data['Model'])
                wheelNP.reparentTo(nodeRoot) 
                if not hasattr(obj, 'wheels'):
                    obj.wheels = []
                obj.wheels.append(wheelNP)               
                pos = caster.stringListToListFloat(wheel_data['Pos'].split(','))                
                wheel.setNode(wheelNP.node())
                wheel.setChassisConnectionPointCs(Point3(*pos))
                front = eval(wheel_data['Front'])
                wheel.setFrontWheel(front)
                if front == True:
                    obj.steersIdx.append(i)
                else:
                    obj.forcesIdx.append(i)
                wheel.setWheelDirectionCs(Vec3(0, 0, -1))
                wheel.setWheelAxleCs(Vec3(1, 0, 0))
                wheel.setWheelRadius(0.25)
                wheel.setMaxSuspensionTravelCm(40.0)
            
                wheel.setSuspensionStiffness(40.0)
                wheel.setWheelsDampingRelaxation(2.3)
                wheel.setWheelsDampingCompression(4.4)
                wheel.setFrictionSlip(100.0);
                wheel.setRollInfluence(0.1)
                i += 1
    
    def processBulletData(self, prototypeData, gameObject, nodeRoot, bulletWorld):
        # Bullet physical checking
        if prototypeData.has_key('PhysicBody') and bulletWorld is not None:            
            physicNode = self.createBulletBody(prototypeData['PhysicBody'], 
                                                gameObject.nodePath, bulletWorld)
            isTerrain = False
            if isinstance(physicNode, tuple):
                gameObject.vehicle = physicNode[1]
                physicNode = physicNode[0]
                gameObject.physicNode = NodePath(physicNode)
                self.createWheel(prototypeData['PhysicBody'], 
                                 gameObject, nodeRoot)
            else:
                gameObject.physicNode = NodePath(physicNode)
            if isinstance(physicNode, BulletCharacterControllerNode):
                gameObject.physicNode.setPos(gameObject.nodePath.getPos())
                gameObject.physicNode.setHpr(gameObject.nodePath.getHpr())
                gameObject.physicNode.setScale(gameObject.nodePath.getScale())
                self.resetTransform(gameObject.nodePath)                
            elif not isinstance(gameObject.physicNode.node().getShape(0), BulletHeightfieldShape): 
                gameObject.physicNode.setPos(gameObject.nodePath.getPos())
                gameObject.physicNode.setHpr(gameObject.nodePath.getHpr())
                gameObject.physicNode.setScale(gameObject.nodePath.getScale())
                self.resetTransform(gameObject.nodePath)
            else:
                isTerrain = True
                
             
            # Default pos, hpr, scale            
            if prototypeData['PhysicBody'].has_key('Pos'):
                pos = prototypeData['PhysicBody']['Pos'].split(',')
                move = VBase3(*caster.stringListToListFloat(pos))
                gameObject.nodePath.setPos( gameObject.nodePath.getPos() + move)                        
            gameObject.nodePath.reparentTo(gameObject.physicNode)  
            
            # Restore height for terrain
            if isTerrain:
                height = gameObject.nodePath.getPythonTag('height')
                gameObject.physicNode.setZ( height/2)
               
            # Check mask
            if prototypeData['PhysicBody'].has_key('BitMask'):
                masks_data = prototypeData['PhysicBody']['BitMask']
                masks_number = caster.stringListToListInt(masks_data.split(','))
                mask = BitMask32()
                for number in masks_number:
                    mask.setBit(number)
                gameObject.physicNode.setCollideMask(mask)
            else:
                gameObject.physicNode.setCollideMask(BitMask32.allOn())
                      
            self.attachPhysicNode(gameObject, bulletWorld)
            gameObject.realNP = gameObject.nodePath
            gameObject.nodePath = gameObject.physicNode
        
    def attachPhysicNode(self, gameObject, world):
        physicNode = gameObject.physicNode        
        if isinstance(physicNode.node(), BulletRigidBodyNode):
            world.attachRigidBody(physicNode.node())
        elif isinstance(physicNode.node(), BulletGhostNode):
            world.attachGhost(physicNode.node())
        elif isinstance(physicNode.node(), BulletSoftBodyNode):
            world.attachSoftBody(physicNode.node())
        elif isinstance(physicNode.node(), BulletCharacterControllerNode):
            world.attachCharacter(physicNode.node())
        if hasattr(gameObject, 'vehicle') and gameObject.vehicle:
            world.attachVehicle(gameObject.vehicle)
         
    
    def createSingleBulletShape(self, data, nodePath):               
        if data.has_key('HeightFieldImage'):
            heightFieldImg  = data['HeightFieldImage'] 
        if data.has_key('Distance'):
            distance = caster.stringToFloat(data['Distance'], 0)
        if data.has_key('Normal'):
            normal = caster.stringListToListFloat(data['Normal'].split(','), 1)
        if data.has_key('Dynamic'):
            isDynamic = False
        if data.has_key('Radius'):
            radius = caster.stringToFloat(data['Radius'], 0)
        if data.has_key('Height'):
            height = caster.stringToFloat(data['Height'], 0)      
        if data.has_key('HalfVector'):
            halfVec = caster.stringListToListFloat(data['HalfVector'].split(','), 1)
        points = []
        isGeom = False
        triangles = []
        if data.has_key('Geom'):
            isGeom = True
        elif data.has_key('Points'):            
            for p in data['Points'].values():
                point = caster.stringListToListFloat(p.split(','), 0)
                points.append(point)
        elif data.has_key("Triangles"):
            for triangleData in data['Triangles'].values():
                triangle = []
                for p in triangleData.values():
                    point = caster.stringListToListFloat(p.split(','), 0)
                    triangle.append(point)
                triangles.append(triangle)
        if data.has_key('Pos'):
            pos = caster.stringListToListFloat(data['Pos'].split(','), 0)
        else:
            pos = [0, 0, 0]           
        shapeType = data['Type']        
        if "sphereshape" in shapeType.lower():
            shape = BulletSphereShape(radius)
        elif "planeshape" in shapeType.lower():
            shape = BulletPlaneShape(Vec3(*normal), distance)
        elif "boxshape" in shapeType.lower():                
            shape = BulletBoxShape(Vec3(*halfVec))
        elif "cylindershape" in shapeType.lower():
            shape = BulletCylinderShape(radius, height, ZUp)
        elif "capsuleshape" in shapeType.lower():                
            shape = BulletCapsuleShape(radius, height, ZUp)
        elif "coneshape" in shapeType.lower():
            shape = BulletCapsuleShape(radius, height, ZUp)
        elif "convexhull" in shapeType.lower():
            shape = BulletConvexHullShape()
            if isGeom == True:
                geomNodes = nodePath.findAllMatches('**/+GeomNode')
                geomNode = geomNodes.getPath(0).node()
                geom = geomNode.getGeom(0)
                shape.addGeom(geom)
            elif len(points) > 0:
                for p in points:
                    shape.addPoint(p)
        elif 'trianglemesh' in shapeType.lower():
            mesh = BulletTriangleMesh()
            if isGeom == True:
                geomNodes = nodePath.findAllMatches('**/+GeomNode')                
                for idx in xrange(0, len(geomNodes)):                    
                    geomNode = geomNodes.getPath(idx).node()                
                    for geom in geomNode.getGeoms():                    
                        mesh.addGeom(geom)
            elif len(triangles) > 0:
                for triangle in triangles:
                    mesh.addTriangle(*triangle)                    
            shape = BulletTriangleMeshShape(mesh, dynamic=isDynamic)
        elif "heightfield" in shapeType.lower():    
            img = PNMImage(heightFieldImg)      
            img.makeGrayscale()
            shape = BulletHeightfieldShape(img, height, ZUp)  
            shape.setUseDiamondSubdivision(True)      
            offset = img.getXSize() / 2.0 - 0.5            
            nodePath.setPos(-offset, -offset, -height / 2.0)
            nodePath.setPythonTag('height', height)
        return shape, pos
    
    def createBulletBody(self, data, nodePath, world):
        shapes = {}
        for key, value in data['Shapes'].items():
            shape, pos = self.createSingleBulletShape(value, nodePath) 
            shapes[key] = { 'shape' : shape, 'pos' : pos}
        # Create basic body node
        name = nodePath.getName() + data['Name']
        nodeType = data['Type']
        if "rigid" in nodeType.lower() or "vehicle" in nodeType.lower():
            node = BulletRigidBodyNode(name)   
            for shape in shapes.values():
                if isinstance(shape['shape'], BulletTriangleMeshShape) or\
                    isinstance(shape['shape'], BulletHeightfieldShape):                    
                    node.addShape( shape['shape'])
                else:
                    pos = Vec3(*shape['pos'])
                    node.addShape( shape['shape'], TransformState.makePos(pos))
            if data.has_key('Mass'):                
                node.setMass(caster.stringToFloat(data['Mass'], 1))         
        elif "ghost" in nodeType.lower():
            node = BulletGhostNode(name)     
            for shape in shapes.values():
                if isinstance(shape['shape'], BulletTriangleMeshShape) or\
                    isinstance(shape['shape'], BulletHeightfieldShape):                     
                    node.addShape( shape['shape'])
                else:                    
                    pos = Vec3(*shape['pos'])
                    node.addShape( shape['shape'], TransformState.makePos(pos))
        # Create extend body node
        elif "character" in nodeType.lower():            
            stepHeight = caster.stringToFloat(data['StepHeight'], 0.4)
            node = BulletCharacterControllerNode(shapes.values()[0]['shape'], stepHeight, name)
            
        if "vehicle" in nodeType.lower():            
            node.setDeactivationEnabled(False)
            vehicle = BulletVehicle(world, node)            
            vehicle.setCoordinateSystem(ZUp)            
            return node, vehicle     
        return node
    
    def processMaterialInfo(self, data, nodePath):
        if data.has_key("Material") and len(data['Material']) > 0:
            myMaterial = Material()
            material = data['Material']
            if material.has_key('Ambient'):
                ambient = caster.stringListToListFloat(material['Ambient'].split(','))
                myMaterial.setAmbient(VBase4(*ambient))
            if material.has_key('Diffuse'):
                diffuse = caster.stringListToListFloat(material['Diffuse'].split(','))
                myMaterial.setDiffuse(VBase4(*diffuse))
            if material.has_key('Emission'):
                emission = caster.stringListToListFloat(material['Emission'].split(','))
                myMaterial.setEmission(VBase4(*emission))
            if material.has_key('Shininess'):
                shininess = caster.stringToFloat(material['Shininess'], 2.0)
                myMaterial.setShininess(shininess)
            if material.has_key('Specular'):
                specular = caster.stringListToListFloat(material['Specular'].split(','))
                myMaterial.setSpecular(VBase4(*specular))            
            nodePath.setMaterial(myMaterial)
            
    def processTags(self, data, gameObject):
        if data.has_key("Tags"):            
            tags = data['Tags'].split(",")
            tags = [x.strip() for x in tags]
            gameObject.appendTags(*tags)
            
    def processScripts(self, data, gameObject):        
        if data.has_key("Scripts"):                      
            scriptFiles = data['Scripts'].split(",")
            scriptFiles = [x.strip() for x in scriptFiles]
            for scriptPath in scriptFiles:
                PROJECT_FOLDER = os.getcwd()            
                scriptFilePath = os.path.join(PROJECT_FOLDER, scriptPath)
                name = os.path.basename(scriptFilePath)
                name, ext = os.path.splitext(name) 
                SCRIPT_FOLDER = os.path.dirname(scriptFilePath)            
                if sys.path.count(SCRIPT_FOLDER) == 0:                
                    sys.path.append(SCRIPT_FOLDER)            
                scriptFile = [name]
                modules = map(__import__, scriptFile)
                for module in modules:
                    #module.owner = model
                    ##print "MODEL > GETPOS", model.getPos()
                    script = module.Scripting(gameObject)
                    #taskMgr.add(script.run, gameObject.nodeName + scriptFilePath + 'script', uponDeath=script.cleanUp)
                    gameObject.appendScripts(script)
        
    
class GameModelBuilder(BaseBuilder):
    def __init__(self):
        pass
    

    def createObject(self, Data, gameObjectList, nodeRoot, world, prototype):
        model = prototype.clone()        
        model.setName(Data['Name'])
        if Data.has_key("Pos") and len(Data['Pos']) > 2 :
            posParam = Data['Pos']
            if not isinstance(posParam, list):
                posParam = caster.stringListToListFloat(Data['Pos'].split(','))            
            model.nodePath.setPos(*posParam)
        if Data.has_key("Hpr") and len(Data['Hpr']) > 2:
            hprParam = Data['Hpr']
            if not isinstance(hprParam, list):
                hprParam = caster.stringListToListFloat(Data['Hpr'].split(','))  
            model.nodePath.setHpr(*hprParam)
        self.processTags(Data, model)
        self.processAttributes(Data, model)
        prototypeData = prototype.describeDict
        if Data.has_key('PhysicBody'):
            if prototypeData.has_key('PhysicBody'):
                prototypeData['PhysicBody'].update(Data['PhysicBody'])
            else:
                prototypeData['PhysicBody'] = Data['PhysicBody']
        # Bullet physical checking
        self.processBulletData(prototypeData, model, nodeRoot, world)
        if Data.has_key('Parent'):
            if Data['Parent'] == "":
                model.nodePath.reparentTo(nodeRoot)
            else:
                model.nodePath.reparentTo(gameObjectList['GameModels'][Data['Parent']])
        else:
            model.nodePath.reparentTo(nodeRoot)
        self.processScripts(Data, model)
        # Update describe dict
        model.describeDict.update(Data)
        #print "BUILD OBJECT DONE"
        return model

    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):        
        #print "DATA@@@@@@@ ", Data
        #print "GameObjectList", gameObjectList
        prototype = gameObjectList['GameModels'][Data['objectID']] 
        if Data.has_key('TransformFile'):
            #print "transform file", Data['TransformFile']
            transform = json.load(open(Data['TransformFile']))
            positions = transform['positions']
            name = Data['Name']
            model = GameObject(name)
            idx = 0      
            for idx, position in enumerate(positions):
                if transform.has_key('hprs'):
                    Data['Hpr'] = transform['hprs'][idx]
                Data['Pos'] = position
                Data['Name'] = name + str(idx)
                child = self.createObject(Data, gameObjectList, model.nodePath, world, prototype)                
                #print "build instance"
                idx += 1      
            model.reparentTo(nodeRoot)                  
        else:  
            model = self.createObject(Data, gameObjectList, nodeRoot, world, prototype)
        return model
    
    def asynchronize(self, *nodePaths):
        pass

    def buildType(self, data):
        ##print "TRACE: GameModelBuilder --> BuilderGameModel"
        
        modelParts = {}
        anims = {}
        isActor = False
        nodeName = data['Name'] #It can override when add to Map_
        if data.has_key('Parts'):
            for part in data['Parts'].values():
                #Add Model Path to modelParts Dict
                name = part['Model']['Name']
                path = part['Model']['Path']
                modelParts[name] = path
                
                #Add animation to anims Dict.
                #Each Part maybe have many Animation.
                #So each Part have a Animation Dict
                
                anim = {}                        
                if part.has_key('Anims'):
                    for item in part['Anims'].values():
                        isActor = True
                        animPath = item['Path']
                        animName = item['Name']
                        anim[animName] = animPath
                    anims[name] = anim
                
            #Load for Panda View
            #-- Load panda model OR Actor            
            if(isActor == True):            
                nodePath = Actor(modelParts, anims)
                nodePath.setName(nodeName)
            else:
                nodePath = base.loader.loadModel(modelParts.values()[0])
                nodePath.setName(nodeName)
        else:
            nodePath = NodePath(nodeName)    
        #-- Set basic Attribute
        #-- -- Set Default Position             
        if data.has_key("Pos") and len(data['Pos']) > 2:  
            posParam =  caster.stringListToListFloat(data['Pos'].split(','))
            nodePath.setPos(*posParam)
            
        #-- -- Set Default Hpr
        if data.has_key("Hpr") and len(data['Hpr']) > 2:
            hprParam = caster.stringListToListFloat(data['Hpr'].split(','))
            nodePath.setHpr(*hprParam)
        
        #-- -- Set Default Color
        if data.has_key("Color") and len(data['Color']) > 3:
            colorParam = data['Color'].split(',')
            colorParamList =  caster.stringListToListFloat(colorParam)
            nodePath.setColor(*colorParamList)
        
        #-- -- Set Default Scale
        if data.has_key("Scale") and len(data['Scale']) > 2:
            scaleParam = caster.stringListToListFloat(data['Scale'].split(','))
            nodePath.setScale(*scaleParam)
            
        self.processMaterialInfo(data, nodePath)
        
        # -- Set TwoSide Default
        if data.has_key("TwoSided"):
            nodePath.setTwoSided(caster.stringToBool(data['TwoSided'], 0))
        
        if data.has_key("Bin") and len(data['Bin']) > 1:
            binParam = data['Bin'].split(',')
            nodePath.setBin(binParam[0], caster.stringToInt(binParam, 0))
        
        model = ModelFactory.createModel(data['Type'],
                                         nodePath = nodePath,
                                         nodeName = nodeName,
                                         describeDict=data) 
        self.processTags(data, model)       
        self.processScripts(data, model)     
        self.processAttributes(data, model)
            
        return model
                
        
        
class LightBuilder(BaseBuilder):
        
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
        
        if gameObjectList.has_key('GameModels'):
            models = gameObjectList['GameModels']
        else:
            models = {}
        if gameObjectList.has_key('Terrains'):
            terrains = gameObjectList['Terrains']
        else:
            terrains = {}
        prototype = gameObjectList['Lights'][Data['objectID']]
        lightNP = self.createLight(prototype.describeDict)
        light = prototype.clone()
        light.nodePath = lightNP
        light.nodePath.reparentTo(nodeRoot)  
        if Data.has_key('Affect'):
            affectList = Data['Affect'].split(',')       
            if len(affectList) == 0:
                nodeRoot.setLight(light.nodePath)     
            for modelID in affectList:                
                if not models.has_key(modelID) and terrains.has_key(modelID):                    
                    terrain = terrains[modelID]
                    light.applyTo( terrain.nodePath )
                else:                    
                    light.applyTo( models[modelID].nodePath)    
        else:            
            light.applyTo(nodeRoot)      
        
        if Data.has_key('Shadow') and isinstance(light.node(), DirectionalLight):
            #print "found shadow"
            shadowData = Data['Shadow']
            prototypeId =  shadowData['Prototype']
            prototype = gameObjectList['Shadows'][prototypeId]
            builder = ShadowBuilder()
            shadow = builder.createShadow(prototype.describeDict, light, nodeRoot)
            
            # Assign shadow to root node
            nodeRoot.setPythonTag('Shadow', shadow)
            light.setShadow(shadow)                        
        
        if Data.has_key('AutoShadow'):
            light.applyPSSM(nodeRoot)
            print "auto shadow"            
        self.processScripts(Data, light)
        
        return light
    

    def createLight(self, Data):
        tempLight = AmbientLight(Data['Name'])
        if Data['Type'] == "AmbientLight":
            tempLight = AmbientLight(Data['Name'])
        elif Data['Type'] == "DirectionalLight":
            tempLight = DirectionalLight(Data['Name'])
        elif Data['Type'] == "PointLight":
            tempLight = PointLight(Data['Name'])
            if len(Data['Attenuation']) > 2:
                attenuation = caster.stringListToListFloat(Data['Attenuation'].split(','))
                tempLight.setAttenuation(Point3(*attenuation))
        elif Data['Type'] == "SpotLight":
            tempLight = Spotlight(Data['Name'])
    #            tempLight.setShadowCaster(Data['ShadowCaster']['Bool'], Data['ShadowCaster']['XSize'], Data['ShadowCaster']['YSize'])
            lens = PerspectiveLens()
            if Data['Lens'].has_key('Fov'):
                lens.setFov(Data['Lens']['Fov'])
            if Data['Lens'].has_key('Near'):
                lens.setNear(Data['Lens']['Fov'])
            if Data['Lens'].has_key('Far'):
                lens.setFar(Data['Lens']['Far'])
            if Data['Lens'].has_key('AspectRatio'):
                lens.setAspectRatio(Data['Lens']['AspectRatio'])
            tempLight.setLens(lens)
        colorParam = caster.stringListToListFloat(Data['Color'].split(','))
        tempLight.setColor(VBase4(*colorParam))
        tlnp = NodePath(tempLight)
        if Data.has_key('Hpr'):
            if len(Data['Hpr']) > 2:
                hprParam = caster.stringListToListFloat(Data['Hpr'].split(','))
                tlnp.setHpr(*hprParam)
        if Data.has_key('Pos'):
            if len(Data['Pos']) > 2:
                posParam = caster.stringListToListFloat(Data['Pos'].split(','))
                tlnp.setPos(*posParam)
        return tlnp

    def buildType(self, Data):
#        tlnp = self.createLight(Data)
        light = LightFactory.createLight(Data['Type'], nodePath=NodePath(Data["Name"]), nodeName=Data['Name'], describeDict=Data)
        self.processTags(Data, light)    
        
        return light
       

class ShadowBuilder(BaseBuilder):

    def buildType(self, Data):
        shadow = GameObject()
        shadow.describeDict = Data        
        return shadow
    
    def createShadow(self, Data, *args):
        shadowType = Data['Type']        
        shadow = ShadowFactory.createShadow(shadowType, *args)        
        return shadow        
        

class CameraBuilder(BaseBuilder):
        
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
        prototype = gameObjectList['Cameras'][Data['objectID']]
        cam = prototype.clone()
        #print "cam data", Data
        cam.setName(Data['Name'])
        if Data.has_key('Pos'):
            posParam = caster.stringListToListFloat(Data['Pos'].split(','))
            cam.setPos(*posParam)
        if Data.has_key('Hpr'):
            hprParam = caster.stringListToListFloat(Data['Hpr'].split(','))
            cam.setHpr(*hprParam)
        if Data.has_key('Active'):
            cam.active(True)
            #print "active cam", CameraBase.current
        prototypeData = prototype.describeDict
        if Data.has_key('PhysicBody'):
            if prototypeData.has_key('PhysicBody'):
                prototypeData['PhysicBody'].update(Data['PhysicBody'])
            else:
                prototypeData['PhysicBody'] = Data['PhysicBody']
        # Bullet physical checking
        self.processBulletData(prototypeData, cam, nodeRoot, world)        
        self.processTags(Data, cam)     
        self.processScripts(Data, cam)
        return cam
    
    def buildType(self, Data):
        ##print "TRACE: CameraBuilder --> ProductCam ", Data
        node = NodePath( Camera(base.cam.node()) )
        node.setName(Data['Name'])
        cam = CameraFactory.createCamera(Data['Type'], nodePath=node, nodeName=Data['Name'], describeDict=Data)
        
        if Data.has_key('Pos'):
            posParam = caster.stringListToListFloat(Data['Pos'].split(','))
            cam.setPos(*posParam)
        if Data.has_key('Hpr'):
            hprParam = caster.stringListToListFloat(Data['Hpr'].split(','))
            cam.setHpr(*hprParam)
        
        if Data.has_key('Lens'):
            if Data['Lens'].has_key('Fov'):
                cam.lens.setFov(Data['Lens']['Fov'])            
            if Data['Lens'].has_key('Near'):
                cam.lens.setNear(Data['Lens']['Near'])
            if Data['Lens'].has_key('Far'):
                cam.lens.setFar(Data['Lens']['Far'])
            if Data['Lens'].has_key('AspectRatio'):
                cam.lens.setAspectRatio(Data['Lens']['AspectRatio'])
        self.processTags(Data, cam)     
        self.processScripts(Data, cam)
        return cam

class TerrainBuilder(BaseBuilder):
        
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):             
        prototype = gameObjectList['Terrains'][Data['objectID']]
        terrain = self.buildTerrrain(prototype.describeDict, Data)
        prototypeData = prototype.describeDict
        # Bullet physical checking
        self.processBulletData(prototypeData, terrain, nodeRoot, world)    
        
        # AdditionData
        
        if Data.has_key('Pos'):            
            posParam =  caster.stringListToListFloat(Data['Pos'].split(','))
            terrain.nodePath.setPos(*posParam)
        
        if Data.has_key('Hpr'):            
            posParam =  caster.stringListToListFloat(Data['Hpr'].split(','))
            terrain.nodePath.setHpr(*posParam)
        
        
        terrain.nodePath.reparentTo(nodeRoot)
        self.processTags(Data, terrain) 
        self.processScripts(Data, terrain)    
        return terrain        
    
    def buildTypeMonoTexture(self, Data):
        ##print "TRACE: TerrainBuilder --> builtType()"
        terrain = GeoMipTerrain(Data['Name'])
        terrain.setHeightfield(Data['HeightFieldPath'])
        terrain.setBlockSize(caster.stringToInt(Data['BlockSize'], 0))
        terrain.setNear(caster.stringToFloat(Data['Near'], 0))
        terrain.setFar(caster.stringToFloat(Data['Far'], 0))
        terrain.setFocalPoint(base.camera)
        posParam = caster.stringListToListFloat(Data['Pos'].split(','))
        
        terrainNode = terrain.getRoot()        
        terrainNode.setSz(caster.stringToFloat(Data['Setsz'], 0))
        terrainNode.setPos(*posParam)
#        terrain.setMinLevel(caster.stringToInt(Data['MinLevel'], 0))
        terrain.setAutoFlatten(GeoMipTerrain.AFMOff)
        
        # Set Texture
        terrainNode.clearTexture()
        scaleParam = caster.stringListToListFloat(Data['tScale'].split(','))
        terrainNode.setShaderInput('tscale', Vec4(*scaleParam))
        
        ts = TextureStage('ts')
        tex = base.loader.loadTexture(Data['TexturePath'])
        terrainNode.setTexture(ts, tex)
        terrain.generate()
        
        terrainBase = TerrainBase(nodeName=Data['Name'], nodePath=terrainNode, describeDict=Data)
        #terrainBase.nodePath.reparentTo(nodeRoot)
        return terrainBase
    
    def buildTerrrain(self, Data, instanceData):                
        terrain = myGeoMipTerrain(Data['Name'])
        terrain.setHeightfield(Data['HeightFieldPath'])
        
        loaded = False
        if Data.has_key('CacheFile'):
            if os.path.exists(Data['CacheFile']):
                root = loader.loadModel(Data['CacheFile'])
                loaded = True
        if not loaded:
            # Set terrain properties
            if Data.has_key('BlockSize'):
                terrain.setBlockSize(caster.stringToInt(Data['BlockSize'], 32))        
            terrain.setFocalPoint(base.camera)
            if Data.has_key('Near'):
                terrain.setNear(caster.stringToFloat(Data['Near'], 0.01))
            if Data.has_key('Far'):
                terrain.setFar(caster.stringToFloat(Data['Far'], 1000))
            if Data.has_key('MinLevel'):
                terrain.setMinLevel(caster.stringToFloat(Data['MinLevel'], 0.01))            
            #print "generate terrain", terrain        
            # Generate it.
            terrain.generate()                        
            
            if Data.has_key("CacheFile"):
                #print "write cache file"
                terrain.getRoot().writeBamFile(Data['CacheFile'])
        
            # Store the root NodePath for convenience
            root = terrain.getRoot()        
        
        if Data.has_key('Setsz'):
            root.setSz(caster.stringToFloat(Data['Setsz'], 0))    # z (up) scale
        if Data.has_key('Setsz'):
            root.setShaderInput('tscale', Vec4(*caster.stringListToListFloat(Data['tscale'].split(','))))    # texture scaling
        if Data.has_key('Setsz'):
            root.setShaderInput('scale', 1, 1, caster.stringToFloat(Data['Setsz'], 2), 1)
        
        

        # texture
        terrain.setMultiTexture(Data, root)      

        self.processMaterialInfo(Data, root)
        
        terrainBase = TerrainBase(nodeName=Data['Name'], nodePath=root, describeDict=Data, terrain=terrain)
                 
        return terrainBase        
    
    def buildType(self, Data):
        terrainBase = TerrainBase(nodeName=Data['Name'], nodePath=None, describeDict=Data, terrain=None)
        self.processScripts(Data, terrainBase)
        self.processTags(Data, terrainBase)    
        return terrainBase       
        

class SkyBuilder(BaseBuilder):
        
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
        prototype = gameObjectList['Skys'][Data['objectID']]
        sky = prototype.clone()
        sky.nodePath.reparentTo(nodeRoot)
        return sky
    
    def buildSkyDome(self, Data):
        modelPath =  Data['Model']           
        textureFile = Data['TextureFile']
        
        name = Data['Name']
                        
        shaderFile = Data['ShaderFile']
        shaderInputs = Data['ShaderInputs']
        
        scale = Vec3( *caster.stringListToListFloat(Data['Scale'].split(',')) )
        
        height = caster.stringToFloat(Data['ZUnderZero'], 0)        
        
        speed = caster.stringToFloat(Data['CloudSpeed'], 1)
        
        skydome = SkyDome(name, scale, modelPath, textureFile, shaderFile, height, speed, shaderInputs)
        
        sky = SkyMonoLayer(skydome, nodeName=name, nodePath=skydome.dome, describeDict=Data)
        self.processTags(Data, sky)  
        return sky
    
    def buildSkyMulti(self, Data):
        #print "build sky multi"
        skyNode = NodePath(Data['Name'])
        skybox = None
        cloud = None
        name = Data['Name']
        if Data.has_key('SkyBox'):            
            skyboxData = Data['SkyBox']
            skyboxModel = skyboxData['Model']
            skyboxScale = caster.stringToFloat(skyboxData['Scale'], 1000)                      
            skybox = SkyBox(name, skyboxModel, skyboxScale)
            skybox.box.reparentTo(skyNode)            
            if skyboxData.has_key('Depth'):
                skybox.setDepth(eval(skyboxData['Depth']))  
        if Data.has_key('Cloud'):
            cloudData = Data['Cloud']
            cloudModel = cloudData['Model']
            cloudTexture = cloudData['Texture']
            cloudScale = Vec3( *caster.stringListToListFloat(cloudData['Scale'].split(','), 10000) )
            cloudHeight = caster.stringToFloat(cloudData['Height'], -9600)
            cloudSpeed = caster.stringToFloat(cloudData['Speed'], 0.03)
            cloud = CloudLayer(name, cloudModel, cloudTexture, cloudScale, cloudHeight, cloudSpeed)
            cloud.clouds.reparentTo(skyNode)
            
        sky = SkyMultiLayer(skybox, cloud, nodeName=name, nodePath=skyNode, describeDict=Data)
        self.processTags(Data, sky)  
        return sky
    
    def buildSkybox(self, Data):
        modelPath =  Data['Model']           
        textureFile = Data['TextureFile']        
        name = Data['Name']                                  
        scale = Vec3( *caster.stringListToListFloat(Data['Scale'].split(','), 100) )
        
        skybox = SkyBox(name, modelPath, scale, textureFile)
        if Data.has_key('Hpr'):
            hpr = Vec3( *caster.stringListToListFloat(Data['Hpr'].split(',')) )
            skybox.box.setHpr(hpr)

        if Data.has_key('Far'):
            skybox.setFar(caster.stringToFloat(Data['Far'], 0))
        if Data.has_key('Height'):
            skybox.setHeight(caster.stringToFloat(Data['Height'], 0))
        if Data.has_key('Depth'):
            skybox.setDepth(eval(Data['Depth']))
        sky = SkyMonoLayer(skybox, nodeName=name, nodePath=skybox.box, describeDict=Data)
        self.processTags(Data, sky)  
        return sky
    
    def buildType(self, Data):
        if Data['Type'].lower() == "skymonolayer" or  Data['Type'].lower() == "skydome":
            return self.buildSkyDome(Data)
        elif Data['Type'].lower() == "skymultilayer":
            return self.buildSkyMulti(Data)
        elif Data['Type'].lower() == "skybox":
            return self.buildSkybox(Data)

class FogBuilder(BaseBuilder):
        
    def addFogShaderInputToNode(self, node, fog):
        fogMode = fog.getMode()        
        if fogMode is Fog.MLinear:
            node.setShaderInput('fog_mode', 0)                        
        elif fogMode is Fog.MExponential:
            node.setShaderInput('fog_mode', 1)            
        elif fogMode is Fog.MExponential:
            node.setShaderInput('fog_mode', 2)
            
            
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):          
        fog = copy.deepcopy(gameObjectList['Fogs'][Data['objectID']])
        if Data.has_key('Affects'):
            affectList = Data['Affects'].split(',')
            for modelID in affectList.items():
                node = models[modelID].nodePath
                node.setFog(fog.nodePath)
                self.addFogShaderInputToNode(node, fog)
        else:            
            nodeRoot.setFog(fog.nodePath)
            self.addFogShaderInputToNode(nodeRoot, fog)
        
        
        return fog
    
    def buildType(self, Data):
        tempFog = Fog(Data['Name'])
        if Data['Type'].lower() == "linearfog":
            tempFog.setMode(Fog.MLinear)
        elif Data['Type'].lower() == "exponential":
            tempFog.setMode(Fog.MExponential)
        else:
            tempFog.setMode(Fog.MExponentialSquared)
        
        color = caster.stringListToListFloat(Data['Color'].split(','))
        tempFog.setColor(*color)
        if Data.has_key('LinearRange') and len(Data['LinearRange']) > 1:
            linearData = caster.stringListToListFloat(Data['LinearRange'].split(','))
            tempFog.setLinearRange(*linearData)
        if Data.has_key('LinearFallback') and len(Data['LinearFallback']) > 2:
            linearFallBackData = caster.stringListToListFloat(Data['LinearFallback'].split(','))
            tempFog.setLinearFallback(*linearFallBackData)
        if Data.has_key('ExpDensity'):
            if Data['ExpDensity'] != None:                
                tempFog.setExpDensity(caster.stringToFloat(Data['ExpDensity'], 0.005))
        
        base.setBackgroundColor(*color)
        fog = FogFactory.createFog(Data['Type'], nodePath=tempFog, nodeName=Data['Name'], describeDict=Data)        
        render.setShaderInput('fog_mode', -1) # fix shader error because missing input           
        self.processTags(Data, fog)  
        return fog


class WaterBuilder(BaseBuilder):
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
        prototype = gameObjectList['Waters'][Data['objectID']]
        water = prototype.clone()
        water.nodePath.reparentTo(nodeRoot)
        if Data.has_key('Pos'):            
            posParam =  caster.stringListToListFloat(Data['Pos'].split(','))
            water.nodePath.setPos(*posParam)
            
        if Data.has_key('Hpr'):            
            posParam =  caster.stringListToListFloat(Data['Hpr'].split(','))
            water.nodePath.setHpr(*posParam)
                
        return water
    
    def buildType(self, Data):
        #_water_level with z is height of water
        _water_level = Vec4(0.0, 0.0, caster.stringToFloat(Data['Height'], 0), 1.0)
        #_water_node (x1, y1, x2, y2, z) Left-Top Point and Right-Bottom Point , Z - height of water
        size = caster.stringListToListFloat(Data['Size'].split(","))
        if Data.has_key('Pos'):
            pos = caster.stringListToListFloat(Data['Pos'].split(","))
        else:
            pos = [0, 0]   
        ##print size[0], size[1]
        halfWidth = size[0] / 2
        halfHeight = size[1] / 2
        water = WaterNode(pos[0] - halfWidth, pos[1] - halfHeight, pos[0] + halfWidth, pos[1] + halfHeight, _water_level.getZ(), Data)
        waterBase = WaterBase(nodeName=Data['Name'], nodePath=water.waterNP, describeDict=Data,
                              plane=water.waterPlane, cam=water.watercamNP)        
        self.processTags(Data, waterBase)  
        return waterBase
    
    
    
class GrassBuilder(BaseBuilder):
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
#        smokeData = {'GrassTexture' : 'Models/grassPack.png',
#                     'Name' : 'grass1',
#                     'Pos' : '50, 50, 30',
#                     'Scale' : '0.05',
#                     'ShaderPath' : 'Shader/grass.sha',
#                     'ModelPath' : 'Models/grass'}
#        gr = GrassNode( Data)
#        gr.getNodePath().reparentTo(nodeRoot)
        
        grass = (gameObjectList['Grasss'][Data['objectID']]).clone()
        if Data.has_key("Pos") and len(Data['Pos']) > 2 :
            posParam = Data['Pos']
            if not isinstance(posParam, list):
                posParam = caster.stringListToListFloat(Data['Pos'].split(','))            
            grass.nodePath.setPos(*posParam)
        grass.nodePath.reparentTo(nodeRoot)
        return grass
        
    def buildType(self, Data):
#        Data = {'GrassTexture' : 'Models/grassPack.png',
#                     'Name' : 'grass1',
#                     'Pos' : '50, 50, 30',
#                     'Scale' : '0.05',
#                     'ShaderPath' : 'Shader/grass.sha',
#                     'ModelPath' : 'Models/grass'}
        gr = GrassNode(Data)
    
        grassBase = GrassBase(nodeName=Data['Name'], nodePath=gr.getNodePath(), describeDict=Data)        
        self.processTags(Data, grassBase)  
        return grassBase
    
    
class ParticleBuilder(BaseBuilder):
    def buildGameObject(self, Data, gameObjectList, nodeRoot, world=None):
        prototype = gameObjectList['Particles'][Data['objectID']]
        particle = copy.deepcopy(prototype)  
        effect = self.createParticle(particle.describeDict)
        #effect.reparentTo(particle.nodePath)
        particle.setEffect(effect)
        if Data.has_key("Pos"):     
            posParam =  caster.stringListToListFloat(Data['Pos'].split(','))
            particle.nodePath.setPos(*posParam)
        if Data.has_key("Scale"):
            scaleParam =  caster.stringListToListFloat(Data['Scale'].split(','))
            particle.nodePath.setScale(*scaleParam)  
        particle.nodePath.reparentTo(nodeRoot)      
        particle.startEffect(nodeRoot)
        prototypeData = prototype.describeDict
        if Data.has_key('PhysicBody'):
            if prototypeData.has_key('PhysicBody'):
                prototypeData['PhysicBody'].update(Data['PhysicBody'])
            else:
                prototypeData['PhysicBody'] = Data['PhysicBody']
        # Bullet physical checking
        self.processBulletData(prototypeData, particle, nodeRoot, world)
        self.processScripts(Data, particle)
        self.processAttributes(Data, particle)
        return particle

    def createParticle(self, Data):
        particleEffect = ParticleEffect()
        particleEffect.loadConfig(Data['Path'])
        return particleEffect
        
    def buildType(self, Data):
        base.enableParticles()        
        particleNP = NodePath(Data['Path'])                
        #particleNP.hide(BitMask32.bit(4))
        particle = ParticleBase(nodeName=Data['Name'], nodePath=particleNP, describeDict=Data)  
        centerNP = particle.center      
        centerNP.setDepthWrite(False)
        centerNP.setShaderOff(1)
        centerNP.setLightOff(1)
        centerNP.setBin('fixed', 40)
        self.processTags(Data, particle)  
        return particle
    
# Device input builder    
class InputBuilder():    
    def buildInput(self):
        pass
    
class Panda3DInputBuilder():
    def buildInput(self, key, data):
        tags = data['Tags'].split(',')
        for tag in tags:
            tags[tags.index(tag)] = tag.strip() # Make it well format
        pandaInput = Panda3DInput(key, tags)
        return pandaInput
    
# 2D builder

class GuiBuilder():
    def buildGui(self):
        pass
    
class DirectGuiBuilder():
    def buildGui(self, guiMgr, var_name, data):         
        # GUI type        
        #print "build gui", data
        gui_type = data['type']
        data.pop('type')        
        # Process data   
        self.processEvents(data)
        self.processScripts(data, guiMgr)
        self.processGuiScripts(data, var_name, guiMgr)
        self.processParent(data, guiMgr)
        self.processGeom(data, 'geom')
        self.processGeom(data, 'thumb_geom')
        self.processGeom(data, 'boxGeom')
        self.processEval(data)
        self.processFont(data)
        trans = self.processTrans(data)
        # Parse data
        self.parseData(data)
        
        #print "gui data", data
        gui = getattr(sys.modules[__name__], gui_type)(**data)   
        gui.setName(var_name)
        if trans:
            gui.setTransparency(TransparencyAttrib.MAlpha) 
            texture = gui.findTexture('*')            
            if texture:
                texture.setWrapU(Texture.WMClamp)
                texture.setWrapV(Texture.WMClamp)
        return gui
    
    def processEvents(self, data):
        self.processEvent('command', data)
        self.processEvent('focusInCommand', data)
        self.processEvent('focusOutCommand', data)
        self.processEvent('itemMakeFunction', data)
        
    def processGuiScripts(self, data, var_name, guiMgr):
        if data.has_key('gui_scripts'):
            scripts_data = data['gui_scripts']
            init_script = self.processScript('init', scripts_data)
            run_script = self.processScript('run', scripts_data)
            data.pop('gui_scripts')
            if init_script:
                guiMgr.addGuiScript(var_name, init_script)
            if run_script:
                guiMgr.addGuiScript(var_name, run_script, 'run')
            
        
    def processScripts(self, data, guiMgr):
        if data.has_key('scripts'):
            scripts_data = data['scripts']
            init_script = self.processScript('init', scripts_data)
            run_script = self.processScript('run', scripts_data)
            data.pop('scripts')
            if init_script:
                guiMgr.addScript(init_script)
            if run_script:
                guiMgr.addScript(run_script, 'run')
            
    
    def processScript(self, tagname, data):
        if data.has_key(tagname):
            filepath = data[tagname][0]
            funcName = data[tagname][1]
            modules = self.getModule(filepath)
            for module in modules:
                func = getattr(module, funcName)
                return func                
                  
                
    def processParent(self, data, guiMgr):
        if data.has_key('parent'):
            parent = data['parent']                
            if parent == "render2d" or parent == "aspect2d":
                data['parent'] = eval(data['parent'])
            else:
                parentGui = guiMgr.getGui(parent)   
                if parentGui:
                    data['parent'] = parentGui     
                    
    def processFont(self, data):
        options = ['font', 'text_font']
        for key in options:
            if data.has_key(key):
                font_file = data[key]
                font = loader.loadFont(font_file)
                data[key] = font
    

    def getModule(self, filepath):
        scriptFile = filepath
        PROJECT_FOLDER = os.getcwd()
        scriptFilePath = os.path.join(PROJECT_FOLDER, scriptFile)
        name = os.path.basename(scriptFilePath)
        name, ext = os.path.splitext(name)
        SCRIPT_FOLDER = os.path.dirname(scriptFilePath)
        if sys.path.count(SCRIPT_FOLDER) == 0:
            sys.path.append(SCRIPT_FOLDER)
        scriptFile = [name]
        #print "script file", scriptFile
        modules = map(__import__, scriptFile)
        return modules

    def processEvent(self, tagname, data):
        if data.has_key(tagname):  
            if isinstance(data[tagname], list):  
                funcName = data[tagname][1]     
                filepath = data[tagname][0]       
                modules = self.getModule(filepath)
                for module in modules:
                    data[tagname] = getattr(module, funcName)          
                    #print "event", data['command']  
    
    def processInherit(self, data):
        if data.has_key('inherit'):
            inherit = data['inherit']
            data.pop('inherit')
            if isinstance(inherit, str) or isinstance(inherit, unicode):
                json_data = open(inherit)
                inherit_data = json.load(json_data)
            else:
                inherit_data = inherit
            data_load = self.processInherit(inherit_data)
            if data_load.has_key('childs'):
                for key, child in data_load['childs'].items():
                    inherit_data_child = self.processInherit(child)
                    inherit_data_child.update(data_load['childs'][key])
                    data_load['childs'][key] = inherit_data_child
            inherit_data.update(data_load)
            return inherit_data
        return {}
    
    def processTrans(self, data):
        if data.has_key('trans'):
            trans = eval(data['trans'])
            data.pop('trans')
            return trans
            
    
    def processEval(self, data):
        evalList = ['align', 'text_align']
        for evalItem in evalList:
            if data.has_key(evalItem):
                data[evalItem] = eval(data[evalItem])
        
      
    
    def processGeom(self, data, geomKey):
        if data.has_key(geomKey):
            geom_data = data[geomKey]
            geom = getGeom(geom_data['MapPath'], *geom_data['ImageNames'])            
            data[geomKey] = geom
            return geom
        
    
    def parseData(self, data):
        for key, value in data.items(): 
            if isinstance(value, list):
                data[key] = tuple(value)
        
