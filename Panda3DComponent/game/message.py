'''
Created on Mar 27, 2013

@author: anhsaodem
'''
from utility.pritority import Priority
from utility import instruction
import datetime

class GameMessage():
    def __init__(self, fromTag, toTagList, info):
        self.fromTag = fromTag
        self.toTagList = toTagList
        self.info = info
        
class GameQuest():
    Done = 'Quest Completed'
    Doing = 'Quest Doing'
    
    def __init__(self, name, *messages):
        self.name = name
        self.messages = [].extend(messages)        
        for mess in self.messages:
            mess.status = GameQuest.Doing
        
    def appendMess(self, message):
        self.messages.append(message)
        
    def isFinish(self):
        for mess in self.messages:
            if not mess.status == GameQuest.Done:
                return False
        return True
    
    def getDoneList(self):
        doneLst = []
        for mess in self.messages:            
            doneLst.append(mess.status == GameQuest.Done)
        return doneLst
    
    def checkMessDone(self, message):
        for mess in self.messages:
            if mess.info == message.info and\
                mess.fromTag == message.fromTag and\
                mess.toTagList == message.toTagList:
                mess.status = GameQuest.Done
    
""" Why we don't define a quest under a script file (scripting) ??"""
class GameQuestV2():
    def __init__(self, name, info, messages, setStatusWhenDone = {}):
        """
        Create a Quest. This Quest maybe is a single quest or a parent quest which contain children quest. (composite pattern)
        name : name of quest.
        info : info of quest.
        childQuest : is a list of GameQuestV2 (children) {Quest1_Name : [Quest1,[dependent]], Quest2_Name : [Quest2, [dependent]]}
        message: {GameMessage1 : [current_number, require_number, TextInfo], GameMessage2 : [current_number, require_number, TextInfo]}
        example: {GameMessage1 : [0, 3, "You kill @#current_number#@ / @#require_number#@ Demon A"], GameMessage1 : [0 , 1, "meet the Master"]} 
        SetStatusWhenDone: When QuestDone, GameQuest Will require QuestManager set someStatus to Player
        """
        self.name = name
        self.info = info
        self.messages = messages 
        self.finishMessages = []
        self.setStatusWhenDone = setStatusWhenDone
        self.childQuest = {} #childQuest 
        
    def addFinishMessage(self, message):       
        self.finishMessages.append(message)
        
    def editMess(self, gameMessage, currentNumber, requireNumber, textInfo):
        """
        Append / edit a requirement in Game.
        gameMessage: gameMessage template want to capture.
        currentNumber: set start value.
        requireNumber : how many messages for done.
        textInfo : info want to show for game player.
        """
        self.messages[gameMessage] = [currentNumber, requireNumber, textInfo]
         
    def findQuest(self, nameOfQuest):
        """
        Find a Quest in current Quest and child Quest (loop to the end).
        nameOfQuets: Push Name Of Quest 
        return a List of Quest have match name
        """
        result = []
        if self.name == nameOfQuest:
            result.append(self)
        
        for questName in self.childQuest.keys():
            if questName == nameOfQuest:
                result.append(self.childQuest[questName][0])
        
        return result
            
#     def removeChild(self, nameOfQuest):
#         lstDelete
        
        
        
                
    def addChildQuest(self, gameQuestV2, dependentList = []):
        self.childQuest[gameQuestV2.name] = [gameQuestV2, dependentList]
        
    def isFinish(self):
        """
        check finish state. return True or False
        """
        for value in self.messages.values():
            if value[0] < value[1]:
                return False
            
        for cQuest in self.childQuest.values():
            if not cQuest[0].isFinish():
                return False
            else:
                # use  self.childQuest.pop(cQeust[0].name)   instead two lines below.
                self.childQuest[cQuest[0].name] = '' # Don't delete root
                del self.childQuest[cQuest[0].name]
        
        return True
        
    def getQuestStatistic(self):
        """
        return statistic of quest in a List
        Example : ['You kill 2 / 3 Demon A', 'meet the master']            
        """
        result = []
        for value in self.messages.values():
            result.append(value[2].replace("@#current_number#@", str(value[0])).replace("@#require_number#@", str(value[1])))
        
        for cQuest in self.childQuest.values():
            result.extend(cQuest[0].getQuestStatistic())
        return result
    
    def update(self, message):
        """Update quest with list of finish quest"""
        finishes = []
        #update for this quest
        for key, value in self.messages.items():
            if message.info == key.info and\
                message.fromTag == key.fromTag and\
                message.toTagList == key.toTagList:         # if the incoming message is exactly                 
                value[0] += 1
                print "receive message for quest increase value:", message, value[0]
        
        #Update for childQuest
        for cQuest in self.childQuest.values():
            isActive = True
            for dependent in cQuest[1]:
                if self.childQuest.has_key(dependent):
                    isActive = False
                    break
            if isActive:
                finishes.extend(cQuest[0].updateQuest(message))
        if self.isFinish():
            finishes.append(self)    
        return finishes
        
    def updateQuest(self, message):
        """
        update Quest with message 
        """
        #update for this quest
        for key, value in self.messages.items():
            if message.info == key.info and\
                message.fromTag == key.fromTag and\
                message.toTagList == key.toTagList:         # if the incoming message is exactly 
                value[0] += 1
        
        #Update for childQuest
        for cQuest in self.childQuest.values():
            isActive = True
            for dependent in cQuest[1]:
                if self.childQuest.has_key(dependent):
                    isActive = False
                    break
            if isActive:
                cQuest[0].updateQuest(message)
                
        return self.isFinish()
   
        
class QuestManager():
    def __init__(self, questDict, debug=True):
        """
            QuestManager help player can do multiQuest / time
            It help us check the dependent of quests in list.
            Example:
                Meet Master to receive Quest --> Do Quest (Q1) --> Meet Master(Q2).
                Q2 depend on Q1 and It's only active when Q1 done
                
            QuestDict Template:
                {NameOfQuest1: Quest1_Object, NameOfQuest2: Quest2_Object}
        """
        self.quests = questDict
        self.statisticText = []
        self.congratulation = {}
        self.debug = debug
        if self.debug:
            self.screenUpdate()
        #Show Quest to screen.
        
    def screenUpdate(self):
        self.count = 0
        for instr in self.statisticText:
            instr.destroy()
        
        for startTime in self.congratulation.keys():
            now = datetime.datetime.now()
            if (now - startTime).seconds > 5:
                self.congratulation[startTime].destroy()
                del self.congratulation[startTime]
        
        for quest in self.quests.values():
            temp = instruction.addText(0.95 - self.count, quest.name, changeable=True)
            self.statisticText.append(temp)
            self.count += 0.05
            
            for text in quest.getQuestStatistic():
                temp = instruction.addText(0.95 - self.count, text, changeable=True)
                self.statisticText.append(temp)
                self.count += 0.05
        
    def editQuest(self, gameQuest):
        """
        Add / edit a Quest
        QuestObject : GameQuestV2 type.
        dependent : Name of quest that must be done before this quest is active.
        Example : addQuest(gameQuest, ["Kill 100 A demon", "Kill 200 B demon"])
        """
        self.quests[gameQuest.name] = gameQuest
        if self.debug:
            self.screenUpdate()
   
    
    def removeQuest(self, nameOfQuest):
        # use pop() function of dictionary
        self.quests[nameOfQuest] = '' #don't delete root
        del self.quests[nameOfQuest]
        if self.debug:
            self.screenUpdate()
        
    
    def findQuest(self, questName):
        """
        input: Name of Quest
        return : List of Quest object match with name
        """
        result = []
        for quest in self.quests.keys():
            if questName == quest:
                result.append(self.quests[quest])
        
        for quest in self.quests.values():
            result.extend(quest.findQuest(questName))
        
        return result
    
        
    def updateQuest(self, message, player):
        for quest in self.quests.values():
            #check Quest is active (no dependent)
#             isActive = True
#             for dependent in quest[1]:
#                 if self.quests.has_key(dependent):
#                     isActive = False
#             if isActive:
            isFinish = quest.updateQuest(message)
            if isFinish:         # Update and check is It finish
                if self.debug:
                    temp = instruction.addText( 0.5 - 0.05 * len(self.congratulation), "Congratulation !! The " + quest.name + " is completed", changeable=True)
                    self.congratulation[datetime.datetime.now()] = temp
                for key, value in quest.setStatusWhenDone.items():
                    player.statusFlag[key] = value
                self.removeQuest(quest.name)
                
        if self.debug:
            self.screenUpdate()
    
    def clearInstruction(self):
        for key in self.congratulation.keys():
            self.congratulation[key].destroy()
            del self.congratulation[key]

class QuestDispatcher():
    def __init__(self, quests = {}):
        """
        Quest = {Quest1: require1, Quest2: require2, ...}
        Require is a condition for active the quest. Usually use Player status to make a require.
        Example - Use Player Status : 
        Requirement 1 : 
        {
            'keyOfStatus': 'StatusValue',
            'Quest3': 'done',
            'Quest2': 'done',
        }
        Require include player's status not have any key like questName. #(A Quest done will be loop)
        """
        self.quests = quests
        self.currentQuests = []
    
    def addQuest(self, require, quest):
        """
        Require : is status of Player to Active Quest
        Ex: 
        {"Quest1":"Done"}
        quest : GameQuestObject
        """
        self.quests[quest] = require
        
    
    def getQuestValidWithStatus(self, playerStatus):
        questValid = []
        for quest, require in self.quests.items():
            for key in require.keys():
                print "!@!@!@!Quest Check ", quest.name, "Require ", require, "player status ", playerStatus
                if not playerStatus.has_key(key) or playerStatus[key] != require[key]:
                    break
                questValid.append(quest)
        print "VALID QUEST", questValid
        return questValid
    
    
    
    
    
###
### Quest Deliver
###

class QuestDeliver():    
    allquest = {} # Quests of all deliver in game
    
    def __init__(self):                
        self.quests = {}
        self.dependents = []# Depedent quests from another deliver
        self.requires = {} # Require attribute for GameObject which will receive quests
        self.refreshRequire()
        QuestDeliver.allquest.update(self.quests)
        
    def refreshRequire(self):
        for quest in self.quests.values():
            if not self.requires.has_key(quest.name):
                self.requires[quest.name] = {}
        
    def add(self, quest, require_attributes = {}):
        print "add quest"
        self.quests[quest.name] = quest
        self.__addRequire(quest, require_attributes)
        QuestDeliver.allquest[quest.name] = quest
        print "add global quest", quest.name, QuestDeliver.allquest

    def __addRequire(self, quest, require_attributes):
        if len(quest.childQuest) > 0:
            for q in quest.childQuest.values():
                self.__addRequire(q[0], require_attributes)
        else:
            self.requires[quest.name] = require_attributes
        
    def get(self, name):
        if self.quests.has_key(name):
            return self.quests[name]
        for quest in self.quests.values():
            q = self.__getQuest(quest, name)
            if q:
                return q

    def __getQuest(self, quest, name):
        if len(quest.childQuest) > 0:
            if name in quest.childQuest.keys():
                return quest.childQuest[name][0]
            for q in quest.childQuest.values():
                self.__getQuest(q[0], name)
        
    def addDependent(self, dependQuestName):   
        """
        Add depedent quests from another deliver
        """        
        self.dependents.append(dependQuestName)
        
    
    @staticmethod
    def getQuest(name):
        if QuestDeliver.allquest.has_key(name):
            return QuestDeliver.allquest[name]
        
    def getAvaiQuests(self, receiver, attributes = {}):
        avaiQuests = []
        finishedQuests = receiver.finishes
        # Check depent quests from another deliver
        for dependQuest in self.dependents:
            quest = QuestDeliver.getQuest(dependQuest)
            if not quest or not dependQuest in finishedQuests:
                print "don't have depedent quests"
                return avaiQuests
        # Check hierachy quests from this deliver
        for quest in self.quests.values():
            avaiQuests.extend(self._getAvailable(quest, receiver, attributes))
        return avaiQuests
    
    def _getAvailable(self, quest, receiver, attributes = {}):
        finishedQuests = receiver.finishes
        avais = []
        if len(quest.childQuest) > 0:
            for child in quest.childQuest.values():                   
                avais.extend(self._getAvailable(child[0], receiver))
            # Childs all finish
            if len(avais) == 0:
                if quest.name not in finishedQuests.keys():
                    avais.append(quest) 
        else:
            if quest.name not in finishedQuests.keys():
                # Check GameObject attributes with require attribute for this quest
                require_attribs = self.requires[quest.name]
                isAvai = True
                for name, value in require_attribs.items():
                    if not (name in attributes.keys() and attributes[name] == value):
                        isAvai = False
                        print "don't have require"
                        break
                if isAvai == True:
                    avais.append(quest)
        return avais

class QuestDeliverReturn(QuestDeliver):                
    def getReturnQuest(self, receiver):
        readyFinishes = receiver.readyFinishes
        for name in readyFinishes.keys():
            quest = self.get(name)         
            if quest:
                return quest
    
    def _getAvailable(self, quest, receiver, attributes = {}):
        finishedQuests = receiver.finishes
        currentQuests = receiver.currents
        avais = []
        if len(quest.childQuest) > 0:
            for child in quest.childQuest.values():                   
                avais.extend(self._getAvailable(child[0], receiver))
            # Childs all finish
            if len(avais) == 0:
                if quest.name not in finishedQuests.keys() and quest.name not in currentQuests.keys():
                    avais.append(quest) 
        else:
            if quest.name not in finishedQuests.keys():
                # Check GameObject attributes with require attribute for this quest
                require_attribs = self.requires[quest.name]
                isAvai = True
                for name, value in require_attribs.items():
                    if not (name in attributes.keys() and attributes[name] == value):
                        isAvai = False
                        print "don't have require"
                        break
                if isAvai == True:
                    avais.append(quest)
        return avais
        

###
### Quest Receiver
###

class QuestReceiver():    
    allcurrents = {}
    
    def __init__(self):
        self.currents = {}
        self.finishes = {}
    
    def isDoing(self, quest_name):
        return self.currents.has_key(quest_name)
    
    def add(self, quest):
        self.currents[quest.name] = quest
        QuestReceiver.allcurrents[quest.name] = quest
        
    def finish(self, quest):
        if isinstance(quest, str):
            quest = self.currents[quest]
        self.currents.pop(quest.name)
        QuestReceiver.allcurrents.pop(quest.name)
        self.finishes[quest.name] = quest
        for finishMess in quest.finishMessages:
            screen.dispatch(finishMess)
        
    def update(self, message):
        process = False
        for quest in self.quests.values():
            finishQuests = quest.update(message)
            for q in finishQuests:
                self.finish(q)
            process = len(finishQuests) > 0
        return process
                
    @staticmethod
    def getCurrent(self, name):
        if QuestReceiver.allcurrents.has_key(name):
            return QuestReceiver.allcurrents[name]

class QuestReceiverReturn(QuestReceiver):
    def __init__(self):
        QuestReceiver.__init__(self)
        self.readyFinishes = {}
        
    def readyFinish(self, quest):
        if isinstance(quest, str):
            quest = self.currents[quest]
        self.readyFinishes[quest.name] = quest
        
    def finish(self, quest):
        if isinstance(quest, str):
            quest = self.currents[quest]
        if self.currents.has_key(quest.name):
            self.currents.pop(quest.name)
        if self.readyFinishes.has_key(quest.name):
            self.readyFinishes.pop(quest.name)
        self.finishes[quest.name] = quest
        for finishMess in quest.finishMessages:
            screen.dispatch(finishMess)
        
        
    def update(self, message):
        process = False
        for quest in self.currents.values():
            finishQuests = quest.update(message)
            for q in finishQuests:
                self.readyFinish(q)
            for q in self.readyFinishes.values():
                if hasattr(q, 'returnMessage') and q.returnMessage:
                    if q.updateReturn(message):
                        print "return finish"                    
                        self.finish(q)
                        process = True
                else:
                    print "normal finish"
                    self.finish(q)
                    process = True
        return process
 
class GameQuestReturn(GameQuestV2):   
    def __init__(self, name, info, messages, return_mess=None, setStatusWhenDone={}):
        GameQuestV2.__init__(self, name, info, messages, setStatusWhenDone=setStatusWhenDone)
        self.returnMessage = return_mess
        
    def updateReturn(self, message):
        #update for this quest        
        if self.returnMessage.info == message.info and\
            self.returnMessage.fromTag == message.fromTag and\
            self.returnMessage.toTagList == message.toTagList:         # if the incoming message is exactly                 
            return True
        
        
    
        
    
        
class MessageDispatcher():
    def __init__(self):
        self.tags = {}  
        self.receivedObjects = []  
        
    def addObject(self, gameObject):
        for tag in gameObject.tags:
            if tag not in self.tags:
                self.tags[tag] = {}
            self.tags[tag][gameObject.getName()] = gameObject
            
        
    def removeObject(self, gameObject):
        for tag in gameObject.tags:
            if self.tags[tag].has_key(gameObject.getName()):
                self.tags[tag][gameObject.getName()] = gameObject    
    
    
    def dispatch(self, gameMessage):        
        tagList = gameMessage.toTagList
        """  receivedObjects : one object in many tags
        """
        receivedObjects = []
        for tag in tagList:
            if self.tags.has_key(tag):                
                for gameObj in self.tags[tag].values():
                    if gameObj not in receivedObjects:         
                        gameObj.eventQueue.append(gameMessage)
                        receivedObjects.append(gameObj)
        print "From:", gameMessage.fromTag, "To:", gameMessage.toTagList, "info: ", gameMessage.info
        print "RECEIVED OBJECT : ", receivedObjects

