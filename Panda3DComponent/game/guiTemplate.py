from game.factory import GuiFactory, MapManager
from direct.interval.IntervalGlobal import LerpScaleInterval, Sequence, Func
from panda3d.core import TransparencyAttrib
from game.map import *


class DialogBox():
    def __init__(self, dialogName, title, content, messagePostBack2TagList):
        """
        Create a dialog with:
        dialog name = dialogName
        title = title
        content = content
        """
        self.frame = {'dialog': False}
        self.dialogName = dialogName
        #content process Add \n
        self.content = ""
        
        contentLen = len(content)
        charPerLine = 40 # max 200 char per line
        words = content.split(' ')
        lines = []
        currentLine = ""
        for word in words:
            if word == "\n":
                lines.append(currentLine)
                currentLine = ""
            elif len(currentLine) + len(word) < charPerLine:
                currentLine += ' ' + word
            else:
                lines.append(currentLine)
                currentLine = word
        lines.append(currentLine) # Append final line
        
        for line in lines:
            self.content += line + "\n"
            
        print "lines ", lines
        print "CONTENTS", self.content
        
        self.template = {
            "Direct": {    
                dialogName : {
                    "inherit" : "map/ben_game/default_gui/model_iron_dialog.json",
                    "childs" : {
                        "options_title": {
                            "type" : "OnscreenText",
                            "text" : title,
                            "fg" : [1, 1, 1, 1],
                            "scale" : [0.015, 0.040],
                            "pos" : [-0.15, 0.285]
                        },
                        "title_seperator": {
                            "type" : "OnscreenImage",
                            "image" : "image/ben_image/white_horizon_seperator.png",
                            "scale" : [0.15, 1, 0.001],
                            "pos" : [0, 0, 0.24]
                        },
                        "loadImage" : {        
                            "type" : "OnscreenImage",
                            "scale" : [0.05, 0.05, 0.2],
                            "pos" : [-0.12, 0, 0.03],
                            "image" : "image/ben_image/QuestImage/Quest1.png"
                        },
                        "ContentText" :{
                            "type" : "OnscreenText",
                            "text" : self.content,
                            "fg" : [1, 1, 1, 1],
                            "scale" : [0.012, 0.030],
                            "pos" : [0.06, 0.185]
                        },
                        "OK_button" : {
                            "inherit": "map/ben_game/default_gui/button_black.json",
                            "text" : "OK",
                            "text_scale": [0.06, 0.17],
                            "scale": 0.15,
                            "pos": [0.05, 0, -0.3],
                            "geom_scale" : [0.4, 1, 0.5],
                            "command" : ["script/ben_script/playscreen.py", "buttonClick"],
                            "extraArgs" : ['OK_button', messagePostBack2TagList]
                        },
                        "close_button" : {
                            "inherit": "map/ben_game/default_gui/button_black.json",
                            "text" : "Close",
                            "text_scale": [0.06, 0.17],
                            "scale": 0.15,
                            "pos": [0.12, 0, -0.3],
                            "geom_scale" : [0.4, 1, 0.5],
                            "command" : ["script/ben_script/playscreen.py", "buttonClick"],
                            "extraArgs" : ['close_button', messagePostBack2TagList]
                        }
                    }
                }
            }    
        }
        
    def openDialog(self):
        gui = Map.current.gui           # Call GUI with current Screen
        self.dialogGui = GuiFactory.product(data=self.template) # Create Gui from template
        self.dialogGui.startScripts()   # Call script
        self.frame['dialog'] = True     # Mark Dialog in Open
        gui.updateGui(self.dialogGui)   # Update GUI on screen
        self.dialog_frame = gui.getGui(self.dialogName)      # Get Gui from GUI manager
        self.dialog_frame.setTransparency(TransparencyAttrib.MAlpha) # Make it transparent
        self.dialog_frame.show()                        #Show it
        current_scale = self.dialog_frame.getScale()    #get scale for animation
        # Animation                    # It will bigger in 0.5 second from scale 0.1
        bigger = LerpScaleInterval(self.dialog_frame, 0.2, current_scale, (0.1, 0.1, 0.1))
        bigger.start()
    
    def closeDialog(self):
        gui = Map.current.gui
        dialog_frame = gui.getGui(self.dialogName)          # Get dialog
        current_scale = dialog_frame.getScale()             # Animation repair
        smaller = LerpScaleInterval(dialog_frame, 0.2, (0.1, 0.1, 0.1), current_scale)
        fade = Sequence(smaller,
                        Func(self.remove_options_frame)
                        )
        fade.start()
        
    def remove_options_frame(self):
        gui = Map.current.gui
        options_frame = gui.getGui(self.dialogName)
        options_frame.cleanup()
        gui.removeGui(self.dialogName)    
        self.frame['options'] = False      
