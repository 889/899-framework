'''Now support file ext: .xml
   This module output for app (in RAM) : dict
                         
   How To Expand code:
       - Inheritance from FileIO and define readFile() and writeFile() method
       - Output of readFile() always dictionary 
       - writeFile() doesn't need output
       - filePath attribute is the path to resource file.    
'''

from xml.dom import minidom
from xml.dom.minidom import Document, Node

class FileIO(object):
    '''Abstract class for game storage.
       Ability: 
       - read from file to dict               
       - write dict to file.
    '''
    def __init__(self, filePath):
        if self.__class__ == FileIO:
            raise Exception("FileIO is abstract class. You can't use.")   
        self.filePath = filePath
    
    def readFile(self):
        pass
        
    def writeFile(self, data):
        pass
    

class XmlIO(FileIO):
    '''XmlIO for xml file storage.
       
       How To Use:
       - Create XmlIO with filePath to xml file
       - Then, use method write or read is up to you
       - If you want interact with other file you can change filePath attribute
       
    '''
    
    def readFile(self):
        '''Read data from xml file.
        
        Return: dictionary which parse from xml file.
        
        '''
        dom = minidom.parse(self.filePath)
        ret = self.__loadChildAndSub(dom)        
        return ret
        
    def writeFile(self, data):
        '''Write data from xml file. 
        Data is dictionary whose syntax like readFile() return.
        
        Return: None.
        
        '''
        document = Document()
        self.__appendChildAndSub(document, document, data)
        xmlString = document.toprettyxml(indent="  ")
        print xmlString      
        out = open(self.filePath, 'w')
        out.write(xmlString)        
        
    def getAllElementsTextByTagName(self, tagname):
        dom = minidom.parse(self.filePath)
        ret = self.getChildElementsByTagName(dom, tagname)
        return ret
    
    def getChildElementsByTagName(self, node, tagname):  
        ret = []
        attrs = [attr for attr in node.childNodes if attr.nodeType == attr.ELEMENT_NODE]
        for attr in attrs:
            if self.__countChild(attr) > 0:
                elements = self.getChildElementsByTagName(attr, tagname)
                ret.extend(elements)
                nodelist = attr.getElementsByTagName(tagname)
                if len(nodelist) > 0:
                    text = self.__getText(nodelist[0])
                    if text not in ret:
                        ret.append(text)
        return ret

    def __getText(self, element):
        '''Get Text inside Node, or called Value of Node.
        
        Params:
            - element: Element() in minidom module of python. 
            It represent for tag in xml file.
            
        Return: text inside node
        
        '''
        rc = []
        for node in element.childNodes:        
            if node.nodeType == node.TEXT_NODE:            
                rc.append(node.data)       
        return ''.join(rc).strip()
    
    def __countChild(self, element):    
        '''Count number of child inside element param.
        
        Params:
            - element: Element() in minidom module of python. 
            It represent for tag in xml file.            
        
        Return: text inside node
        
        '''
        if element.nodeType == element.ELEMENT_NODE:
            return (element.childNodes.length - 1) / 2
        return 0  
    
    def __appendChildAndSub(self, doc, parent, dictData):
        '''Append child and sub to 'doc' param.
        
        Params:
            - doc: Document() in minidom module of python. 
            It represent for xml document.
            
            - parent: Element() in minidom module of python.
            
            - dictData: dictionary of data for parent tag.      
        
        Return: whole xml element will add to 'doc' param.
        
        '''
        assert isinstance(dictData, dict)
        assert isinstance(doc, Document)
        assert isinstance(parent, Node)
        for key, val in dictData.items():
            if isinstance(val, dict):
                # Tag have child
                if val.has_key('HaveId'):                    
                    name = parent.tagName[0:-1]
                    element = doc.createElement(name)
                    element.setAttribute("Id", key)
                else:
                    element = doc.createElement(key)
                parent.appendChild(element)                
                self.__appendChildAndSub(doc, element, val)
            else:
                # Tag have text value             
                if key != 'HaveId':   
                    element = doc.createElement(key)
                    eText = doc.createTextNode(str(val))
                    element.appendChild(eText)
                    parent.appendChild(element)
    
    def __loadChildAndSub(self, element):
        '''Load child and sub from doc element.
        
        Params:
            - element: Element() in minidom module of python.
            
        Return: dictionary of data.
        
        '''
        ret = {}     
        attrs = [attr for attr in element.childNodes if attr.nodeType == attr.ELEMENT_NODE]
        for attr in attrs:
            if self.__countChild(attr) > 0:
                idAttr = attr.getAttribute("Id")                 
                if len(idAttr) > 0:
                    content = self.__loadChildAndSub(attr)
                    content['HaveId'] = 'True'
                    ret.update({idAttr: content})
                else:   
                    ret.update({attr.tagName: self.__loadChildAndSub(attr)})
            else:
                ret.update({attr.tagName: self.__getText(attr)})
        return ret
    
    
if __name__ == '__main__':    
    xmlIn = XmlIO('map_1.xml')
    data = xmlIn.readFile()
    
    xmlOut = XmlIO('map_3.xml')
    xmlOut.writeFile(data)
    
    xmlTest = XmlIO('map_3.xml')
    xmlTest.readFile()