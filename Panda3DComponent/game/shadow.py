from game.baseobject import GameObject
from panda3d.core import WindowProperties, FrameBufferProperties, GraphicsPipe, Texture, GraphicsOutput, Vec4, OrthographicLens 
from panda3d.core import BitMask32, NodePath, PandaNode, Shader, RenderState, ShaderAttrib, ColorWriteAttrib, CullFaceAttrib
from panda3d.core import Point3, Vec3, Camera
import math
import sys

class ShadowFactory():
    '''Factory pattern: use for creating model in this module.'''
    @staticmethod
    def createShadow(shadowType, *args, **kwargs):
        light = getattr(sys.modules[__name__], shadowType)(*args, **kwargs)
        return light
    
class ShadowBase(GameObject):
    MIN_SORT = -60
    MIN_CAM_MASK = 64    
    
    def __init__(self, light, nodeRoot):
        self.npRoot = render
        self.setLight(light)
        self.shaderInput = {}
        self.pos = Vec3(0, 0, 0)
        self.near = 1
        self.far = 100
        self.hpr = Vec3(0, 0, 0)
        
    def setLight(self, light):
        self.light = light
        self.setPos(light.getPos())
        self.setHpr(light.getHpr())
    
    def setHpr(self, hpr):
        self.hpr = hpr
        
    def getHpr(self, hpr):
        return self.hpr
    
    def setPos(self, pos):
        self.pos = pos
        
    def getPos(self):
        return self.pos
    
    def setNear(self, near):
        self.near = near
        
    def setFar(self, far):
        self.far = far
        
    def getNear(self):
        return self.near
    
    def getFar(self):
        return self.far
    
    def update(self):
        if self.light:            
            self.setPos(self.light.getPos())
#            print "Set POS ", self.getPos()
#            self.setPos(Vec3(0, 128, 80))
#            self.setHpr(base.cam.getHpr())
            self.setHpr(self.light.getHpr())
            alpha = self.light.getP() + 90
            distance = 5
            filmSize = 10
            heightZ = distance * math.cos(alpha * math.pi / 180)
            widthX = distance * math.sin(alpha * math.pi / 180)
            # For reality Use replace Vec3(20, 20, 10) by base.camera.getPos()
            self.setPos(Vec3(20, 20, 5) + (widthX,filmSize/2 - 2,heightZ))

#            alpha = self.light.getP() + 90
#            distance = 10
#            heightZ = distance * math.cos(alpha * math.pi / 180)
#            widthX = distance * math.sin(alpha * math.pi / 180)
#            self.setPos(Vec3(20, 20, 10) + (widthX,0,heightZ))

    
    def calculateSort(self, index):
        return self.MIN_SORT + index
    
    def calCameraMask(self, index):
        return self.MIN_CAM_MASK * ( 2**index)
    
    def createShadowBuffer(self, index=0, resolution=[1024, 1024]):
        winprops = WindowProperties.size(*resolution)
        props = FrameBufferProperties()
        props.setRgbColor(1)
        props.setAlphaBits(1)
        props.setDepthBits(1)        
        iSort = self.calculateSort(index)
        shadowBuffer = base.graphicsEngine.makeOutput(
                 base.pipe, "shadow buffer %d" % index, iSort,
                 props, winprops,
                 GraphicsPipe.BFRefuseWindow,
                 base.win.getGsg(), base.win)
        
#        winprops = WindowProperties()    
#        winprops.setSize( *resolution )
#        fbprops = FrameBufferProperties()
#        fbprops.setColorBits(1)
#        fbprops.setAlphaBits(1)
#        fbprops.setStencilBits(0)
#        fbprops.setDepthBits( 1 )
#        shadowBuffer = base.graphicsEngine.makeOutput(base.pipe, "shadow buffer %d" % index, iSort, fbprops, winprops, 
#                                                   GraphicsPipe.BFRefuseWindow, 
#                                                   base.win.getGsg(), base.win)   
             
        return shadowBuffer
    
    def createShadowCamera(self, shadowBuffer, rootNode, index=0):
        lens = OrthographicLens()
        lens.setFilmSize(10, 10)
        lens.setNearFar(1,10)
        iSort = self.calculateSort(index)
        npCam = base.makeCamera( shadowBuffer, sort = iSort, camName = "shadow camera %d" % index)
        npCam.node().setLens( lens )        
        npCam.reparentTo(render)
        if rootNode:
            npCam.node().setScene( rootNode )
        iCameraMask = self.calCameraMask(index)
        if iCameraMask:
            npCam.node().setCameraMask( BitMask32(iCameraMask) )
            # hide chanel for camera
            #npCam.node().setCameraMask(BitMask32.bit(4))
#        npCam.node().showFrustum()                    
        return npCam
    
    def createShadowTexture(self, shadowBuffer):
        depthmap = Texture()
        shadowBuffer.addRenderTexture(depthmap, GraphicsOutput.RTMCopyTexture, GraphicsOutput.RTPDepth)
#        if (base.win.getGsg().getSupportsShadowFilter()):
#            depthmap.setMinfilter(Texture.FTShadow)
#            depthmap.setMagfilter(Texture.FTShadow)       
#        depthmap.setBorderColor(Vec4(1,1,1,1))
#        depthmap.setWrapU(Texture.WMBorderColor)
#        depthmap.setWrapV(Texture.WMBorderColor)
                
        return depthmap    
    
    def createShadowShader(self, shaderFile, shaderInputs):        
        # Shader input
#        mci = NodePath(PandaNode("Main Camera Initializer"))
#        mci.setShader(Shader.load(shaderFile))
#           
#        base.camera.node().setInitialState(mci.getState())
        self.shaderInput.update(shaderInputs)
        
        sa = ShaderAttrib.make( )
        sa = sa.setShader(Shader.load(shaderFile))
        
        cam = base.cam.node()        
        cam.setTagStateKey('Shadow')
        cam.setTagState('True', RenderState.make(sa)) 
        
    def applyShadowInputToNode(self, node, **additionInput):
        for key, item in self.shaderInput.items():
            node.setShaderInput(key, item)    
        for key, item in additionInput.items():
            node.setShaderInput(key, item)
                
    def GetFrustumBoundsAtDepthZ(self, fDepth, tupHalfFovRad = None, npCam = None):
        # fHalfHVFovRad =  0.5 * FOV (in radians)   where FOV = (Hor_Fov, Ver_Fov)
        if npCam:
            vb2FovRad = npCam.node().getLens().getFov()
            x = fDepth * math.tan( math.radians( vb2FovRad[0]/2 ) )
            y = fDepth * math.tan( math.radians( vb2FovRad[1]/2 ) )
        else:
            x = fDepth * math.tan( tupHalfFovRad[0] )
            y = fDepth * math.tan( tupHalfFovRad[1] )
        #                ur                            ul                    ll                            lr
        ### Remember y+ is forward or Depth
        return Point3( x, fDepth, y), Point3( -x, fDepth, y), Point3( -x, fDepth, -y), Point3( x, fDepth, -y)
    
    def PSSMSplit(self, iSection, intTotalSections, fNear = None, fFar = None, npCam = None):
        #Returns the z-split depth for PSSM for section i, given intTotalSections        
        if npCam:
            fNear = npCam.node().getNear()
            fFar = npCam.node().getFar()
        return 0.5 * 0.5*( fNear * ((fFar / fNear) ** (1.*iSection/intTotalSections)) + fNear + (fFar - fNear) * (1.*iSection/intTotalSections) )
        
    
class SingleShadow(ShadowBase):
    def __init__(self, *args, **kwargs):
        ShadowBase.__init__(self, *args, **kwargs)
        self.sBuffer = self.createShadowBuffer()
        self.sCam = self.createShadowCamera(self.sBuffer, self.npRoot)         
        self.sDepthTex = self.createShadowTexture(self.sBuffer)
#        self.sCam .node().setInitialState( RenderState.make(CullFaceAttrib.makeReverse(), ColorWriteAttrib.make(ColorWriteAttrib.COff)) )
#        self.sCam .setShaderOff( 999 )
        pushBias = 0.04
        print "use shadow.sha"
        self.createShadowShader("Shader/shadow.sha",
                           {
                            'light': self.sCam,
                            'Ldepthmap': self.sDepthTex,
                            'push': Vec4(pushBias, pushBias, pushBias, 0)                            
                            })

                
    def setPos(self, pos):
        ShadowBase.setPos(self, pos)
        if hasattr(self, 'sCam'):
            self.sCam.setPos(pos)
        
    def setHpr(self, hpr):
        ShadowBase.setHpr(self, hpr)
        if hasattr(self, 'sCam'):
            self.sCam.setHpr(hpr)
                    

class PSSMShadow(ShadowBase):    
    booTexelIncrement = True
    
    def __init__(self, *args, **kwargs):
        ShadowBase.__init__(self, *args, **kwargs)        
        self.npViewCam = base.cam
        # code lui set far                
        base.camLens.setFar(256)        
        self.lstDepthRange = []
        self.lstShadowCam = []
        self.lstShadowBuff = []
        self.lstShadowTex = []
        self.lstShadowSize = [1024, 1024, 1024]
        self.initViewCamInfo()
        self.initShadowBuffTexCam()
        self.vec3LightDir = Vec3(1, 0, 0)        
        
    def initViewCamInfo(self):
        cam =  self.npViewCam
        self.fNear = cam.node().getLens().getNear()
        self.fFar = cam.node().getLens().getFar()
        self.nearUR , self.nearUL, self.nearLL, self.nearLR = self.GetFrustumBoundsAtDepthZ( self.fNear, npCam = self.npViewCam )
        self.farUR ,    self.farUL,   self.farLL,    self.farLR = self.GetFrustumBoundsAtDepthZ( self.fFar, npCam = self.npViewCam )
        
        # Generate depth range list
        self.lstDepthRange.append(self.fNear)
        for i in xrange( len(self.lstShadowSize ) - 1):            
            print "section", i, len(self.lstShadowSize)
            self.lstDepthRange.append( self.PSSMSplit( iSection = i + 1, intTotalSections = len( self.lstShadowSize ),
                                                          fNear = self.fNear, fFar = self.fFar ) )
        
        self.lstDepthRange.append( self.fFar )
        print "range, ", self.lstDepthRange
        # Generate depth bound list
        self.lstDepthBound = [ ( self.nearUR , self.nearUL, self.nearLL, self.nearLR  ) ]
        for i in xrange( len(self.lstDepthRange) -2 ):
            fSplitDepth = self.lstDepthRange[i + 1]
            fUR, fUL, fLL, fLR = self.GetFrustumBoundsAtDepthZ(fSplitDepth, npCam = self.npViewCam)
#            fUL = (self.farUL - self.nearUL)/(self.fFar - self.fNear)*(fSplitDepth - self.fNear)
#            fLL = (self.farLL - self.nearLL)/(self.fFar - self.fNear)*(fSplitDepth - self.fNear)
#            fLR = (self.farLR - self.nearLR)/(self.fFar - self.fNear)*(fSplitDepth - self.fNear)
            self.lstDepthBound.append( (fUR, fUL, fLL, fLR) )        
        self.lstDepthBound.append( ( self.farUR ,    self.farUL,   self.farLL,    self.farLR ) )
        print "bound, ", self.lstDepthBound
        
    def initShadowBuffTexCam(self):
        for index in xrange( len( self.lstShadowSize ) ):
            buff = self.createShadowBuffer(index, resolution=[self.lstShadowSize[index], self.lstShadowSize[index]])
            cam = self.createShadowCamera(buff, self.npRoot, index)
            tex = self.createShadowTexture(buff)
            self.lstShadowBuff.append(buff)
            self.lstShadowCam.append(cam)
            self.lstShadowTex.append(tex)
            pushBias = 0.14
            caminput = "cam%d"%index            
            print "use pssmshadow.sha", self.lstDepthRange[1:4]
            self.createShadowShader("Shader/pssmshadow.sha",
                       {
                        caminput: cam,
                        'Ldepthmap%d'%index: tex,                        
                        'push': Vec4(pushBias, pushBias, pushBias, 0),
                        'LdepthRange': Vec3(*self.lstDepthRange[1:4]),
                        'maincam' : self.npViewCam
                        })
            
        
    def SetShadowCamPosFilmSize(self, iCam):
        centerPoint = self.npRoot.getRelativePoint( self.npViewCam, 
                Vec3( 0, 1.*(self.lstDepthRange[iCam] + self.lstDepthRange[iCam+1])/2, 0) )
#        if iCam ==1 :
#            print "centerPoint"
        ## We temporary set the shadow cam above the frustum center in world space
        
        ## We use the MSDN sample for snapping texel units to try to reduce shimmering
        ## I'm probably doing it incorrectly. oh well!
        fWorldUnitsPerTexel = 1.*(self.lstDepthRange[iCam+1] - self.lstDepthRange[iCam])/self.lstShadowSize[iCam]
        
        npCam = self.lstShadowCam[iCam]
        npCam.setPos( centerPoint + self.vec3LightDir*(self.lstDepthRange[iCam+1] - self.lstDepthRange[iCam]) )
        npCam.lookAt ( centerPoint )
        dNear = 10000
        dFar = -10000
        ## We compute two bounding boxes
        ## One using the normal z/x axis
        ## and the other using a set axis rotated by 45'  We do this instead of computing
        ## the convex hull to figure out which camera orientation is the best.
        x_up_min = 10000
        x_up_max = -10000
        z_up_min = 10000
        z_up_max = -10000
        ## in world space
        ## We take the frustum bounds and convert them into shadow cam space coordinates
        ## Then using the transformed pts to find the bounds for the shadow cam and reshift
        ## the shadowcam to the center of the bounds.
        ## We would do something similiar here if we want to draw even tighter bounds against
        ## objects in world space.    
#        print "list bound", self.lstDepthBound[0]    
        for pt3 in self.lstDepthBound[iCam]:
#            print "pt3", pt3, iCam
            cspt3 = npCam.getRelativePoint( self.npViewCam, pt3 )
            dNear = min( dNear, cspt3[1] )
            dFar = max( dFar, cspt3[1] )
            x_up_max = max( x_up_max, cspt3[0] )
            x_up_min = min( x_up_min, cspt3[0] )
            z_up_max = max( z_up_max, cspt3[2] )
            z_up_min = min( z_up_min, cspt3[2] )
            
        for pt3 in self.lstDepthBound[iCam+1]:
            cspt3 = npCam.getRelativePoint( self.npViewCam, pt3 )
            dNear = min( dNear, cspt3[1] )
            dFar = max( dFar, cspt3[1] )
            x_up_max = max( x_up_max, cspt3[0] )
            x_up_min = min( x_up_min, cspt3[0] )
            z_up_max = max( z_up_max, cspt3[2] )
            z_up_min = min( z_up_min, cspt3[2] )
        
        xOffset = (x_up_max + x_up_min)/2
        zOffset = (z_up_max + z_up_min)/2
        vec2Move = self.npRoot.getRelativeVector( npCam, Vec3( xOffset, 0, zOffset ) )
        npCam.setPos( centerPoint + (self.vec3LightDir*(self.lstDepthRange[iCam+1] - self.lstDepthRange[iCam]) + vec2Move) )
        
        fFilmWidth = (x_up_max - x_up_min)
        fFilmHeight = (z_up_max - z_up_min )
        if False:
            fFilmWidth = math.floor( fFilmWidth/fWorldUnitsPerTexel )*fWorldUnitsPerTexel
            fFilmHeight = math.floor( fFilmHeight/fWorldUnitsPerTexel )*fWorldUnitsPerTexel
        npCam.node().getLens().setFilmSize( fFilmWidth , fFilmHeight)
        npCam.node().getLens().setNearFar( dNear, dFar)
#        print "cam %d" %iCam, "near=", dNear , "far=", dFar, "hpr=", npCam.getHpr(), "pos=", npCam.getPos()
        
    def update(self):    
#        ShadowBase.update(self)
        self.vec3LightDir = -self.npRoot.getRelativeVector(self.light.nodePath,Vec3(0,1,0))
        for i in xrange(len(self.lstShadowCam)):
            self.SetShadowCamPosFilmSize( i )
        
    
    def generatePSSMs(self, ):
        pass
        
    def applyPSSMsToScene(self):
        pass
    
    
    
    