from baseobject import GameObject
from panda3d.core import * 
from game.shadow import SingleShadow, PSSMShadow

class TerrainBase(GameObject):
    def __init__(self, *args, **kwargs):
        self.terrain = kwargs['terrain']
        print "Init terrain", self.terrain
        GameObject.__init__(self, *args, **kwargs)
        # Default settings
#        self.updateAmbientLight(Vec4(0.1, 0.1, 0.1, 1.0))
#        self.updateDirectLight(Vec4(0.7, 0.7, 0.7, 1.0))
#        self.updateLightVector(Vec3(1, 0, 0))
        
    def clone(self):
        clone = GameObject.clone(self)
        clone.setNodePath(clone.terrain.generate())    
        return clone
        
    def start(self):
#        self.myTask = taskMgr.add(self.update, "TerrainUpdate" + self.nodeName)
        pass
    
    def stop(self):
#        taskMgr.remove("TerrainUpdate" + self.nodeName)
        pass
    
    def update(self, task):
        print self.terrain
        self.terrain.update()
        return task.cont
        
    def activeShadow(self, shadow=None):        
        if isinstance(shadow, SingleShadow):
            self.realNP.setTag('Normal', 'Shadow')            
            print "shadow terrain"
        elif isinstance(shadow, PSSMShadow):
            print "pssm shadow terrain"
            self.realNP.setTag('Normal', 'PSSMShadow')     
        else:
            print "shadow terrain normal"   
            self.realNP.setTag('Normal', 'NoShadow')      
                    

from utility import caster
import random
import  sys, os

SPEED = 0.5

# Figure out what directory this program is in.
MYDIR=os.path.abspath(sys.path[0])
MYDIR=Filename.fromOsSpecific(MYDIR).getFullpath()
    
class myGeoMipTerrain(GeoMipTerrain):
    def __init__(self, name):
        GeoMipTerrain.__init__(self, name)        
        
    def update(self):
#        print "Self ", self
#        print self is GeoMipTerrain
#        print "GEOMIPTERRAIN", GeoMipTerrain
        GeoMipTerrain.update(self)
        
    def setMonoTexture(self):
        root = self.getRoot()
        ts = TextureStage('ts')
        tex = loader.loadTexture('textures/land01_tx_512.png')
        root.setTexture(ts, tex)
        
    def _setup_camera(self, shaPath, tagStateName):        
        sa = ShaderAttrib.make( )
        sa = sa.setShader(loader.loadShader(shaPath))   
        
        cam = base.cam.node()
        cam.setLens(base.camLens)
        cam.setTagStateKey('Normal')
        cam.setTagState(tagStateName, RenderState.make(sa)) 
        
        
                
    def setMultiTexture(self, Data, root):
        
        
        for idx in xrange(1, 5):
            key = 'tex{0}Path'.format(idx)
            if Data.has_key(key):
                tex = loader.loadTexture(Data[key])            
                tex.setMinfilter(Texture.FTNearestMipmapLinear)
                tex.setMagfilter(Texture.FTLinear)
                ts = TextureStage('tex{0}'.format(idx))    # stage 0
                root.setTexture(ts, tex)    
            
        # Alpha map
        for idx in xrange(1, 5):
            key = 'alp{0}Path'.format(idx)
            if Data.has_key(key):
                tex = loader.loadTexture(Data[key])            
                tex.setMinfilter(Texture.FTNearestMipmapLinear)
                tex.setMagfilter(Texture.FTLinear)
                ts = TextureStage('alp{0}'.format(idx))    # stage 0
                root.setTexture(ts, tex)        
        # Detail map
        if Data.has_key('detailPath'):        
            tex = loader.loadTexture(Data['detailPath'])            
            tex.setMinfilter(Texture.FTNearestMipmapLinear)
            tex.setMagfilter(Texture.FTLinear)
            ts = TextureStage('detail'.format(idx))    # stage 0
            root.setTexture(ts, tex)  
         
        # Set shaders apply on terrains
        root.setTag("Normal", "NoShadow")
#        root.setTag("Clipped", "True") # temp , remove when fix water.py
        
        # Setup shaders
        shadowPath = 'Shader/shadowterrain.sha'
        pssmShadowPath = 'Shader/pssmshadowterrain.sha'
        if Data.has_key('ShadowShaderPath'):
            shadowPath = Data['ShadowShaderPath']
        if Data.has_key('PSSMShadowShaderPath'):
            shadowPath = Data['PSSMShadowShaderPath']    
        self._setup_camera(Data['shaderPath'], 'NoShadow')
        self._setup_camera(shadowPath, 'Shadow')
        self._setup_camera(pssmShadowPath, 'PSSMShadow')