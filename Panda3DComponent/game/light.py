from baseobject import GameObject
from panda3d.core import DirectionalLight, AmbientLight, Point3, Vec3
import sys
from utility.pritority import Priority
import math, copy
from game.camera import CameraBase

class LightFactory():
    '''Factory pattern: use for creating model in this module.'''
    @staticmethod
    def createLight(lightType, *args, **kwargs):
        light = getattr(sys.modules[__name__], 'LightBase')(*args, **kwargs)
        return light
    
class LightBase(GameObject):
    
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        self.shadow = None    
        self.pssm = []  

        
    def start(self):
        taskMgr.add(self.update, "update light", sort=Priority.Light)
            
    def stop(self):        
        taskMgr.remove("update light")
            
    def update(self, task):
        if self.shadow:
            self.shadow.update()
        if len(self.pssm) > 0:
            self.updatePSSM()
       
        return task.cont
    
    def setPos(self, *args):
        GameObject.setPos(self, *args)
        if self.shadow:
            self.shadow.update()
            
    def setHpr(self, *args):
        GameObject.setHpr(self, *args)
        if self.shadow:
            self.shadow.update()       
        
                
    def applyTo(self, node):    
        node.setLight(self.nodePath)    
        if isinstance(self.nodePath.node(), DirectionalLight):
            node.setShaderInput("direct0", self.nodePath)
        elif isinstance(self.nodePath.node(), AmbientLight):                      
            node.setShaderInput("ambient0", self.nodePath)        

    def applyPSSM(self, nodeRoot, size=3):
        # Initialize main camera info and some attribute for splits frustum  
        self.lstDepthRange = []  
        self.__initViewCamInfo(size)    
        self.pssm = []
        self.pssm.append(self.nodePath)
        self.__setDirectShadow(self.nodePath)
        for idx in range(1, size):            
            # Create sub light
            light = copy.deepcopy(self.nodePath)
            light.reparentTo(nodeRoot)
            # Set up sub light
            self.__setDirectShadow(light, nodeRoot)
            self.pssm.append(light)

    def __setDirectShadow(self, light, nodeRoot=None, filmsize=[15, 15], nearfar=[-20,100], buffersize=[1024, 1024]):
        light.node().setShadowCaster(True, *buffersize)
        #light.node().showFrustum()
        light.node().getLens().setFilmSize(*filmsize)
        light.node().getLens().setNearFar(*nearfar)
        if nodeRoot:
            light.node().setScene(nodeRoot)

    def __initViewCamInfo(self, size=3):
        cam =  base.cam
        base.camLens.setFar(1000)
        self.fNear = cam.node().getLens().getNear()
        self.fFar = cam.node().getLens().getFar()
        self.nearUR , self.nearUL, self.nearLL, self.nearLR = self.__getFrustumBoundsAtDepthZ( self.fNear, npCam = cam )
        self.farUR ,    self.farUL,   self.farLL,    self.farLR = self.__getFrustumBoundsAtDepthZ( self.fFar, npCam = cam )
        
        # Generate depth range list
        self.lstDepthRange.append(self.fNear)
        for i in xrange( size - 1):   
            self.lstDepthRange.append( self.__pssmSplit( iSection = i + 1, intTotalSections = size,
                                                          fNear = self.fNear, fFar = self.fFar ) )
        
        self.lstDepthRange.append( self.fFar )
        
        # Generate depth bound list
        self.lstDepthBound = [ ( self.nearUR , self.nearUL, self.nearLL, self.nearLR  ) ]
        for i in xrange( len(self.lstDepthRange) -2 ):
            fSplitDepth = self.lstDepthRange[i + 1]
            fUR, fUL, fLL, fLR = self.__getFrustumBoundsAtDepthZ(fSplitDepth, npCam = cam)
#            fUL = (self.farUL - self.nearUL)/(self.fFar - self.fNear)*(fSplitDepth - self.fNear)
#            fLL = (self.farLL - self.nearLL)/(self.fFar - self.fNear)*(fSplitDepth - self.fNear)
#            fLR = (self.farLR - self.nearLR)/(self.fFar - self.fNear)*(fSplitDepth - self.fNear)
            self.lstDepthBound.append( (fUR, fUL, fLL, fLR) )        
        self.lstDepthBound.append( ( self.farUR ,    self.farUL,   self.farLL,    self.farLR ) )
        

    def __getFrustumBoundsAtDepthZ(self, fDepth, tupHalfFovRad = None, npCam = None):
        # fHalfHVFovRad =  0.5 * FOV (in radians)   where FOV = (Hor_Fov, Ver_Fov)
        if npCam:
            vb2FovRad = npCam.node().getLens().getFov()
            x = fDepth * math.tan( math.radians( vb2FovRad[0]/2 ) )
            y = fDepth * math.tan( math.radians( vb2FovRad[1]/2 ) )
        else:
            x = fDepth * math.tan( tupHalfFovRad[0] )
            y = fDepth * math.tan( tupHalfFovRad[1] )
        #                ur                            ul                    ll                            lr
        ### Remember y+ is forward or Depth
        return Point3( x, fDepth, y), Point3( -x, fDepth, y), Point3( -x, fDepth, -y), Point3( x, fDepth, -y)
    
    def __pssmSplit(self, iSection, intTotalSections, fNear = None, fFar = None, npCam = None):
        #Returns the z-split depth for PSSM for section i, given intTotalSections        
        if npCam:
            fNear = npCam.node().getNear()
            fFar = npCam.node().getFar()
        return 0.5 * 0.5*( fNear * ((fFar / fNear) ** (1.*iSection/intTotalSections)) + fNear + (fFar - fNear) * (1.*iSection/intTotalSections) )


    def clearPSSM(self):
        if len(self.pssm) > 0:
            for light in self.pssm[1:]:
                light.removeNode()
        self.pssm = []

    def updatePSSM(self):
        self.npViewCam = base.camera

        nodeRoot = screen.current.nodeRoot 
        
        self.vec3LightDir = -nodeRoot.getRelativeVector(self.nodePath, Vec3(0,1,0))            
        for idx, light in enumerate(self.pssm):
            if idx == len(self.pssm) - 1:
                return
            centerPoint = nodeRoot.getRelativePoint( self.npViewCam, 
                Vec3( 0, 1.*(self.lstDepthRange[idx] + self.lstDepthRange[idx+1])/2, 0) )
            
            npCam = self.pssm[idx]
            npCam.setPos( nodeRoot, centerPoint + self.vec3LightDir*(self.lstDepthRange[idx+1] - self.lstDepthRange[idx]) )
            npCam.lookAt ( centerPoint )
            dNear = 100000
            dFar = -100000
            ## We compute two bounding boxes
            ## One using the normal z/x axis
            ## and the other using a set axis rotated by 45'  We do this instead of computing
            ## the convex hull to figure out which camera orientation is the best.
            x_up_min = 100000
            x_up_max = -100000
            z_up_min = 100000
            z_up_max = -100000

            for pt3 in self.lstDepthBound[idx]:
    #            print "pt3", pt3, idx
                cspt3 = npCam.getRelativePoint( self.npViewCam, pt3 )
                dNear = min( dNear, cspt3[1] )
                dFar = max( dFar, cspt3[1] )
                x_up_max = max( x_up_max, cspt3[0] )
                x_up_min = min( x_up_min, cspt3[0] )
                z_up_max = max( z_up_max, cspt3[2] )
                z_up_min = min( z_up_min, cspt3[2] )
                
            for pt3 in self.lstDepthBound[idx+1]:
                cspt3 = npCam.getRelativePoint( self.npViewCam, pt3 )
                dNear = min( dNear, cspt3[1] )
                dFar = max( dFar, cspt3[1] )
                x_up_max = max( x_up_max, cspt3[0] )
                x_up_min = min( x_up_min, cspt3[0] )
                z_up_max = max( z_up_max, cspt3[2] )
                z_up_min = min( z_up_min, cspt3[2] )
            
            xOffset = (x_up_max + x_up_min)/2
            zOffset = (z_up_max + z_up_min)/2
            vec2Move = nodeRoot.getRelativeVector( npCam, Vec3( xOffset, 0, zOffset ) )
            npCam.setPos( nodeRoot, centerPoint + (self.vec3LightDir*(self.lstDepthRange[idx +1] - self.lstDepthRange[idx]) + vec2Move) )
            
            fFilmWidth = (x_up_max - x_up_min)
            fFilmHeight = (z_up_max - z_up_min )
            npCam.node().getLens().setFilmSize( fFilmWidth , fFilmHeight)
            npCam.node().getLens().setNearFar( dNear, dFar)   
            

    
    def getColor(self):
        return self.node().getColor()
    
    def setColor(self, *args):
        self.node().setColor(*args)   
        
    def setShadow(self, shadow):     
        self.shadow = shadow
        
        
        

