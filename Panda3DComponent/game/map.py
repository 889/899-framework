from panda3d.core import Vec3
from panda3d.bullet import *
from game.message import MessageDispatcher
from game.terrain import TerrainBase
from game.gui import GuiManager
from utility.pritority import Priority
from game.input import Pointer

class Map(object):
    lstObjects = []
    current = None
    dispatcher = None
    
    def __init__(self, objects=None, world=None, camera=None):
        if objects:
            self.gameObjects = objects
        else:
            self.gameObjects = {}
        if world:
            self.world = world
        else:
            self.world = BulletWorld()
            self.world.setGravity(Vec3(0, 0, -9.81))
        self.physicObjects = {}
        self.pointer = Pointer()
        self.camera = camera
        self.readyObject = None
        self.inputs = []
        self.gui = None
        self.nodeRoot = None
        self.refreshObjects()
        self.refreshPhysicList()
        self.initDispatcher()
        
    def setGui(self, gui):
        if isinstance(gui, GuiManager):        
            self.gui = gui
        
    def addInput(self, *inputs):
        for inp in inputs:
            if inp not in self.inputs:
                self.inputs.append(inp)
        
    def removeInput(self, *inputs):
        for inp in inputs:
            self.inputs.remove(inp)
        
    def startInput(self):
        for inp in self.inputs:
            inp.start()
            
    def stopInput(self):
        for inp in self.inputs:
            inp.stop()
            
    def refreshPhysicList(self):
        if self.gameObjects:
            for obj in self.getListObjects():
                if obj.physicNode:
                    self.physicObjects[obj.physicNode.getName()] = obj
        
    def refreshObjects(self):
        # Remove NoneType object           
        for items in self.gameObjects.values():
            if isinstance(items, dict):
                for key, item in items.items():
                    if not item:
                        items.pop(key)
        
    def initDispatcher(self):
        self.messDispatcher = MessageDispatcher()
        if self.gameObjects:
            for obj in self.getListObjects():
                self.messDispatcher.addObject(obj)
        
    def active(self):
        Map.lstObjects = self.getListObjects()
        Map.current = self
        Map.dispatcher = self.messDispatcher
        if self.camera:
            self.camera.active(True)
            base.disableMouse()
        else:            
            base.enableMouse()
                
        
    def enableDebugWorld(self, enable):     
        if self.world:
            if enable == True:
                print "enable debug world"  
                debugNode = BulletDebugNode('Debug')
#                debugNode.showWireframe(True)
#                debugNode.showConstraints(False)
#                debugNode.showBoundingBoxes(False)
#                debugNode.showNormals(False)
                self.debugNP = render.attachNewNode(debugNode)
                self.debugNP.hide()
                self.world.setDebugNode(self.debugNP.node())
                base.accept('f1', self.toogleDebugWorld)                    
            else:
                self.world.clearDebugNode()
                if hasattr(self, 'debugNP'):                    
                    self.debugNP.removeNode()
                        
    def toogleDebugWorld(self):
        if self.debugNP.isHidden():
            print "show"
            self.debugNP.show()
        else:
            self.debugNP.hide()
        
    def setWorld(self, world):
        if isinstance(world, BulletWorld):
            self.world = world
        
    def getWorld(self):
        return self.world
    
    def getRoot(self):
        return self.nodeRoot
    
    def setRoot(self, nodeRoot):
        self.nodeRoot = nodeRoot
        
    def getLights(self): 
        self.getGroupObject("Lights")
        
    def getFog(self):
        self.getGroupObject("Fogs")
    
        
    def getGroupObject(self, group_name):
        if self.gameObjects.has_key(group_name):
            return self.gameObjects[group_name].values()
            
    def clear(self):
        self.gameObjects.clear()
        
    def applyToScene(self, scene):       
        for entity in self.getListObjects():
            if entity:
                entity.start()      
                entity.startScripts()
        self.enableDebugWorld(True)
        self.active()
        self.startInput()
        if self.gui:
            self.gui.applyToScene()
        taskMgr.add(self.updateBulletWorld, "update bullet world", sort=Priority.Physic)          
        if self.nodeRoot:
            self.nodeRoot.reparentTo(scene) 
            
    def rejectFromScene(self):
        for entity in self.getListObjects():
            if entity:
                entity.stop()     
                print entity.getName()                   
                entity.stopScripts()
        self.enableDebugWorld(False)
        self.stopInput()
        if self.gui:
            self.gui.rejectFromScene(True)
        taskMgr.remove("update bullet world")
        if self.nodeRoot:
            self.nodeRoot.detachNode()
        
            
    def appendObject(self, prototypeType, obj):     
        objGroup = prototypeType + "s"
        if not self.gameObjects.has_key(objGroup):
            self.gameObjects[objGroup] = {}   
        self.gameObjects[objGroup][obj.getName()] = obj
        self.messDispatcher.addObject(obj)    
        if obj.physicNode:
            self.physicObjects[obj.physicNode.getName()] = obj
            
    def updateObject(self, prototypeType, obj):
        objGroup = prototypeType + "s"
        if not self.gameObjects.has_key(objGroup):
            self.gameObjects[objGroup] = {}   
        self.messDispatcher.addObject(obj)    
        if obj.physicNode:
            self.physicObjects[obj.physicNode.getName()] = obj
            obj.physicNode.reparentTo(self.nodeRoot)
            
        
    def getObject(self, objId):
        for group in self.gameObjects.values():
            if group.has_key(objId):
                return group[objId]

    def getObjectByType(self, prototypeType, objId):        
        group = self.gameObjects[prototypeType+"s"]        
        if group.has_key(objId):
            return group[objId]
        
    def removeObject(self, obj):        
        for group in self.gameObjects.values():
            if group.has_key(obj.getName()):                
                if obj.physicNode:
                    self.removeBulletNode(obj)
                print "stop"
                obj.stop() 
                print "stop script"       
                obj.stopScripts()
                print "pop"
                group.pop(obj.getName())
                print "pop from message dispatcher"
                self.messDispatcher.removeObject(obj)
                print "destroy"
                obj.destroy() 
                return True
    
    def getObjectFromPhysicNode(self, node):
        if node.getName() in self.physicObjects.keys():
            return self.physicObjects[node.getName()]
                        
    def removeBulletNode(self, gameObject):
#         self.physicObjects.pop(gameObject.physicNode.getName())
        print "remove bullet node"
        physicNode = gameObject.physicNode        
        world = self.world
        if isinstance(physicNode.node(), BulletRigidBodyNode):
            world.removeRigidBody(physicNode.node())
        elif isinstance(physicNode.node(), BulletGhostNode):
            world.removeGhost(physicNode.node())
        elif isinstance(physicNode.node(), BulletSoftBodyNode):
            world.removeSoftBody(physicNode.node())
        elif isinstance(physicNode.node(), BulletCharacterControllerNode):
            world.removeCharacter(physicNode.node())
        if hasattr(gameObject, 'vehicle'):
            print "remove vehicle to bullet world"
            world.removeVehicle(gameObject.vehicle)
                
    def replaceObject(self, newObj, oldObj, stop=False):
        for group in self.gameObjects.values():
            if group.has_key(oldObj.getName()):
                print "replace object"
                if oldObj.physicNode:    
                    self.removeBulletNode(oldObj)
                self.messDispatcher.removeObject(oldObj)
                if stop:
                    oldObj.stop()        
                    oldObj.stopScripts()
                oldObj.destroy()
                group.pop(oldObj.getName())                
                oldObj.nodePath.removeNode()                   
                group[newObj.getName()] = newObj
                self.messDispatcher.addObject(newObj) 
                if newObj.physicNode:    
                    self.physicObjects[newObj.physicNode.getName()] = newObj      # Old One Same prototype Object have same physicNode.Name self.physicObjects[newObj.physicNode.getName()] = newObj      
            
    def getListObjects(self):
        objects = []        
        for items in self.gameObjects.values():
            if isinstance(items, dict):
                for item in items.values():
                    objects.append(item)
        return objects
    
    def useCommonShader(self):
        if self.gameObjects.has_key('Terrains'):
            for obj in self.gameObjects['Terrains'].values():
                if isinstance(obj, TerrainBase):
                    terrain = obj
                    terrain.activeShadow(None)
                
    def useShadowShader(self, shadow):
        # apply to all game object
        root = self.nodeRoot
        root.setTag('Shadow', 'True')
        shadow.applyShadowInputToNode(root)
        # change another shader for terrain   
#         for obj in self.gameObjects['Terrains'].values():
#             if isinstance(obj, TerrainBase):
#                 terrain = obj
#                 terrain.activeShadow(shadow)
#                 shadow.applyShadowInputToNode(terrain.nodePath, scale=terrain.nodePath.getScale())
        
    def printPrettyTree(self):
        print "map"
        indent = r"    %s"
        for key, item in self.gameObjects.items():
            print indent % key
            if isinstance(item, dict):                
                for name in item.keys():
                    print indent % indent % name
    
    @staticmethod
    def dispatch(mess):
        if Map.dispatcher:
            Map.dispatcher.dispatch(mess)
            
    @staticmethod
    def collisionPair(object1, object2):
        if Map.current:            
            world = Map.current.world
            result = world.contactTestPair(object1.physicNode.node(), object2.physicNode.node())
            isCollision = result.getNumContacts() > 0
            if isCollision:
                return result.getContacts()
                
                    
    # Update
    def updateBulletWorld(self, task):
        dt = globalClock.getDt()
        self.world.doPhysics(dt)
        return task.cont
                    

class WaitMap(Map):  
    def __init__(self, objects=None, world=None, camera=None):
        Map.__init__(self, objects, world, camera)
    
    def belongTo(self, other_map):
        other_map.waitMap = self
        self.otherMap = other_map        
        
    def update(self):
        if hasattr(self.otherMap, 'percent'):
            print self.otherMap.percent
        else:
            print "no setup percent attribute for map"
            


    
    
