from panda3d.core import NodePath, Plane, PlaneNode, Vec3, Vec4, Point3, Texture, TransparencyAttrib
from baseobject import GameObject
from utility import caster
import math

# Shader input: Grass
_grassDistance = 1200
_grass    = Vec4( 0, 0, 0, _grassDistance )    # jitter, skip, skip, distance


class GrassBase(GameObject):
    pass
    
    def start(self):
        taskMgr.add(self.update, "grass update")
        
    def stop(self):
        taskMgr.remove("grass update")
        
    def update(self, task ):
        # Grass jitter
        dx  = 1.8 * math.sin( task.time * 1.6 )
        dx += 2.1 * math.sin( task.time * 0.5 )
        dx += 2.6 * math.sin( task.time * 0.1 )
                
        _grass.setX( dx )
        
        self.nodePath.setShaderInput( 'grass', _grass )
        return task.cont

class GrassNode():
    def __init__( self, Data):
        self._setupGrass(Data)
        
    def getNodePath(self):
        return self.grassNP


    def _setupGrass(self, Data):
        tex = loader.loadTexture( Data['GrassTexture'])
        tex.setMinfilter( Texture.FTLinearMipmapLinear )
        tex.setMagfilter( Texture.FTLinearMipmapLinear )

        # Auxiliary clip plane
        clipPlane = Plane( Vec3( 0, -1, 0 ), Point3( 0, _grassDistance, 0 ) )
        clipNode = PlaneNode( 'clipPlane' )
        clipNode.setPlane( clipPlane )
        clipNP = base.camera.attachNewNode( clipNode )

        # Grass
        self.grassNP = NodePath(Data['Name'])# nodeRoot.attachNewNode( Data['NodeName'] )
        self.grassNP.setPos( *caster.stringListToListFloat(Data['Pos'].split(',')) )
        self.grassNP.setScale(caster.stringToFloat(Data['Scale'], 0))
        self.grassNP.setTexture( tex )
        self.grassNP.setTwoSided( True )
        self.grassNP.setTransparency( TransparencyAttrib.MAlpha )
        self.grassNP.setDepthWrite( False )
        self.grassNP.setShader( loader.loadShader( Data['ShaderPath']) )
        self.grassNP.setClipPlane( clipNP )

        # Grass cells
        for i in range( 16 ):
            for j in range( 16 ):
                path = Data['ModelPath'] + '/cell-%02i-%02i.egg' % ( i, j )
                try:
                    np = loader.loadModel( path )
                    np.reparentTo( self.grassNP )
                except:
                    pass

        return self.grassNP