from baseobject import GameObject
from panda3d.core import * #Vec3, OmniBoundingVolume, Vec4, DirectionalLight, PointLight
from direct.filter.CommonFilters import CommonFilters
import math
from utility import geomutil
from direct.filter.FilterManager import FilterManager
from game.map import Map
from direct.task import Task

class Sun1(GameObject):
    def __init__(self):
        self.clock = DayTime(0, 60000)
        self.nodePath = base.camera.attachNewNode('sun')
        loader.loadModel("Models/sphere").reparentTo(self.nodePath)
        self.nodePath.setTwoSided(True)
        self.nodePath.setScale(0.08)
        self.nodePath.setColorScale(1.0, 1.0, 1.0, 1.0, 10001)
        self.nodePath.setLightOff(1)
        self.nodePath.setShaderOff(1)
        self.nodePath.setFogOff(1)
        self.nodePath.setCompass()
        self.nodePath.setBin('background', 1)
        self.nodePath.setDepthWrite(False)
        self.nodePath.setDepthTest(False)        
        self.nodePath.node().setBounds(OmniBoundingVolume())    
        
        taskMgr.add(self.update, "sun update")
        
    def setPos(self, pos):
        pos.normalize()
        self.nodePath.setPos(pos)
        self.nodePath.lookAt(base.camera)

        
    def update(self, task):
        dt = globalClock.getDt()
        self.clock.update(dt)
        angle = self.clock.totalTime * math.pi / DayTime.TOTAL_SECOND_IN_DAY
        y = math.sin(angle)
        z = math.cos(angle)        
        self.setPos(Vec3(0, 0.866025403784, 0.5))
        return task.cont
    


class bulb():
    def __init__(self, fReadOnly, name, node, bulbColor=Vec4(1,1,1,1), fireColor=Vec4(0.64,0.9,0.9,1), bulbsize=1, fireScale=1.5):
        

        if node == None:
            node = Map.current.nodeRoot #render
        self.node = node

        self.bulb = self.createBillboard(node, 4, 4, "Texture/dot.png")
        self.bulb.setScale(bulbsize)
        self.bulb.setColor(bulbColor)

        self.fire = self.createBillboard(node, 4, 4, "Texture/flare1.png")
        self.fire.setScale(bulbsize)
        self.fire.setColor(fireColor)

        self.fBulbState=True
        self.fEnable = True

    def createBillboard(self, parent, nx, ny, file):
        np = geomutil.createPlane('myplane',nx,ny,1,1)
        np.setHpr(0,90,0)
        np.reparentTo(parent)
        #self.bulb.setScale(bulbsize)
        np.setBillboardPointWorld()
        np.setDepthWrite( False )
        np.setTransparency( TransparencyAttrib.MAlpha )
        attrib = ColorBlendAttrib.make(ColorBlendAttrib.MAdd, ColorBlendAttrib.OIncomingAlpha, ColorBlendAttrib.OOne)
        np.node().setAttrib(attrib)
        np.setBin('fixed', 0)
        tex = loader.loadTexture(file)
        tex.setMinfilter( Texture.FTLinearMipmapLinear )
        tex.setMagfilter( Texture.FTLinearMipmapLinear )
        tex.setAnisotropicDegree(2)
        np.setTexture(tex)
        np.setLightOff()
        return np


    def Destroy(self):
        if not self.fEnable:
            return
        self.fEnable = False
        self.bulb.removeNode()
        self.fire.removeNode()
        
class Sun(GameObject):
    def __init__(self, name="sun", lensFilterOn = True):
        #Fast Food
        self.sunNP = NodePath(name)
        print "SUN NODEPATH NAME ", self.sunNP.getName()
        self.sunNP.reparentTo(Map.current.nodeRoot)
        
        GameObject.__init__(self)
        
        pl = PointLight("pointLight") 
        self.light = self.sunNP.attachNewNode(pl)
        self.light.node().setColor(VBase4(1, 1, 0, 1))
        self.light.node().setAttenuation(Point3(0.005, 0.005, 0.005))
        Map.current.nodeRoot.setLight(self.light)
        self.bulb = bulb(False, "sun", self.sunNP)
        self.bulb.fire.setScale(3)
        self.bulb.bulb.setScale(3)
        self.bulb.bulb.setColor((1,1,0,1))
        self.bulb.fire.setColor((1,1,0.5,1))
        store.inActiveLenSphere = True
        self.lensEffectDistance = 300 # how far the lens sphere will be active ( 0 --> distance)
        if True:#sssssssdwsslensFilterOn:
            try:
                self.filterManager = FilterManager(base.win, base.cam)
                self.lensFilter(self.filterManager)
                store.filterManager = self.filterManager
                store.enableLenSphere = self.lensFilter
                print "Filter is On"
#                 taskMgr.add(self.cal4LenSphereEff, "Call4LenSphereEffect")
            except:
                print "Exist a lens filter before"
        
        GameObject.appendInitFunc(self, self.cal4LenSphereEff)
        
        GameObject.realNP = self.sunNP
        GameObject.nodePath = self.sunNP
        self.nodePath = self.sunNP
        GameObject.startScripts(self)
        
    def setPos(self, *args):
        self.bulb.fire.setPos(*args)
        self.bulb.bulb.setPos(*args)
        self.light.setPos(*args)

    def cal4LenSphereEff(self, task):
#         self.lensFilter(manager)
        camForwardVec = base.render.getRelativeVector(base.cam, (0, 1, 0))
        camForwardVec.normalize ()
        camSunVec = (self.bulb.bulb.getPos() - base.cam.getPos(render))
        distance = camSunVec.length()
        camSunVec.normalize ()
        angleInDeg = camForwardVec.angleDeg(camSunVec)
        print base.camera.getPos(render), "--------",base.cam.getPos(render), "-------", self.bulb.bulb.getPos()
        angleRad = camForwardVec.angleRad(camSunVec)
        print angleInDeg, " -------- ", angleRad
        
#         tranformVec = base.render.getRelativeVector(base.cam, camSunVec)
#         print tranformVec.angleDeg((0,1,0))
#         print camForwardVec, camSunVec, angleInDeg, angleInRad
#         print base.camera.getPos(), self.bulb.bulb.getPos()

        if angleInDeg < 45 and base.camLens.getFar() > distance and self.lensEffectDistance > distance and store.inActiveLenSphere == False:
            try:
                store.enableLenSphere(store.filterManager)
                store.inActiveLenSphere = True
            except:
                print "store.enableLenSphere or filtermanager is not apply"
        elif store.inActiveLenSphere == True and (angleInDeg > 45 or base.camLens.getFar() < distance or self.lensEffectDistance < distance):
            try:
                store.filterManager.cleanup()
                store.inActiveLenSphere = False
            except:
                print "Awesome... This is stupid error"
            
        return task.cont
        
    def lensFilter(self, manager):
    
        # ATI video cards (or drivers) are not too frendly with the input 
        # variables, so I had to transfer most of parameters to the shader
        # code.
    
        # Threshold (x,y,z) and brightness (w) settings
        threshold = Vec4(0.4, 0.4, 0.4, 1.0) # <----
        
        # FilterManager
#         manager = FilterManager(base.win, base.cam)
        tex1 = Texture()
        tex2 = Texture()
        tex3 = Texture()
        finalquad = manager.renderSceneInto(colortex=tex1)
        # First step - threshold and radial blur
        interquad = manager.renderQuadInto(colortex=tex2)
        interquad.setShader(Shader.load("Shader/invert_threshold_r_blur.sha"))
        interquad.setShaderInput("tex1", tex1)
        interquad.setShaderInput("threshold", threshold)
        # Second step - hardcoded fast gaussian blur. 
        # Not very important. This step can be omitted to improve performance
        # with some minor changes in lens_flare.sha
        interquad2 = manager.renderQuadInto(colortex=tex3)
        interquad2.setShader(Shader.load("Shader/gaussian_blur.sha"))
        interquad2.setShaderInput("tex2", tex2)
        # Final - Make lens flare and blend it with the main scene picture
        finalquad.setShader(Shader.load("Shader/lens_flare.sha"))
        finalquad.setShaderInput("tex1", tex1)
        finalquad.setShaderInput("tex2", tex2)
        finalquad.setShaderInput("tex3", tex3)
        
    def destroy(self):
        self.sunNP.detachNode()
        store.filterManager.cleanup()
        store.inActiveLenSphere = False
        GameObject.stopScripts(self)
        Map.current.nodeRoot.clearLight(self.light)

class DayTime(object):
    '''
    Time of day.
    
    params:
        startime : time in milisecond of day
        speed : speed of clock . 1 is normal : human speed clock
    '''
    TOTAL_SECOND_IN_DAY = 86400
    def __init__(self, startime=0, speed=1):
        self.startTime = startime
        self.speed = speed # speed = 1 is real time of human
        self.totalTime = startime # second
        self.second = 0        
        self.minute = 0        
        self.hour = 6
        
    def isMorning(self):
        return self.hour >= 6 and self.hour <= 11
    
    def isNoon(self):
        return self.hour > 11 and self.hour <= 13
    
    def isAfterNoon(self):
        return self.hour > 13 and self.hour <= 17
    
    def isEvening(self):
        return self.hour > 17 and self.hour <= 22
    
    def isNight(self):
        return self.hour > 22 or self.hour < 6 
    
    def update(self, time):
        dt = time * self.speed
        self.totalTime += dt
        self.second = int( self.totalTime % 60 )
        self.minute = int( (self.totalTime / 60) % 60 )
        self.hour =  int( (self.totalTime / (60 * 60)) % 24 ) + 6
             
        
        



        
        