from utility import caster
from panda3d.core import Vec3, Vec4, NodePath, Texture, TextureStage

import copy
from utility.pritority import Priority
import pickle
import __builtin__
import os
from direct.actor.Actor import Actor


class Store():
    datapath = os.path.join(os.getcwd(), 'game.data')
    
    def __init__(self):
        pass                
        
    def setFilePath(self, filepath=datapath):        
        self.datapath = filepath
        
    def setAbsPath(self, abspath):        
        self.datapath = os.path.join(os.getcwd(), abspath)
    
    def save(self, filepath=datapath):
        f = open(self.datapath, 'wb')
        pickle.dump(self, f)
        f.close()
        
    def load(self, filepath=datapath):
        try:
            f = open(self.datapath, 'rb')
            self = pickle.load(f)
            __builtin__.store = self
        except:
            pass
        return self
    
class FlagObject():
    def __init__(self, nodename="", *tags):
        self.eventQueue = []
        self.statusFlag = {}
        self.tags = []        
        self.nodeName = nodename
        self.tags.append(nodename)
        if len(tags) > 0:
            self.tags.extend(tags)       
        # Registried events
        self.eventConditions = {}  
        self.flushEvents = {}
        
    def getName(self):       
        return self.nodeName
    
    
    def registryFlush(self, name, toTag=None, fromTag=None, info=None):
        self.flushEvents[name] = {"info": info,
                                 "from": fromTag,
                                 "to" : toTag,
                                 }
      
    def registryEvent(self, name, func, args=None, toTag=None, fromTag=None):   
        '''
        Registry event function with condition (toTag and fromTag)
        If toTag in toTagList and fromTag is the same as event which game object received
        the func will start.
         
        Notice: if event received not have fromTag, it's will be ok if
        your fromTag is not the same)
        
        We must calll updateEvent() function every frame to check event which registried.
        '''
        if toTag == None and fromTag == None:
            raise Exception("Can not 'None' both toTag and fromTag parameter!")    
        if args == None:
            args = [] 
        self.eventConditions[name] = {"func": func,
                                 "from": fromTag,
                                 "to" : toTag,
                                 "args" : args,
                                 "command" : ""
                                 }
    def removeEvent(self, name): 
        if self.eventConditions.has_key(name):   
            self.eventConditions.pop(name)
        
    def clearEvent(self):
        self.eventConditions.clear()
        
    def flushEvent(self):
        for event in self.eventQueue:               
            # Remove flush event
            for condition in self.flushEvents.values():
                fit = True
                if condition["to"] not in event.toTagList:
                    fit = False                
                elif condition['from'] and event.fromTag != condition["from"]:
                    fit = False
                elif condition['info'] and event.info != condition["info"]:
                    fit = False  
                if fit:
                    self.eventQueue.remove(event)    
    
    # In dev, not in use
    def updateEvent(self):
        '''
        Each event function which registried before will start.
        When update Event Finish, events which processed will be removed.        
        '''
              
        for event in self.eventQueue:
            process = False
            # Process registry event
            for name, condition in self.eventConditions.items():
                fit = True
                if condition["to"] not in event.toTagList:
                    fit = False                
                elif condition['from'] and event.fromTag != condition["from"]:
                    fit = False
                if fit:
                    if "require info" == condition['command']:
                        condition['args'].pop()
                        condition['args'].append(event.info)
                    if "require info" in condition['args']:                        
                        condition['args'].remove("require info")
                        condition['args'].append(event.info)
                        condition['command'] = "require info"
                    self.eventConditions[name]['func'](*condition["args"])    
                    process = True
            if process:
                self.eventQueue.remove(event)            
            

class GameObject(FlagObject):
    def __init__(self, *args, **kwargs):        
        # Physic node
        self.physicNode = None
        # Scripting
        self.scripts = []
        self.funcs = {}
        self.initFuncs = []
        self.funcTasks = []
        # Texture
        self.texs = []
        self.ts = []
        # Shader Input
        self.shaderInputs = []
        # Default value
        self.nodePath = NodePath()
        self.realNP = self.nodePath
        if (len(kwargs) > 0):
            if kwargs.has_key('parent'):
                self.parent = kwargs['parent']
            self.nodeName = kwargs['nodeName']
            self.nodePath = kwargs['nodePath']  
            self.realNP = self.nodePath          
            self.describeDict = kwargs['describeDict']
            self.additionShaderInput()
            FlagObject.__init__(self, self.nodeName)     
        else:
            self.nodePath = NodePath(*args)
            FlagObject.__init__(self, *args)   
        # Attributes (like hp,mp,height,...)
        self.attributes = Store()   
           
    # --- 899 addition methods ---
        
    def getVecForward(self, invert=False):
        direction = 1
        if invert:
            direction = -1
        return base.render.getRelativeVector(self.nodePath, (0, direction, 0))
    
    def getVecRight(self, invert=False):
        direction = 1
        if invert:
            direction = -1
        return base.render.getRelativeVector(self.nodePath, (direction, 0, 0))

    def addTask(self, *args, **kwargs):
        taskMgr.add(*args, **kwargs)    
        
    def appendTags(self, *tags):
        for tag in tags:
            self.tags.append(tag)
            
    def appendScripts(self, *scripts):
        for script in scripts:
            self.scripts.append(script)
            
    def appendFuncs(self, *funcs, **kwargs):
        for func in funcs:
            self.funcs[func] = kwargs
            
    def removeFuncs(self, *funcs):
        for func in funcs:
            self.funcs.pop(func)            
            
    def appendInitFunc(self, initFunc):
        self.initFuncs.append(initFunc)
        
    def appendAndRunFunc(self, func, **kwargs):
        self.appendFuncs(func, **kwargs)
        if not kwargs.has_key('sort'):
            kwargs["sort"] = Priority.Model
        funcTask = taskMgr.add(func, "script %s" % self.getName(), **kwargs)
        self.funcTasks.append( funcTask )
            
    def appendAndRunFuncs(self, funcs, **kwargs):
        funcTasks = []
        self.appendFuncs(*funcs, **kwargs)
        if not kwargs.has_key('sort'):
            kwargs["sort"] = Priority.Model
        for func in funcs:
            funcTask = taskMgr.add(func, "script %s" % self.getName(), **kwargs)
            funcTasks.append(funcTask)
            self.funcTasks.append(funcTask)
        return funcTasks
    
    def startScripts(self):
        for func in self.initFuncs:
            self.funcTasks.append( taskMgr.add(func, "script %s" % self.getName(), sort=Priority.Init) )
        for script in self.scripts:            
            script.task = taskMgr.add(script.run, "script %s" % self.getName(), uponDeath=script.cleanUp, sort=Priority.Model)
        for func, kwargs in self.funcs.items():
            self.funcTasks.append( taskMgr.add(func, "script %s" % self.getName(), **kwargs) )
            
    def appendAndRunScripts(self, *scripts):
        lstScript = []
        # Check if scripst in filepaths
        for script in scripts:
            if isinstance(script, str):
                filePath = script
                modules = getModule(filePath)
                for module in modules:                    
                    script = module.Scripting(self)             
            lstScript.append(script)
        self.appendScripts(*lstScript)
        for script in lstScript:            
            script.task = taskMgr.add(script.run, "script %s" % self.getName(), uponDeath=script.cleanUp, sort=Priority.Model)
            
    def stopScripts(self):
        for script in self.scripts:  
            if hasattr(script, 'task'):
                taskMgr.remove(script.task)
        for functask in self.funcTasks:
            taskMgr.remove(functask)
        self.funcTasks = []
        
    def appendAttributeFromFile(self, filePath):
        pass
            
    def appendAttribute(self, attrs):
        for name, value in attrs.items():
            try:
                value = eval(value)
            except:
                pass
            setattr(self.attributes, name, value)
            
    def additionShaderInput(self):
        if isinstance(self.nodePath, NodePath):            
            self.setShaderInput('scale', self.getScale())
            self.setShaderInput('eye', base.camera)
    
    def clone(self):
        clone = copy.deepcopy(self)        
        return clone
    
    def start(self):
        pass
    
    def stop(self):
        pass
    
    def destroy(self):     
        self.nodePath.hide()   
        taskMgr.doMethodLater(2, self.__destroyLate, "destroy node")
            
    def __destroyLate(self, task):
        if isinstance(self.realNP, Actor):
            self.realNP.cleanup()
        if isinstance(self.nodePath, Actor):
            self.nodePath.cleanup()    
        self.nodePath.removeNode()
        return task.done
    
    def setNodePath(self, node):
        if self.physicNode:
            self.realNP = node
        else:
            self.nodePath = node
            self.realNP = node
            
    #--- Panda3d Wrapper--- (Edit in need)            
    def setName(self, name):
        if self.nodeName != None:
            self.tags.remove(self.nodeName)
            self.tags.append(name)
        self.nodeName = name
        self.nodePath.setName(name)
        self.describeDict['Name'] = name
                
    def clearColor(self):
        self.nodePath.clearColor()
    
    def clearAntialias(self):
        self.nodePath.clearAntialias()
    
    def clearAudioVolume(self):
        self.nodePath.clearAudioVolume()
        
    def clearBillboard(self):
        self.nodePath.clearBillboard()
        
    def clearBin(self):
        self.nodePath.clearBin()
        
    def clearClipPlane(self, *args):
        self.nodePath.clearClipPlane(*args)
        
    def clearColorScale (self):
        self.nodePath.clearColorScale()
        
    def clearCompass(self):
        self.nodePath.clearCompass()
        
    def clearDepthOffset(self):
        self.nodePath.clearDepthOffset()
        
    def clearDepthTest(self):
        self.nodePath.clearDepthTest()
        
    def clearDepthWrite(self):
        self.nodePath.clearDepthWrite()
        
    def clearEffect(self, typeHandle):
        self.nodePath.clearEffect(typeHandle)
        
    def clearEffects(self):
        self.nodePath.clearEffects()
        
    def clearFog(self):
        self.nodePath.clearFog()
        
    def clearLight(self, *args):
        self.nodePath.clearLight(*args)
        
    def clearMat(self):
        self.nodePath.clearMat()
        
    def clearMaterial(self):
        self.nodePath.clearMaterial()
        
    def clearNormalMap(self):
        self.nodePath.clearNormalMap()
        
    def clearOccluder(self, *args):
        self.nodePath.clearOccluder(*args)
        
    def clearProjectTexture(self, textureStage):
        self.nodePath.clearProjectTexture(textureStage)
        
    def clearPythonTag(self, key):
        self.nodePath.clearPythonTag(key)

    def clearRenderMode(self):
        self.nodePath.clearRenderMode()
        
    def clearScissor(self):
        self.nodePath.clearScissor()
        
    def clearShader(self):
        self.nodePath.clearShader()
        
    def clearShaderInput(self, name):
        self.nodePath.clearShaderInput(name)
        
    def clearTag(self):
        self.nodePath.clearTag()
        
    def clearTexGen(self, *args):
        self.nodePath.clearTexGen(*args)
        
    def clearTexProjector(self, *args):
        self.nodePath.clearTexProjector(*args)
        
    def clearTexTransform(self, *args):
        self.nodePath.clearTexTransform(*args)
        
    def clearTexture(self, *args):
        self.nodePath.clearTexture(*args)
        
    def clearTransform(self, *args):
        self.nodePath.clearTransform(*args)
        
    def clearTransparency(self):
        self.nodePath.clearTransparency()
        
    def clearTwoSided(self):
        self.nodePath.clearTwoSided()
        
    def compareTo(self, otherNodePath):
        self.nodePath.compareTo(otherNodePath)
        
    def composeColorScale(self, *args):
        self.nodePath.composeColorScale(*args)
    
    def copyTo(self, *args):
        self.nodePath.copyTo(*args)
        
    def detachNode(self, *args):
        self.nodePath.detachNode(*args)
        
    def doBillboardAxis(self, camNode, offset):
        self.nodePath.doBillboardAxis(camNode, offset)
        
    def doBillboardPointEye(self, camNode, offset):
        self.nodePath.doBillboardPointEye(camNode, offset)
        
    def doBillboardPointWorld(self, camNode, offset):    
        self.nodePath.doBillboardPointWorld(camNode, offset)
        
    def encodeToBamStream(self, *args):
        self.nodePath.encodeToBamStream(*args)
        
    def find(self, path):
        return self.nodePath.find(path)
            
    def findAllMatches(self, path):
        return self.nodePath.findAllMatches(path)
    
    def findAllMaterials(self, *args):
        return self.nodePath.findAllMaterials(*args)
    
    def findAllPathsTo(self, pandaNode):
        return self.nodePath.findAllPathsTo(pandaNode)
    
    def findAllTexcoords(self, *args):
        return self.nodePath.findAllTexcoords(*args)
    
    def findAllTextures(self, *args):
        return self.nodePath.findAllTextures(*args)
    
    def findAllTextureStages(self, *args):
        return self.nodePath.findAllTextureStages(*args)
    
    def findAllVertexColumns(self, *args):
        return self.nodePath.findAllVertexColumns(*args)
    
    def findMaterial(self, name):
        return self.nodePath.findMaterial(name)
    
    def findNetPythonTag(self, key):
        return self.nodePath.findNetPythonTag(key)
    
    def findNetTag(self, key):
        return self.nodePath.findNetTag(key)
    
    def findPathTo(self, node):
        return self.nodePath.findPathTo(node)
        
    def findTexture(self, *args):
        return self.nodePath.findTexture(*args)
    
    def findTextureStage(self, name):
        return self.nodePath.findTextureStage(name)
    
    def flattenLight(self):
        return self.nodePath.flattenLight()
    
    def flattenMedium(self):
        return self.nodePath.flattenMedium()
    
    def flattenStrong(self):
        return self.nodePath.flattenStrong()
    
    def forceRecomputeBounds(self):
        self.nodePath.forceRecomputeBounds()
        
    def getAncestor(self, *args):
        return self.nodePath.getAncestor(*args)
    
    def getAncestors(self):
        return self.nodePath.getAncestors()
    
    def getAntialias(self):
        return self.nodePath.getAntialias()
    
    def getAttrib(self, typeHandle):
        return self.nodePath.getAttrib(typeHandle)
    
    def getAudioVolume(self):
        return self.nodePath.getAudioVolume()
    
    def getBinDrawOrder(self):
        return self.nodePath.getBinDrawOrder()
    
    def getBinName(self):
        return self.nodePath.getBinName()
    
    def getBounds(self, *args):
        return self.nodePath.getBounds(*args)
    
    def getChild(self, *args):
        return self.nodePath.getChild(*args)
    
    def getChildren(self, *args):
        return self.nodePath.getChildren(*args)
    
    def getCollideMask(self):
        return self.nodePath.getCollideMask()
            
    def getColor(self):
        return self.nodePath.getColor()
    
    def getColorScale(self):
        return self.nodePath.getColorScale()
    
    def getCommonAncestor(self, *args):
        return self.nodePath.getCommonAncestor(*args)
    
    def getDepthOffset(self):
        return self.nodePath.getDepthOffset()
    
    def getDepthTest(self):
        return self.nodePath.getDepthTest()
    
    def getDepthWrite(self):
        return self.nodePath.getDepthWrite()
    
    def getDistance(self, otherNode):
        return self.nodePath.getDistance(otherNode)
    
    def getEffect(self, typeHandle):
        return self.nodePath.getEffect(typeHandle)
    
    def getEffects(self):
        return self.nodePath.getEffects()
    
    def getErrorType(self):
        return self.nodePath.getErrorType()
    
    def getFog(self):
        return self.nodePath.getFog()
    
    def getH(self, *args):
        return self.nodePath.getH(*args)
    
    def getHiddenAncestor(self, *args):
        return self.nodePath.getHiddenAncestor(*args)
    
    def getInstanceCount(self):
        return self.nodePath.getInstanceCount()
        
    def getHpr(self, *args):
        return self.nodePath.getHpr(*args)
    
    def getKey(self):
        return self.nodePath.getKey()
    
    def getMat(self, *args):
        return self.nodePath.getMat(*args)
    
    def getMaterial(self):
        return self.nodePath.getMaterial()
    
    def getName(self):
        if self.physicNode != None:
            return self.realNP.getName()
        return self.nodePath.getName()
    
    def getNetAudioVolume(self):
        return self.nodePath.getNetAudioVolume()
    
    def getNetPrevTransform(self, *args):
        return self.nodePath.getNetPrevTransform(*args)
    
    def getNetPythonTag(self, key):
        return self.nodePath.getNetPythonTag(key)
    
    def getNetState(self, *args):
        return self.nodePath.getNetState(*args)
    
    def getNetTag(self, key):
        return self.nodePath.getNetTag(key)
    
    def getNetTransform(self, *args):
        return self.nodePath.getNetTransform(*args)
    
    def getNode(self, *args):
        return self.nodePath.getNode(*args)
    
    def getNodes(self):
        return self.nodePath.getNodes()
    
    def getNumChildren(self, *args):
        return self.nodePath.getNumChildren(*args)
    
    def getNumNodes(self, *args):
        return self.nodePath.getNumNodes(*args)
    
    def getP(self, *args):
        return self.nodePath.getP(*args)
    
    def getParent(self, *args):
        return self.nodePath.getParent(*args)
    
    def getPos(self, *args):
        return self.nodePath.getPos(*args)
    
    def getPosDelta(self, *args):
        return self.nodePath.getPosDelta(*args)
    
    def getPrevTransform(self, *args):
        return self.nodePath.getPrevTransform(*args)
    
    def getPythonTag(self, key):
        return self.nodePath.getPythonTag(key)
    
    def getQuat(self, *args):
        return self.nodePath.getQuat(*args)
    
    def getR(self, *args):
        return self.nodePath.getR(*args)
    
    def getRelativePoint(self, *args):
        return self.nodePath.getRelativePoint(*args)
    
    def getRenderMode(self):
        return self.nodePath.getRenderMode()
    
    def getRenderModePerspective(self):
        return self.nodePath.getRenderModePerspective()
    
    def getRenderModeThickness(self):
        return self.nodePath.getRenderModeThickness()
    
    def getSa(self):
        return self.nodePath.getSa()
    
    def getSb(self):
        return self.nodePath.getSb()
    
    def getScale(self, *args):
        return self.nodePath.getScale(*args)
    
    def getSg(self):
        return self.nodePath.getSg()
    
    def getShader(self):
        return self.nodePath.getShader()
    
    def getShaderInput(self, *args):
        return self.nodePath.getShaderInput(*args)
    
    def getShear(self, *args):
        return self.nodePath.getShear(*args)
    
    def getShxy(self, *args):
        return self.nodePath.getShxy(*args)
    
    def getShxz(self, *args):    
        return self.nodePath.getShxz(*args)
    
    def getShyz(self, *args):
        return self.nodePath.getShyz(*args)
    
    def getSort(self, *args):
        return self.nodePath.getSort(*args)
    
    def getSr(self):
        return self.nodePath.getSr()
    
    def getStashedAncestor(self, *args):
        return self.nodePath.getStashedAncestor(*args)
    
    def getStashedChildren(self, *args):
        return self.nodePath.getStashedChildren(*args)
    
    def getState(self, *args):
        return self.nodePath.getState(*args)
    
    def getSx(self, *args):
        return self.nodePath.getSx(*args)
    
    def getSy(self, *args):
        return self.nodePath.getSy(*args)
    
    def getSz(self, *args):
        return self.nodePath.getSz(*args)
    
    def getTag(self, key):
        return self.nodePath.getTag(key)
    
    def getTexGen(self, textureStage):
        return self.nodePath.getTexGen(textureStage)
    
    def getTexGenLight(self, textureStage):
        return self.nodePath.getTexGenLight(textureStage)
    
    def getTexHpr(self, *args):
        return self.nodePath.getTexHpr(*args)
    
    def getTexOffset(self, *args):
        return self.nodePath.getTexOffset(*args)
    
    def getTexPos(self, *args):
        return self.nodePath.getTexPos(*args)
    
    def getTexProjectorFrom(self, textureStage):
        return self.nodePath.getTexProjectorFrom(textureStage)
    
    def getTexProjectorTo(self, textureStage):
        return self.nodePath.getTexProjectorTo(textureStage)
    
    def getTexRotate(self, *args):
        return self.nodePath.getTexRotate(*args)
    
    def getTexScale(self, *args):
        return self.nodePath.getTexScale(*args)
    
    def getTexScale3d(self, *args):
        return self.nodePath.getTexScale3d(*args)
    
    def getTexTransform(self, *args):
        return self.nodePath.getTexTransform(*args)
    
    def getTexture(self, *args):
        return self.nodePath.getTexture(*args)
    
    def getTop(self, *args):
        return self.nodePath.getTop(*args)
    
    def getTopNode(self, *args):
        return self.nodePath.getTopNode(*args)
    
    def getTransform(self, *args):
        return self.nodePath.getTransform(*args)
    
    def getTransparency(self):
        return self.nodePath.getTransparency()
    
    def getTwoSided(self):
        return self.nodePath.getTwoSided()
    
    def getX(self, *args):
        return self.nodePath.getX(*args)
    
    def getY(self, *args):
        return self.nodePath.getY(*args)
    
    def getZ(self, *args):
        return self.nodePath.getZ(*args)
    
    def hasAntialias(self):
        return self.nodePath.hasAntialias()
    
    def hasAttrib(self, typeHandle):
        return self.nodePath.hasAttrib(typeHandle)
    
    def hasAudioVolume(self):
        return self.nodePath.hasAudioVolume()
    
    def hasBillboard(self):
        return self.nodePath.hasBillboard()
    
    def hasBin(self):
        return self.nodePath.hasBin()
    
    def hasClipPlane(self, clipPlaneNode):
        return self.nodePath.hasClipPlane(clipPlaneNode)
    
    def hasClipPlaneOff(self, *args):
        return self.nodePath.hasClipPlaneOff(*args)
    
    def hasColor(self):
        return self.nodePath.hasColor()
    
    def hasColorScale(self):
        return self.nodePath.hasColorScale()
    
    def hasCompass(self):
        return self.nodePath.hasCompass()
    
    def hasDepthOffset(self):
        return self.nodePath.hasDepthOffset()
    
    def hasDepthTest(self):
        return self.nodePath.hasDepthTest()
    
    def hasDepthWrite(self):
        return self.nodePath.hasDepthWrite()
    
    def hasEffect(self, typeHandle):
        return self.nodePath.hasEffect(typeHandle)
    
    def hasFog(self):
        return self.nodePath.hasFog()
    
    def hasFogOff(self):
        return self.nodePath.hasFogOff()
    
    def hasLight(self):
        return self.nodePath.hasLight()
    
    def hasLightOff(self, *args):
        return self.nodePath.hasLightOff(*args)
    
    def hasMat(self):
        return self.nodePath.hasMat()
    
    def hasMaterial(self):
        return self.nodePath.hasMaterial()
    
    def hasNetPythonTag(self, key):
        return self.nodePath.hasNetPythonTag(key)
    
    def hasNetTag(self, key):
        return self.nodePath.hasNetTag(key)
    
    def hasOccluder(self, occulerNode):
        return self.nodePath.hasOccluder(occulerNode)
    
    def hasParent(self, *args):
        return self.nodePath.hasParent(*args)
    
    def hasPythonTag(self, key):
        return self.nodePath.hasPythonTag(key)
    
    def hasRenderMode(self):
        return self.nodePath.hasRenderMode()
    
    def hasScissor(self):
        return self.nodePath.hasScissor()
    
    def hasTag(self, key):
        return self.nodePath.hasTag(key)
    
    def hasTexcoord(self, name):
        return self.nodePath.hasTexcoord(name)
    
    def hasTexGen(self, textureStage):
        return self.nodePath.hasTexGen(textureStage)
    
    def hasTexProjector(self, textureStage):
        return self.nodePath.hasTexProjector(textureStage)
    
    def hasTexTransform(self, textureStage):
        return self.nodePath.hasTexTransform(textureStage)
    
    def hasTexture(self, *args):
        return self.nodePath.hasTexture(*args)
    
    def hasTextureOff(self, *args):
        return self.nodePath.hasTextureOff(*args)
    
    def hasTransparency(self):
        return self.nodePath.hasTransparency()
    
    def hasTwoSided(self):
        return self.nodePath.hasTwoSided()
    
    def hasVertexColumn(self, name):
        return self.nodePath.hasVertexColumn(name)
    
    def headsUp(self, *args):
        self.nodePath.headsUp(*args)
       
    def hide(self, *args):
        self.nodePath.hide(*args)
        
    def hideBounds(self):
        self.nodePath.hideBounds()
        
    def instanceTo(self, *args):
        return self.nodePath.instanceTo(*args)
    
    def instanceUnderNode(self, *args):    
        return self.nodePath.instanceUnderNode(*args)
    
    def isAncestorOf(self, *args):
        return self.nodePath.isAncestorOf(*args)
    
    def isEmpty(self):
        return self.nodePath.isEmpty()
    
    def isHidden(self, *args):
        return self.nodePath.isHidden(*args)
    
    def isSameGraph(self, *args):
        return self.nodePath.isSameGraph(*args)
    
    def isSingleton(self, *args):
        return self.nodePath.isSingleton(*args)
    
    def isStashed(self):
        return self.nodePath.isStashed()
    
    def listTags(self):
        self.nodePath.listTags()
        
    def lookAt(self, *args):
        self.nodePath.lookAt(*args)
        
    def ls(self, *args):
        self.nodePath.ls(*args)
        
    def node(self):
        if self.realNP is not self.nodePath:
            return self.realNP.node()
        return self.nodePath.node()
    
    def output(self, outStream):
        self.nodePath.output(outStream)
                    
    def premungeScene(self, *args):
        self.nodePath.premungeScene(*args)
        
    def prepareScene(self, gsg):
        self.nodePath.prepareScene(gsg)
        
    def projectTexture(self, textureStage, texture, projectorNode):
        self.nodePath.projectTexture(textureStage, texture, projectorNode)
        
    def removeNode(self, *args):
        self.nodePath.removeNode(*args)
        
    def reparentTo(self, *args):
        self.nodePath.reparentTo(*args)
        
    def reverseLs(self, *args):
        return self.nodePath.reverseLs(*args)
        
    def setAllColorScale (self, *args):
        self.nodePath.setAllColorScale (*args)
        
    def setAlphaScale(self, *args):
        self.nodePath.setAlphaScale(*args)
        
    def setAntialias(self, *args):
        self.nodePath.setAntialias(*args)
        
    def setAttrib(self, *args):
        self.nodePath.setAttrib(*args)
        
    def setAudioVolume(self, *args):
        self.nodePath.setAudioVolume(*args)
        
    def setAudioVolumeOff(self, *args):
        self.nodePath.setAudioVolumeOff(*args)
        
    def setBillboardAxis(self, *args):
        self.nodePath.setBillboardAxis(*args)
        
    def setBillboardPointEye(self, *args):
        self.nodePath.setBillboardPointEye(*args)
        
    def setBillboardPointWorld(self, *args):
        self.nodePath.setBillboardPointWorld(*args)
        
    def setBin(self, *args):
        self.nodePath.setBin(*args)
        
    def setClipPlane(self, *args):
        self.nodePath.setClipPlane(*args)
        
    def setClipPlaneOff(self, *args):
        self.nodePath.setClipPlaneOff(*args)
        
    def setCollideMask(self, *args):
        self.nodePath.setCollideMask(*args)
                
    def setColorOff(self, *args):
        self.nodePath.setColorOff(*args)        
    
    def setPos(self, *args):
        self.nodePath.setPos(*args)
        
    def setScale(self, *args):
        self.nodePath.setScale(*args)
        
    def setHpr(self, *args):
        self.nodePath.setHpr(*args)
        
    def setColor(self, *args):
        self.nodePath.setColor(*args)
        
    def setColorScale(self, *args):
        self.nodePath.setColorScale(*args)
        
    def setColorScaleOff(self, *args):
        self.nodePath.setColorScaleOff(*args)
        
    def setCompass(self, *args):    
        self.nodePath.setCompass(*args)
        
    def setDepthOffset(self, *args):
        self.nodePath.setDepthOffset(*args)
        
    def setDepthTest(self, *args):
        self.nodePath.setDepthTest(*args)
        
    def setDepthWrite(self, *args):
        self.nodePath.setDepthWrite(*args)
        
    def setEffect(self, renderEffect):
        self.nodePath.setEffect(renderEffect)
        
    def setEffects(self, renderEffects):
        self.nodePath.setEffects(renderEffects)
        
    def setFluidPos(self, *args):
        self.nodePath.setFluidPos(*args)
        
    def setFluidX(self, *args):
        self.nodePath.setFluidX(*args)
        
    def setFluidY(self, *args):
        self.nodePath.setFluidY(*args)
        
    def setFluidZ(self, *args):
        self.nodePath.setFluidZ(*args)
        
    def setFog(self, *args):
        self.nodePath.setFog(*args)
        
    def setFogOff(self, *args):
        self.nodePath.setFogOff(*args)
        
    def setH(self, *args):
        self.nodePath.setH(*args)
        
    def setHprScale(self, *args):
        self.nodePath.setHprScale(*args)
        
    def setInstanceCount(self, iCount):
        self.nodePath.setInstanceCount(iCount)
        
    def setLight(self, *args):
        self.nodePath.setLight(*args)
        
    def setLightOff(self, *args):
        self.nodePath.setLightOff(*args)
        
    def setMat(self, *args):
        self.nodePath.setMat(*args)
        
    def setMaterial(self, *args):
        self.nodePath.setMaterial(*args)
        
    def setMaterialOff(self, *args):
        self.nodePath.setMaterialOff(*args)
        
    def setNormalMap(self, *args):
        self.nodePath.setNormalMap(*args)
        
    def setOccluder (self, occluderNode):
        self.nodePath.setOccluder(occluderNode)
        
    def setP(self, *args):
        self.nodePath.setP(*args)
    
    def setPrevTransform(self, *args):    
        self.nodePath.setPrevTransform(*args)
        
    def setPythonTag(self, key, value):
        self.nodePath.setPythonTag(key, value)
        
    def setQuat(self, *args):
        self.nodePath.setQuat(*args)
        
    def setR(self, *args):
        self.nodePath.setR(*args)
        
    def setRenderMode(self, *args):
        self.nodePath.setRenderMode(*args)
        
    def setRenderModeFilled(self, *args):
        self.nodePath.setRenderModeFilled(*args)
        
    def setRenderModePerspective(self, *args):
        self.nodePath.setRenderModePerspective(*args)
        
    def setRenderModeThickness(self, *args):
        self.nodePath.setRenderModeThickness(*args)
        
    def setRenderModeWireframe(self, *args):
        self.nodePath.setRenderModeWireframe(*args)
        
    def setSa(self, sa):
        self.nodePath.setSa(sa)
        
    def setSb(self, sb):
        self.nodePath.setSb(sb)
        
    def setScissor(self, *args):        
        self.nodePath.setScissor(*args)
        
    def setSg(self, sg):
        self.nodePath.setSg(sg)
        
    def setShader(self, *args):
        self.nodePath.setShader(*args)
        
    def setShaderAuto(self, *args):
        self.nodePath.setShaderAuto(*args)
        
    def setShaderInput(self, *args):
        self.nodePath.setShaderInput(*args)
        name = args[0]
        self.shaderInputs.append(name)
            
        
    def setShaderOff(self, *args):
        self.nodePath.setShaderOff(*args)
        
    def setShear(self, *args):
        self.nodePath.setShear(*args)
        
    def setShxy (self, *args):
        self.nodePath.setShxy(*args)
        
    def setShxz (self, *args):
        self.nodePath.setShxz(*args)
        
    def setShyz (self, *args):
        self.nodePath.setShyz(*args)
        
    def setSr(self, *args):
        self.nodePath.setSr(*args)
        
    def setState(self, *args):
        self.nodePath.setState(*args)
        
    def setSx(self, *args):
        self.nodePath.setSx(*args)
        
    def setSy(self, *args):
        self.nodePath.setSy(*args)
        
    def setSz(self, *args):
        self.nodePath.setSz(*args) 
    
    def setTag(self, key, value):
        self.nodePath.setTag(key, value)
        
    def setTexGen(self, *args):
        self.nodePath.setTexGen(*args)
        
    def setTexHpr(self, *args):
        self.nodePath.setTexHpr(*args)
        
    def setTexOffset(self, *args):
        self.nodePath.setTexOffset(*args)
        
    def setTexPos(self, *args):
        self.nodePath.setTexPos(*args)
        
    def setTexProjector(self, *args):
        self.nodePath.setTexProjector(*args)
        
    def setTexRotate(self, *args):
        self.nodePath.setTexRotate(*args)
        
    def setTexScale(self, *args):
        self.nodePath.setTexScale(*args)
        
    def setTexTransform(self, *args):
        self.nodePath.setTexTransform(*args)
        
    def setTexture(self, *args):
        self.nodePath.setTexture(*args)
        tex, ts = None, None
        for arg in args:
            if isinstance(arg, Texture):
                tex = arg
            if isinstance(arg, TextureStage):
                ts = arg
        self.texs.append(tex)
        self.ts.append(ts)
        
    def setTextureOff(self, *args):
        self.nodePath.setTextureOff(*args)
        
    def setTransform(self, *args):
        self.nodePath.setTransform(*args)
                
    def setTransparency(self, *args):
        self.nodePath.setTransparency(*args)
        
    def setTwoSided(self, *args):
        self.nodePath.setTwoSided(*args)
        
    def setX(self, *args):
        self.nodePath.setX(*args)
        
    def setY(self, *args):
        self.nodePath.setY(*args)
        
    def setZ(self, *args):
        self.nodePath.setZ(*args)
    
    def show(self):
        self.nodePath.show()
        
    def showBounds(self):
        self.nodePath.showBounds()
        
    def showThrough(self, *args):
        self.nodePath.showThrough(*args)
        
    def showTightBounds(self):
        self.nodePath.showTightBounds()
        
    def stash(self, *args):
        self.nodePath.stash(*args)
        
    def stashTo(self, *args):
        self.nodePath.stashTo(*args)
        
    def unifyTextureStages(self):
        self.nodePath.unifyTextureStages()
        
    def unstash(self, *args):
        self.nodePath.unstash(*args)
        
    def unstashAll(self, *args):
        self.nodePath.unstashAlll(*args)
        
    def verifyComplete(self, *args):
        return self.nodePath.verifyComplete(*args)
        
    def writeBamFile(self, *args):
        return self.nodePath.writeBamFile(*args)
        
    def writeBounds(self, outStream):
        self.nodePath.writeBounds(outStream)
        
    def wrtReparentTo(self, *args):
        self.nodePath.wrtReparentTo(*args)
        
    def attachNewNode(self, *args):
        self.nodePath.attachNewNode(*args)    

