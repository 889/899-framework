from game.io import XmlIO
import sys
from panda3d.core import NodePath
from game.builder import *

class Prototype():
    """
    List of prototype kind
    """
    GameModel = "GameModel"
    Sky = "Sky"
    Terrain = "Terrain"
    Camera = "Camera"
    Particle = "Particle"
    Fog = "Fog"
    Grass = "Grass"
    Light = "Light"
    Water = "Water"


class GameEntityManager():
    objectList = {}
    prototypeList = {}
    cacheData = {}
    filename = ""
        
    def __init__(self):        
        """
        This Object provide the function to manage all entity in current map
        """
        pass
        
    def loadFile(self, filename):
        """
            Load Data from XML file and push it to cacheData
        """
        xmlIn = XmlIO(filename)
        GameEntityManager.filename = filename
        self.cacheData = xmlIn.readFile()
        return self.cacheData
    
    def isEmptyData(self):        
        return len(GameEntityManager.cacheData) == 0
        
    def InitPrototype(self, filename, prototypeId):
        """
        """
        if self.prototypeList.has_key(prototypeId):
            return
        if self.isEmptyData():
            self.loadFile(filename)            
        gameNode = self.cacheData['Game']
        print "game node game", gameNode
        print "filename", filename
        for key, group in gameNode.items():
            print "checking prototypeId", prototypeId, "in group", key              
            entitiesDict = {}                        
            if group.has_key(prototypeId):    
                print "group has id", prototypeId            
                objectData = group[prototypeId]
                managerType = key[0:-1] + "Builder"
                builder = getattr(sys.modules[__name__], managerType)()
                gameObject = builder.buildType(objectData)                
                entitiesDict[prototypeId] = gameObject
                self.prototypeList.update(entitiesDict)    
                if GameEntityManager.objectList.has_key(key):            
                    GameEntityManager.objectList[key].update(entitiesDict)
                else:
                    GameEntityManager.objectList[key] = entitiesDict
                print "add to manager", GameEntityManager.objectList[key]
                return
            
    def InitPrototypes(self, filename, idList):
        for prototypeId in idList:
            self.InitPrototype(filename, prototypeId)
        
    def InitAllPrototype(self, filename, idList=None):
        if idList:
            self.InitPrototypes(filename, idList)
            return        
        if self.isEmptyData():            
            self.loadFile(filename)        
        gameNode = self.cacheData['Game']             
        for key, value in gameNode.items():
            entitiesDict = {}
            for objectType, objectData in value.items():
                managerType = key[0:-1] + "Builder"
                builder = getattr(sys.modules[__name__], managerType)()
                gameObject = builder.buildType(objectData)                
                entitiesDict[objectType] = gameObject
            self.prototypeList.update(entitiesDict)
            GameEntityManager.objectList[key] = entitiesDict
         
    def existObject(self, obj):     
        return obj in self.prototypeList            
        
    def existObjectName(self, name):
        for obj in self.prototypeList:
            if name == obj.getName():
                return True
            
        
    @staticmethod        
    def build(prototypeType, prototypeId, objId, **kwargs):
        """
        Add a object from anywhere
        prototypeType: is a type of prototype. it's maybe a Prototype.GameModel, Prototype.Sky ... See more in Prototype.
        prototypeID  : is a modelID in your prototype XML like <GameModel Id="Aventador">
        objID: is objectID like ID in your XML Map like <GameModel Id="RaceCar"> <Prototype>Aventador</Prototype>        
        **kwargs: other input for object.
        """
        if not GameEntityManager.objectList.has_key(prototypeType + "s") or\
         not GameEntityManager.objectList[prototypeType + "s"].has_key(prototypeId):
            mapMgr.initPrototype([prototypeId])
        data = {
                "objectID": prototypeId,
                "Name": objId
                }
        data.update(kwargs)
        objectList = GameEntityManager.objectList
        mymap = screen.current
        builderType = prototypeType + "Builder"
        builder = getattr(sys.modules[__name__], builderType)()
        print "build in game", data, mymap.nodeRoot, mymap.world , prototypeId
        obj = builder.buildGameObject(data, objectList, mymap.nodeRoot, mymap.world)
        mymap.appendObject(prototypeType, obj)
        return obj
        
                    

class ModelManager(GameEntityManager):   
    prototypeList = {}
    cacheData = {}
     
    def __init__(self):
        GameEntityManager.__init__(self)
        print "Init Model manager"

class TerrainManager(GameEntityManager):    
    prototypeList = {}
    cacheData = {}
    
    def __init__(self):
        GameEntityManager.__init__(self)
        print "Init Terrain Manager"
        
class NatureManager(GameEntityManager):    
    prototypeList = {}
    cacheData = {}
    
    def __init__(self):
        GameEntityManager.__init__(self)
        print "Init Natural Manager"
        
class CameraManager(GameEntityManager):    
    prototypeList = {}
    cacheData = {}
    
    def __init__(self):
        GameEntityManager.__init__(self)
        print "Init Camera Manager"