from utility.mouse import setCursorFile
import json

class Cursor(object):
    def __init__(self):
        self.type_dict = {}
        
    def add(self, name, filepath):
        self.type_dict[name] = filepath
        
    def set(self, name):
        if self.type_dict.has_key(name):
            setCursorFile(self.type_dict[name])
            
    def load(self, filelist):
        self.type_dict = json.load(open(filelist, 'r'))
        
    def pop(self, name):
        return self.type_dict.pop(name)
    
    