'''All Factory need in game...:-s
'''
import sys
from game.io import XmlIO
from panda3d.core import NodePath, Vec3
from panda3d.bullet import BulletWorld
from game.map import Map
from game.manager import *
from game.builder import *
import json
from game.gui import GuiManager
import time
from direct.stdpy import thread

from direct.interval.IntervalGlobal import Sequence, Func, Wait


class BaseFactory():
    '''Abstract Factory for creating game.'''
    @staticmethod
    def product(*args, **kwargs):
        pass
    
class BuildTypeFactory(BaseFactory):
    @staticmethod
    def product(*args, **kwargs):
        filename = kwargs['filename']
        idList = None
        if kwargs.has_key('idList'):
            idList = kwargs['idList']            
        xmlIn = XmlIO(filename)
        data = xmlIn.readFile()
        #print data
        data = data['Game']
        for key, value in data.items():
            key = key.replace('Path', '')
            managerType = key[0:len(key)] + "Manager"
            managerClass = getattr(sys.modules[__name__], managerType)
            manager = managerClass()            
            manager.InitAllPrototype(value, idList)
            managerClass.prototypeList = manager.prototypeList
        
        game = GameEntityManager.objectList
        return game
    
class MapFactory(BaseFactory):    
    @staticmethod
    def product(*args, **kwargs):    
        filename = kwargs['filename']      
        if kwargs.has_key('world') and kwargs['world']:
            world = kwargs['world']
        else:
            world = BulletWorld()
            world.setGravity(Vec3(0, 0, -9.81))
        #nodeRoot = NodePath(filename)
          
        game = GameEntityManager.objectList
        print game
        xmlMap = XmlIO(filename)
        mapData = xmlMap.readFile()
        
        nodeRoot = NodePath(PandaNode("GameRoot"))
        
        mapElement = {}
        gameNode = mapData['Game']
        for key, value in gameNode.items():            
            gameObjects = {}   
            if isinstance(value, dict):
                for objectType, objectData in value.items():
                    builderType = key[0:-1] + "Builder"
                    builder = getattr(sys.modules[__name__], builderType)()
                    id = objectData['Prototype']
                    objectData['objectID'] = id  
                    objectData['Name'] = objectType                    
                    object = game[key][id]                          
                    gameObject = builder.buildGameObject(objectData, game, nodeRoot, world)
                    gameObjects[objectType] = gameObject    
                mapElement[key] = gameObjects
        # Check fog
        if not mapElement.has_key('Fogs') or len(mapElement['Fogs']) == 0:
            nodeRoot.setShaderInput('fog_mode', -1)
            
        # Check MapType
        if gameNode.has_key('MapType'):
            m = getattr(sys.modules[__name__], gameNode['MapType'])(mapElement, world, CameraBase.current)
        else:   
            m = Map(mapElement, world, CameraBase.current)   
        
        # Clear current camera
        CameraBase.clearCurrent()     
        
        # Set nodeRoot to map
        m.nodeRoot = nodeRoot
        
        # Check shadow  
        shadow = nodeRoot.getPythonTag("Shadow")        
        if shadow:
            print "Enable Shadow"
            m.useShadowShader(shadow)
        else:
            m.useCommonShader()
        
        return m

class GuiFactory(BaseFactory):
    
    @staticmethod
    def recursive(guiMgr, key, value):        
        lst_gui = {}
        for gui_var_name, gui_data in value.items():
            guiBuilderType = key + "GuiBuilder"            
            builder = getattr(sys.modules[__name__], guiBuilderType)()
            
            # Process inherit data            
            data_load = builder.processInherit(gui_data)
            if gui_data.has_key('childs') and data_load.has_key('childs'):                
                data_load['childs'].update(gui_data['childs'])
                gui_data.pop('childs')
            data_load.update(gui_data)  
            gui_data = data_load
            
            lst_child = {}
            if gui_data.has_key("childs"):
                childs_data = gui_data["childs"]
                gui_data.pop("childs")
                lst_child = GuiFactory.recursive(guiMgr, key, childs_data)
                            
            if guiMgr.existGui(gui_var_name):
                guiMgr.removeGui(gui_var_name)           
            gui_object = builder.buildGui(guiMgr, gui_var_name, gui_data)
            lst_gui[gui_var_name] = gui_object
            guiMgr.childs[gui_var_name] = gui_object
            guiMgr.parents[gui_var_name] = {"gui" : gui_object, "childs" : lst_child}
            for child in lst_child.values():
                child.reparentTo(gui_object)
        return lst_gui
            
    @staticmethod
    def product(*args, **kwargs):
        if kwargs.has_key('gui'):
            guiMgr = kwargs['gui']
        else:
            guiMgr = GuiManager()
        if kwargs.has_key('filename'):
            filename = kwargs['filename']  
            json_data = open(filename)
            data = json.load(json_data)
        else:
            data = kwargs['data']
        for key, value in data.items():            
            if isinstance(value, dict):
                lst_gui = GuiFactory.recursive(guiMgr, key, value)
                guiMgr.roots.update(lst_gui)   
        guiMgr.refresh()     
        return guiMgr   
        

class InputFactory(BaseFactory):
    @staticmethod
    def product(*args, **kwargs):    
        filename = kwargs['filename']      
        xmlMap = XmlIO(filename)
        inputData = xmlMap.readFile()
        inputNode = inputData['Input'] 
        inputs = []
        for key, value in inputNode.items():            
            if isinstance(value, dict):
                for objectType, objectData in value.items():
                    managerType = key + "InputBuilder"
                    builder = getattr(sys.modules[__name__], managerType)()
                    inp = builder.buildInput(objectType, objectData)
                    inputs.append(inp)
        return inputs
                    


class MapManager(object):
    current = None
    
    def __init__(self):        
        self.mapList = {}
        self.currentIdx = 0     
        self.previous = None
        self.currentMap = None
        MapManager.current = self           
        
    def loadBulletWorld(self, data):
        world = BulletWorld()
        if data.has_key('Gravity'):
            gravity = caster.stringListToListFloat(data['Gravity'].split(','), 0)
        else:
            gravity = [0, 0, -9.81]
        world.setGravity(Vec3(*gravity))
        return world
        
    def loadMapListFromFile(self, filename):
        pass
        
    def getCurrentMap(self):
        return self.mapList.values()[self.currentIdx]
        
    def nextMap(self):
        idx = self.currentIdx + 1
        self.goToMap(idx)
        
    def goToMap(self, idx):
        pass
    

        
class TotalMapManager(MapManager):
    def loadMapListFromFile(self, filename):
        xmlIn = XmlIO(filename)
        xmlData = xmlIn.readFile()        
        
        prototype = xmlData['Game']['Prototypes'].values()[0]
        prototypePath = prototype['Path']
        BuildTypeFactory.product(filename = prototypePath)
        
        xmlData = xmlData['Game']['Maps']    
        
        
        for key, value in xmlData.items():            
            world = None
            if value.has_key('World'):
                world = self.loadBulletWorld(value['World'])            
            self.mapList[key] = MapFactory.product(filename=value['Path'], world=world)
            if value.has_key('Input'):
                inputs = InputFactory.product(filename=value['Input'])
                self.mapList[key].addInput(*inputs)
            if value.has_key('Gui'):
                gui = GuiFactory.product(filename=value['Gui'])
    
        self.mapList.values()[self.currentIdx].applyToScene(render)
        self.mapList.values()[self.currentIdx].active()

    def goToMap(self, idx):
        self.previous = self.getCurrentMap()
        self.getCurrentMap().rejectFromScene()
        self.currentIdx = idx
        self.currentIdx = self.currentIdx % len(self.mapList)
        print "Next map in Idx", self.currentIdx
        self.getCurrentMap().applyToScene(render)

class CacheMapManager(MapManager):
    
    def getCurrentMap(self):
        return self.currentMap
    
    def getIdList(self, filename):
        xmlIn = XmlIO(filename)
        return xmlIn.getAllElementsTextByTagName('Prototype')
        
    def loadMapListFromFile(self, filename, idx=0): 
        # Read map list xml file
        xmlIn = XmlIO(filename)
        xmlData = xmlIn.readFile()      
        # Read map info
        mapsData = xmlData['Game']['Maps']
        self.maps = mapsData.values()
        self.keys = mapsData.keys()
        self.items = mapsData
        mapsLen = len(self.maps)        
        if isinstance(idx, str):
            key = idx
            idx = self.keys.index(key)  
        idx = idx % mapsLen        
        mapLoad = self.maps[idx]
        key = self.keys[idx]
        prototypeData = xmlData['Game']['Prototypes'].values()[0]
        self.prototypePath = prototypeData['Path']
        if mapLoad.has_key('Path'):
            mapPath = mapLoad['Path']
            world = None
            idList = self.getIdList(mapPath)        
            # Initialize prototypes only for map with idx == 0                  
            
            BuildTypeFactory.product(filename=self.prototypePath, idList=idList)                
            # Load map
            if mapLoad.has_key('World'):
                world = self.loadBulletWorld(mapLoad['World'])            
            self.mapList[key] = MapFactory.product(filename=mapPath, world=world)
        else:
            self.mapList[key] = Map()    
        if mapLoad.has_key('Input'):
            inputs = InputFactory.product(filename=mapLoad['Input'])
            self.mapList[key].addInput(*inputs)
            
        if mapLoad.has_key('Gui'):
            gui = GuiFactory.product(filename=mapLoad['Gui'])
            self.mapList[key].setGui(gui)
            gui.rejectFromScene()
            
        self.currentIdx = 0     
        if self.mapList.has_key(key):
            self.currentMap = self.mapList[key]
        # Apply map to scene
        self.mapList.values()[self.currentIdx].applyToScene(render)
        self.mapList.values()[self.currentIdx].active()
                
    def goToWaitMap(self, waitmap_name, next_map_name, delay=0.5):
        s = Sequence(
                 Func(self.goToMap, waitmap_name),
                 Wait(delay),
                 Func(self.goToMap, next_map_name))        
        s.start()
        
                
    def getIdxFromName(self, map_name):
        return self.keys.index(map_name) 
    
    def initPrototype(self, idList):
        BuildTypeFactory.product(filename=self.prototypePath, idList=idList) # Load map

    def loadMap(self, map_name):        
        self.previous = self.getCurrentMap()
        idx = self.keys.index(map_name) 
        self.currentIdx = idx
        self.currentIdx = self.currentIdx % len(self.maps)
        mapLoad = self.maps[self.currentIdx]
        map_name = self.keys[self.currentIdx]
        print "Cache: Next map", self.mapList, map_name
        if not self.mapList.has_key(map_name):
            if mapLoad.has_key('Path'):
                mapPath = mapLoad['Path']
                idList = self.getIdList(mapPath)
                print "Cache: Next map in Idx", self.currentIdx # Initialize prototypes only for map with idx == 0
                BuildTypeFactory.product(filename=self.prototypePath, idList=idList) # Load map
                world = None
                if mapLoad.has_key('World'):
                    world = self.loadBulletWorld(mapLoad['World'])
                self.mapList[map_name] = MapFactory.product(filename=mapPath, world=world)
            else:
                self.mapList[map_name] = Map()
            if mapLoad.has_key('Input'):
                inputs = InputFactory.product(filename=mapLoad['Input'])
                self.mapList[map_name].addInput(*inputs)
            if mapLoad.has_key('Gui'):
                gui = GuiFactory.product(filename=mapLoad['Gui'])
                self.mapList[map_name].setGui(gui)
                gui.rejectFromScene()
        
        if self.mapList.has_key(map_name):
            self.currentMap = self.mapList[map_name]
            

    def goToMap(self, idx):
        self.getCurrentMap().rejectFromScene()                
        self.loadMap(idx)
        # Apply map to scene
        self.getCurrentMap().applyToScene(render)