from game.baseobject import Store
from game.factory import MapManager, TotalMapManager, CacheMapManager
from game.map import Map
from utility import getModule
import __builtin__
from game.manager import GameEntityManager

class Game():
    def __init__(self):
        self.store = Store()
        self.totalMgr = TotalMapManager()
        self.cacheMgr = CacheMapManager()
        self.mapMgr = self.cacheMgr             
        self.updateBuiltin()
        
    def editorMode(self, active):
        if active == True:
            self.mapMgr = self.totalMgr
        else:
            self.cacheMgr = self.totalMgr
        self.updateBuiltin()
            
    def updateBuiltin(self):
        print "update builtin"
        # Global variables
        __builtin__.store = self.store
        __builtin__.mapMgr = self.mapMgr
        __builtin__.screen = Map
        __builtin__.getModule = getModule
        __builtin__.entityMgr = GameEntityManager
        
game = Game()