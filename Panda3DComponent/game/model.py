'''This contains all model entity like: human, car, building, tree, flower, etc...
These models is loaded from .egg or .bam file.
These models 'now' only references to:
    - nodepath.
    - parent (model).
    - nodename (use for searching in scene graph).
    
HOW TO USE:
    - First, you call createModel static method of ModelFactory class.
    - Finally, you retreive the result by variable.
'''
import sys
from baseobject import GameObject
from direct.actor.Actor import Actor
from panda3d.bullet import BulletCharacterControllerNode
from panda3d.core import Vec3

class ModelFactory():
    '''Factory pattern: use for creating model in this module.'''
    @staticmethod
    def createModel(modelType, *args, **kwargs):
        model = getattr(sys.modules[__name__], modelType)(*args, **kwargs)
        return model
    
class GameModel(GameObject):    
    def clone(self):
        clone = GameObject.clone(self)
        if isinstance(self.nodePath, Actor):
            clone.nodePath = Actor(other=self.nodePath)
        return clone    
    
# Empty nodePath
class Component(GameObject):
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        
# Organism in the worl
class Organism(GameModel):
    '''All about animal, human, plant, insects, ... live in the world.'''
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        self.crouching = False
        self.direction = Vec3(0, 0, 0)
        self.speed = 1
        self.omega = 0
        self.jumpHeight = 5
        self.jumpSpeed = 8
        
    # Edit in use (these lines of code below is only an example)
    def move(self):  
        if isinstance(self.physicNode.node(), BulletCharacterControllerNode):
            node = self.physicNode.node()
            node.setAngularMovement(self.omega)
            node.setLinearMovement(self.direction * self.speed, True)
        
    # Edit in use (these lines of code below is only an example)    
    def jump(self):
        if isinstance(self.physicNode.node(), BulletCharacterControllerNode):
            self.player.setMaxJumpHeight(self.jumpHeight)
            self.player.setJumpSpeed(self.jumpSpeed)
            self.player.doJump()
      
    
    def doCrouch(self):
        if isinstance(self.physicNode.node(), BulletCharacterControllerNode):
            self.crouching = not self.crouching
            sz = self.crouching and 0.6 or 1.0 
            node = self.physicNode.node()
            node.getShape().setLocalScale(Vec3(1, 1, sz))     
            self.physicNod.setScale(Vec3(1, 1, sz) * 0.3048)
            self.physicNod.setPos(0, 0, -1 * sz)
        
class Human(Organism):
    pass

class Monster(Organism):
    pass

class Insect(Organism):
    pass

class Plant(Organism):
    pass

class Tree(Organism):
    pass

class Rock(Organism):
    pass

class Animal(Organism):
    pass

class Architecture(GameModel):
    pass

class Building(Architecture):
    '''Building class.'''
    pass

class Road(Architecture):
    pass

class Vehicle(GameModel):      
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        self.vehicle = None
        # Steering info
        self.steersIdx = []
        self.forcesIdx = []
        self.steering = 0.0           
        self.steeringClampMax = 45.0     
        self.steeringIncrementMax = 5 
        self.turnState = 'stand'
        
        # Engine info        
        self.force = 0
        self.brake = 0     
        
        # Wheels
        self.wheels = []
    
    def destroy(self):    
        for wheel in self.wheels:
            wheel.removeNode()
        
    def setMaxForce(self, value):
        self.tuning = self.vehicle.getTuning()
        self.tuning.setMaxSuspensionForce(value)
                
    def go(self, force = 100, brake = 2):
        self.force = force
        self.brake = brake
        
    def back(self, force = 100, brake = 2):
        self.brake = brake
        self.force = -force
        
    def slide(self):
        self.brake = abs(self.force * 1.0 / 100)
        self.force = 0
        
    def turnRight(self):
        self.mark = -1
        self.turnState = 'turn'
        
    def turnLeft(self):        
        self.mark = 1
        self.turnState = 'turn'
        
    def notTurn(self):
        self.turnState = 'stand'
        
        
        
            
    def updateVehicle(self):   
        dt = globalClock.getDt()        
        if self.turnState == 'turn':          
            self.steeringIncrement = self.steeringIncrementMax / max ( abs(self.vehicle.getCurrentSpeedKmHour())/ 3, 1) 
            self.steeringClamp = self.steeringClampMax /  max ( abs(self.vehicle.getCurrentSpeedKmHour()/ 90), 1)
            self.steering += self.mark * dt * self.steeringIncrement * 30
            self.steering = self.mark * min(abs(self.steering), self.steeringClamp)
        else:
            dt *= 5
            self.steering =  self.steering * ( 1 - dt) + 0 * dt
        
        # Apply engine and brake to rear wheels
        for idx in self.forcesIdx:
            self.vehicle.applyEngineForce(self.force, idx)
            self.vehicle.setBrake(self.brake, idx)
        # Apply steering to front wheels
        for idx in self.steersIdx:
            self.vehicle.setSteeringValue(self.steering, idx)
      

class HouseHold(GameModel):
    pass

class Item(GameModel):
    pass

class Car(Vehicle):
    '''Car class.'''
    pass