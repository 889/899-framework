from baseobject import GameObject
from panda3d.core import NodePath

class ParticleBase(GameObject):

	def __init__(self, *args, **kwargs):
		GameObject.__init__(self, *args, **kwargs)
		self.center = NodePath('center' + self.getName())	

	def destroy(self):
		self.particle.cleanup()
		self.center.removeNode()
		GameObject.destroy(self)			

	def setEffect(self, effect):
		self.particle = effect

	def startEffect(self, nodeRoot):
		self.center.reparentTo(nodeRoot)
		np = self.nodePath
		if not nodeRoot:
			nodeRoot = np
		self.particle.start(parent=np, renderParent=self.center)
#    def __init__(self, nodePath, nodeName):
#        self.nodeName = nodeName
#        self.nodePath = nodePath

        