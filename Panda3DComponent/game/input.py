from game.message import GameMessage
from panda3d.core import CollisionRay, CollisionNode, CollisionHandlerQueue, CollisionTraverser, CollisionTube
from panda3d.core import Geom, GeomVertexFormat, GeomVertexWriter, GeomLines, GeomVertexData, GeomNode 
from panda3d.core import Point3
  

class Pointer():
    def __init__(self):
#        self.selectedNode = None
#        self.cQueue = CollisionHandlerQueue()        
#        self.cTrav = CollisionTraverser() ###
#        pickerNode = CollisionNode('mouseRay') ###
#        pickerNP = base.camera.attachNewNode(pickerNode) ###
#        pickerNode.setFromCollideMask(GeomNode.getDefaultCollideMask())###
#        self.pickerRay = CollisionRay()###
#        pickerNode.addSolid(self.pickerRay)###
#        self.cTrav.addCollider(pickerNP, self.cQueue)###
        pass
        
    
    def getClosest(self): # Physic Engine Collision - Bullet
        '''
        return the result for test closest in bullet
        can use these function with the return value:
            result.getHitPos()
            result.getNode()
            result.getHitNormal()
            result.getHitFraction()
        '''
        if base.mouseWatcherNode.hasMouse(): ###
            pMouse = base.mouseWatcherNode.getMouse()
            pFrom = Point3()
            pTo = Point3()
            base.camLens.extrude(pMouse, pFrom, pTo)
             
            # Transform to global coordinates
            pFrom = render.getRelativePoint(base.camera, pFrom)
            pTo = render.getRelativePoint(base.camera, pTo)
            from datetime import datetime
            t = datetime.now()
            result = screen.current.world.rayTestClosest(pFrom, pTo)
            delay = datetime.now() - t
#             print "delay:", delay
#             print "has Hit", result.hasHit()
#             print "get Hit Pos", result.getHitPos()
#             print "get Hit Normal", result.getHitNormal()
#             print "get hit fraction",result.getHitFraction()
#             print "get node", result.getNode()
            
            if result.hasHit():
                return result
            else:
                return None

    def select(self): # PandaCollision
        pass
#        import datetime
#        print datetime.datetime.now()
#        if base.mouseWatcherNode.hasMouse(): ###
#            # This gives up the screen coordinates of the mouse.
#            mpos = base.mouseWatcherNode.getMouse()    ###        
#            # This makes the ray's origin the camera and makes the ray point 
#            # to the screen coordinates of the mouse.
#            self.pickerRay.setFromLens(base.camNode, mpos.getX(), mpos.getY())###
#            self.cTrav.traverse(base.render) ###
#            # Assume for simplicity's sake that myHandler is a CollisionHandlerQueue.            
#            if self.cQueue.getNumEntries() > 0:  ###       
#                self.cQueue.sortEntries()
#                self.selectedNode = self.cQueue.getEntry(0).getIntoNodePath()### 
#                return str(self.selectedNode)
#        
#        print datetime.datetime.now()
        
        
class Input():
    #pointer = Pointer()
    def fire(self, key, tags):
        mess = GameMessage('System', tags, tags)
        print "fire", mess.fromTag, mess.toTagList, mess.info, mess
                
        if "mouse1" == key:            
            mess = GameMessage('System', tags, tuple(tags) + (screen.current.pointer.getClosest(),))
#            
#            #print self.model.nodeName()
        screen.dispatch(mess)
 

class Panda3DInput(Input):
    
    def __init__(self, key, tags):
        self.key = key
        self.tags = tags
        self.start()
        
    def start(self):
        base.accept(self.key, self.fire, [self.key, self.tags])
        
    def stop(self):
        base.ignore(self.key)
        
        
        