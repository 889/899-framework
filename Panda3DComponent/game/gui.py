from direct.gui.DirectGui import *
from utility.pritority import Priority

class GuiManager():
    """
    Because Panda3D doesn't have manager for DirectGUI. Because Panda3d subclass style.
    So this GuiManager is created to support manage all gui in scene.
    
    Now: It's still bug when adding new feature (getParent)
    """        
    INIT_TYPE = "init"
    RUN_TYPE = 'run'
    
    def __init__(self):
        self.aspect2dRoots = {} # childs of aspect2d     
        self.childs = {}  # all gui control
        self.render2dRoots = {} # childs of render2d
        self.roots = {} # childs of aspect2d and render2d
        self.scripts = {} # all scripts for all gui        
        self.parents = {} # all gui control with its child (still bug with dynamic create gui)
        self.guiScripts = {} # script for seperate gui in manager, start script at run time
        
    
    def getParent(self, gui):
        """
        Get parent of gui.
        
        Still bug when creating gui from script, exist guis doesn't update.
        """
        print "parent guis:", self.parents
        for parent in self.parents.values():
            childs = parent['childs']
            if gui in childs.values():
                return parent['gui']
        
    def updateGui(self, otherGuiMgr):
        """
        Update gui childs, roots from other gui.
        (Need call refresh() after this method)
        """
        self.roots.update(otherGuiMgr.roots)
        self.childs.update(otherGuiMgr.childs)
        self.refresh()
        
    def addGuiScript(self, gui_name, script, script_type=INIT_TYPE):
        """
        Append script to seperate gui.
        Need call startGuiScripts at run time to use these scripts.
        """
        if not self.guiScripts.has_key(gui_name):
            self.guiScripts[gui_name] = {}    
        scripts = self.guiScripts[gui_name]
        if not scripts.has_key(script_type):
            scripts[script_type] = []
        scripts[script_type].append(script)
        
    def startGuiScripts(self, gui_name):
        """
        Start scripts of gui control have gui_name like the parameter.
        Use in run time.
        """
        for groupname, scripts in self.guiScripts[gui_name].items():
            if groupname == GuiManager.INIT_TYPE:
                for script in scripts:
                    taskMgr.add(script, "gui init script", sort=Priority.Init)
            else:
                for script in scripts:
                    taskMgr.add(script, "gui script", sort=Priority.Gui)
        
        
    def addScript(self, script, script_type=INIT_TYPE):
        """
        Add scripts for gui control.
        Need call startScripts() when dynamic create gui control.
        """
        if not self.scripts.has_key(script_type):
            self.scripts[script_type] = []
        self.scripts[script_type].append(script)
            
    def startScripts(self):
        """
        Start all scripts for gui control.
        """
        for groupname, scripts in self.scripts.items():
            if groupname == GuiManager.INIT_TYPE:
                for script in scripts:
                    taskMgr.add(script, "gui init script", sort=Priority.Init)
            else:
                for script in scripts:                    
                    taskMgr.add(script, "gui script", sort=Priority.Gui)
    
    def stopScripts(self):
        """
        Stop all scripts for gui control.
        """
        taskMgr.remove("gui script")
        
    def getGui(self, gui_name):
        """
        Get gui control from gui manager.
        Common use: screen.current.gui.getGui(gui)
        """
        if self.childs.has_key(gui_name):
            return self.childs[gui_name]
    
    def existGui(self, gui_name):
        """
        Check if gui exist in gui manager.
        """
        return self.childs.has_key(gui_name)
    
    def removeGui(self, gui_name):
        """
        Remove gui from childs, roots, aspect2dRoots, render2dRooots.
        
        """
        if self.childs.has_key(gui_name):
            if self.aspect2dRoots.has_key(gui_name):
                self.aspect2dRoots.pop(gui_name)
                self.roots.pop(gui_name)
            if self.render2dRoots.has_key(gui_name):
                self.render2dRoots.pop(gui_name)
                self.roots.pop(gui_name)
            if self.parents.has_key(gui_name):
                self.parents.pop(gui_name)            
            gui = self.childs.pop(gui_name)
            gui.removeNode()
            
    def refresh(self):        
        """
        Refresh gui position in roots, aspect2d, render2d.
        (Not in self.parents, still bug here)
        """
        for child_name, child in self.roots.items():            
            parent = child.getParent() 
            #print "child that:", child_name, child
            if parent != aspect2d or parent != render2d:                
                if self.roots.has_key(child.getName()):
                    self.roots.pop(child.getName()) 
                if self.aspect2dRoots.has_key(child.getName()):
                    self.aspect2dRoots.pop(child.getName())
                if self.render2dRoots.has_key(child.getName()):
                    self.render2dRoots.pop(child.getName())                 
        for child in self.childs.values():
            if child.getParent() == aspect2d:
                self.aspect2dRoots[child.getName()] = child
                self.roots[child.getName()] = child
            if child.getParent() == render2d:
                self.render2dRoots[child.getName()] = child
                self.roots[child.getName()] = child
                
    def rejectFromScene(self, stopScript=False):
        """
        Reject all gui control from scene. Common using in map.py
        """
        self.refresh()    
        for child in self.aspect2dRoots.values():            
            child.detachNode()
        for child in self.render2dRoots.values():            
            child.detachNode()
        if stopScript == True:
            self.stopScripts()

    def applyToScene(self):
        """
        Apply all gui control to scene. Common using in map.py
        """
        for child in self.aspect2dRoots.values():
            child.reparentTo(aspect2d) 
        for child in self.render2dRoots.values():
            child.reparentTo(render2d)  
        self.startScripts()       
    