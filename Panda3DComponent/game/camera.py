from utility.mouse import *
from panda3d.core import Vec3, Camera
from baseobject import GameObject
import sys
from utility.pritority import Priority

class CameraFactory():
    '''Factory pattern: use for creating model in this module.'''
    @staticmethod
    def createCamera(camType, *args, **kwargs):
        camera = getattr(sys.modules[__name__], camType)(*args, **kwargs)
        return camera
    
class CameraBase(GameObject):
    current = None
    
    def __init__(self, *args, **kwargs):
        GameObject.__init__(self, *args, **kwargs)
        if not isinstance(self.nodePath.node(), Camera):
            raise Exception("This node is not Camera") 
             
    def startScripts(self):
        if self.isActive():
            for func in self.initFuncs:
                self.funcTasks.append( taskMgr.add(func, "script %s" % self.getName(), sort=Priority.Init) )        
            for script in self.scripts:            
                script.task = taskMgr.add(script.run, "script %s" % self.getName(), uponDeath=script.cleanUp, sort=Priority.Camera)
            for func in self.funcs:
                self.funcTasks.append( taskMgr.add(func, "script %s" % self.getName(), sort=Priority.Camera) )
            
    def start(self):
        taskMgr.add(self.update, "update camera", sort=Priority.Camera)
        
    def stop(self):
        taskMgr.remove("update camera")
        
    def update(self, task):
        pass
        
    def getLens(self):
        return self.node().getLens()
    
    def setLens(self, lens):
        self.node().setLens(lens)
    
    def setFov(self, fov):
        lens = self.getLens()
        lens.setFov(fov)
        self.setLens(lens)
        
    def setNear(self, near):
        lens = self.getLens()
        lens.setNear(near)
        self.setLens(lens)
        
    def setFar(self, far):
        lens = self.getLens()
        lens.setFar(far)
        self.setLens(lens)
        
    def setFilmSize(self, filmSize):
        lens = self.getLens()
        lens.setFilmSize(filmSize)
        self.setLens(lens)
                    
    def isActive(self):
        return CameraBase.current == self
            
    def active(self, isActive):
        if isActive:
            if CameraBase.current:
                base.camera.reparentTo(CameraBase.camParent)
                CameraBase.current.stop()
                CameraBase.current.stopScripts()
            CameraBase.current = self       
            CameraBase.camParent = base.camera.getParent()     
            base.camera.reparentTo(self.realNP)
            base.cam.node().setScene(render)
            base.cam.node().setLens(self.getLens())
            CameraBase.current.start()
            CameraBase.current.startScripts()
        else:
            if CameraBase.current:
                base.camera.reparentTo(CameraBase.camParent)
                CameraBase.current.stop()
                CameraBase.current.stopScripts()
            CameraBase.current = None       
            
    @staticmethod
    def clearCurrent():    
        CameraBase.current = None
                    
class StaticCamera(CameraBase):            
    def __init__(self, *args, **kwargs):
        CameraBase.__init__(self, *args, **kwargs)
        
class FreeCamera(CameraBase):
    def __init__(self, *args, **kwargs):
        CameraBase.__init__(self, *args, **kwargs)
        self.prevMouse = getMousePos()        
        self.speed = 40
        self.rotSpeed = 400        
        self.direction = {"left":0, "right":0, "forward":0, "backward":0}
        
    def active(self, isActive):
        CameraBase.active(self, isActive)        
        
    def start(self):        
        global hidden
        self.prevMouseHidden = hidden
        print "start free cam", self.prevMouseHidden
        hideMouse(True)
        CameraBase.start(self)
        base.accept("arrow_up", self.setDirection, ["forward", True])
        base.accept("arrow_down", self.setDirection, ["backward", True])
        base.accept("arrow_left", self.setDirection, ["left", True])
        base.accept("arrow_right", self.setDirection, ["right", True])
        base.accept("arrow_up-up", self.setDirection, ["forward", False])
        base.accept("arrow_down-up", self.setDirection, ["backward", False])
        base.accept("arrow_left-up", self.setDirection, ["left", False])
        base.accept("arrow_right-up", self.setDirection, ["right", False])  
        setMousePos(base.win.getXSize() / 2, base.win.getYSize() / 2)   
        
    def stop(self):        
        hideMouse(self.prevMouseHidden)
        CameraBase.stop(self)
        base.ignore("arrow_up")
        base.ignore("arrow_down")
        base.ignore("arrow_left")
        base.ignore("arrow_right")
        base.ignore("arrow_up-up")
        base.ignore("arrow_down-up")
        base.ignore("arrow_left-up")
        base.ignore("arrow_right-up")  
        
    def setDirection(self, name, toogle):
        self.direction[name] = toogle   

    def update(self, task):  
        dt = globalClock.getDt()
        camPos = self.nodePath.getPos()
        # Keyboard
        vecForward = base.render.getRelativeVector(self.nodePath,(0,1,0))
        vecRight = base.render.getRelativeVector(self.nodePath,(1,0,0)) 
        vecForward *= dt * self.speed
        vecRight *= dt * self.speed                
        if (self.direction["forward"] == True):
            self.nodePath.setPos(camPos + vecForward)
        if (self.direction["backward"] == True):
            self.nodePath.setPos(camPos - vecForward)
        if (self.direction["left"] == True):
            self.nodePath.setPos(camPos - vecRight)
        if (self.direction["right"] == True):
            self.nodePath.setPos(camPos + vecRight)            
        #Mouse           
        mousePos = getMousePos()            
        if mousePos is not None :        
            curP = self.nodePath.getP()
            curH = self.nodePath.getH()
            self.nodePath.setP(curP + mousePos[1] * dt * self.rotSpeed)
            self.nodePath.setH(curH - mousePos[0] * dt * self.rotSpeed * 4)
        base.camera.setPos(self.getPos())
        base.camera.setHpr(self.getHpr())
        setMousePos(base.win.getXSize() / 2, base.win.getYSize() / 2)        
        return task.cont                    
    
    
class FollowCamera(CameraBase):
    def __init__(self, *args, **kwargs):
        CameraBase.__init__(self, *args, **kwargs)
#        self.height = 10
#        self.back = 15
        self.height = 0.35
        self.back = 8
        self.distance = 15000  # The camera will not farer this number
        self.target = None
        self.lookHeight = 0
        self.direction = -1
        
    def start(self):
        taskMgr.add(self.update, "update follow camera", sort=Priority.Camera)
        
    def stop(self):
        taskMgr.remove("update follow camera")
        
    def update(self, task):
        if isinstance(self.target, GameObject):
            dt = globalClock.getDt()            
            target = self.target.nodePath            
            vecForward = base.render.getRelativeVector(target, (0,self.direction,0))                        
            vecForward *= -self.back
            vecForward += Vec3(0, 0, self.height)
            prevPos = self.getPos()
#             pos = target.getPos() + vecForward
#             p = prevPos * ( 1 - dt) 
#             p += pos * dt
            
            pos = self.target.getPos() + vecForward     #current position
            disVector = self.target.getPos() - self.getPos()
            distance = disVector.length()
            #better 12 < distance < 15
            if distance > self.distance - 3:
                dt = self.distance/distance
            else:
                dt = 1 - dt
            p = prevPos * dt                 # newCamPos = prePos * (1-dt) + currentPos * (dt)
            p += pos * ( 1 - dt)                           #What the hell is it ?
            
            self.setPos(p)            
            lookPos = target.getPos()
            lookPos.setZ(lookPos.getZ() + self.lookHeight)
            self.lookAt(lookPos)
            
            
        return task.cont
        
#class FreeCamera(CameraBase):    
#    def __init__(self, speed=40.0, rotSpeed=400, *args, **kwargs):        
#        CameraBase.__init__(self, *args, **kwargs)
#        self.prevMouse = getMousePos()        
#        self.speed = speed
#        self.rotSpeed = rotSpeed        
#        self.direction = {"left":0, "right":0, "forward":0, "backward":0}     
#    
#    def onScene(self):
#        base.accept("arrow_up", self.setDirection, ["forward", True])
#        base.accept("arrow_down", self.setDirection, ["backward", True])
#        base.accept("arrow_left", self.setDirection, ["left", True])
#        base.accept("arrow_right", self.setDirection, ["right", True])
#        base.accept("arrow_up-up", self.setDirection, ["forward", False])
#        base.accept("arrow_down-up", self.setDirection, ["backward", False])
#        base.accept("arrow_left-up", self.setDirection, ["left", False])
#        base.accept("arrow_right-up", self.setDirection, ["right", False])  
#        setMousePos(base.win.getXSize() / 2, base.win.getYSize() / 2)   
#    
#    def update(self, task):        
#        if self.isActive():
#            dt = globalClock.getDt()
#            camPos = self.nodePath.getPos()
#            # Keyboard
#            vecForward = base.render.getRelativeVector(self.nodePath,(0,1,0))
#            vecRight = base.render.getRelativeVector(self.nodePath,(1,0,0)) 
#            vecForward *= dt * self.speed
#            vecRight *= dt * self.speed                
#            if (self.direction["forward"] == True):
#                self.nodePath.setPos(camPos + vecForward)
#            if (self.direction["backward"] == True):
#                self.nodePath.setPos(camPos - vecForward)
#            if (self.direction["left"] == True):
#                self.nodePath.setPos(camPos - vecRight)
#            if (self.direction["right"] == True):
#                self.nodePath.setPos(camPos + vecRight)            
#            #Mouse           
#            if self.firstTime:
#                mousePos = [0, 0]
#                self.nodePath.setPos(self.pos)
#                self.nodePath.setHpr(self.hpr)                
#                self.firstTime = False
#            else:
#                mousePos = getMousePos()            
#            if mousePos is not None :        
#                curP = self.nodePath.getP()
#                curH = self.nodePath.getH()
#                self.nodePath.setP(curP + mousePos[1] * dt * self.rotSpeed)
#                self.nodePath.setH(curH - mousePos[0] * dt * self.rotSpeed * 4)
#            setMousePos(base.win.getXSize() / 2, base.win.getYSize() / 2)
#            self.hpr = self.nodePath.getHpr()
#            self.pos = self.nodePath.getPos()
#        return task.cont                    
#    
#    def setDirection(self, name, toogle):
#        self.direction[name] = toogle    
#        
#        
#class StaticCamera(CameraBase):
#    def __init__(self, *args, **kwargs):        
#        CameraBase.__init__(self, *args, **kwargs)              
        