# Author: thien0291 with Ninth's help
# Panda3D Forum >> Showcase >> ...
#
# based partially on "Roaming Ralph" and the "Nature Demo" from the panda forums and Gsk Terrain Demo
# 
# Simple Ocean Surface with Perlin Noise Algorithm
# 
# Using A Perlin Noise and push it to shader. This texture color will set height of map (Geomipmap)
#
# Using a panda task to slide texture by time.
# 
# This sample using GLSL to run almost graphic device (ATI video card has some trouble when push texture to vertex shader - Cg)
# In this sample. I don't use LOD. but if you can modify it ... why don't try ?!
#
# Contact:
# Panda3d Forum: thien0291
# Facebook : http://facebook.com/vngoogle
# Skype : webmaster3t
# Yahoo Messenger : thien291
#
# Special thanks to Ninth (Panda3d forum)
#
# For using in other project. Remember Coca cola law (I can drink beer :]])g
#
# *Coca cola law. If I see you in a beautiful day, I hope have a coca cola can :)
'''
Ocean Shader 
'''

import direct.directbase.DirectStart
from pandac.PandaModules import CollisionTraverser,CollisionNode
from pandac.PandaModules import CollisionHandlerQueue,CollisionRay
from pandac.PandaModules import Filename
from pandac.PandaModules import PandaNode,NodePath,Camera,TextNode
from pandac.PandaModules import Vec3,Vec4,BitMask32
from pandac.PandaModules import TextureStage
from pandac.PandaModules import TexGenAttrib
from pandac.PandaModules import GeoMipTerrain
from pandac.PandaModules import CardMaker
from pandac.PandaModules import Texture
from pandac.PandaModules import TextureStage
from pandac.PandaModules import WindowProperties
from pandac.PandaModules import TransparencyAttrib
from pandac.PandaModules import AmbientLight
from pandac.PandaModules import DirectionalLight
from pandac.PandaModules import VBase4
from pandac.PandaModules import Vec4
from pandac.PandaModules import Point3

from pandac.PandaModules import Plane
from pandac.PandaModules import PlaneNode
from pandac.PandaModules import PStatClient
from pandac.PandaModules import CullFaceAttrib
from pandac.PandaModules import RenderState
from pandac.PandaModules import ShaderAttrib

from direct.gui.OnscreenText import OnscreenText
from direct.actor.Actor import Actor
from direct.task.Task import Task
from direct.showbase.DirectObject import DirectObject
import random, sys, os, math
from panda3d.core import Shader

SPEED = 0.5

# Figure out what directory this program is in.
MYDIR=os.path.abspath(sys.path[0])
MYDIR=Filename.fromOsSpecific(MYDIR).getFullpath()
print('running from:'+MYDIR)

# Function to put instructions on the screen.
def addInstructions(pos, msg):
    return OnscreenText(text=msg, style=1, fg=(1,1,1,1),
            pos=(-1.3, pos), align=TextNode.ALeft, scale = .05)

def addTextField(pos, msg):
    return OnscreenText(text=msg, style=1, fg=(1,1,1,1),
            pos=(-1.3, pos), align=TextNode.ALeft, scale = .05, mayChange=True)

# Function to put title on the screen.
def addTitle(text):
    return OnscreenText(text=text, style=1, fg=(1,1,1,1),
                    pos=(1.3,-0.95), align=TextNode.ARight, scale = .07)

        
class myGeoMipTerrain(GeoMipTerrain):
    def __init__(self, name):
        GeoMipTerrain.__init__(self, name)
        self.count = 0
        render.setShaderInput('slide4', Vec4(1,1,1,1))
        taskMgr.add(self.slide, "task surface slide")
    
    def slide(self, task):        
        """ It will make ocean surface slide by time -- See more in shaders/ver.sha"""
        self.count = (self.count + globalClock.getDt()) % 512
        render.setShaderInput('slide4', Vec4(self.count, self.count, self.count, self.count))
        print "self count", self.count
        self.update(self)
        return task.cont

    def update(self, dummy):
        #GeoMipTerrain.update(self)
        print "I am Update"
        
    def _setup_camera(self):
        
        sa = ShaderAttrib.make( )
        GLSLshader = Shader.load(Shader.SLGLSL, 'shaders/ver.sha', 'shaders/Frag.sha')
        sa = sa.setShader(GLSLshader)
        
        cam = base.cam.node()
        cam.getLens().setNear(0.001)
        cam.getLens().setFar(5000)
        cam.setTagStateKey('Normal') 
        cam.setTagState('True', RenderState.make(sa)) 
        
    def setMultiTexture(self):
        root = self.getRoot()
        root.setShaderInput('tscale', Vec4(16.0, 16.0, 16.0, 1.0))    # texture scaling
        
        
        ambient = Vec4(0.34, 0.3, 0.3, 1)
        direct = Vec4(0.74, 0.7, 0.7, 1)
        root.setShaderInput('lightvec', Vec4(0.7, 0.2, -0.2, 1))
        root.setShaderInput('lightcolor', direct)
        root.setShaderInput('ambientlight', ambient)

        tex1 = loader.loadTexture('textures/BlueTex.jpg')
        tex1.setMinfilter(Texture.FTNearestMipmapLinear)
        tex1.setMagfilter(Texture.FTLinear)
        tex2 = loader.loadTexture('textures/water.tga')
        tex2.setMinfilter(Texture.FTNearestMipmapLinear)
        tex2.setMagfilter(Texture.FTLinear)
        tex3 = loader.loadTexture('textures/water.tga')
        tex3.setMinfilter(Texture.FTNearestMipmapLinear)
        tex3.setMagfilter(Texture.FTLinear)
        tex4 = loader.loadTexture('textures/perlinNoise.jpg')
        tex4.setMinfilter(Texture.FTNearestMipmapLinear)
        tex4.setMagfilter(Texture.FTLinear)

        alp1 = loader.loadTexture('textures/land01_Alpha_1.png')
        alp2 = loader.loadTexture('textures/land01_Alpha_2.png')
        alp3 = loader.loadTexture('textures/land01_Alpha_3.png')
        alp4 = loader.loadTexture('textures/land01_Alpha_3.png')

        ts = TextureStage('tex1')    # stage 0
        root.setTexture(ts, tex1)        
        ts = TextureStage('tex2')    # stage 1
        root.setTexture(ts, tex2)
        ts = TextureStage('tex3')    # stage 2
        root.setTexture(ts, tex3)
        ts = TextureStage('tex4')    # stage 2
        root.setTexture(ts, tex4)

        ts = TextureStage('alp1')    # stage 3
        root.setTexture(ts, alp1)
        ts = TextureStage('alp2')    # stage 4
        root.setTexture(ts, alp2)
        ts = TextureStage('alp3')    # stage 5
        root.setTexture(ts, alp3)
        ts = TextureStage('alp4')    # stage 5
        root.setTexture(ts, alp4)

        # enable use of the two separate tagged render states for our two cameras
        root.setTag( 'Normal', 'True' ) 
        root.setTag( 'Clipped', 'True' ) 
        self._setup_camera()

        
class World(DirectObject):

    def __init__(self):
  
        # -------------------------------------------------------------------
        # Set up the environment
        
        # GeoMipTerrain
        self.terrain = myGeoMipTerrain('terrain')
        self.terrain.setHeightfield(Filename('textures/perlinNoise.jpg'))

        # Set terrain properties
        self.terrain.setBlockSize(512)
        self.terrain.setFactor(100)
        self.terrain.setFocalPoint(base.camera)
        self.terrain.setNearFar(512,512)

        # Store the root NodePath for convenience
        root = self.terrain.getRoot()
        root.reparentTo(render)
        root.setSz(1)    # z (up) scale

        # Generate it.
        self.terrain.generate()

        # texture
        # self.terrain.setMonoTexture()
        self.terrain.setMultiTexture()      
        self.environ = self.terrain   
        
        # add some lighting
        ambient = Vec4(0.34, 0.3, 0.3, 1)
        direct = Vec4(0.74, 0.7, 0.7, 1)
        
        # ambient light
        alight = AmbientLight('alight')
        alight.setColor(ambient)
        alnp = render.attachNewNode(alight)
        render.setLight(alnp)
        
        # directional ("the sun")
        dlight = DirectionalLight('dlight')
        dlight.setColor(direct)
        dlnp = render.attachNewNode(dlight)
        dlnp.setHpr(0.7,0.2,-0.2)
        render.setLight(dlnp)

addInstructions(-15, "Water Effect with perlinNoise Algorithm")
addInstructions(-25, "Water Effect with perlinNoise Algorithm")
addInstructions(-35, "Water Effect with perlinNoise Algorithm")
base.setFrameRateMeter(True)
print('instancing world...')
w = World()

base.wireframeOn()
print('calling run()...')
run()

