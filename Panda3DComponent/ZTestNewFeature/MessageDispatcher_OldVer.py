'''
Created on Mar 27, 2013

@author: anhsaodem
'''
class SmokeGameObject():
    def __init__(self, nodeName, objectTag):
        self.eventQueue = []
        self.nodeName = nodeName         
        self.objectTag = objectTag
        self.objectTag.append(nodeName)

class GameMessage():
    def __init__(self, fromTag, toTagList, info):
        self.fromTag = fromTag
        self.toTagList = toTagList
        self.info = info
        
class MessageDispatcher():
    def __init__(self):
        self.objectTagsList = {}
    
    #----------------------------------------------------------------
    
    def addObjectWithTag(self, gameObject, tagList):
        #Add to tag {'Car|Ferrari|F40' : objectRef}
        #Find Tag can use with if tag in key
        key = "|gameObject|"
        for tag in tagList:
            key = key + tag + "|"
            
        self.objectTagsList[key] = gameObject
        
    #----------------------------------------------------------------
    
    def addObject(self, gameObject):
        tagList = gameObject.objectTag
        self.addObjectWithTag(gameObject, tagList)
    
    #----------------------------------------------------------------
    
    def findObjectWithTag(self, tag):
        # Find all object has tag
        lstResult = []
        tag = '|' + tag + "|"
        for tagList in self.objectTagsList.keys():
            if tag in tagList:
                lstResult.append(self.objectTagsList[tagList])
        
        return lstResult
    
    #----------------------------------------------------------------
    
    def dispatch(self, gameMessage):
        receiveObjects = {}        
        tagList = gameMessage.toTagList
        
        for tag in tagList:
            lstObjectHasTag = self.findObjectWithTag(tag)
            for object1 in lstObjectHasTag:
                receiveObjects[object1.nodeName] = object1 # There is no object has duplicate in this Dictionary
        
        for key in receiveObjects.keys():
            receiver = receiveObjects[key]
            receiver.eventQueue.append(gameMessage)



## Scenario for Unit Test    

MD = MessageDispatcher()

gameObject1 = SmokeGameObject('F40_No1', ['car', 'ferrari', 'F40'])
MD.addObject(gameObject1)


gameObject2 = SmokeGameObject('F40_No2', ['car', 'ferrari', 'F40'])
MD.addObject(gameObject2)

gameObject3 = SmokeGameObject('L640_No3', ['car', 'lamborghini', 'L640'])
MD.addObject(gameObject3)

print MD.objectTagsList
print "FIND OBJECT F40_No2: ", MD.findObjectWithTag('F40_No2')
print "FIND ALL OBJECT IS CAR :", MD.findObjectWithTag('car')

print "BEFORE SEND MESSAGE "
print 'F40_No1 Event Queue', gameObject1.eventQueue
print 'F40_No2 Event Queue', gameObject2.eventQueue

event = GameMessage('F40_No1', ['F40_No2'], {'testKey':'testValue'})
MD.dispatch(event)
print 'F40_No1 Event Queue', gameObject1.eventQueue
print 'F40_No2 Event Queue', gameObject2.eventQueue
print 'Lamborghini Event Queue', gameObject3.eventQueue







